<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('products-offers',function(){
	$products = App\Product::where('status', 'PUBLICADO')->where('type', 'EXCLUSIVE')->with('category', 'images')->get();

	if (!$products) {
	    return response()->json(['message' => 'No existen datos']);
	}

    return response()->json($products);
});

Route::get('products',function(){
	$products = App\Product::where('status', 'PUBLICADO')->with('category', 'images')->get();
    //return datatables()->eloquent(App\Product::where('status', 'PUBLICADO')->with('category', 'images'))->toJson();

    if (!$products) {
	    return response()->json(['message' => 'No existen datos']);
	}

    return response()->json($products);
});

//Api for query from code qr
Route::get('product/{code}',function($code){
	$product = App\Product::where('code', $code)->with('category', 'images')->first();
	if (!$product) {
	    return response()->json(['message' => 'Artículo no encontrado']);
	}

    return response()->json($product);
});
