<?php

// Web

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Route;
use App\Image;

Route::get('/', 'Web\PageController@index')->name('page.index');
Route::get('categoria/{slug}', 'Web\PageController@category')->name('page.category');
Route::get('etiqueta/{slug}', 'Web\PageController@tag')->name('page.tag');

Route::get('/articulos', 'Web\PageController@more_product')->name('page.more_product');
Route::get('/detalle/{product}', 'Web\PageController@detail_product')->name('page.detail_product');
Route::post('articulo/store',    'Web\PageController@store')->name('articulo.store'); //->middleware('can:products.create');
Route::put('articulo/{product}/', 'Web\PageController@update')->name('articulo.update'); //->middleware('can:products.edit');
Route::post('articulo/sold/{product}/', 'Web\PageController@soldproduct')->name('articulo.soldproduct'); //->middleware('can:products.edit');
Route::get('articulo/Published/{product}/', 'Web\PageController@PublishedProduct')->name('articulo.PublishedProduct'); //->middleware('can:products.edit');
Route::get('articulo/reservar/{product}/', 'Web\PageController@reservedProduct')->name('articulo.reservar'); //->middleware('can:products.edit');
Route::get('/comprar/{product}', 'Web\PageController@comprar')->name('page.comprar');
Route::post('comprar/store/{product}',    'Web\PageController@comprar_store')->name('comprar.store');
Route::get('/homestaging', function () { return view('homestaging');})->name('homestaging');
Route::get('/saldos', function () { return view('saldos');})->name('saldos');
Route::get('/restauracion', function () { return view('restauracion');})->name('restauracion');
Route::get('/nosotros', function () { return view('nosotros');})->name('nosotros');
Route::get('/privacidad-cookies', function () { return view('politica-privacidad');})->name('politicas');
Route::get('/como-funciona', function () { return view('como-funciona');})->name('how_func');
Route::get('/terminos-condiciones', function () { return view('terminos-condiciones');})->name('terminos-condiciones');
// Route::get('/privacidad-cookies', function () { return response(view('politica-privacidad'))->cookie('Politicas','valor', 1);})->name('politicas');
Route::get('/como-vender', 'Web\PageController@to_sell')->name('page.to_sell');
Route::get('/como-comprar', 'Web\PageController@to_buy')->name('page.to_buy');
Route::get('/contact', 'Web\PageController@contacto')->name('contact');
Route::post('/contact', 'ContactoController@store')->name('contact.store');

Route::get('/welcome','MailSend@welcome');
Auth::routes(['verify' => true]);

Route::get('/img_product/{attachment}', function($attachment) {
    $file = sprintf('public/%s', $attachment);
    if(File::exists($file)) {
        return Image::make($file)->response();
    }
});
Route::get('Contrato/excel/{agreement}', 'Web\PageController@downloadExcel')->name('agreements.excel');

Auth::routes();
// admin
Route::middleware(['auth'])->group(function () {
    Route::get('/quiero-vender', 'Web\PageController@quiero_vender')->name('quiero-vender');
    Route::get('/form-vender', 'Web\PageController@form_product')->name('form-vender');
    Route::get('/form-visit', 'Web\PageController@form_visit')->name('form-visit');

    Route::get('/detalle/{product}/edit', 'Web\PageController@detail_edit_product')->name('page.detail_edit_product');
    Route::get('/mis_productos', 'Web\PageController@client_products')->name('client_products');
    Route::get('/Contrato/{agreement}/', 'Web\PageController@detail_client_agreement')->name('contrato.detalle');
    Route::get('/Contrato/pass_agreement/{token}', 'Web\PageController@pass_agreement')->name('pass.contrato');
    Route::get('/Contrato/DownloadPdf/{agreement}/', 'Web\PageController@download_pdf')->name('contrato.download');
    Route::delete('/articulo/de_baja/{product}/', 'Web\PageController@destroy')->name('article.destroy');
    Route::get('/editProfile', 'ProfileController@edit')->name('profile.edit');
    Route::put('/profile/update/{user}', 'ProfileController@update')->name('profile.update');
    Route::post('visits/store',       'VisitController@store')->name('visits.store');
    Route::get('qr-products/{product}',     'Admin\ProductController@qr_products')->name('products.qr_products');
    Route::delete('visits/remove/images/{image}',    'VisitController@removeImage')->name('removeImage');
    Route::post('products/assign_portada/{product}',     'Admin\ProductController@assign_portada')->name('products.assign_portada');

    // ========admin===========
    Route::get('company/details', 'CompanyController@index')->name('company.show')->middleware('admin');
    Route::get('company/{company}/edit', 'CompanyController@edit')->name('company.edit')->middleware('admin');
    Route::put('company/{company}/update', 'CompanyController@update')->name('company.update');

    Route::get('/admin', 'Web\AdminController@index')->name('admin')->middleware('admin');
    Route::get('/admin/profile/{user}', 'Web\AdminController@profile')->name('profile')->middleware('admin');
    Route::resource('categories',   'Admin\CategoryController')->middleware('admin');;
    Route::resource('tags',         'Admin\TagController')->middleware('admin');;

    // routes for products
    Route::get('/admin/products',       'Admin\ProductController@index')->name('products.index')->middleware('admin');
    Route::get('/admin/products/outled',       'Admin\ProductController@outet')->name('products.outet')->middleware('admin');
    Route::get('/admin/products/register', 'Admin\ProductController@create')->name('products.create')->middleware('admin');
    Route::get('/admin/products/sinDescuento/register', 'Admin\ProductController@createSinDecuento')->name('products.createSinDecuento')->middleware('admin');
    Route::get('/admin/products/beliani/register', 'Admin\ProductController@createBeliani')->name('products.createBeliani')->middleware('admin');
    Route::get('/admin/products/beliani/{product}/convert', 'Admin\ProductController@toBeliani')->name('products.toBeliani')->middleware('admin');
    Route::post('products/store',       'Admin\ProductController@store')->name('products.store');
    Route::put('products/{product}/update', 'Admin\ProductController@update')->name('products.update');
    Route::get('products/{product}',        'Admin\ProductController@show')->name('products.show')->middleware('admin');
    Route::delete('delete/products/{product}/basico',     'Admin\ProductController@destroy')->name('products.destroy.debaja');
    Route::get('products/{product}/rechazar',   'Admin\ProductController@regected')->name('products.regected')->middleware('admin');
    Route::put('products/rechazar/{product}',     'Admin\ProductController@rechazar')->name('products.rechazar');
    Route::post('products/more_images/{product}',     'Admin\ProductController@more_images')->name('products.more_images');
    Route::put('products/reservar/{product}',       'Admin\ProductController@reservar')->name('products.reservar');
    Route::put('products/publicNow/{product}',       'Admin\ProductController@detenerReserva')->name('products.detenerReserva');
    Route::put('products/ConcluirArticulo/{product}',       'Admin\ProductController@concluirArticulo')->name('products.concluirArticulo');
    Route::put('products/ConcluirArticulo/{product}',       'Admin\ProductController@venderConcluido')->name('products.vender.concluido');
    Route::get('products/{product}/edit',   'Admin\ProductController@edit')->name('products.edit')->middleware('admin');
    Route::put('publicar/{product}/',      'Admin\ProductController@publicar')->name('products.publicar');
    Route::post('products/img_default/{product}',     'Admin\ProductController@img_default')->name('products.img_default');

    Route::get('/admin/products/pendientes',       'Admin\ProductController@pendientes')->name('products.pendientes')->middleware('admin');
    Route::get('/admin/products/debaja',       'Admin\ProductController@debaja')->name('products.debaja')->middleware('admin');
    Route::get('/admin/products/reservados',       'Admin\ProductController@reservados')->name('products.reservados')->middleware('admin');
    Route::get('/admin/products/reservadosBasicos',       'Admin\ProductController@reservadosBasicos')
        ->name('products.reservadosBasicos')->middleware('admin');

    //agreements
    Route::delete('products/{product}',     'AgreementController@elminarArticulo')->name('contrato.products.destroy');
    Route::get('agreements', 'AgreementController@index')->name('agreements.index')->middleware('admin');
    Route::get('agreements/create', 'AgreementController@create')->name('agreements.create')->middleware('admin');
    Route::post('agreements/store',       'VisitController@store')->name('agreements.store');
    Route::get('agreements/{agreement}/show', 'AgreementController@show')->name('agreements.show')->middleware('admin');
    Route::get('agreements/{agreement}/details', 'AgreementController@details')->name('agreements.detail')->middleware('admin');
    Route::get('agreements/{agreement}/edit', 'AgreementController@edit')->name('agreements.edit')->middleware('admin');
    Route::put('agreements/{agreement}/update', 'AgreementController@update')->name('agreements.update');
    Route::put('agreements/concluir/{agreement}', 'AgreementController@concluirContrato')->name('agreements.concluirContrato');
    Route::put('agreements/cambiar/{agreement}', 'AgreementController@changeAgreement')->name('agreements.change');
    Route::delete('agreements/eliminar/{agreement}', 'AgreementController@destroy')->name('agreements.destroy');
    Route::get('agreements/verify/{token}', 'AgreementController@verifyAgreement')->name('agreements.verify');
    Route::get('agreements/export/{agreement}/', 'AgreementController@export')->name('agreements.export')->middleware('admin');
    Route::post('agreements/import/{agreement}/', 'AgreementController@importAgre')->name('agreements.import'); // TODO: falta crear para q reciva un archivo
    Route::get('agreements/sendEmail/{agreement}/', 'AgreementController@sendContratoToUser')->name('agreements.sendAgrementEmail')->middleware('admin');
    Route::get('agreements/resendEmail/{agreement}/', 'AgreementController@resendContratoToUser')->name('agreements.resendContratoToUser')->middleware('admin');
    Route::get('agreements/Publicar/{agreement}/', 'AgreementController@publicar')->name('agreements.publicar')->middleware('admin');

    Route::get('agreements/Codigos/{agreement}/', 'AgreementController@print_qr')->name('agreements.print_qr')->middleware('admin');

    //Visitas
    Route::get('visits/index',       'VisitController@index')->name('visits.index')->middleware('admin');
    Route::get('visits/{visit}',       'VisitController@show')->name('visits.show')->middleware('admin');

    Route::put('visits/makeVistado/{visit}', 'VisitController@makeVistado')->name('visits.makeVistado');
    Route::post('visits/saveContizacion/{agreement}', 'VisitController@cotizarStore')->name('visit.products.store');
    Route::post('visits/assign',       'VisitController@assign')->name('visits.assign');
    Route::post('visits/reprograming', 'VisitController@reprograming')->name('visits.reprograming');
    Route::delete('visits/{visit}',    'VisitController@destroy')->name('visits.destroy');
    Route::get('visits/images/remove/{id}',    'VisitController@removeArticle')->name('visits.removeArticle');
    Route::get('contrato/{visit}/rechazar',   'VisitController@regected')->name('visits.regected')->middleware('admin');

    Route::get('visits/contrato/{visit}/cotizar',    'VisitController@cotizar')->name('visits.cotizar');

    //Clientes
    Route::get('clients', 'Admin\ClientController@index')->name('clients.index')->middleware('admin');
    Route::get('clients/contrato', 'Admin\ClientController@contrato')->name('clients.contrato')->middleware('admin');
    Route::put('clients/{client}', 'Admin\ClientController@update')->name('clients.update');
    Route::get('clients/{client}', 'Admin\ClientController@show')->name('clients.show')->middleware('admin');
    Route::delete('clients/{client}', 'Admin\ClientController@destroy')->name('clients.destroy');
    Route::get('clients/{client}/edit', 'Admin\ClientController@edit')->name('clients.edit')->middleware('admin');
    Route::delete('users/disabled/{user}', 'Admin\UserController@disabled')->name('users.disabled');

    //Gestores
    Route::get('handlers', 'Admin\HandlerController@index')->name('handlers.index')->middleware('admin');
    Route::get('handlers/create', 'Admin\HandlerController@create')->name('handlers.create')->middleware('admin');
    Route::post('handlers/store',       'Admin\HandlerController@store')->name('handlers.store');
    Route::put('handlers/{user}', 'Admin\HandlerController@update')->name('handlers.update');
    Route::get('handlers/{handler}', 'Admin\HandlerController@show')->name('handlers.show')->middleware('admin');
    Route::delete('handlers/{user}', 'Admin\HandlerController@destroy')->name('handlers.destroy');
    Route::get('handlers/{handler}/edit', 'Admin\HandlerController@edit')->name('handlers.edit')->middleware('admin');

    //    routes for users
    Route::get('users', 'Admin\UserController@index')->name('users.index')->middleware('admin');
    Route::put('users/{user}', 'Admin\UserController@update')->name('users.update');
    Route::get('users/{user}', 'Admin\UserController@show')->name('users.show')->middleware('admin');
    Route::delete('users/{user}', 'Admin\UserController@destroy')->name('users.destroy');
    Route::get('users/{user}/edit', 'Admin\UserController@edit')->name('admin.users.edit')->middleware('admin');

    //compras
    Route::get('compras/solicitadas', 'ComprasController@index')->name('compras.index')->middleware('admin');
    Route::get('Vendidos', 'ComprasController@getArticlesSolds')->name('compras.solds')->middleware('admin');
    Route::get('Reservados', 'ComprasController@getArticlesReserved')->name('compras.reservados')->middleware('admin');
    Route::get('compra/details/{compra}', 'ComprasController@show')->name('compras.show')->middleware('admin');
    Route::delete('compras/{compra}', 'ComprasController@destroy')->name('compras.destroy');

    Route::get('compra/create/{product}', 'ComprasController@create')->name('compras.create'); //->middleware('can:products.create')->middleware('admin');
    Route::post('soldcomplete/{product}/',      'ComprasController@store')->name('compras.store');
    // Route::put('compra/{compra}', 'ComprasController@update')->name('compras.update');
    // Route::get('compras/{compra}/edit', 'ComprasController@edit')->name('compras.edit')->middleware('admin');

    Route::get('/contactos', 'ContactoController@index')->name('contactos.index');
    Route::get('/contact/{contacto}', 'ContactoController@show')->name('contactos.show');
    Route::delete('/contact/{contacto}', 'ContactoController@destroy')->name('contactos.destroy');

    //reportes
    Route::get('reportes/vendidos', 'ReportController@vendidos')->name('reporte.vendidos')->middleware('admin');
    Route::post('reportes/vendidos', 'ReportController@vendidos')->name('reporte.vendidos');
    Route::get('reportes/no_vendidos', 'ReportController@no_vendidos')->name('reporte.no_vendidos')->middleware('admin');
    Route::post('reportes/no_vendidos', 'ReportController@no_vendidos')->name('reporte.no_vendidos');
    Route::get('reportes/reservados', 'ReportController@reservados')->name('reporte.reservados')->middleware('admin');
    Route::post('reportes/reservados', 'ReportController@reservados')->name('reporte.reservados');
    Route::get('reportes/contratos_active', 'ReportController@contratos_active')->name('reporte.contratos_active')->middleware('admin');
    Route::post('reportes/contratos_active', 'ReportController@contratos_active')->name('reporte.contratos_active');
    Route::get('reportes/contratos_close', 'ReportController@contratos_close')->name('reporte.contratos_close')->middleware('admin');
    Route::post('reportes/contratos_close', 'ReportController@contratos_close')->name('reporte.contratos_close');
    Route::get('reportes/contrato_gestor', 'ReportController@contrato_gestor')->name('reporte.contrato_gestor')->middleware('admin');
    Route::post('reportes/contrato_gestor', 'ReportController@contrato_gestor')->name('reporte.contrato_gestor');
    Route::post('utilidad/store/{agreement}', 'UtilidadController@store')->name('utilidad.store')->middleware('admin');
    Route::put('utilidad/pagar/{utilidad}', 'UtilidadController@update')->name('utilidad.pagar')->middleware('admin');
    Route::get('utilidad/detalle/{utilidad}', 'UtilidadController@show')->name('utilidad.detail')->middleware('admin');


    // ==========gestor=========
    // Route::get('/admin', 'Web\AdminController@index')->name('admin');
    // Route::get('/admin/profile/{user}', 'Web\AdminController@profile')->name('profile');
    // Route::resource('categories',   'Admin\CategoryController');
    // Route::resource('tags',         'Admin\TagController');

    // // blog
    // Route::get('/blog/experiencias', 'Web\BlogController@index')->name('blog.experencias');

    // // routes for products
    // Route::get('/admin/products',       'Admin\ProductController@index')->name('products.index');
    // Route::get('/admin/products/register', 'Admin\ProductController@create')->name('products.create');
    // Route::post('products/store',       'Admin\ProductController@store')->name('products.store');
    // Route::put('products/{product}/update', 'Admin\ProductController@update')->name('products.update');
    // Route::get('products/{product}',        'Admin\ProductController@show')->name('products.show');
    // Route::delete('products/{product}',     'Admin\ProductController@destroy')->name('products.destroy');
    // Route::put('products/rechazar/{product}',     'Admin\ProductController@rechazar')->name('products.rechazar');
    // Route::post('products/more_images/{product}',     'Admin\ProductController@more_images')->name('products.more_images');
    // Route::put('products/reservar/{product}',       'Admin\ProductController@reservar')->name('products.reservar');
    // Route::put('products/publicNow/{product}',       'Admin\ProductController@detenerReserva')->name('products.detenerReserva');
    // Route::get('products/{product}/edit',   'Admin\ProductController@edit')->name('products.edit');
    // Route::put('publicar/{product}/',      'Admin\ProductController@publicar')->name('products.publicar');
    // Route::post('products/img_default/{product}',     'Admin\ProductController@img_default')->name('products.img_default');

    // //agreements
    // Route::get('agreements', 'AgreementController@index')->name('agreements.index');
    // Route::get('agreements/create', 'AgreementController@create')->name('agreements.create');
    // Route::post('agreements/store',       'VisitController@store')->name('agreements.store');
    // Route::get('agreements/{agreement}/details', 'AgreementController@show')->name('agreements.show');
    // Route::get('agreements/{agreement}/edit', 'AgreementController@edit')->name('agreements.edit');
    // Route::put('agreements/{agreement}/update', 'AgreementController@update')->name('agreements.update');
    // Route::delete('agreements/{agreement}', 'AgreementController@destroy')->name('agreements.destroy');
    // Route::get('agreements/verify/{token}', 'AgreementController@verifyAgreement')->name('agreements.verify');
    // Route::get('agreements/export/{agreement}/', 'AgreementController@export')->name('agreements.export');
    // Route::post('agreements/import/{agreement}/', 'AgreementController@importAgre')->name('agreements.import'); // TODO: falta crear para q reciva un archivo
    // Route::get('agreements/sendEmail/{agreement}/', 'AgreementController@sendContratoToUser')->name('agreements.sendAgrementEmail');
    // Route::get('agreements/resendEmail/{agreement}/', 'AgreementController@resendContratoToUser')->name('agreements.resendContratoToUser');
    // Route::get('agreements/Publicar/{agreement}/', 'AgreementController@publicar')->name('agreements.publicar');

    // //Visitas
    // Route::get('visits/index',       'VisitController@index')->name('visits.index');
    // Route::get('visits/{visit}',       'VisitController@show')->name('visits.show');

    Route::post('visits/addArticle', 'VisitController@addArticle')->name('visits.addArticle');
    // Route::post('visits/saveContizacion/{agreement}', 'VisitController@cotizarStore')->name('visit.products.store');
    // Route::post('visits/assign',       'VisitController@assign')->name('visits.assign');
    // Route::post('visits/reprograming', 'VisitController@reprograming')->name('visits.reprograming');
    // Route::delete('visits/{visit}',    'VisitController@destroy')->name('visits.destroy');
    // Route::get('visits/images/remove/{id}',    'VisitController@removeArticle')->name('visits.removeArticle');

    // Route::get('visits/contrato/{visit}/cotizar',    'VisitController@cotizar')->name('visits.cotizar');

    // //Clientes
    // Route::get('clients', 'Admin\ClientController@index')->name('clients.index');
    // Route::put('clients/{client}', 'Admin\ClientController@update')->name('clients.update');
    // Route::get('clients/{client}', 'Admin\ClientController@show')->name('clients.show');
    // Route::delete('clients/{client}', 'Admin\ClientController@destroy')->name('clients.destroy');
    // Route::get('clients/{client}/edit', 'Admin\ClientController@edit')->name('clients.edit');

    // //Gestores
    // Route::get('handlers', 'Admin\HandlerController@index')->name('handlers.index');
    // Route::get('handlers/create', 'Admin\HandlerController@create')->name('handlers.create');
    // Route::post('handlers/store',       'Admin\HandlerController@store')->name('handlers.store');
    // Route::put('handlers/{user}', 'Admin\HandlerController@update')->name('handlers.update');
    // Route::get('handlers/{handler}', 'Admin\HandlerController@show')->name('handlers.show');
    // Route::delete('handlers/{user}', 'Admin\HandlerController@destroy')->name('handlers.destroy');
    // Route::get('handlers/{handler}/edit', 'Admin\HandlerController@edit')->name('handlers.edit');

    // //    routes for users
    // Route::get('users', 'Admin\UserController@index')->name('users.index');
    // Route::put('users/{user}', 'Admin\UserController@update')->name('users.update');
    // Route::get('users/{user}', 'Admin\UserController@show')->name('users.show');
    // Route::delete('users/{user}', 'Admin\UserController@destroy')->name('users.destroy');
    // Route::get('users/{user}/edit', 'Admin\UserController@edit')->name('admin.users.edit');

    // //compras
    // Route::get('compras/solicitadas', 'ComprasController@index')->name('compras.index');
    // Route::get('Vendidos', 'ComprasController@getArticlesSolds')->name('compras.solds');
    // Route::get('compra/details/{compra}', 'ComprasController@show')->name('compras.show');
    // Route::delete('compras/{compra}', 'ComprasController@destroy')->name('compras.destroy');

    // Route::get('compra/create/{product}', 'ComprasController@create')->name('compras.create'); //->middleware('can:products.create');
    // Route::post('soldcomplete/{product}/',      'ComprasController@store')->name('compras.store');
    // // Route::put('compra/{compra}', 'ComprasController@update')->name('compras.update');
    // // Route::get('compras/{compra}/edit', 'ComprasController@edit')->name('compras.edit');

    // //reportes
    // Route::get('reportes/vendidos', 'ReportController@vendidos')->name('reporte.vendidos');
    // Route::post('reportes/vendidos', 'ReportController@vendidos')->name('reporte.vendidos');
    // Route::get('reportes/no_vendidos', 'ReportController@no_vendidos')->name('reporte.no_vendidos');
    // Route::post('reportes/no_vendidos', 'ReportController@no_vendidos')->name('reporte.no_vendidos');
    // Route::get('reportes/reservados', 'ReportController@reservados')->name('reporte.reservados');
    // Route::post('reportes/reservados', 'ReportController@reservados')->name('reporte.reservados');
    // Route::get('reportes/contratos_active', 'ReportController@contratos_active')->name('reporte.contratos_active');
    // Route::post('reportes/contratos_active', 'ReportController@contratos_active')->name('reporte.contratos_active');
    // Route::get('reportes/contratos_close', 'ReportController@contratos_close')->name('reporte.contratos_close');
    // Route::post('reportes/contratos_close', 'ReportController@contratos_close')->name('reporte.contratos_close');
});

// Route::middleware(['auth'])->group(function () {
//     Route::group(['prefix'=>'admin'], function(){
//         Route::group(['middleware' => [sprintf('role:%s', \App\Role::ADMIN)]], function () {

//         });

//         Route::group(['middleware' => [sprintf('role:%s', \App\Role::HANDLER)]], function () {

//         });

//     });
// });
