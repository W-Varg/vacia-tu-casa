@extends('layouts.pdf')
@section('content')

<h1>{{ $heading}}</h1>
<div class="text-right">
  <p>{{$content}}</p>
</div>
<div class="">
  <table class="table table-striped" width="100%" style="width:100%" border="0">
    <tr>
      <th>Imagen</th>
      <th>Nombre</th>
      <th>Categoria</th>
      <th>Estado</th>
      <th>Precio</th>
    </tr>
    {{-- <tbody> --}}
    @foreach($agreement->products as $product)
    <tr>
      <td class="text-center">
        <a href="{{ route ('products.show', $product) }}">
          @if ($product->img)
          <img class="rounded" src="{{ asset($product->img) }}" alt="" height="50" />
          @else
          <img src="{{ asset('assets/images/product-1.png') }}" alt="" height="50" />
          @endif
        </a>
      </td>
      <td> <a href="{{ route ('products.show', $product) }}"> {{ $product->name }} </a> </td>
      <td> {{ $product->category->name }} </td>
      <td class="text-warning"> {{ $product->status }} </td>
      <td>  {{ $product->normal_price }} <i class="fa fa-euro"></i> </td>
      <td>
        {{-- <a class="btn btn-sm btn-default"href="{{ route ('products.edit', $product) }}"><i
          class="fa  fa-pencil"></i> Editar</a> --}}
      </td>
    </tr>
    @endforeach
    </tbody>
  </table>
</div>
@endsection
