@extends('layouts.web')
@section('content')
<style>
    .image-one {
        max-height: 80vh;
    }

    .image-two {
        max-height: 80vh;
    }

    .middle-line {
        /* margin-top: -7vh; */
    }

    .container {
        width: 70%;
        margin-left: auto;
        margin-right: auto;
    }

    .with-100 {
        width: 100%;
    }

    .two-background {
        background: #ecf2ee;
    }

    .video-container {
        position: relative;
        padding-bottom: 56.25%;
        /* 16:9 */
        height: 0;
    }

    .video-container iframe {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
    }

    @media (max-width: 978px) {
        .container {
            width: 93%;
        }
    }
</style>
{{-- {{ asset('assets/banners/nosotros.jpg') }} --}}
<section class="main-header" class="banner banners-justify"
    style="background-image:url({{ asset('assets/banners/nosotros_banner.jpg') }}); margin-top:70px;">
    <header>
        <div class="container text-center">
            {{-- <h2 class="h2 title">BIENVENIDO A</h2> --}}
        </div>
    </header>
</section>

<div class="container">
    <div class="row my-1">
        <div class="col-sm-12">
            <p class="fs-1-4">
                <span class="">CONÓCENOS</span>
            </p>
            <p class="fs-3 text-main text-bold">
                <span class="">HOME TO HOME</span>
            </p>
        </div>
        <br>
        <div class="col-sm-12">
            <div class="video-container">
                <iframe width="560" height="315" src="https://www.youtube.com/embed/4bmsJ6xldyo" frameborder="0"
                    allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="row my-1">
        <div class="col-sm-12 text-justify">
            <p class=" fs-1-3">
                Somos una empresa familiar con más de 50 años de experiencia en el sector del mueble. Nuestra pasión es crear espacios
                acogedores, hogares dónde encontrar el refugio y el calor que buscamos al llegar a casa.
            </p>
        </div>
        <div class="col-sm-12 text-justify">
            <p class=" fs-1-3">
                Actualmente estamos apostando por la recuperación de muebles de segunda mano, al tiempo que comercializamos mobiliario y
                artículos de decoración provenientes de prestigiosas empresas a precios muy competitivos. Nos encargamos de todo el proceso,
                desde la toma de medidas de las diferentes estancias hasta la elaboración del proyecto de decoración con todo detalle.
            </p>
        </div>
        <div class="col-sm-12 text-justify">
            <p class=" fs-1-3">
                Disponemos de una amplia variedad de estilos para adaptarnos a todos los gustos. Te asesoramos y resolvemos todas tus dudas sobre
                decoración y últimas tendencias, sin olvidar la funcionalidad y los mejores acabados.
            </p>
        </div>
        <div class="container-fluid">
            <div class="parent row my-2">
                <img src="{{ asset('images/nosotros1.png') }}" alt="sf" class="img-responsive center-block" />
            </div>
        </div>
    </div>
</div>
<div class="with-100 two-background">
    <div class="container">
        <div class="row my-1">
            <div class="col-sm-12">
                <p class="fs-1-4">
                    <span class="">COMPROMETIDOS</span>
                </p>
                <p class="fs-3 text-main text-bold">
                    <span class="">CON EL PLANETA</span>
                </p>
                <p class="fs-1-3 text-justify">
                    Queremos contribuir a salvar árboles y concienciar a las nuevas generaciones de la necesidad de aprovechar esos
                    maravillosos muebles que buscan una segunda oportunidad para lucir como nuevos, recuperando técnicas naturales de
                    restauración y creando hogares únicos con personalidad
                </p>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row my-1">
        <div class="col-sm-12 text-center my-2">
            <p class="fs-3">
                <span class=" middle-line text-black">Amor</span>
            </p>
            <p class="fs-1-4">
                <span class=" middle-line">Mantén tu amor por la naturaleza, pues es la
                    verdadera forma de entender la vida</span>
            </p>
        </div>
        <div class="container-fluid">
            <div class="row">
                <img src="{{ asset('images/nosotros_amor.jpg') }}" alt="sf" class="img-responsive center-block" />
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row my-1">
        <div class="col-sm-12 text-center my-2">
            <p class="fs-3">
                <span class=" middle-line text-black">Respeto</span>
            </p>
            <p class="fs-1-4">
                <span class=" middle-line">
                    La naturaleza no es un sitio por el que pasar <br>
                    de visita, sino un hogar que todos deberíamos amar
                    respetar y valorar más.
                </span>
            </p>
        </div>
        <div class="container-fluid">
            <div class="row">
                <img src="{{ asset('images/nosotros_respeto.png') }}" alt="sf" class="img-responsive center-block" />
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row my-1">
        <div class="col-sm-12 text-center my-2">
            <p class="fs-3">
                <span class="text-black middle-line">Conciencia</span>
            </p>
            <p class="fs-1-3">
                <span class=" middle-line">
                    &#8220; Lo que estamos haciendo a los bosques del mundo <br>
                    es un espejo de lo que nos hacemos a nosotros <br>
                    mismos y a los demás &#8221; - Mahatma Gandhi
                </span>
            </p>
        </div>
        <div class="container-fluid">
            <div class="row">
                <img src="{{ asset('images/nosotros_conciencia.jpg') }}" alt="sf" class="img-responsive center-block" />
            </div>
        </div>
    </div>
</div>
@endsection
