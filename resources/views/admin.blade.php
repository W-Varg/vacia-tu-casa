@extends('layouts.admin')
@section('content')
<style>
    .info-box-content {
        margin-left: 0px;
    }
</style>
<div class="row">
    <div class="col-lg-4 col-md-4 col-12">
        <div class="info-box pull-up bg-hexagons-dark">
            <a href="{{ route('clients.index')}}">
                <span class="info-box-icon bg-danger"><i class="fa fa-users"></i></span>
                <div class="info-box-content pull-right text-right">
                    <span class="info-box-number">{{ $clientes }}</span>
                    <span class="info-box-text">Clientes</span>
                </div>
            </a>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>
    <div class="col-lg-4 col-md-4 col-12">
        <div class="info-box pull-up bg-hexagons-dark">
            <a href="{{ route('visits.index')}}">
                <span class="info-box-icon bg-info"><i class="fa fa-drivers-license-o"></i></span>

                <div class="info-box-content pull-right text-right">
                    <span class="info-box-number">{{$porVisitar}}</span>
                    <span class="info-box-text">Por Visitar</span>
                </div>
            </a>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>
    <div class="col-lg-4 col-md-4 col-12">
        <div class="info-box pull-up bg-hexagons-dark">
            <a href="{{ route('products.index')}}">
                <span class="info-box-icon bg-success"><i class="fa fa-shield"></i></span>

                <div class="info-box-content pull-right text-right">
                    <span class="info-box-number">{{$publicados}}</span>
                    <span class="info-box-text">Publicados</span>
                </div>
            </a>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>
</div>
@endsection