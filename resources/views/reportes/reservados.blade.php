@extends('layouts.admin')
@section('styles')
<link href="{{ asset('adm/assets/vendor_components/Magnific-Popup-master/dist/magnific-popup.css') }}" rel="stylesheet">
@endsection
@section('content')
<div class="row">
    <div class="col-md-12 col-lg-12">
        <div class="box box-solid bg-dark">
            <div class="box-header with-border">
                <h3 class="box-title">Reporte de Articulos Reservados</h3>
                {{-- <a href="{{ route ('products.create')}}" class="btn btn-info float-right">Registrar</a> --}}
                {!! Form::open(['route' => 'reporte.reservados' ]) !!}
                {!! Form::token() !!}
                {!! Form::date('start', $start) !!}
                {!! Form::date('end', $end) !!}
                {!! Form::submit('Filtrar') !!}
                {!! Form::close() !!}
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#reservados" role="tab">
                            <span><i class="fa  fa-file"></i></span> Reservados </a>
                    </li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content tabcontent-border">
                    <div class="tab-pane active" id="reservados" role="tabpanel">
                        <div class="pad">
                            <table class="table table-striped">
                                <thead>
                                    <th>#</th>
                                    <th>Codigo Articulo</th>
                                    <th>Nombre</th>
                                    <th>Cliente</th>
                                    <th>Monto Reserva</th>
                                    <th>Estado</th>
                                    <th>Fecha</th>
                                    <th>Acciones</th>
                                </thead>
                                <tbody>
                                    @foreach($products as $product)
                                    <tr>
                                        <th> {{ $loop->iteration }}</th>
                                        <th> {{ $product->code }}</th>
                                        <th> {{ $product->name }}</th>
                                        <td> {{ $product->user->name }} </td>
                                        <td>
                                            @if ($product->compra)
                                            {{-- <a class="btn btn-sm btn-success" href="{{ route ('agreements.show', $product->client_agreement) }}">
                                            {{ $product->name }} </a> --}}
                                            {{$product->compra->monto_reserva}} <i class="fa fa-euro"></i>
                                            @endif
                                        </td>
                                        <td><span class="text-success">Reservado</span></td>
                                        <td> {{ date('d-m-Y   H:i', strtotime($product->updated_at)) }} </td>
                                        <td>
                                            <a class="btn btn-sm btn-primary" href="{{ route ('products.show', $product) }}"> Ver</a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
