@extends('layouts.admin')
@section('styles')
<link href="{{ asset('adm/assets/vendor_components/Magnific-Popup-master/dist/magnific-popup.css') }}" rel="stylesheet">
@endsection
@section('content')
<div class="row">
    <div class="col-md-12 col-lg-12">
        <div class="box box-solid bg-dark">
            <div class="box-header with-border">
                <h3 class="box-title">Reporte de Articulos Vendidos</h3>
                {!! Form::open(['route' => 'reporte.vendidos' ]) !!}
                {!! Form::token() !!}
                <span class="float-right">TOTAL: <span class="text-warning">{{$total}} <i class="fa fa-euro"></i></span> </span>
                {!! Form::date('start', $start) !!}
                {!! Form::date('end', $end) !!}
                {!! Form::submit('Filtrar') !!}
                {!! Form::close() !!}
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    {{-- <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#basicos" role="tab">
                            <span><i class="fa  fa-file"></i></span> Basicos </a> </li> --}}

                    <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#premium" role="tab"><span> <i
                                    class="fa  fa-file-powerpoint-o"></i></span> Premium </a> </li>

                    <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#standart" role="tab"><span> <i
                                    class="fa  fa-file"></i></span> Estandar </a> </li>


                    <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#beliani" role="tab"><span> <i
                                    class="fa  fa-file"></i></span> Beliani </a> </li>

                    {{-- <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#oulet" role="tab"><span> <i
                                    class="fa  fa-file"></i></span> Oulet </a> </li> --}}
                </ul>
                <!-- Tab panes -->
                <div class="tab-content tabcontent-border">
                    {{-- <div role="tabpanel" class="tab-pane active" id="basicos">
                        <div class="content">
                            @include('reportes.tables.basicos_table', ['products'=> $basicos])
                        </div>
                    </div> --}}
                    <div role="tabpanel" class="tab-pane active" id="premium">
                        <div class="content">
                            @include('reportes.tables.premium_table', ['products'=> $premium])
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="standart">
                        <div class="content">
                            @include('reportes.tables.standart_table', ['products'=> $estandar])
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="beliani">
                        <div class="content">
                            @include('reportes.tables.beliani_table', ['products'=> $beliani])
                        </div>
                    </div>
                    {{-- <div role="tabpanel" class="tab-pane" id="oulet">
                        <div class="content">
                            @include('reportes.tables.oulet_table', ['products'=> []])
                        </div>
                    </div> --}}
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
