<div class="pad">
    <table class="table table-striped">
        <thead>
            <th>#</th>
            <th>Codigo Articulo</th>
            <th>Nombre</th>
            <th>Cliente</th>
            <th>Precio</th>
            <th>Estado</th>
            <th>Fecha</th>
            <th>Acciones</th>
        </thead>
        <tbody>
            @foreach($products as $product)
            <tr>
                <th> {{ $loop->iteration }}</th>
                <th> {{ $product->code }}</th>
                <th> {{ $product->name }}</th>
                <td> {{ $product->user->name }} </td>
                <td>
                    @if ($product->compra)
                    {{$product->compra->pago}} <i class="fa fa-euro"> </i>
                    @endif
                </td>
                <td>
                    @if ($product->status == 'VENDIDO')
                    <span class="text-success">Vendido</span>
                    @else
                    <span class="text-warning"> : {{ $product->status}} </span>
                    @endif
                </td>
                <td>
                    @if ($product->compra)
                    {{ date('d-m-Y', strtotime($product->compra->updated_at)) }}
                    @else
                    {{ date('d-m-Y', strtotime($product->updated_at)) }}
                    @endif
                </td>
                <td>
                    @if ($product->compra)
                    <a class="btn btn-sm btn-primary" href="{{ route ('compras.show', $product->compra) }}"> Ver</a>
                    @else
                    <a class="btn btn-sm btn-primary" href="{{ route ('products.show', $product) }}"> Ver</a>
                    @endif
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
