@extends('layouts.admin')

@section('content')
<div class="row">
    <div class="col-12">
        <div class="box box-solid bg-dark">
            <div class="box-header with-border flex">
                <h3 class="box-title">Detalle de Comision de contrato</h3>
                {{-- <h6 class="box-subtitle"> Listado de Estancias</h6> --}}
                <a href="{{ route ('reporte.contrato_gestor')}}" class="btn btn-danger float-right">Listar Reporte y Gestor</a>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-6 text-right"><strong>Codigo contrato</strong></div>
                            <div class="col-6 text-left">
                                {{-- Ver
                                <a class="btn btn-sm btn-warning" href="{{ route ('agreements.detail', $agreement) }}"> --}}
                                    @if ($agreement->name == 'estandar')
                                    E-000{{ $agreement->id }}
                                    @else
                                    P-000{{ $agreement->id }}
                                    @endif
                            </div>
                        </div>
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-6 text-right">Fecha Fin Contrato:</div>
                            <div class="col-6 text-left"> {{ date('d-m-Y', strtotime($agreement->date_end)) }}</div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-6 text-right">Total Ventas</div>
                            <div class="col-6 text-left text-red"> <strong>{{ $utilidad->utilidad }} <i class="fa fa-euro"></strong> </i></div>
                        </div>
                    </div>
                    {{-- estandar --}}
                    @if ($utilidad->client_agreement->name == 'estandar')
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-6 text-right">Comisión H2H</div>
                            <div class="col-6 text-left text-success"><strong>{{ $utilidad->vtc_cuota }} <i class="fa fa-euro"></i></strong></div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="row text-info">
                            <div class="col-6 text-right">Comisión para Gestor </div>
                            <div class="col-6 text-left text-info"><strong> {{ $utilidad->gestor_cuota }} <i class="fa fa-euro"></i></strong></div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-6 text-right">Comisión para Cliente</div>
                            <div class="col-6 text-left text-info">{{ $utilidad->cliente_cuota }} <i class="fa fa-euro"></i></div>
                        </div>
                    </div>
                    @else
                    {{-- premiun --}}
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-6 text-right">Comisión H2H</div>
                            <div class="col-6 text-left text-success"><strong>{{ $utilidad->vtc_cuota }} <i class="fa fa-euro"></i></strong></div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="row text-info">
                            <div class="col-6 text-right">Comisión para Gestor </div>
                            <div class="col-6 text-left text-info"><strong> {{ $utilidad->gestor_cuota }} <i class="fa fa-euro"></i></strong></div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-6 text-right">Comisión para Cliente</div>
                            <div class="col-6 text-left text-info">{{ $utilidad->cliente_cuota }} <i class="fa fa-euro"></i></div>
                        </div>
                    </div>
                    @endif
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-6 text-right"><strong> Gestor de contrato :</strong></div>
                            <div class="col-6 text-left">
                                <a class="btn btn-sm btn-info" href="{{ route ('profile', $utilidad->user_id) }}">
                                    {{ $utilidad->gestor_name }} </a>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-6 text-right">
                                {{-- <strong>Estado De pago de Comision :</strong> --}}
                            </div>
                            <div class="col-6 text-left">
                                @if ($utilidad->status == 'pendiente')
                                <span class="text-red"> {{ $utilidad->status}} </span>
                                @else
                                <span class="text-success"> {{ $utilidad->status}} </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    @if ($utilidad->fecha_pago)
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-6 text-right"><strong>Fecha de pago :</strong></div>
                            <div class="col-6 text-left"> {{   date('d-m-Y', strtotime($utilidad->fecha_pago)) }} </div>
                        </div>
                    </div>
                    @endif

                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-6 text-right"><strong>Contrado cerrado el :</strong></div>
                            <div class="col-6 text-left"> {{   date('d-m-Y', strtotime($utilidad->created_at)) }} </div>
                        </div>
                    </div>
                </div>
            </div>
            @if (auth()->user()->role->id == \App\Role::ADMIN)
            <div class="box-footer text-center">
                @if ($utilidad->status == 'pendiente')
                {!! Form::model($utilidad,['route' => ['utilidad.pagar', $utilidad],
                'method'=> 'PUT', 'class' =>['inline'] ]) !!}
                <button type="submit" class="btn btn-sm btn-outline-secondary">Pagar Comision</button>
                {!! Form::close() !!}
                @endif
            </div>
            @endif
        </div>
    </div>
</div>
@endsection
