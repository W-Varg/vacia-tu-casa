@extends('layouts.admin')

@section('content')
<style>
    .form-control {
        width: 15rem;
        display: inline;
    }
</style>
<div class="row">
    <div class="col-md-12 col-lg-12">
        <div class="box box-solid bg-dark">
            <div class="box-header with-border">
                <h3 class="box-title">Reporte de Contratos por Gestor Cerrados</h3>
                {{-- <a href="{{ route ('products.create')}}" class="btn btn-info float-right">Registrar</a> --}}
                {!! Form::open(['route' => 'reporte.contrato_gestor' ]) !!}
                {!! Form::token() !!}
                {!! Form::date('start', $start) !!}
                {!! Form::date('end', $end) !!}
                {{ Form::select('id',['' => 'Selecciona a un gestor'] + $gestores->all(), null, [ 'class' => 'form-control', 'id' => 'id' ]) }}
                {!! Form::submit('Filtrar') !!}
                {!! Form::close() !!}
                @if ($user)
                <h5>GESTOR <span class="text-warning">{{$user->name}}</span> </h5>
                @endif
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#contratos" role="tab"><span><i
                                    class="fa  fa-file"></i></span> Contratos </a> </li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content tabcontent-border">
                    <div class="tab-pane active" id="contratos" role="tabpanel">
                        <div class="pad">
                            <table class="table table-striped">
                                <thead>
                                    <th>#</th>
                                    <th>Codigo Contrato</th>
                                    {{-- <th>Contrato</th> --}}
                                    <th>Cantidad</th>
                                    <th>Fecha Fin</th>
                                    <th>Utilidad</th>
                                    <th>Gestor</th>
                                    <th>Comision Gestor</th>
                                    <th>Comision HomeToHome</th>
                                    <th>Estado</th>
                                    @if (auth()->user()->role->id == \App\Role::ADMIN)
                                    <th>Acciones</th>
                                    @endif

                                </thead>
                                @if (auth()->user()->role->id == \App\Role::ADMIN)
                                <tbody>
                                    @foreach($utilidades as $utilidad)
                                    <tr>
                                        <td> {{ $loop->iteration }}</td>
                                        <td>
                                            <a class="btn btn-sm btn-warning"
                                                href="{{ route ('agreements.detail', $utilidad->client_agreement) }}">
                                                @if ($utilidad->client_agreement->name == 'estandar')
                                                E-000{{ $utilidad->client_agreement->id }}
                                                @else
                                                P-000{{ $utilidad->client_agreement->id }}
                                                @endif
                                            </a>
                                        </td>
                                        {{-- <td> {{$utilidad->type_contrato}}</td> --}}
                                        <td> {{$utilidad->cantidad_vend}}</td>
                                        <td> {{ date('d-m-Y', strtotime($utilidad->updated_at)) }} </td>
                                        <td> {{ $utilidad->utilidad }} <i class="fa fa-euro"></i> </td>
                                        <td> {{ $utilidad->gestor_name }} </td>
                                        <td> {{ $utilidad->gestor_cuota }} <i class="fa fa-euro"></i> </td>
                                        <td> {{ $utilidad->vtc_cuota }} <i class="fa fa-euro"></i> </td>
                                        <td>
                                            @if ($utilidad->status == 'pendiente')
                                            <span class="text-warning"> {{ $utilidad->status}} </span>
                                            @else
                                            <span class="text-success"> {{ $utilidad->status}} a gestor </span>
                                            @endif
                                        </td>
                                        <td>
                                            <a class="btn btn-sm btn-info" href="{{ route ('utilidad.detail', $utilidad) }}"> Detalle</a>
                                            @if ($utilidad->status == 'pendiente')
                                            {!! Form::model($utilidad,['route' => ['utilidad.pagar', $utilidad],
                                            'method'=> 'PUT', 'class' =>['inline'] ]) !!}
                                            <button type="submit" class="btn btn-sm btn-outline-secondary">Pagar Comision</button>
                                            {!! Form::close() !!}
                                            @endif
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                                @endif
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
