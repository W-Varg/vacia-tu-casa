@extends('layouts.admin')

@section('content')
<div class="row">
    <div class="col-md-12 col-lg-12">
        <div class="box box-solid bg-dark">
            <div class="box-header with-border">
                <h3 class="box-title">Reporte de Contractos Activos</h3>
                {{-- <a href="{{ route ('products.create')}}" class="btn btn-info float-right">Registrar</a> --}}
                {!! Form::open(['route' => 'reporte.contratos_active' ]) !!}
                {!! Form::token() !!}
                {!! Form::date('start', $start) !!}
                {!! Form::date('end', $end) !!}
                {!! Form::submit('Filtrar') !!}
                {!! Form::close() !!}
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#standart" role="tab"><span><i
                                    class="fa  fa-file"></i></span> Estándar </a> </li>
                    <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#premium" role="tab"><span><i
                                    class="fa  fa-file-powerpoint-o"></i></span> Premium </a> </li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content tabcontent-border">
                    <div class="tab-pane active" id="standart" role="tabpanel">
                        <div class="pad">
                            <table class="table table-striped">
                                <thead>
                                    <th>#</th>
                                    <th>Codigo Contrato</th>
                                    <th>Contrato</th>
                                    <th>Cliente</th>
                                    <th>Estado</th>
                                    <th>Fecha</th>
                                    <th>Acciones</th>
                                </thead>
                                <tbody>
                                    @foreach($estandarts as $agreement)
                                    <tr>
                                        <th> {{ $loop->iteration }}</th>
                                        <th> E000{{ $agreement->id }}</th>
                                        <td> <a href="{{ route ('agreements.show', $agreement) }}"> {{ $agreement->name }} </a> </td>
                                        <td> {{ $agreement->client->user->name }} </td>
                                        <td>
                                            {{-- @if ($agreement->verified)
                                            <span class="text-success">Verificado</span>
                                            @else
                                            <span class="text-danger">No Verificado</span>
                                            @endif --}}
                                            <span class="text-warning bg-status-{{$agreement->status}}"> {{ $agreement->status}} </span>
                                        </td>
                                        <td> {{date('d-m-Y', strtotime($agreement->created_at)) }} </td>
                                        <td>
                                            <a class="btn btn-sm btn-primary" href="{{ route ('agreements.show', $agreement) }}"> Ver</a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            {{-- <nav class="mt-15 pb-10">
                                <ul class="pagination justify-content-center">
                                    {{ $estandarts->links() }}
                                </ul>
                            </nav> --}}
                        </div>
                    </div>

                    <div class="tab-pane pad" id="premium" role="tabpanel">
                        <div class="pad">
                            <table class="table table-striped">
                                <thead>
                                    <th>#</th>
                                    <th>Codigo Contrato</th>
                                    <th>Contrato</th>
                                    <th>Cliente</th>
                                    <th>Estado</th>
                                    <th>Fecha</th>
                                    <th>Acciones</th>
                                </thead>
                                <tbody>
                                    @foreach($premiums as $agreement)
                                    <tr>
                                        <th> {{ $loop->iteration }}</th>
                                        <th> P000{{ $agreement->id }}</th>
                                        <td> <a href="{{ route ('agreements.show', $agreement) }}"> {{ $agreement->name }} </a> </td>
                                        <td> {{ $agreement->client->user->name }} </td>
                                        <td>
                                            {{-- @if ($agreement->verified)
                                            <span class="text-success">Verificado</span>
                                            @else
                                            <span class="text-danger">No Verificado</span>
                                            @endif --}}
                                            <span class="text-warning bg-status-{{$agreement->status}}"> {{ $agreement->status}} </span>
                                        </td>
                                        <td> {{date('d-m-Y', strtotime($agreement->created_at)) }} </td>
                                        <td>
                                            <a class="btn btn-sm btn-primary" href="{{ route ('agreements.show', $agreement) }}"> Ver</a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            {{-- <nav class="mt-15 pb-10">
                                <ul class="pagination justify-content-center">
                                    {{ $premiums->links() }}
                                </ul>
                            </nav> --}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
