@extends('layouts.web')
@section('content')
<section class="main-header" style="background-image:url(assets/images/gallery-3.jpg)">
    <header>
        <div class="container text-center">
            <h2 class="h2 title">¡Gracias por contactar con nosotras!</h2>

            <ol class="breadcrumb breadcrumb-inverted">
                <li><a href="/"><span class="icon icon-home"></span></a></li>
                <li><a class="active" href=""></a></li>
            </ol>
        </div>
    </header>
</section>
<section class="our-team">
    <div class="container">
            <div>
                <section class="">
                <article class="row">
                    {{-- <h1 class="col-md-12">¡Gracias por contactar con nosotras!</h1> --}}
                    <p>El equipo de hometohome te da la bienvenida a este precioso proyecto en el que
                        apostamos por amueblar de una manera sostenible y emocionante.                        </p>
                        <p>Gestionamos la venta de tus muebles para darles una segunda oportunidad.</p>
                </article>
                <hr>
                    <article class="cuerpo">
                        <div class="row">
                            <div class="col-md-9">
                                <h3><span></span>Opción 1 Servicio Básico
                                </h3>
                                <p>Vende tu mismo desde casa. Realiza buenas fotos y súbelas. Necesitamos además que describas el artículo breve y honestamente, indica si tiene algún desperfecto. Añade las medidas: Largo, ancho, alto</p>
                            </div>
                            <div class="limpiar"></div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="texto-der-mini col-md-12">
                                <h3><span></span>Opción 2 Servicio estándar
                                </h3>
                                <p>Tus artículos permanecen en tu domicilio. Buscamos compradores y cerramos la venta. Nos ocupamos de todo, solo tendrás que recibir al interesado para la retirada del artículo.
                                </p>
                                <p>
                                    <ul>
                                        <li>Tiempo aproximado 12 a 16 semanas.
                                        </li>
                                        <li>La retirada y transporte la asumen los compradores.
                                        </li>
                                    </ul>
                                </p>
                                <p>    Tarifa</p>
                                <p class="negrita">
                                    <ul>
                                        <li>25% de comisión sobre las ventas efectuadas
                                        </li>
                                        <li>Visita para valoración y dossier fotográfico, 40€
                                        </li>
                                    </ul>
                                </p>
                            </div>
                            <div class="limpiar"></div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="texto-der-mini col-md-12">
                                <h3><span></span>Opción 3 Servicio Premium                                </h3>
                                <p>Se retiran los artículos de tu casa y se llevan a un local de exposición. </p>
                                <p>
                                    <ul><li>Tiempo del contrato 12 a 16 semanas.</li></ul>
                                </p>
                                <p>Tarifa</p>
                                <p>
                                    <ul>
                                        <li>40% de comisión sobre las ventas efectuadas</li>
                                        <li>
                                            La retirada y transporte al local no está incluida en el precio Podemos sugerirte una empresa de confianza con precio ajustado.                                        </li>
                                            <li>Visita para valoración detallada: 40€
                                            </li>
                                    </ul>
                                </p>
                            </div>
                            <div class="limpiar"></div>
                        </div>
                    </article>
                </section>
            </div>
    </div>
</section>
@endsection
