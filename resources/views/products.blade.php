@extends('layouts.web')
@section('content')
<style scoped>
    .owl-carousel .owl-item img {
        /* max-width: 100% !important;
        max-height: 500px !important;
        margin: 0 auto; */
    }

    /* .img-responsive{
        max-height: 275px
    } */
    .oportunidad {
        color: #235952;
    }

    .label {
        font-weight: bold;
    }

    .vendido {
        color: #ffff;
        background: #ec275d;
    }

    .mensaje {
        padding: 10px;
        background: #079192;
        margin: 10px;
        color: #ffffff;
    }
</style>

@include('partials.product.header-category')

<section class="products">
    <div class="ma-2">
        <div class="row">
            <!--product items-->
            <div class="col-md-12 col-xs-12">

                <div class="sort-bar clearfix">
                    <div class="sort-results pull-left">
                        @if (isset($_GET['precio']) || isset($_GET['estancia']) || isset($_GET['ciudad']) || isset($_GET['name']) )
                        <a href="{{ route('page.more_product')}}" class="btn btn-primary btn-sm"> Mostrar Todos los articulos</a>
                        @endif
                    </div>
                </div>
                @if ($message)
                <div class="mt-3 row">
                    <div class="col-12">
                        <div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <i class="icon icon-checkmark-circle"></i> {{ $message }}
                        </div>
                    </div>
                </div>
                @endif
                @if ($textitle)
                <div class="mt-3 row">
                    <div class="col-12">
                        <div class="mensaje">
                            <i class="icon icon-checkmark-circle"></i> <strong>{{$textitle}}</strong>
                            <p> {{$textweb}} </p>
                        </div>
                    </div>
                </div>
                @endif
                <div class="row">
                    <!-- === product-item === -->
                    @foreach ($products as $item)
                    <div class="col-md-3 col-xs-12 d-flex align-items-stretch my-1">
                        <article>
                            <div class="fondo">
                                <div class="info">
                                    <span class="add-favorite bg-sucess">
                                        @if ($item->client_agreement)
                                        {{-- tiene contrato --}}
                                        <a data-title="Iniciar Chat" data-title-added="Enviar sms"
                                            href="https://web.whatsapp.com/send?phone={{ $item->client_agreement->handler->user->profile->phone_number }}"
                                            target="_blank"><i class="fa fa-whatsapp"></i>
                                        </a>
                                        @else
                                        {{-- basico --}}
                                        <a data-title="Iniciar Chat" data-title-added="Enviar sms"
                                            href="https://web.whatsapp.com/send?phone={{ $item->user->profile->phone_number }}"
                                            target="_blank"><i class="fa fa-whatsapp"></i>
                                        </a>
                                        @endif
                                    </span>
                                    <span class="view">
                                        <a href="#productid{{ $item->id }}" class="mfp-open" data-title="Ver Detalles"><i
                                                class="icon icon-eye"></i></a>
                                    </span>
                                    <span class="shared" onclick="copyToClipboard('{{Request::url()}}/detalle/{{$item->id}}')">
                                        @if ($item->client_agreement)
                                        {{-- tiene contrato --}}
                                        <a data-title="Copiar Enlace para compartir" data-title-added="Clipboard"
                                            href="https://web.whatsapp.com/send?phone={{ $item->client_agreement->handler->user->profile->phone_number }}&text={{Request::url()}}"
                                            ><i class="fa fa-share"></i>
                                        </a>
                                        @else
                                        {{-- basico --}}
                                        <a data-title="Copiar Enlace para compartir" data-title-added="Clipboard"
                                            href="https://web.whatsapp.com/send?phone={{ $item->user->profile->phone_number }}&text={{Request::url()}}"
                                            ><i class="fa fa-share"></i>
                                        </a>
                                        @endif
                                    </span>
                                </div>
                                <div class="btn btn-add">
                                    <a class="" href=" {{route('page.comprar', $item)}} "><i class="icon icon-cart"></i></a>
                                </div>
                                <div class="figure-grid">
                                    @if ($item->variant == 'NOVEDAD')
                                    <span class="label label-info">{{$item->variant}}</span>
                                    @endif
                                    @if ($item->variant == 'OFERTA')
                                    <span class="label label-primary">{{$item->variant}}</span>
                                    @endif
                                    @if ($item->variant == 'OPORTUNIDAD')
                                    <span class="label label-danger oportunidad">{{$item->variant}}</span>
                                    @endif
                                    @if ($item->variant == 'VENDIDO')
                                    <span class="label vendido">{{$item->variant}}</span>
                                    @endif
                                    <div class="image pt-2">
                                        <a href="#productid{{ $item->id }}" class="mfp-open">
                                            <div class="image-container">
                                                <img src="{{ asset($item->img) }}" alt="" class="img-responsive center-block" />
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="text-center bg-white">
                                    <p class="box-title title-article"><a class=""
                                            href="{{ route('page.detail_product', $item->id) }}">{{ $item->name }}</a></p>
                                    <hr class="borde-grueso">
                                    <p class="text-center fs-1-4 flexbox-price">
                                        @if ($item->reduced_price)
                                        <sub class="precio-normal"> {{ $item->reduced_price}} <i class="fa fa-euro"></i></sub>
                                        <sub class="tachado"> {{ $item->normal_price}} <i class="fa fa-euro"></i></sub>
                                        @else
                                        <sub>{{ $item->normal_price}} <i class="fa fa-euro"></i></sub>
                                        @endif
                                    </p>
                                </div>
                            </div>
                        </article>
                    </div>
                    @endforeach
                </div>

                <div class="pagination-wrapper">
                    {{ $products->appends($pagelinks)->links() }}
                </div>

            </div>
        </div>

        <!-- ============  Product info popup - quick view ============ -->
        @foreach ($products->sortBy('variant') as $item)
        <div class="popup-main mfp-hide" id="productid{{ $item->id }}">
            <!-- === product popup === -->
            <div class="product">
                <div class="popup-title" style="padding-right: 50px">
                    <div class="h1 title">{{ $item->name }}<small> <a class="text-red"
                                href="{{ route('page.category', $item->category->slug )}}">{{ $item->category->name }}</a></small>
                    </div>
                </div>
                <!-- === product gallery === -->
                <div class="owl-product-gallery">
                    @foreach ($item->images as $img)
                    <img src="{{ $img->path }}" alt="" />
                    @endforeach
                </div>

                <!-- === product-popup-info === -->
                <div class="popup-content">
                    <div class="product-info-wrapper">
                        <div class="row">
                            <!-- === left-column === -->
                            <div class="col-sm-6">
                                <div class="info-box">
                                    <strong>Contacto:</strong>
                                    <span>{{ $item->user->name }}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- === product-popup-footer === -->
                <div class="popup-table">
                    <div class="popup-cell">
                        <div class="price">
                            @if ($item->reduced_price)
                            <span class="h3"> {{$item->reduced_price}} <i class="fa fa-euro"></i> <small> <i class="fa fa-euro"></i>
                                    {{$item->normal_price}}</small></span>
                            @else
                            <small style="text-decoration: none;"> {{$item->normal_price}} <i class="fa fa-euro"></i> </small>
                            @endif
                        </div>
                    </div>
                    <div class="popup-cell">
                        <div class="popup-buttons">
                            <a href="{{ route('page.detail_product', $item->id) }}"><span class="icon icon-eye"></span>
                                <span class="hidden-xs">Mas Detalles</span></a>
                            <a href="{{ route('page.comprar', $item)}}"> <span class="icon icon-cart"></span> <span
                                    class="hidden-xs">Comprar</span></a>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        @endforeach
    </div>
</section>

@endsection
@section('scripts')
<script>
    function copyToClipboard(text) {
    text = text.replace("/articulos",'');
    const elem = document.createElement('textarea');
    elem.value = text;
    document.body.appendChild(elem);
    elem.select();
    document.execCommand('copy');
    document.body.removeChild(elem);
    alert("Enlace Copiado");
}
</script>
@endsection
