@extends('layouts.web')
@section('content')
<section class="main-header" style="background-image:url(assets/images/gallery-3.jpg)">
    <header>
        <div class="container text-center">
            <h2 class="h2 title">¿Cómo funciona?</h2>

            <ol class="breadcrumb breadcrumb-inverted">
                <li><a href="/"><span class="icon icon-home"></span></a></li>
                <li><a class="active" href="">Cómo funciona</a></li>
            </ol>
        </div>
    </header>
</section>
<section class="our-team">
    <div class="container">
            <div>
                <section class="intro-comofunciona">
                    <article class="row">
                        <h1 class="col-md-12">Si compras los muebles</h1>
                        <p>¿Eres fan de los muebles de segunda mano, te gustan los muebles rústicos con un toque
                            antiguo o
                            vintage, los cuales puedes reformar, redecorar…? ¿Cómo funciona nuestro servicio?
                        </p>
                        <div class="cajas col-md-4 uno">
                            <h3>¿Dónde comprar?</h3>
                        </div>
                        <div class="cajas col-md-4 dos">
                            <h3>Hogares y almacén</h3>
                        </div>
                        <div class="cajas col-md-4 tres">
                            <h3>¿Cómo me lo llevo?</h3>
                        </div>
                        <div class="limpiar"></div>
                    </article>
                </section>
                <section class="filas-comofunciona">
                    <article class="cuerpo">
                        <div class="row">
                            <div class="col-md-3">
                                <img
                                    src="http://vaciatucasa.com/wp-content/imagenesnuevas/filas-comofunciona-fila-uno.png">
                            </div>
                            <div class="col-md-9">
                                <h3><span>1</span>¿Dónde comprar?</h3>
                                <p>¿Buscas algún artículo en concreto? ¿O sólo estás echando un vistazo? Sea
                                    como sea, en
                                    nuestra web dispones de fotografías y detalles de los artículos que
                                    poseemos. Los
                                    muebles están en exposición en nuestro rincón o directamente en los hogares
                                    de los
                                    cliente.</p>
                            </div>
                            <div class="limpiar"></div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-md-3"><img
                                    src="http://vaciatucasa.com/wp-content/imagenesnuevas/filas-comofunciona-fila-dos.png">
                            </div>
                            <div class="texto-der-mini col-md-5">
                                <h3><span>2</span>Nuestro rincón</h3>
                                <p>Puedes visitar nuestro rincón en Leganés, donde tenemos una exposición de los
                                    artículos.
                                    Muchas veces una foto convence, pero no enamora. ¡Ojo! Si quieres venir a
                                    nuestro
                                    almacén, tienes que llamarnos y concertar una cita para que podamos
                                    dedicarte el tiempo
                                    que necesites.</p>
                            </div>
                            <div class="texto-der-mini col-md-4">
                                <h3>Hogares</h3>
                                <p>También tenemos algunos artículos directamente en los hogares de nuestros
                                    clientes, es
                                    decir, ellos prestan sus casas como expositores donde podrás ver exactamente
                                    el
                                    producto, tomar medidas, y por supuesto, tomar una decisión final sobre si
                                    adquirirlo o
                                    no.</p>
                            </div>
                            <div class="limpiar"></div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-md-3"><img
                                    src="http://vaciatucasa.com/wp-content/imagenesnuevas/filas-comofunciona-fila-tres.png">
                            </div>
                            <div class="texto-der-mini col-md-5">
                                <h3><span>3</span>Te los llevas a casa</h3>
                                <p>La mayoría de los artículos son de segunda mano, lo que significa, que han
                                    tenido una
                                    vida anterior. Nosotros fotografiamos, medimos y describimos con la mayor
                                    claridad
                                    posible todos los artículos que comercializamos. Sólo tienes que decirnos
                                    cuándo quieres
                                    recogerlo y nosotros avisaremos al propietario si el mueble se encuentra en
                                    su hogar,
                                    sino podrás recogerlo en nuestro almacén.</p>
                            </div>
                            <div class="texto-der-mini col-md-4">
                                <h3>Nuestro transporte</h3>
                                <p>Nosotros queremos facilitar al máximo tu compra y podemos proporcionarte
                                    transportistas,
                                    agencias de portes compartidos, etc. Estos profesionales son independientes
                                    a
                                    Vaciatucasa. Aunque te facilitemos esta información, ten en cuenta que en
                                    última
                                    instancia eres tú quien toma la decisión de con quién y cómo retirar y
                                    transportar el
                                    artículo.</p>
                            </div>
                            <div class="limpiar"></div>
                        </div>
                    </article>
                </section>
            </div>

            <div class="text-center">
                <section class="banner-comofunciona bg-primary">
                    <article class="row">
                        <div class="col-md-3"> <img
                                src="http://vaciatucasa.com/wp-content/imagenesnuevas/banner-comofunciona-col-uno.png">
                            <p>Zona outlet: muebles no vendidos de las colecciones anteriores</p>
                        </div>
                        <div class="col-md-3"> <img
                                src="http://vaciatucasa.com/wp-content/imagenesnuevas/banner-comofunciona-col-dos.png">
                            <p>Más de 10.000 referencias gestionadas el año pasado</p>
                        </div>
                        <div class="col-md-3"> <img
                                src="http://vaciatucasa.com/wp-content/imagenesnuevas/banner-comofunciona-col-tres.png">
                            <p>¿Quieres decorar tu casa? Te podemos ayudar</p>
                        </div>
                        <div class="col-md-3"> <img
                                src="http://vaciatucasa.com/wp-content/imagenesnuevas/banner-comofunciona-col-cuatro.png">
                            <p>La evolución digital del “Garage Sale”</p>
                        </div>
                        <div class="limpiar"></div>
                    </article>
                </section>
            </div>

            <div>
                <section class="medio-comofunciona">
                    <article class="row">
                        <div class="col-md-12">

                            <h2 class="text-center h4">Si vendes tus muebles</h2>
                            <p>Si te mudas o te trasladas y necesitas deshacerte del mobiliario de tu hogar
                                ubicado
                                en la
                                Comunidad de Madrid, nosotros gestionamos la venta de tus muebles,
                                electrodomésticos
                                y objetos
                                de decoración, sin que tengas que hacer nada.</p>
                        </div>
                        <div class="cajas col-md-4 uno">
                            <h3>Contacta con nosotros</h3>
                        </div>
                        <div class="cajas col-md-4 dos">
                            <h3>Te llamamos</h3>
                        </div>
                        <div class="cajas col-md-4 tres">
                            <h3>Vendemos tus muebles</h3>
                        </div>
                        <div class="limpiar"></div>
                    </article>
                </section>
                <section class="final-comofunciona">
                    <article class="row">
                        <div class="row">
                            <div class="col-md-3"><img
                                    src="http://vaciatucasa.com/wp-content/imagenesnuevas/final-comofunciona-fila-uno.png">
                            </div>
                            <div class="col-md-9">
                                <h3 class="title"><span>1 </span>Contacta con nosotros desde nuestro formulario.
                                </h3>
                                <p>En muchas ocasiones, necesitas deshacerte del mobiliario de tu hogar y no
                                    sabes muy bien
                                    cómo hacerlo. Los muebles pasan largos períodos publicados en aplicaciones
                                    de venta de
                                    muebles u objetos de segunda mano y muchas veces no se terminan de vender.
                                    Además, hacer
                                    fotografías a todos los muebles o gestionar todos los mensajes de posibles
                                    clientes es
                                    agotador, ¿verdad? Vacíatucasa tiene la solución.</p>
                            </div>
                            <div class="limpiar"></div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-3"><img
                                    src="http://vaciatucasa.com/wp-content/imagenesnuevas/final-comofunciona-fila-dos.png">
                            </div>
                            <div class="col-md-9">
                                <h3 class="title"><span>2 </span>Nos ponemos en contacto contigo</h3>
                                <p>Vacíatucasa es la solución para tus muebles. La mayoría de los artículos son
                                    de segunda
                                    mano, lo que significa, que han tenido una vida anterior. Nosotros,
                                    fotografiamos,
                                    medimos y describimos con la mayor claridad posible todos los artículos que
                                    comercializamos. Siempre intentamos reflejar la realidad lo mejor posible,
                                    para que te
                                    sientas cómodo vendiendo a través de nosotros y que no tengas dudas.</p>
                            </div>
                            <div class="limpiar"></div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-3"><img
                                    src="http://vaciatucasa.com/wp-content/imagenesnuevas/final-comofunciona-fila-tres.png">
                            </div>
                            <div class="col-md-9">
                                <h3 class="title"><span>3 </span>Nos encargamos de vender tus muebles</h3>
                                <p>Muchos de nuestros clientes, poseen un trastero en su hogar para almacenar
                                    ese mueble
                                    hasta la venta. Sin embargo, no siempre es así, para aquellas personas que
                                    realizan una
                                    mudanza de manera urgente y no disponen de sitio donde almacenar sus
                                    muebles, nosotros
                                    ponemos a su disposición nuestro local. <b>El objetivo siempre es el mismo:
                                        darles una
                                        segunda oportunidad a los muebles usados</b>.</p>
                            </div>
                            <div class="limpiar"></div>
                        </div>
                    </article>
                </section>
            </div>
    </div>
</section>
@endsection