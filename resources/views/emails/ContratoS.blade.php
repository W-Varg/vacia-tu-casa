@component('mail::message')
# ASUNTO: Contrato Estándar de Articulos

DE UNA PARTE ***{{ $agreement->client->user->name }} {{ $agreement->client->user->profile->last_name }}***, con DNI ***{{ $agreement->client->user->profile->dni }}*** residiendo en Madrid con codigo postal **{{ $agreement->client->user->profile->code_postal }}** y reconociendo la relación de objetos, mencionada en este contrato ***000{{ $agreement->id}}*** y recogida en el ANEXO 1 de su propiedad.

DE OTRA PARTE vaciatucasa & hometohome  con domicilio comercial en Leganés Calle Asturias, 9 28913

## ACUERDAN

Vaciatucasa es una empresa dedicada a la gestión de la venta de los artículos que el cliente quiere vender.  En ningún momento  vaciatucasa es propietaria de ninguno de los objetos recogidos en el ANEXO 1.

Las gestiones que HOMETOHOME realizará serán las siguientes:

**1)** Inventariado, fotografiado y/o grabación de todos los artículos para su venta.

**2)** Valoración de todos los artículos. Dicha valoración figura en el (ANEXO 1)

**3)** Realizar las gestiones oportunas para la venta de dichos artículos , tales como:


- <p>Realización de los anuncios de los artículos en nuestra Web.</p>
- <p>Publicación de dichos anuncios en Webs especializadas</p>
- <p>Publicidad en redes sociales.</p>
- <p>Realizar una política de descuento para favorecer la venta:</p>

    <p> 1. A las 6 semanas se aplicará el 15%.</p>
    <p> 2. A las 8 semanas se aplicará el 30%.</p>
    <p> 3. A las 12 semanas se aplicará el 50%.</p>
    <p> 4. A las 16 semanas se da por finalizado el contrato y se procede al abono del importe correspondiente de la venta total.</p>
    {{-- <p> 4. Al término del contrato (12 semanas), excepcionalmente se podrá ampliar el contrato hasta un máximo de 16 semanas y se aplicará un descuento del 50% del precio original.</p> --}}
- <p>Intermediar con todos los posibles compradores, encargándose de la coordinación de la visita.</p>
- <p>Gestionar y cerrar el precio de cada artículo, para lo cual el cliente le confiere expresa representación por medio de este documento.</p>

**4)** Vaciatucasa dispone de una capacidad de negociación sobre el precio fijado con el cliente de un máximo del 15% de descuento sin previa consulta.

La duración del presente contrato es de 16 semanas, **iniciándose el día de publicación de los artículos en la Web.**

La comisión para HOMETOHOME aplicada a dicha gestión será del 25 % del precio final de todos los artículos vendidos.

Dado que el cobro de la venta de los bienes será realizado por el cliente o/y vaciatucasa, las partes acuerdan reunirse de forma presencial o telemáticamente en el día de finalización del contrato, con el objeto de liquidar por completo los importes cobrados fruto de la venta de los enseres en el porcentaje arriba descrito.

El cliente otorga exclusividad a vaciatucasa para gestionar la venta de todos los artículos desde el momento de la firma del contrato hasta la finalización del mismo.

Durante la duración del contrato, en caso de que el cliente quiera retirar de la venta alguno o algunos de los artículos que figuran en el ANEXO 1 pagará una penalización a HOMETOHOME, del 15%  del valor de tasación inicial de cada artículo o artículos anulados.

Por el presente documento se consiente de forma expresa que los datos de carácter personal facilitados sean transmitidos a terceros con la exclusiva finalidad del cumplimiento del presente contrato.

___

# A N E X O 1

Relación de artículos que el cliente: ***{{ $agreement->client->user->name }} {{ $agreement->client->user->profile->last_name }}***  desea vender:

@component('mail::table')
| Fotografía    | Nombre de Articulo| precio de Articulo  |
| ------------- |:-----------------:| -------------------:|
@foreach($agreement->products as $product)
| <img src="{{ asset($product->img) }}" height="60"> |   *{{ $product->name }}*  | ***{{ $product->normal_price }}*** |
@endforeach
|               |                   |                     |
|               |    TOTAL    | EUROS ***{{ $total }}***  |
@endcomponent

@component('mail::button', ['url' => 'http://'.$url.'/agreements/verify/'.$agreement->verifyAgreement->token]) Aprobar Contrato @endcomponent

@component('mail::button', ['url' => 'https://web.whatsapp.com/send?phone='.$gestor, 'color' => 'success' ]) contactar con gestor @endcomponent


Accede a tu área de cliente para aprobar el contrato

**Agradecemos tu confianza. _{{ config('app.name') }}_**
@endcomponent
