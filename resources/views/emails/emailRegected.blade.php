@component('mail::message')

@component('vendor.mail.html.title1', ['color' => 'text--black']) Hola, {{ $product->user->name }} @endcomponent

@component('vendor.mail.html.title1', ['color' => 'text--primary']) Tu Articulo @endcomponent

@component('vendor.mail.html.title1', ['color' => 'text--red']) {{ $product->name }} @endcomponent

@component('vendor.mail.html.title1', ['color' => 'text--primary']) Ha sido rechazado @endcomponent

@component('vendor.mail.html.logo_rechazado') @endcomponent

{!! $razon !!}

{{-- @component('mail::table')
| Fotografía | Nombre de Articulo| precio de Articulo |
| ------------- |:-----------------:| -------------------:|
| <img src="{{ asset($product->img) }}" height="60"> | *{{ $product->name }}* | ***{{ $product->normal_price }}*** |
@endcomponent --}}

@component('vendor.mail.html.parrafo')
Recibe un cordial saludo <br> El equipo hometohomedecor
@endcomponent

@component('mail::button', ['url' => 'http://'.$url ])
Ir a {{ config('app.name') }}
@endcomponent

@endcomponent
