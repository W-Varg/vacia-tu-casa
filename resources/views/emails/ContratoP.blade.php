@component('mail::message')
# ASUNTO: Contrato Premium de Articulos

{{-- En Madrid {{ date('d-m-Y', strtotime($agreement->created_at)) }} --}}

DE UNA PARTE ***{{ $agreement->client->user->name }} {{ $agreement->client->user->profile->last_name }}***, con DNI ***{{ $agreement->client->user->profile->dni }}*** residiendo en Madrid con codigo postal **{{ $agreement->client->user->profile->code_postal }}** y reconociendo la relación de objetos, mencionada en este contrato ***000{{ $agreement->id}}*** y recogida en el ANEXO 1 de su propiedad.

DE OTRA PARTE vaciatucasa & hometohome  con domicilio comercial en Leganés Calle Asturias, 9 28913

## CONDICIONES

Vaciatucasa es una empresa dedicada a la gestión de la venta de todos los artículos (ANEXO 1) que el cliente quiere vender.  En ningún momento  vaciatucasa es propietaria de ninguno de los objetos recogidos en el ANEXO 1.

### Gestiones a realizar:


**1)** Inventariado, fotografiado y/o grabación de todos los artículos que el cliente desea vender.

**2)** Valoración de todos los artículos. Dicha valoración figura en el (ANEXO 1)

**3)** Realizar las gestiones oportunas para la venta de los artículos del cliente,tales como:

- <p>Realización de los anuncios delos artículos en nuestra Web.</p>
- <p>Publicación de dichos anuncios en Webs especializadas</p>
- <p>Publicidad en redes sociales.</p>
- <p>Realizar una política de descuento para favorecer la venta:</p>

    <p> 1. A las 6 semanas se aplicará el 15%.</p>
    <p> 2. A las 8 semanas se aplicará el 30%.</p>
    <p> 3. A las 12 semanas se aplicará el 50%.</p>
    <p> 4. A las 16 semanas se da por finalizado el contrato y se procede al abono del importe correspondiente de la venta total.</p>
    {{-- <p> 4. Al término del contrato (12 semanas), excepcionalmente se podrá ampliar el contrato hasta un máximo de 16 semanas y se aplicará un descuento del 50% del precio original.</p> --}}
- <p>Intermediar con todos los posibles compradores, encargándose de la coordinación de la visita.</p>
- <p>Gestionar y cerrar el precio de cada artículo, para lo cual el cliente le confiere expresa representación por medio de este documento.</p>

**4)** Vaciatucasa dispone de una capacidad de negociación sobre el precio fijado con el cliente de un máximo del 15% de descuento sin previa consulta.

La duración del presente contrato es de 16 semanas, **iniciándose el día de publicación de los artículos en la Web.**

La comisión para HOMETOHOME aplicada a dicha gestión será del 40% del precio final de todos los artículos vendidos.

El cliente otorga exclusividad a vaciatucasa para gestionar la venta de todos los artículos desde el momento de la firma del contrato hasta la finalización del mismo.

Durante la duración del contrato, en caso de que el cliente quiera retirar de la venta alguno o algunos de los artículos que figuran en el ANEXO 1 pagará una penalización a HOMETOHOME, del 40%  del valor de tasación inicial de cada artículo o artículos anulados.

Por el presente documento se consiente de forma expresa que los datos de carácter personal facilitados sean transmitidos a terceros con la exclusiva finalidad del cumplimiento del presente contrato.


# CLÁUSULAS DEPÓSITO Y EXPOSICIÓN MOBILIARIO

1. El depósito tiene por objeto la exposición y venta de dichos artículos en el plazo de 16 semanas

2. En el caso de que algún artículo quede sin vender, el depósito será gratuito respecto de aquél.

3. El transporte y montaje de los artículos hasta el local de depósito corre a cargo del depositante, al igual que, en su caso, los gastos de retirada y desmontaje.

4. El local de depósito está situado en Calle Asturias nº 9 de Leganés, C.P. 28913 (Madrid), lugar donde permanecerán los artículos el tiempo que dure el depósito.

5. Este cuidará conforme a criterio profesional de la exposición de los artículos,  atendiendo a los potenciales compradores y percibiendo el importe de la venta, para su posterior liquidación con el cliente.

6. Para la RETIRADA de los artículos no vendidos, el depositante remitirá correo electrónico a la dirección silvia.vaciatucasa@gmail.com a fin de concertar cita.

MUY IMPORTANTE: EL PLAZO DE RETIRADA DE ARTÍCULOS NO VENDIDOS ES DE 7 DÍAS HÁBILES. EN EL CASO DE NO HABERLOS RETIRADO EN ESE PLAZO SE CONSIDERARÁN ABANDONADOS Y PASARÁN A PROPIEDAD DEL DEPOSITARIO.

___

# A N E X O 1

Relación de artículos que el cliente: ***{{ $agreement->client->user->name }} {{ $agreement->client->user->profile->last_name }}***  desea vender:

@component('mail::table')
| Fotografía    | Nombre de Articulo| precio de Articulo  |
| ------------- |:-----------------:| -------------------:|
@foreach($agreement->products as $product)
| <img src="{{ asset($product->img) }}" height="60"> |   *{{ $product->name }}*  | ***{{ $product->normal_price }}*** |
@endforeach
|               |                   |                     |
|               |    TOTAL    | EUROS ***{{ $total }}***  |
@endcomponent

@component('mail::button', ['url' => 'http://'.$url.'/agreements/verify/'.$agreement->verifyAgreement->token]) Aprobar Contrato @endcomponent

@component('mail::button', ['url' => 'https://web.whatsapp.com/send?phone='.$gestor, 'color' => 'success' ]) contactar con gestor @endcomponent


Accede a tu área de cliente para aprobar el contrato


**Agradecemos tu confianza. _{{ config('app.name') }}_**
@endcomponent
