
@component('mail::message')

 {{ $product->user->name }}
<h1>{{ $details['title'] }}</h1>
<p>{{ $details['body'] }}</p>

Articulo {{ $product->name}}, a  {{ $product->normal_price}} Euros

{{-- {{  Request::path() }} --}}
{{-- \Request::segment(1) --}}


@component('mail::button', ['url' => 'http://'.$url.'/detalle/'.$product->id , 'class' => 'btn btn-warning'])
Ver Articulo
@endcomponent

Agradecemos tu confianza.<br>
{{ config('app.name') }}
@endcomponent
