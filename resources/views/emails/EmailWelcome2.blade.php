@component('mail::message')
## DATOS RECIBIDOS CORRECTAMENTE!

#¡Gracias por tu interés!

### Hemos recibido tu solicitud, nos pondremos en contacto contigo lo antes posible  :-)

Solo queda que confirmes tu email

[{{ config('app.name') }}]($url)

@endcomponent
