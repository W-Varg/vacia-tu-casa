@component('mail::message')
<img src="{{ asset('images/Contratofinalizado.png') }}" height="65">
<p></p>
<p></p>

<center> ¡Felicidades!</center>

<p></p>
<p></p>

Tus artículos han sido publicados.


***Cliente: {{ $agreement->client->user->name }} {{ $agreement->client->user->profile->last_name }}***

Los artículos del contrato {{$agreement->name}} ***000{{ $agreement->id }}*** han sido publicados en  nuestra web y en redes sociales.Puedes realizar el seguimiento de las ventas EN TU ZONA DE CLIENTE

Gracias por tu confianza


@component('mail::table',['color'=>'success'])
| Nombre de Articulo| Precio |
|:-----------------:| -------------------:|
@foreach($agreement->products as $product)
| *{{ $product->name }}* | ***{{ $product->normal_price }} EUROS*** |
@endforeach
| | |
| TOTAL | ***{{ $total }} EUROS*** |
@endcomponent

@component('mail::button', ['url' => 'https://web.whatsapp.com/send?phone='.$agreement->handler->user->profile->phone_number, 'color' =>'success' ]) contactar con gestor @endcomponent


@component('mail::button', ['url' => 'http://'.$url ])
Ir a {{ config('app.name') }}
@endcomponent

**Agradecemos tu confianza.**
@endcomponent
