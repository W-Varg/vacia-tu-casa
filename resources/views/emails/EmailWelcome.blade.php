@component('mail::message')
<h2> ¡Hola {{ auth()->user()->name }}, gracias por contactar con nosotras!</h2>
<br>

Nuestra empresa, <strong>{{ config('app.name') }}</strong> gestiona la venta de muebles y decoración. No compramos, buscamos comprador y cobramos
una comisión sobre la venta final.

<h3>Opción 1. Servicio Básico</h3>
<div class="text-justify">
    <p>Tú personalmente gestionas la venta desde nuestra plataforma.</p>
</div>

Sube tus artículos:
<div class="text-justify">
    <p>- <strong>TÍTULO</strong>, Que lo describa en tres palabras</p>
    <p> - <strong>DESCRIPCIÓN</strong>, Real, concisa y honesta. Precio original aproximado, cuenta cómo es tu mueble... No olvides señalar si
        tiene algún desperfecto.
    </p>
    <p> - <strong>MEDIDAS</strong>, Largo, ancho, alto </p>
    <p> - <strong>FOTOGRAFÍAS</strong>, Mínimo SEIS, de la mayor calidad posible, mejor con luz natural, la principal del mueble entero de
        frente, desde diferentes ángulos y de los detalles más relevantes. El 90% de la venta depende de la imagen. No olvides despejarlo, que se
        vea bonito.</p>
</div>
<h3> <strong>¡Esta opción es totalmente gratuita!</strong> </h3>


<h3>Opción 2. Servicio estándar</h3>

Tus artículos permanecen en tu domicilio. Buscamos compradores y cerramos la venta, solo tendrás que recibirlos para la retirada del artículo.
<div class="text-justify">
    <p>- Tiempo del contrato 8-16 semanas desde la firma.</p>
    <p>- La retirada y transporte las asumen los compradores.</p>
</div>
<strong> Tarifas:</strong>
<div class="text-justify">
    <p> - 25% de comisión sobre las ventas efectuadas </p>
</div>
<h3> Opción 3. Servicio Premium</h3>
<div class="text-justify">
    <p>Se retiran los artículos de tu casa y se llevan a un local de exposición.</p>
</div>

<div class="text-justify">
    <p> - Tiempo del contrato 8-16 semanas desde la firma. </p>
    <p> - El transporte no está incluido en el precio (Podemos sugerirte una empresa de confianza con tarifas económicas) </p>
</div>
<strong> Tarifas:</strong>

<div class="text-justify">
    <p> - 40% de comisión sobre las ventas efectuadas</p>
</div>

<p>
Concertamos una visita previa en tu domicilio para comprobar el estado de conservación,
fotografiar y medir tus muebles. <strong>Importe: 40€.</strong>
</p>


Quedamos a tu disposición para cualquier consulta o comentario
Un saludo


@component('mail::button', ['url' => 'http://'.$url ])
Ir a {{ config('app.name') }}
@endcomponent

@endcomponent
