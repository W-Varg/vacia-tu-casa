@extends('layouts.web')
@section('content')
<section class="main-header" style="background-image:url(assets/banners/bosque.jpg)">
    <header>
        <div class="container text-center">
            {{-- <h2 class="h2 title">Registro realizado con éxito.</h2> --}}
            <ol class="breadcrumb breadcrumb-inverted">
                <li><a href="/"><span class="icon icon-home active"></span></a></li>
            </ol>
        </div>
    </header>
</section>

<section class="our-team">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                @if (auth()->user())
                <h3>
                    Gracias por ofrecer una segunda oportunidad a tus muebles. Hola {{ auth()->user()->name }}, ya puedes empezar a vender con
                    Home To Home
                </h3>
                @endif
            </div>
        </div>
    </div>
</section>
@endsection
