@component('mail::message')

@component('vendor.mail.html.title1', ['color' => 'text--black']) ¡Hola, {{ $client }}! @endcomponent

@component('vendor.mail.html.title1', ['color' => 'text--primary']) Tu Contrato @endcomponent

@component('vendor.mail.html.title1', ['color' => 'text--red']) {{ $code }} @endcomponent

@component('vendor.mail.html.title1', ['color' => 'text--primary']) Ha sido rechazado @endcomponent

@component('vendor.mail.html.logo_rechazado') @endcomponent

{!! $razon !!}

@component('vendor.mail.html.parrafo')
Recibe un cordial saludo <br> El equipo hometohomedecor
@endcomponent

@component('mail::button', ['url' => 'http://'.$url ])
Ir a {{ config('app.name') }}
@endcomponent

@endcomponent
