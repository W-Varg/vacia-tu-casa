@component('mail::message')
<section style="margin: 5vh;">
    <div class="container">
        <div class="text-center pa-05">
            <div class="pa-2 bg-white">
                <img src="{{ asset('images/publicado.png') }}" alt="" class="img-responsive center-block" />
            </div>
            <div class="pa-2 bg-white">
                <img src="{{ asset('images/publicado2.png') }}" alt="" class="img-responsive center-block" />
            </div>
            <div class="pa-2 bg-white">
                <img src="{{ asset('images/publicado3.png') }}" alt="" class="img-responsive center-block" />
            </div>
        </div>
        <table class="action" align="center" width="100%" cellpadding="0" cellspacing="0" role="presentation">
            <tr>
                <td align="center">
                    <table width="100%" border="0" cellpadding="0" cellspacing="0" role="presentation">
                        <tr>
                            <td align="center">
                                <table border="0" cellpadding="0" cellspacing="0" role="presentation">
                                    <tr>
                                        <td>
                                            <p>
                                                <a href="{{ 'http://'.$url.'/detalle/'.$product->id }}" class="button button-primary" target="_blank"> {{ 'Ver Articulo' }} </a>
                                            </p>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <table width="100%" border="0" cellpadding="0" cellspacing="0" role="presentation">
                        <tr>
                            <td align="center">
                                <table border="0" cellpadding="0" cellspacing="0" role="presentation">
                                    <tr>
                                        <td>
                                            <a href="{{ 'http://'.$url }}" class="button button-primary" target="_blank"> Ir a {{ config('app.name') }} </a>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
</section>
@endcomponent
