@component('mail::message')
<img src="{{ asset('images/Contratofinalizado.png') }}" height="65">

<p></p>
<p></p>
<center> <b> ¡Contrato Finalizado!</b> </center>
<p></p>
<p></p>

Hola, {{ $cliente->name }} {{ $cliente->profile->last_name }}


Te informamos de que el tiempo estipulado en el contrato estándar 000{{ $contrato->id }} ha finalizado.

El importe de nuestra gestión es del 25% del total: ***{{ str_replace('.', ',', $utilidad->utilidad) }} €***

{{-- ----------- --}}
Importe cobrado por Hometohome: {{ str_replace('.', ',', $cobrado_gestor) }} €.

Importe cobrado por el cliente: {{ str_replace('.', ',', ($utilidad->utilidad *0.75) - $cobrado_cliente) }} €.

@if ((($utilidad->utilidad) - $cobrado_gestor) >= 0)
Saldo a favor de Hometohome: {{ str_replace('.', ',', ($utilidad->utilidad) - $cobrado_gestor) }} €.
@else
Saldo a favor de Hometohome:
@endif

@if ((($utilidad->utilidad *0.75) - $cobrado_cliente) >0)
Saldo a favor del cliente: {{ str_replace('.', ',', ($utilidad->utilidad *0.75) - $cobrado_cliente) }} €.
@else
Saldo a favor del cliente:
@endif
{{-- ----------- --}}

Comision de Hometohome: {{ str_replace('.', ',', ($utilidad->utilidad) * 0.25) }} €.

En caso de saldo a nuestro favor, cuando puedas haznos el ingreso al siguiente número de cuenta:

ES63 1465 01 00972039206613 <br>
BANCO ING Direct <br>
TITULAR Silvia Robledo Armentera <br>
Concepto: Numero de Contrato premiun 000{{ $contrato->id }}


En caso contrario, cuando puedas indícanos un número de cuenta y titular.

***Adjunto enviamos el Excel con las ventas efectuadas.***

***Agradeceríamos que nos dedicaras unas líneas sobre tu experiencia con nosotros escribiendo una pequeña reseña en google.***

Un afectuoso saludo,

Equipo vaciatucasa-hometohome

{{-- @component('mail::button', ['url' => 'http://'.$url.'/Contrato/excel/'.$agreement->id, 'color' => 'success']) Excel @endcomponent --}}


@component('mail::button', ['url' => 'http://'.$url]) Ir a HOMETOHOME @endcomponent

@endcomponent
