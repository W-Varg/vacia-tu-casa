@component('mail::message')
<img src="{{ asset('images/Contratofinalizado.png') }}" height="65">
<p></p>
<p></p>
<center> <b> Contrato Finalizado</b> </center>
<p></p>
<p></p>

Hola, ***{{ $cliente->name }} {{ $cliente->profile->last_name }}***

Te informamos de que el tiempo estipulado en el contrato premium 000{{ $contrato->id }} ha finalizado.

El importe de nuestra gestión es del 40% del total: {{ str_replace('.', ',', $utilidad->utilidad) }} €.
De tal forma debemos ingresar el importe de: ***{{ str_replace('.', ',', $saldo) }} €.*** Por favor, cuando puedas, indícanos un número de cuenta y titular.

 ***Adjunto enviamos el Excel con las ventas efectuadas.***



 Recordarte que han quedado artículos sin vender (detallado en el excel). Como sabes dispones de un plazo de 7 días, a partir de la fecha de hoy, para retirar los artículos. Por favor confirma si quieres hacerlo y ponte en contacto con Silvia (silvia.vaciatucasa@gmail.com), para concertar el día y la hora de la recogida.



En caso de que antes del **{{ date('d-m-Y', strtotime(Carbon\Carbon::today()->addDays(7))) }}** no se retiren, los artículos se considerarán abandonados y pasarán a propiedad del depositario, siendo destinados a una ONG.



***Agradeceríamos que nos dedicaras unas líneas sobre tu experiencia con nosotros escribiendo una pequeña reseña en google.***

Un afectuoso saludo,

vaciatucasa-hometohome

{{-- @component('mail::button', ['url' => 'http://'.$url.'/Contrato/excel/'.$agreement->id, 'color' => 'success']) Excel @endcomponent --}}

@component('mail::button', ['url' => 'http://'.$url]) Ir a HOMETOHOME @endcomponent

@endcomponent
