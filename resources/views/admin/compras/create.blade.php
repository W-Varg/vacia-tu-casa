@extends('layouts.admin')

@section('content')

<div class="box box-solid bg-dark">
	<div class="box-header with-border">
		<h3 class="box-title">Registrar Artículo</h3>
        {{-- <h6 class="box-subtitle"> Registrar un articulo para la venta</h6> --}}
        <a href="{{ route ('products.index')}}" class="btn btn-danger float-right">Cancelar</a>
	</div>
	<!-- /.box-header -->
	<div class="box-body">
		{!! Form::open(['route' => 'products.store', 'files' =>true ]) !!}	
			@include('admin.products.productForm')
		{!! Form::close() !!}
	</div>
	<div class="box-footer">
		vista previa del articulo
	</div>
</div>

@endsection