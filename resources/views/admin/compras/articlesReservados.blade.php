@extends('layouts.admin')

@section('content')
<div class="row">
    <div class="col-md-12 col-lg-12">
        <div class="box box-solid bg-dark">
            <div class="box-header with-border">
                <h3 class="box-title">Reservados</h3>
                {{-- <a href="{{ route ('products.create')}}" class="btn btn-info float-right">Registrar</a> --}}
                <h6 class="box-subtitle"> Listado de Articulos Reservados</h6>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#Reservados" role="tab"><span><i
                                    class="fa  fa-file"></i></span> Articulos Reservados </a> </li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content tabcontent-border">
                    <div class="tab-pane active" id="Reservados" role="tabpanel">
                        <div class="pad">
                            <table class="table table-striped">
                                <thead>
                                    <th>#</th>
                                    <th>Cliente</th>
                                    <th>telefono</th>
                                    <th>Email</th>
                                    <th>Producto</th>
                                    <th>Vendedor</th>
                                    {{-- <th>Fecha Reservado</th> --}}
                                    <th>Importe</th>
                                    <th>Fecha Reserva</th>
                                    <th></th>
                                </thead>
                                <tbody>
                                    @foreach($compras as $compra)
                                    <tr>
                                        <th> {{ $loop->iteration }}</th>
                                        {{-- <td> <a href="{{ route ('agreements.show', $compra) }}">
                                        {{ $compra->name }} </a> </td> --}}
                                        <td>
                                            @if ($compra->comprador)
                                            <a class="text-warning" href="{{ route ('profile', $compra->buyer) }}">
                                                {{ $compra->buyer->name }} {{ $compra->buyer->profile->last_name}}
                                            </a>
                                            @else
                                            {{ $compra->nameClient }}
                                            @endif
                                        </td>
                                        <td>
                                            <a href="https://web.whatsapp.com/send?phone={{ $compra->phone }}" class="btn btn-primary btn-sm"
                                                target="_blank">
                                                <i class="fa fa-whatsapp"></i>
                                                {{$compra->phone}}</a>
                                        </td>
                                        <td>{{ $compra->email }}</td>
                                        <td><a class="text-primary" href="{{ route ('products.show', $compra->product) }}">
                                                {{ $compra->product->name }} </a></td>
                                        <td><a class="text-success" href="{{ route ('profile', $compra->seller) }}">
                                                {{ $compra->seller->name}} </a></td>
                                        <td> <span class="text-danger"> {{ $compra->monto_reserva}} <i class="fa fa-euro"></i>
                                            </span> {{ $compra->have_reserva}}</td>
                                        <td> {{ date('d-m-Y', strtotime($compra->fecha_reserva)) }} </td>
                                        <td>
                                            {{-- <a class="btn btn-sm btn-primary" href="{{ route ('compras.show', $compra) }}"> Ver</a> --}}
                                            <a class="btn btn-sm btn-success" href="{{ route ('compras.create', $compra->product) }}"> Completar
                                                Venta</a>
                                            {!! Form::open(['route' =>['products.detenerReserva', $compra->product], 'method' =>
                                            'PUT', 'class' => ['inline'] ]) !!}
                                            <button class="btn btn-sm btn-danger"> Detener Reserva</button>
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
                <nav class="mt-15 pb-10">
                    <ul class="pagination justify-content-center">
                        {{ $compras->links() }}
                    </ul>
                </nav>
            </div>
        </div>
        <!-- /.box -->
    </div>
</div>

@endsection
