@extends('layouts.admin')
@section('styles')
<link href="{{ asset('adm/assets/vendor_components/Magnific-Popup-master/dist/magnific-popup.css') }}" rel="stylesheet">
@endsection
@section('content')
<!-- Popup CSS -->
<section class="content-header">
    <h1>
        Venta de Articulo <small> NO: {{ $product->code }}</small>
    </h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('admin')}}"><i class="fa fa-dashboard"></i> Admin</a></li>
        <li class="breadcrumb-item"><a href="{{ route('products.index')}}">Lista de Articulos</a></li>
        <li class="breadcrumb-item active">Venta de Articulo</li>
    </ol>
</section>
<br>
<div class="row">
    <div class="col-lg-12">
        <div class="box box-solid bg-dark">
            <div class="box-header with-border flex">
                <div class="ribbon text-right bg-warning">{{ $product->status }}</div>
                <h1> </h1>
                <h3 class="box-title">Articulo: {{ $product->name }}</h3>
                <a href="{{ route ('products.index')}}" class="btn btn-danger float-right">Listar todos los
                    Artículos</a>
            </div>
            <div class="box-body rounded flex">
                <div class="popup-gallery">
                    <div class="row">
                        @foreach ($product->images as $img)
                        <div class="col-2">
                            <a href="{{ asset($img->path) }}"><img src="{{ asset($img->path) }}" alt="" /></a></div>
                        @endforeach
                    </div>
                </div>
            </div>
            <!-- /.box-body -->
            @if ($product->status == 'PUBLICADO' || $product->status == 'RESERVADO')
            <div class="box-footer box-solid bg-dark rounded">
                <div class="row">
                    <div class="col-12 col-md-7">
                        <div class="row mt-5">
                            <div class="col-12 col-md-5 mt-5 pt-5"> {{-- seccion de reserva --}}
                                {{-- @if ($product->status == 'RESERVADO')
                                {!! Form::open(['route' =>['products.detenerReserva', $product], 'method' => 'PUT',
                                'class' => ['text-center'] ]) !!}
                                <button class="btn btn-sm btn-danger"> Detener Reserva</button>
                                {!! Form::close() !!}
                                @else
                                @if ($product->status != 'VENDIDO')
                                {!! Form::model($product,['route' => ['products.reservar', $product], 'method'=> 'PUT', 'class' =>
                                ['align-bottom']]) !!}

                                <div class="form-group">
                                    <label for="monto_reserva">Importe para Reserva <i class="fa fa-euro"></i></label>
                                    <input type="number" class="form-control" placeholder="Monto para Reservar" required="true"
                                        id="monto_reserva" value="{{number_format((float)$product->normal_price, 2, '.', '') / 2}}"
                                        name="monto_reserva">
                                </div>
                                <input name="have_reserva" value="GESTOR" type="radio" id="radio_{{$product->id}}" checked>
                                <label for="radio_{{$product->id}}">Gestor</label>

                                <input name="have_reserva" value="CLIENTE" type="radio" id="radio_{{$product->id}}101">
                                <label for="radio_{{$product->id}}101">Cliente</label>

                                {{ Form::submit('Reservar', ['class' => 'btn btn-success']) }}
                                {{ Form::reset('Limpiar', ['class' => 'btn btn-warning']) }}
                                {!! Form::close() !!}
                                @endif
                                @endif --}}
                            </div>
                            <div class="col-12 col-md-6 py-5  bg-warning rounded"> {{-- seccion de completar compra --}}
                                <div class="row text-center">
                                    <div class="col-md-12 title">
                                        <h4>Datos del Cliente Comprador</h4>
                                    </div>
                                </div>
                                @if ($product->compra) {{-- si tiene comprador registrado--}}
                                @if ($product->compra->monto_reserva)
                                <p> <span class="text-dark">Importe de Reserva : {{ $product->compra->monto_reserva }} <i
                                            class="fa fa-euro"></i></span> </p>
                                <p> <span class="text-dark">Fecha de Reserva :
                                        {{ date('d-m-Y / H:i', strtotime($product->compra->fecha_reserva)) }}
                                    </span> </p>
                                @endif
                                <p>Nombre Cliente : {{ $product->compra->nameClient }} </p>
                                <p>Email : {{ $product->compra->email }} </p>
                                <p>Telefono : {{ $product->compra->phone }}
                                    <a class="btn btn-success btn-xs mx-2 text-white hover-info"
                                        href="https://web.whatsapp.com/send?phone={{ $product->compra->phone }}" target="_blank">
                                        <i class="fa fa-whatsapp"></i> Mensaje</a>
                                </p>
                                <p>Ciudad : {{ $product->compra->city }} </p>
                                @include('admin.compras.soldForm')
                                @else
                                <div class="contact-form-wrapper">
                                    @include('admin.compras.CompraForm')
                                </div>
                                @endif
                            </div>
                        </div>
                    </div>
                    <!-- /.col -->
                    <hr>
                    <div class="col-md-5 col-12 text-right">
                        <p class="lead"><b>Articulo Registrado </b><span class="text-warning">
                                {{ date('d-m-Y / H:i', strtotime($product->created_at)) }} </span></p>
                        <div class="row">
                            <h4 class="col-6"> Estancia: <span class="text-success">{{ $product->category->name }}</span></h4>
                        </div>
                        <div class="total-payment">
                            @if ($product->reduced_price == null)
                            <h3><b>Precio :</b> {{ $product->normal_price}} <i class="fa fa-euro"></i></h3>
                            @else
                            <h3><b>Precio Reducido :</b> <span class="text-danger"><i class="fa fa-eur"></i>
                                    {{ $product->reduced_price}} </span></h3>
                            @endif
                        </div>

                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        @if ($product->status != 'VENDIDO')
                        <a href="{{ route ('products.edit', $product)}}" style="margin-right: 5px;" class="btn btn-info pull-right"> <i
                                class="fa fa-edit"></i>Modificar Artículo</a>
                        @endif
                    </div>
                </div>
            </div>
            @else
            <p class="text-uppercase text-center my-5">ESTE ARTICULO NO SE PUEDE VENDER, POR QUE SU ESTADO ES <span
                    class="text-warning">{{$product->status}}</span> </p>
            @endif
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script src="{{ asset('adm/assets/vendor_components/Magnific-Popup-master/dist/jquery.magnific-popup.min.js') }}">
</script>
<script src="{{ asset('adm/assets/vendor_components/Magnific-Popup-master/dist/jquery.magnific-popup-init.js') }}">
</script>
@endsection
