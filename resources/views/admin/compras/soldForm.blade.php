{!! Form::open(['route' => ['compras.store', $product]]) !!}
<div class="row">
    <div class="col-md-12">
        <div class="form-group text-center">
            <b>Precio Final de Venta :</b>
            @if ($product->reduced_price)
                @if ((number_format((float)$product->reduced_price, 2, '.', '') - $product->compra->monto_reserva) > 0)
                    <input id="pago" name="pago" type="number" min="0" step=".01" required class="form-control" placeholder="Importe" value="{{ number_format((float)$product->reduced_price, 2, '.', '') - $product->compra->monto_reserva}}">
                @else
                    <input id="pago" name="pago" type="number" min="0" step=".01" required class="form-control" placeholder="Importe" value="{{$product->compra->monto_reserva}}">
                @endif
            @else
                @if ((number_format((float)$product->normal_price, 2, '.', '') - $product->compra->monto_reserva) > 0)
                    <input id="pago" name="pago" type="number" min="0" step=".01" required class="form-control" placeholder="Importe" value="{{ number_format((float)$product->normal_price, 2, '.', '') - $product->compra->monto_reserva}}">
                @else
                    <input id="pago" name="pago" type="number" min="0" step=".01" required class="form-control" placeholder="Importe" value="{{$product->compra->monto_reserva}}">
                @endif
            @endif
            <i class="fa fa-euro"></i>
        </div>
    </div>
    <div class="col-md-12 text-center">
        <input name="have_pago" value="GESTOR" type="radio" id="radio1_{{$product->id}}" checked>
        <label for="radio1_{{$product->id}}">Gestor</label>

        <input name="have_pago" value="CLIENTE" type="radio" id="radio_{{$product->id}}_1">
        <label for="radio_{{$product->id}}_1">Cliente</label>
    </div>
    <div class="col-md-12 text-center">
        <input type="submit" class="btn btn-success" value="Marcar Articulo Vendido" />
    </div>
</div>
{!! Form::close() !!}
