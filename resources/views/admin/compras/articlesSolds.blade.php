@extends('layouts.admin')

@section('content')
<div class="row">
    <div class="col-md-12 col-lg-12">
        <div class="box box-solid bg-dark">
            <div class="box-header with-border">
                <h3 class="box-title">Ventas</h3>
                {{-- <a href="{{ route ('products.create')}}" class="btn btn-info float-right">Registrar</a> --}}
                <h6 class="box-subtitle"> Listado de Articulos Vendidos</h6>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#vendidos"
                            role="tab"><span><i class="fa  fa-file"></i></span> Articulos Vendidos </a> </li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content tabcontent-border">
                    <div class="tab-pane active" id="vendidos" role="tabpanel">
                        <div class="pad">
                            <table class="table table-striped">
                                <thead>
                                    <th>#</th>
                                    <th>Cliente</th>
                                    <th>telefono</th>
                                    <th>Email</th>
                                    <th>Producto</th>
                                    <th>Gestor</th>
                                    {{-- <th>Fecha Completado</th> --}}
                                    <th>Importe</th>
                                    <th></th>
                                </thead>
                                <tbody>
                                    @foreach($compras as $compra)
                                    <tr>
                                        <th> {{ $loop->iteration }}</th>
                                        {{-- <td> <a href="{{ route ('agreements.show', $compra) }}">
                                        {{ $compra->name }} </a> </td> --}}
                                        <td>
                                            @if ($compra->comprador)
                                            <a class="text-warning" href="{{ route ('profile', $compra->buyer) }}">
                                                {{ $compra->buyer->name }} {{ $compra->buyer->profile->last_name}}
                                            </a>
                                            @else
                                            {{ $compra->nameClient }}
                                            @endif
                                        </td>
                                        <td>
                                            <a href="https://web.whatsapp.com/send?phone={{ $compra->phone }}"
                                                class="btn btn-primary btn-sm" target="_blank">
                                                <i class="fa fa-whatsapp"></i>
                                                {{$compra->phone}}</a>
                                        </td>
                                        <td>{{ $compra->email }}</td>
                                        <td><a class="text-primary"
                                                href="{{ route ('products.show', $compra->product) }}">
                                                {{ $compra->product->name }} </a></td>
                                        <td><a class="text-success" href="{{ route ('profile', $compra->seller) }}">
                                                {{ $compra->seller->name}} </a></td>
                                        {{-- <td>{{ $compra->updated_at }}</td> --}}
                                        <td> <span class="text-danger">  {{ $compra->pago}} <i class="fa fa-euro"></i>
                                            </span></td>
                                        <td>
                                            <a class="btn btn-sm btn-primary"
                                                href="{{ route ('compras.show', $compra) }}"> Ver Venta</a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
                <nav class="mt-15 pb-10">
                    <ul class="pagination justify-content-center">
                        {{ $compras->links() }}
                    </ul>
                </nav>
            </div>
        </div>
        <!-- /.box -->
    </div>
</div>

@endsection
