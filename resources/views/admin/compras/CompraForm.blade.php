
{!! Form::open(['route' => ['compras.store', $product], 'onclick' => 'return confirm("Estas seguro de vender el artículo?")']) !!}
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label for="name">Nombre de comprador</label>
            @if (auth()->user())
            <input id="name" name="name" type="text" value="" required class="form-control"
                placeholder="Nombre">
            @else
            <input id="name" name="name" type="text" required class="form-control" placeholder="Nombre">
            @endif
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group">
            <label for="email">Correo del comprador</label>
            @if (auth()->user())
            <input id="email" name="email" type="email" value="" required
                class="form-control" placeholder="Correo">
            @else
            <input id="email" name="email" type="email" required class="form-control" placeholder="Correo">
            @endif
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group">
            <label for="phone">Teléfono </label>
            @if (auth()->user())
            <input id="phone" name="phone" type="phone" value="" required
                class="form-control" placeholder="Teléfono">
            @else
            <input id="phone" name="phone" type="phone" required class="form-control" placeholder="Teléfono">
            @endif
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group">
            <label for="city">Ciudad ó Codigo Postal</label>
            @if (auth()->user())
            <input id="city" name="city" type="text" value="" required
                class="form-control" placeholder="Ciudad ó Codigo Postal">
            @else
            <input id="city" name="city" type="text" required class="form-control" placeholder="Ciudad ó Codigo Postal">
            @endif
        </div>
    </div>
    <div class="col-md-12">
        <div class="form-group">
            <label for="comments">Consultas y comentarios</label>
            <textarea id="comments" name="comments" class="form-control" placeholder="Consultas y comentarios"
                rows="3">Articulo Vendido</textarea>
        </div>
    </div>
    <div class="col-md-12">
        <div class="form-group text-center">
            <b>Precio Final de Venta :</b>
            @if ($product->reduced_price)
            <input id="pago" name="pago" type="number" min="0" step=".01" required
                class="form-control" placeholder="Importe" value="{{ $product->reduced_price}}">
            @else
            <input id="pago" name="pago" type="number" min="0" step=".01" value="{{ $product->normal_price}}" required class="form-control">
            @endif
            <i class="fa fa-euro"></i>
        </div>
    </div>
    <div class="col-md-12 text-center">
        <input name="have_pago" value="GESTOR" type="radio" id="radio2_{{$product->id}}" checked>
        <label for="radio2_{{$product->id}}">Gestor</label>

        <input name="have_pago" value="CLIENTE" type="radio" id="radio01_{{$product->id}}_1">
        <label for="radio01_{{$product->id}}_1">Cliente</label>
    </div>

    <div class="col-md-12 text-center">
        <input type="submit" class="btn btn-success" value="Marcar Articulo Vendido" />
    </div>
</div>
{!! Form::close() !!}
