@extends('layouts.admin')
@section('styles')
<link href="{{ asset('adm/assets/vendor_components/Magnific-Popup-master/dist/magnific-popup.css') }}" rel="stylesheet">
@endsection

@section('content')
<!-- Popup CSS -->
<section class="content-header">
    @if ($agreement)
    <h1>
        Contrato N°
        @if ($agreement->name == 'estandar')
        E-000{{ $agreement->id }}
        @else
        P-000{{ $agreement->id }}
        @endif
    </h1>
    @endif

    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('admin')}}"><i class="fa fa-dashboard"></i> admin</a></li>
        <li class="breadcrumb-item"><a href="{{ route('compras.index')}}">venta</a></li>
        <li class="breadcrumb-item"><a>Detalle de Venta</a></li>
        {{-- <li class="breadcrumb-item active">Contrato: 000{{ $agreement->id}} </li> --}}
    </ol>
    <hr>
    @if ($agreement)
    <div class="col-lg-12">
        <div class="row no-space text-center">
            <div class="col-2">
                <div class="pull-up rounded bg-purple bg-banknote-white">
                    <a href="{{ route ('profile', $agreement->visit->client->user)}}">
                        <div class="">
                            <div class="p-15 text-center">
                                <div class="vertical-align-middle">
                                    <span class="font-size-20 text-white">{{$agreement->visit->client->user->name}}</span>
                                    <div class="font-size-10 text-white">Cliente</div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>

            <div class="col-2">
                <div class="weather-day vertical-align">
                    <div class="vertical-align-middle font-size-16">
                        <div class="mb-10">FECHA Y HORA</div>
                        <div class="text-info">{{date('d-m-Y H:i', strtotime($agreement->visit->date_visit)) }}
                            <br>
                            <span class="font-size-12">{{$agreement->visit->hour_visit}}</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-2">
                <div class="weather-day vertical-align">
                    <div class="vertical-align-middle font-size-16">
                        <div class="mb-10">CANTIDAD</div>
                        <i class="wi-day-cloudy font-size-24 mb-10"></i>
                        <div>
                            <span class="font-size-30 text-info">{{$agreement->visit->quantity_product}}</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-2">
                <div class="weather-day vertical-align">
                    <div class="vertical-align-middle font-size-16">
                        <div class="mb-10">CODIGO POSTAL</div>
                        <i class="wi-day-sunny font-size-24 mb-10"></i>
                        <div class="text-info">{{$agreement->visit->cp}}
                            <span class="font-size-12"></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-2">
                <div class="weather-day vertical-align">
                    <div class="vertical-align-middle font-size-16">
                        <div class="mb-10">DIRECCION</div>
                        <i class="wi-day-cloudy-gusts font-size-24 mb-10"></i>
                        <div>
                            <span class="font-size-12 text-info"> {{$agreement->visit->address}}</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-2">
                <div class="weather-day vertical-align">
                    <div class="vertical-align-middle font-size-16">
                        <div class="mb-10">TELEFONO</div>
                        <i class="wi-day-lightning font-size-24 mb-10"></i>
                        <div class="text-info">{{$agreement->visit->phone}}
                            <span class="font-size-12"></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif

</section>
<br>
<div class="row">
    <div class="col-lg-12">
        <div class="box box-solid bg-dark">
            <div class="box-header with-border flex">
                <div class="ribbon text-right bg-warning"> @if ($product->status == 'VENDIDO') Vendido @endif </div>
                <h1> </h1>
                <h3 class="box-title">Articulo: {{ $product->name }}</h3>
                <a href="{{ route ('reporte.vendidos')}}" class="btn btn-danger float-right">Listar Ventas</a>
            </div>
            <div class="box-body rounded flex">
                <div class="popup-gallery">
                    <div class="row">
                        @foreach ($product->images as $img)
                        <div class="col-2">
                            <a href="{{ asset($img->path) }}"><img src="{{ asset($img->path) }}" alt="" /></a></div>
                        @endforeach
                    </div>
                </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer box-solid bg-dark rounded">
                <div class="row">
                    <div class="col-md-4 col-12 text-right">
                        <p>Codigo de Artículo: <span class="text-warning"> {{$product->code}} </span> </p>
                        <p>Tipo de Contrato: <span class="text-warning"> @if ($product->client_agreement) {{ $product->client_agreement->name}}
                                @else Basico @endif </span> </p>
                        {{-- <p>Fecha de Publicación: <span class="text-warning"> {{ date('d-m-Y ', strtotime($product->compra->updated_at)) }} </span> --}}
                        </p>
                        <p class="lead"><b>Artículo Vendido: </b>
                            <span class="text-warning">{{$product->name}} </span> </p>
                        <div class="row">
                            <h4 class="col-6"> Estancia: <span class="text-success">{{ $product->category->name }}</span></h4>
                        </div>
                        <div class="total-payment">
                            @if ($compra->monto_reserva)
                            <p><b>Precio Reserva :</b> <span class="text-danger"><i class="fa fa-eur"></i> {{ $compra->monto_reserva}} </span> {{ $compra->have_reserva}}</p>
                            @endif
                            <p><b>Precio Venta :</b> <span class="text-danger"><i class="fa fa-eur"></i> {{ $compra->pago}} </span> {{ $compra->have_pago}}</p>
                            <h3><b>Precio Total :</b> <span class="text-danger"><i class="fa fa-eur"></i> {{ $compra->pago}} </span></h3>
                        </div>
                    </div>
                    <div class="col-md-4 col-12 py-5  bg-success rounded px-5">
                        <div class="row text-center">
                            <div class="col-md-12 title">
                                <h4>Datos Del Vendedor</h4>
                            </div>
                        </div>
                        {{-- <p> <span class="text-dark">Importe de Venta : {{ $compra->pago }} <i class="fa fa-euro"></i> </span> </p> --}}
                        <p>Nombre : {{ $compra->seller->name }} {{ $compra->seller->profile->last_name }} </p>
                        <p> DNI : {{ $compra->seller->profile->dni }} </p>
                        <p>Dirección : {{ $compra->seller->profile->address }} </p>
                        <p>Móvil : {{ $compra->seller->profile->phone_number }} </p>
                        <p>Correo : {{ $compra->seller->email }} </p>
                    </div>
                    <div class="col-md-4 col-12 py-5  bg-warning rounded px-5">
                        <div class="row text-center">
                            <div class="col-md-12 title">
                                <h4>Datos del Cliente Comprador</h4>
                            </div>
                        </div>

                        <p>Nombre Cliente : {{ $product->compra->nameClient }} </p>
                        <p>Ciudad : {{ $product->compra->city }} </p>
                        <p>Correo : {{ $product->compra->email }} </p>
                        <p>Móvil : {{ $product->compra->phone }} </p>
                    </div>
                    <div class="col-12 py-5 my-5">
                        <div class="text-center">
                            @if ($product->status == 'VENDIDO')
                            {{-- <a class="btn text-center btn-info pull-right"> <i class="fa fa-print"></i>Imprimir Factura</a> --}}
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script src="{{ asset('adm/assets/vendor_components/Magnific-Popup-master/dist/jquery.magnific-popup.min.js') }}">
</script>
<script src="{{ asset('adm/assets/vendor_components/Magnific-Popup-master/dist/jquery.magnific-popup-init.js') }}">
</script>
@endsection
