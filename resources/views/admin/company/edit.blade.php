@extends('layouts.admin')

@section('content')
<div class="row">
    <div class="col-12">
        <div class="box box-solid bg-dark">
            <div class="box-header with-border flex">
                <h3 class="box-title">Editar Datos</h3>
                <a href="{{ route ('company.show')}}" class="btn btn-danger float-right">Cancelar</a>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                {!! Form::model($company,['route' => ['company.update', $company], 'method'=> 'PUT', 'files' => true]) !!}
                    @include('admin.company.companyForm')
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection
