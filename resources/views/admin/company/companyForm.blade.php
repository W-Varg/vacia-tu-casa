<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {{ Form::label('company_name', 'Nombre de la empresa') }}
            {{ Form::text('company_name',null, [ 'class' => 'form-control', 'id' => 'company_name' ]) }}
        </div>
        <div class="form-group">
            {{ Form::label('email', 'Correo electronico') }}
            {{ Form::email('email',null, [ 'class' => 'form-control', 'id' => 'email' ]) }}
        </div>
        <div class="form-group">
            {{ Form::label('phone_number', 'Telefono') }}
            {{ Form::text('phone_number',null, [ 'class' => 'form-control', 'id' => 'phone_number' ]) }}
        </div>
        <div class="form-group">
            {{ Form::label('address', 'Direccion Tienda') }}
            {{ Form::text('address',null, [ 'class' => 'form-control', 'id' => 'address' ]) }}
        </div>
        <div class="form-group">
            {{ Form::label('code_postal', 'Codigo Postal') }}
            {{ Form::text('code_postal',null, [ 'class' => 'form-control', 'id' => 'code_postal' ]) }}
        </div>
        <div class="form-group">
            {{ Form::label('facebook', 'Facebook') }}
            {{ Form::text('facebook',null, [ 'class' => 'form-control', 'id' => 'facebook' ]) }}
        </div>
        <div class="form-group">
            {{ Form::label('instagram', 'Instagram') }}
            {{ Form::text('instagram',null, [ 'class' => 'form-control', 'id' => 'instagram' ]) }}
        </div>
        <div class="form-group">
            {{ Form::label('twitter', 'Twitter') }}
            {{ Form::text('twitter',null, [ 'class' => 'form-control', 'id' => 'twitter' ]) }}
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group">
            {{ Form::label('pinterest', 'Pinterest') }}
            {{ Form::text('pinterest',null, [ 'class' => 'form-control', 'id' => 'pinterest' ]) }}
        </div>
        <div class="form-group">
            {{ Form::label('texto1', 'Texto Banner 1') }}
            {{ Form::text('texto1',null, [ 'class' => 'form-control', 'id' => 'texto1' ]) }}
        </div>
        <div class="form-group">
            {{ Form::label('texto2', 'Texto Banner 2') }}
            {{ Form::text('texto2',null, [ 'class' => 'form-control', 'id' => 'texto2' ]) }}
        </div>
        <div class="form-group">
            {{ Form::label('texto3', 'Texto Banner 3') }}
            {{ Form::text('texto3',null, [ 'class' => 'form-control', 'id' => 'texto3' ]) }}
        </div>
        <div class="form-group">
            <label for="img1">Banner 1 (1920 x 1100)</label>
            <input type="file" name="img1" class="form-control">
        </div>
        <div class="form-group">
            <label for="img2">Banner 2 (1920 x 1100)</label>
            <input type="file" name="img2" class="form-control">
        </div>
        <div class="form-group">
            <label for="img3">Banner 3 (1920 x 1100)</label>
            <input type="file" name="img3" class="form-control">
        </div>
        <div class="form-group">
            <label for="img4">Banner 4 (1920 x 1100)</label>
            <input type="file" name="img4" class="form-control">
        </div>
    </div>
    <div class="col-md-12 text-center">
        <div class="form-group">
            {{ Form::submit('Guardar', ['class' => 'btn btn-sm btn-primary']) }}
        </div>
    </div>
</div>
