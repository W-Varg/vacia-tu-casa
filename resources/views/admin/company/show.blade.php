@extends('layouts.admin')

@section('content')
<div class="row">
    <div class="col-12">
        <div class="box box-solid bg-dark">
            <div class="box-header with-border flex">
                <h3 class="box-title">Datos de la Empresa</h3>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="">

                            <strong class="h5 upper-case">Nombre de la Empresa</strong>
                            <p class="form-control">
                                {{ $company->company_name }}
                            </p>
                        </div>
                        <div class="">

                            <strong class="h5 upper-case">Email</strong>
                            <p class="form-control"> {{ $company->email }} </p>
                        </div>
                        <div class="">

                            <strong class="h5 upper-case">TELEFONO</strong>
                            <p class="form-control"> {{ $company->phone_number }} </p>
                        </div>
                        <div class="">

                            <strong class="h5 upper-case">Codigo Postal</strong>
                            <p class="form-control"> {{ $company->code_postal }} </p>
                        </div>
                        <div class="">

                            <strong class="h5 upper-case">Facebook</strong>
                            <p class="form-control"> {{ $company->facebook }} </p>
                        </div>
                        <div class="">

                            <strong class="h5 upper-case">Instagram</strong>
                            <p class="form-control"> {{ $company->instagram }} </p>
                        </div>
                        <div class="">

                            <strong class="h5 upper-case">Twitter</strong>
                            <p class="form-control"> {{ $company->twitter }} </p>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="">

                            <strong class="h5 upper-case">Pinterest</strong>
                            <p class="form-control"> {{ $company->pinterest }} </p>
                        </div>
                        <div class="">

                            <strong class="h5 upper-case">Direccion Tienda</strong>
                            <p class="form-control"> {{ $company->address }} </p>
                        </div>
                        <div class="">

                            <strong class="h5 upper-case">Titulo 1</strong>
                            <p class="form-control"> {{ $company->texto1 }} </p>
                        </div>
                        <div class="">

                            <strong class="h5 upper-case">Titulo 2</strong>
                            <p class="form-control"> {{ $company->texto2 }} </p>
                        </div>
                        <div class="">

                            <strong class="h5 upper-case">Titulo 3</strong>
                            <p class="form-control"> {{ $company->texto3 }} </p>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <p><strong>Banner 1</strong> {{ $company->img1 }} </p>
                        <p><strong>Banner 2</strong> {{ $company->img2 }} </p>
                        <p><strong>Banner 3</strong> {{ $company->img3 }} </p>
                        <p><strong>Banner 4</strong> {{ $company->img4 }} </p>
                    </div>
                </div>
            </div>
            <div class="box-footer text-center">
                <a href="{{ route ('company.edit', $company)}}" class="btn btn-info">Editar Datos</a>
            </div>
        </div>
    </div>
</div>
@endsection
