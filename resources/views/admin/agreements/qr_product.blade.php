@extends('layouts.admin')

@section('content')
<div class="box box-solid bg-dark">
    <div class="box-header with-border">
        <h3 class="box-title">Codigo QR Articulo</h3>
        {{-- <h6 class="box-subtitle"> Listado de Productos</h6> --}}
        <a href="{{ URL::previous() }}" class="btn btn-danger float-right">Volver Atras</a>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
            <div class="title m-b-md">
                {!!QrCode::size(300)->generate($product->name) !!}
            </div>
                
    </div>
    <div class="box-footer">
        vista previa del articulo
    </div>
    
</div>
@endsection
