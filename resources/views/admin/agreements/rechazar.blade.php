@extends('layouts.admin')
@section('styles')
<link href="{{ asset('adm/assets/vendor_components/Magnific-Popup-master/dist/magnific-popup.css') }}" rel="stylesheet">
@endsection
@section('content')
<div class="box box-solid bg-dark">
    <div class="box-header with-border">
        <h3 class="box-title">Rechazar Contrato </h3>

        <a href="{{ URL::previous()}}" class="btn btn-danger float-right">Cancelar</a>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        {!! Form::open(['route' =>['visits.destroy', $visit], 'method'=>'delete','class' => ['inline'],
        'onclick' => 'return confirm("Estas seguro de Rechazar?")' ]) !!}

        <div class="row">
            {{ Form::hidden('user_id', auth()->user()->id) }}

            <div class="form-group col-md-12">
                {{ Form::label('razon', 'Razon') }}
                {{ Form::textarea('razon', '<strong>Lamentamos no poder ayudarte, pero no recibimos demanda de este estilo de mobiliario.</strong>', [ 'class' => 'form-control', 'id' => 'razon','required'=>'true' ]) }}
            </div>

            <div class="form-group col-md-12 text-center">
                {{ Form::submit('Completar Rechazo', ['class' => 'btn btn-success']) }}
            </div>
        </div>
        {!! Form::close() !!}
    </div>
</div>
@endsection
@section('scripts')
<script src="{{ asset('vendor/ckeditor/ckeditor.js') }}"></script>
<script>
    CKEDITOR.config.height = 200;
    CKEDITOR.config.width = 'auto';
    CKEDITOR.config.language = 'es';
    CKEDITOR.replace('razon');
</script>
@endsection
