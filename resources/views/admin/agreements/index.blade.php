@extends('layouts.admin')

@section('content')
<div class="row">
    <div class="col-md-12 col-lg-12">
        <div class="box box-solid bg-dark">
            <div class="box-header with-border">
                <h3 class="box-title">Contratos</h3>

                {!! Form::open(['route' => 'agreements.index', 'method' => 'GET', 'class'=>['inline']]) !!}
                <div class="form-group">
                    <input class="form-control" placeholder="Buscar por Código o nombre de cliente" value="{{$name}}" name="name" />
                </div>
                {!! Form::close() !!}
                <p class="box-subtitle"> Listado de Contratos, cantidad de registros {{ $estandarts->count() + $premiums->count()}}</p>
            </div>

            <!-- /.box-header -->
            <div class="box-body">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#premium" role="tab"><span><i
                                    class="fa  fa-file-powerpoint-o"></i></span> Premium </a> </li>
                    <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#standart" role="tab"><span><i
                                    class="fa  fa-file"></i></span> Estandar </a> </li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content tabcontent-border">
                    <div class="tab-pane" id="standart" role="tabpanel">
                        <div class="pad">
                            <table class="table table-striped">
                                <thead>
                                    <th>#</th>
                                    <th>Codigo Contrato</th>
                                    <th>Fecha Final</th>
                                    <th>Gestor</th>
                                    <th>Cliente</th>
                                    <th>Estado</th>
                                    <th>Artículos</th>
                                    <th>Acciones</th>
                                </thead>
                                <tbody>
                                    @foreach($estandarts as $agreement)
                                    <tr>
                                        <th> {{ $loop->iteration }}</th>
                                        <td> <a href="{{ route ('agreements.show', $agreement) }}"> E000{{ $agreement->id }} </a> </td>
                                        <th>
                                            @if ($agreement->status === "publicado")
                                            <span class=""> {{ date('d-m-Y', strtotime($agreement->created_at->addDays(112))) }} </span>
                                            @endif
                                        </th>
                                        <td>
                                            @if ($agreement->handler)
                                            {{ $agreement->handler->user->name }}
                                            @endif
                                        </td>
                                        <td> {{ $agreement->client->user->name }} </td>
                                        <td>
                                            @if ($agreement->status === "pendiente")
                                            <span class="text-red"> {{ $agreement->status}} </span>
                                            @endif
                                            @if ($agreement->status === "publicado")
                                            <span class="text-success"> {{ $agreement->status}} </span>
                                            @endif
                                            @if ($agreement->status === "enviado")
                                            <span class="text-warning"> {{ $agreement->status}} </span>
                                            @endif
                                            @if ($agreement->status === "concluido")
                                            <span class="text-yellow"> {{ $agreement->status}} </span>
                                            @endif
                                            @if ($agreement->status === "cerrado" || $agreement->status === "aprobado")
                                            <span class=""> {{ $agreement->status}} </span>
                                            @endif
                                        </td>
                                        <td>
                                            {{$agreement->products->count()}}
                                        </td>

                                        <td>
                                            <a class="btn btn-sm btn-primary" href="{{ route ('agreements.show', $agreement) }}"> Ver</a>
                                            @if ($agreement->status == 'enviado' || $agreement->status == 'aprobado' || $agreement->status ==
                                            'rechazado' || $agreement->status == 'pendiente')
                                            {!! Form::open(['route' =>['agreements.destroy', $agreement], 'method' => 'delete', 'class' =>
                                            ['inline'],'onclick' => 'return confirm("Estas seguro de eliminar?")' ]) !!}
                                            <button type="submit" class="btn btn-sm btn-danger"> Eliminar</button>
                                            {!! Form::close() !!}
                                            @endif

                                            @if ($agreement->status === 'publicado')
                                            {!! Form::open(['route' =>['agreements.change', $agreement], 'method' => 'PUT', 'class' =>
                                            ['inline'], 'onclick' => 'return confirm("Estas seguro de Cambiar de contrato?")']) !!}
                                            <button type="submit" class="btn btn-sm btn-secondary"> Pasar A Premium</button>
                                            {!! Form::close() !!}
                                            @endif


                                            @if ($agreement->status === 'publicado')
                                            {!! Form::open(['route' =>['agreements.concluirContrato', $agreement], 'method' => 'PUT', 'class' =>
                                            ['inline'], 'onclick' => 'return confirm("Estas seguro de Concluir el contrato?")' ]) !!}
                                            <button type="submit" class="btn btn-sm btn-warning"> Concluir contrato</button>
                                            {!! Form::close() !!}
                                            @endif

                                            @if (auth()->user()->role->id == \App\Role::ADMIN && ( $agreement->status != 'cerrado' &&
                                            $agreement->status != 'concluido' && $agreement->status != 'pendiente' ) )

                                            <a class="btn btn-sm btn-info" href="{{ route ('agreements.edit', $agreement) }}">
                                                Cambiar de Gestor
                                            </a>
                                            @endif

                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <div class="box-footer">
                                <nav class="mt-15 pb-10">
                                    <ul class="pagination justify-content-center">
                                        {{ $estandarts->appends($pagelinks)->links() }}
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane active" id="premium" role="tabpanel">
                        <div class="pad">
                            <table class="table table-striped">
                                <thead>
                                    <th>#</th>
                                    <th>Codigo Contrato</th>
                                    <th>Fecha Final</th>
                                    <th>Gestor</th>
                                    <th>Cliente</th>
                                    <th>Estado</th>
                                    <th>Artículos</th>
                                    <th>Acciones</th>
                                </thead>
                                <tbody>
                                    @foreach($premiums as $agreement)
                                    <tr>
                                        <th> {{ $loop->iteration }}</th>
                                        <td> <a href="{{ route ('agreements.show', $agreement) }}"> P000{{ $agreement->id }} </a> </td>
                                        <th>
                                            @if ($agreement->status === "publicado")
                                            <span class=""> {{ date('d-m-Y', strtotime($agreement->created_at->addDays(112))) }} </span>
                                            @endif
                                        </th>
                                        <td>
                                            @if ($agreement->handler)
                                            {{ $agreement->handler->user->name }}
                                            @endif
                                        </td>
                                        <td> {{ $agreement->client->user->name }} </td>
                                        <td>
                                            @if ($agreement->status === "pendiente")
                                            <span class="text-danger"> {{ $agreement->status}} </span>
                                            @endif
                                            @if ($agreement->status === "publicado")
                                            <span class="text-success"> {{ $agreement->status}} </span>
                                            @endif
                                            @if ($agreement->status === "enviado")
                                            <span class="text-warning"> {{ $agreement->status}} </span>
                                            @endif
                                            @if ($agreement->status === "concluido")
                                            <span class="text-yellow"> {{ $agreement->status}} </span>
                                            @endif
                                            @if ($agreement->status === "cerrado" || $agreement->status === "aprobado")
                                            <span class=""> {{ $agreement->status}} </span>
                                            @endif
                                        </td>
                                        <td>
                                            {{$agreement->products->count()}}
                                        </td>
                                        <td>
                                            <a class="btn btn-sm btn-primary" href="{{ route ('agreements.show', $agreement) }}"> Ver</a>
                                            @if ($agreement->status == 'enviado' || $agreement->status == 'aprobado' || $agreement->status ==
                                            'rechazado' || $agreement->status == 'pendiente')
                                            {!! Form::open(['route' =>['agreements.destroy', $agreement], 'method' => 'delete', 'class' =>
                                            ['inline'],'onclick' => 'return confirm("Estas seguro de eliminar?")' ]) !!}
                                            <button type="submit" class="btn btn-sm btn-danger"> Eliminar</button>
                                            {!! Form::close() !!}
                                            @endif

                                            {{-- @if ($agreement->status === 'publicado')
                                            {!! Form::open(['route' =>['agreements.change', $agreement], 'method' =>
                                            'PUT', 'class' => ['inline'], 'onclick' => 'return confirm("Estas seguro de Cambiar de contrato?")'
                                            ]) !!}
                                            <button type="submit" class="btn btn-sm btn-secondary"> Pasar a estandar</button>
                                            {!! Form::close() !!}
                                            @endif --}}

                                            @if ($agreement->status === 'publicado')
                                            {!! Form::open(['route' =>['agreements.concluirContrato', $agreement], 'method' =>
                                            'PUT', 'class' => ['inline'],'onclick' => 'return confirm("Estas seguro de concluir el contrato?")'
                                            ]) !!}
                                            <button type="submit" class="btn btn-sm btn-warning"> Concluir contrato</button>
                                            {!! Form::close() !!}
                                            @endif

                                            @if (auth()->user()->role->id == \App\Role::ADMIN && ( $agreement->status != 'cerrado' &&
                                            $agreement->status != 'concluido' && $agreement->status != 'pendiente' ) )
                                            <a class="btn btn-sm btn-info" href="{{ route ('agreements.edit', $agreement) }}">
                                                Cambiar de Gestor
                                            </a>
                                            @endif
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <div class="box-footer">
                                <nav class="mt-15 pb-10">
                                    <ul class="pagination justify-content-center">
                                        {{ $premiums->appends($pagelinks)->links() }}
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
</div>

@endsection
