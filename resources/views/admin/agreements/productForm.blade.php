<style scoped>
    img {
        height: 150px;
        margin-top: 0.5em;
        margin-left: 0.5em;
        margin-bottom: 0.5em;
        margin-right: 0.5em;
    }
</style>
<div class="row">
    {{ Form::hidden('user_id', auth()->user()->id) }}
    {{ Form::hidden('code', rand()) }}

    <div class="form-group col-md-8">
        {{ Form::label('name', 'Nombre del Artículo') }}
        {{ Form::text('name', null, [ 'class' => 'form-control', 'id' => 'name','maxlength' => 26 ]) }}
    </div>
    <div class="form-group col-md-4">
        {{ Form::label('category_id', 'Estancia') }}
        {{ Form::select('category_id',['' => 'Selecciona tu estancia'] + $categories->all(), null, [ 'class' => 'form-control', 'id' => 'category_id', 'required'=>'true' ]) }}
    </div>

    <div class="form-group col-md-4">
        {{ Form::label('quantity', 'Cantidad') }}
        {{ Form::number('quantity', 1, [ 'class' => 'form-control', 'id' => 'quantity' ]) }}
    </div>

    <div class="form-group col-md-4">
        {{ Form::label('normal_price', 'Precio') }}
        {{ Form::text('normal_price', null, [ 'class' => 'form-control', 'id' => 'normal_price' ]) }}
    </div>
    <div class="form-group col-md-4">
        <label>Imagenes</label>
        <input type="file" name="images[]" id="images" class="form-control" multiple="multiple" accept="image/*" />
    </div>

    <div class="col-md-12">
        <div class="gallery"></div>
    </div>
    <div class="form-group col-md-12">
        {{ Form::label('description', 'Descripción') }}
        {{ Form::textarea('description', null, [ 'class' => 'form-control', 'id' => 'description' ]) }}
    </div>

    <div class="form-group col-md-3">
        {{ Form::label('weight', 'Peso (kg)') }}
        {{ Form::text('weight', null, [ 'class' => 'form-control', 'id' => 'weight' ]) }}
    </div>
    <div class="form-group col-md-3">
        {{ Form::label('long', 'Largo (cm)') }}
        {{ Form::text('long', null, [ 'class' => 'form-control', 'id' => 'long' ]) }}
    </div>
    <div class="form-group col-md-3">
        {{ Form::label('width', 'Ancho (cm)') }}
        {{ Form::text('width', null, [ 'class' => 'form-control', 'id' => 'width' ]) }}
    </div>
    <div class="form-group col-md-3">
        {{ Form::label('height', 'Alto (cm)') }}
        {{ Form::text('height', null, [ 'class' => 'form-control', 'id' => 'height' ]) }}
    </div>
    {{-- this is my template --}}
    <div class="form-group col-md-6">
        <h3 class="">Estado</h3>
        <div class="radio inline">
            {{ Form::radio('status','PUBLICADO',null,['id'=>'10']) }}
            <label for="10">Publicado</label>
        </div>
        <div class="radio inline">
            {{ Form::radio('status','PENDIENTE','active',['id'=>'20']) }}
            <label for="20">Pendiente</label>
        </div>
        <div class="radio inline">
            {{ Form::radio('status','RECHAZADO',null,['id'=>'30']) }}
            <label for="30">Rechazado</label>
        </div>
        <div class="radio inline">
            {{ Form::radio('status','RESERVADO',null,['id'=>'40']) }}
            <label for="40">Reservado</label>
        </div>
    </div>
    <div class="form-group col-md-6 ">
        <h3 class="">Tipo de Articulo</h3>
        <div class="radio inline">
            {{ Form::radio('type','NORMAL','active',['id'=>'100']) }}
            <label for="100">Normal</label>
        </div>
        <div class="radio inline">
            {{ Form::radio('type','EXCLUSIVE',null,['id'=>'200']) }}
            <label for="200">Exclusivo</label>
        </div>
    </div>
    <div class="form-group col-md-12">
        <h3 class="col-12">Estilos <span>(opcional)</span> </h3>
        {{ Form::text('style', null, [ 'class' => 'form-control', 'id' => 'style' , 'placeholder'=> 'Rústico, retro, moderno, vintage, etc' ]) }}
    </div>

    <div class="form-group col-md-12 text-center">
        {{ Form::submit('Guardar', ['class' => 'btn btn-dark']) }}
        {{ Form::reset('Limpiar', ['class' => 'btn btn-warning']) }}
    </div>
</div>
@section('scripts')
<script src="{{ asset('vendor/ckeditor/ckeditor.js') }}"></script>
<script>
    CKEDITOR.config.height = 200;
    CKEDITOR.config.width = 'auto';
    CKEDITOR.config.language = 'es';
    CKEDITOR.replace('description');

    $(function() {
    // Multiple images preview in browser
    var imagesPreview = function(input, placeToInsertImagePreview) {

        if (input.files) {
            var filesAmount = input.files.length;

            for (i = 0; i < filesAmount && i<5; i++) {
                var reader = new FileReader();

                reader.onload = function(event) {
                    $($.parseHTML('<img>')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
                }

                reader.readAsDataURL(input.files[i]);
            }
        }

    };

    $('#images').on('change', function() {
        imagesPreview(this, 'div.gallery');
    });
});

</script>
@endsection
