@extends('layouts.admin')
@section('styles')
<link href="{{ asset('adm/assets/vendor_components/Magnific-Popup-master/dist/magnific-popup.css') }}" rel="stylesheet">
@endsection

@section('content')
<!-- Popup CSS -->
<section class="content-header">
    <h1>
        Contrato N°
        @if ($agreement->name == 'estandar')
        E-000{{ $agreement->id }}
        @else
        P-000{{ $agreement->id }}
        @endif
        @if ($agreement->status == 'cerrado')
        <span class="text-danger"> Cerrado
        </span>
        @endif
    </h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('admin')}}"><i class="fa fa-dashboard"></i> admin</a></li>
        <li class="breadcrumb-item"><a href="{{ route('reporte.contratos_close')}}">contratos concluidos</a></li>
        <li class="breadcrumb-item active">Contrato: 000{{ $agreement->id}} </li>
    </ol>
    <hr>
    <div class="col-lg-12">
        <div class="row no-space text-center">
            @if ($agreement->visit)
            <div class="col-2">
                <div class="pull-up rounded bg-purple bg-banknote-white">
                    <a href="{{ route ('profile', $agreement->visit->client->user)}}">
                        <div class="">
                            <div class="p-15 text-center">
                                <div class="vertical-align-middle">
                                    <span class="font-size-20 text-white">{{$agreement->visit->client->user->name}}</span>
                                    <div class="font-size-10 text-white">Cliente</div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>

            <div class="col-2">
                <div class="weather-day vertical-align">
                    <div class="vertical-align-middle font-size-16">
                        <div class="mb-10">CODIGO POSTAL</div>
                        <i class="wi-day-sunny font-size-24 mb-10"></i>
                        <div class="text-info">{{$agreement->visit->cp}}
                            <span class="font-size-12"></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-2">
                <div class="weather-day vertical-align">
                    <div class="vertical-align-middle font-size-16">
                        <div class="mb-10">DIRECCION</div>
                        <i class="wi-day-cloudy-gusts font-size-24 mb-10"></i>
                        <div>
                            <span class="font-size-12 text-info"> {{$agreement->visit->address}}</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-2">
                <div class="weather-day vertical-align">
                    <div class="vertical-align-middle font-size-16">
                        <div class="mb-10">TELEFONO</div>
                        <i class="wi-day-lightning font-size-24 mb-10"></i>
                        <div class="text-info">{{$agreement->visit->phone}}
                            <span class="font-size-12"></span>
                        </div>
                    </div>
                </div>
            </div>
            @endif

            @if ($agreement->status === 'publicado')
            <div class="col-2">
                <div class="weather-day vertical-align">
                    <div class="vertical-align-middle font-size-16">
                        {!! Form::open(['route' =>['agreements.concluirContrato', $agreement], 'method' =>
                        'PUT', 'class' => ['float-right inline'] ]) !!}
                        <button class="btn btn-sm btn-warning"> Concluir contrato</button>
                        {!! Form::close() !!} </div>
                </div>
            </div>
            @endif
        </div>
    </div>
    <div class="box-body">
        <div class="table-responsive">
            <h2>ARTÍCULOS NO VENDIDOS</h2>
            <table id="example" class="data-table table table-bordered table-hover display nowrap margin-top-10 w-p100">
                <thead>
                    <tr>
                        <th>Codigo</th>
                        <th>Nombre</th>
                        <th>Estado</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($no_vendidos as $product)
                    <tr>
                        <td>{{$product->code}}</td>
                        <td>{{$product->name}}</td>
                        <td>{{$product->status}}
                            @if ($product->status == 'RESERVADO')
                            <a class="mx-1 float-right btn btn-sm btn-success" href="{{ route ('compras.create', $product) }}"> Completar
                                Venta</a>
                            {!! Form::open(['route' =>['products.detenerReserva', $product], 'method' => 'PUT',
                            'class' => ['float-right inline'], 'onclick' => 'return confirm("Estas seguro de Concluir el artículo?")' ]) !!}
                            <button class="btn btn-sm btn-danger"> Concluir</button>
                            {!! Form::close() !!}
                            @else
                            {{-- {!! Form::open(['route' =>['contrato.products.destroy', $product], 'method' =>'delete',
                            'class'=>['float-right inline'], 'onclick' => 'return confirm("Estas seguro de eliminar COMPLETAMENTE el artículo?")' ]) !!}
                                <button type="submit" class="mx-1 btn btn-sm btn-danger"> <i class="fa fa-close"></i> Remover de Contrato</button>
                            {!! Form::close() !!} --}}
                            @endif
                            @if ($product->status == 'PUBLICADO')
                            {!! Form::open(['route' =>['products.concluirArticulo', $product], 'method' => 'PUT',
                            'class' => ['float-right inline'], 'onclick' => 'return confirm("Estas seguro de Concluir el artículo?")' ]) !!}
                            <button class="btn btn-sm btn-warning"> Concluir</button>
                            {!! Form::close() !!}
                            @endif
                            @if ($product->status == 'CONCLUIDO')
                            {!! Form::open(['route' =>['products.vender.concluido', $product], 'method' => 'PUT',
                            'class' => ['float-right inline'],'onclick' => 'return confirm("Estas seguro de vender este artículo?")' ]) !!}
                            <button type="submit" class="btn btn-sm btn-success"> Vender Artículo</button>
                            {!! Form::close() !!}
                            @endif

                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <h2>ARTÍCULOS VENDIDOS</h2>
        <div class="table-responsive">
            <table id="example" class="data-table table table-bordered table-hover display nowrap margin-top-10 w-p100">
                <thead>
                    <tr>
                        <th>Codigo</th>
                        <th>Nombre</th>
                        <th>Estado</th>
                        <th>Importe resevado</th>
                        <th>Importe Venta</th>
                        <th>Fecha Venta</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($vendidos as $product)
                    <tr>
                        <td>{{$product->code}}</td>
                        <td>{{$product->name}}</td>
                        <td>{{$product->status}}</td>
                        <td>{{$product->compra->monto_reserva}} <i class="fa fa-euro"></i>
                            @if ($product->compra->monto_reserva)
                            @if ($product->compra->have_reserva == 'CLIENTE')
                            <span class="text-warning"> {{$product->compra->have_reserva}} </span>
                            @else
                            <span class="text-success">
                                {{-- {{$product->compra->have_reserva}} --}}
                                H2h
                            </span>
                            @endif
                            @endif
                        </td>
                        <td>{{$product->compra->pago}}
                            @if ($product->compra->pago) <i class="fa fa-euro"></i>
                            @if ($product->compra->have_pago == 'CLIENTE')
                            <span class="text-warning"> {{$product->compra->have_pago}} </span>
                            @else
                            <span class="text-success">
                                 {{-- {{$product->compra->have_pago}} --}}
                                H2h
                             </span>
                            @endif
                            @endif
                        </td>
                        <td>{{$product->compra->fecha_venta}} {{$product->compra->id}}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <br>
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <h5> Total importe cobrado por cliente <span class="text-warning"> {{$total_cliente}} <i class="fa fa-euro"></i> </span> </h5>
                <h5> Total importe cobrado por hometohome <span class="text-success"> {{$total_gestor}} <i class="fa fa-euro"></i> </span> </h5>
                <br>
                <h4>TOTAL VENTAS : <span class="text-red"> {{$total}} <i class="fa fa-euro"></i> </span> </h4>
            </div>
            <hr>
            @if ($agreement->name === 'estandar')
            <div class="col-md-12 col-sm-12 text-right">
                {{-- tarea 83, revisar el total de retorno 0048 --}}
                <h4> DIVISION DE <span class="text-red"> {{$total}} <i class="fa fa-euro"></i> </span> EN PORCENTAJES </h4>
                <h5> (25%) para hometohome : <span class="text-primary"> {{$total*0.25}} <i class="fa fa-euro"></i> </span> </h5>
                {{-- <h5> (10%) para gestor : <span class="text-success"> {{$total*0.10}} <i class="fa fa-euro"></i> </span> </h5> --}}
                <h5> (75%) para cliente : <span class="text-primary"> {{$total*0.75}} <i class="fa fa-euro"></i> </span> </h5>
            </div>
            @else
            <div class="col-md-12 col-sm-12 text-right">
                <h4> DIVISION DE <span class="text-red"> {{$total}} <i class="fa fa-euro"></i> </span> EN PORCENTAJES </h4>
                <h5> (40%) para hometohome : <span class="text-primary"> {{$total*0.4}} <i class="fa fa-euro"></i> </span> </h5>
                {{-- <h5> (10%) para gestor : <span class="text-success"> {{$total*0.1}} <i class="fa fa-euro"></i> </span> </h5> --}}
                <h5> (60%) para cliente : <span class="text-primary"> {{$total*0.6}} <i class="fa fa-euro"></i> </span> </h5>
            </div>
            @endif
            <div class="col-md-12 text-right">
                @if (auth()->user()->role->id == \App\Role::ADMIN || auth()->user()->id == $agreement->handler->user->id )
                @if ($agreement->status != 'cerrado')
                @if ($agreement->name === 'estandar')
                {!! Form::open(['route' =>['utilidad.store', $agreement], 'method' => 'post', 'class' => ['inline'], 'onclick' => 'return confirm("Estas seguro de cerrar el contrato?")' ]) !!}
                @else
                {!! Form::open(['route' =>['utilidad.store', $agreement], 'method' => 'post', 'class' => ['inline'], 'onclick' => 'return confirm("Los articulos no vendidos se independizarán del contrato \npara ser vendidos por separado, \nEstas seguro de cerrar el contrato?")' ]) !!}
                @endif
                <div class="form-group">
                    <div class="controls">
                        <input type="checkbox" name="cobrado_por_todas_partes" id="checkbox_1">
                        <label for="checkbox_1">Contrato revisado</label>
                        <br>
                        @if($errors->has('cobrado_por_todas_partes'))
                        <span class="text-red help-block">{{ $errors->first('cobrado_por_todas_partes') }}</span>
                        @endif
                    </div>
                </div>
                @if ($agreement->name === 'estandar')
                <input type="hidden" name="entero" value="{{ ($total*0.75) - $total_cliente}}">
                @else
                <input type="hidden" name="entero" value="{{ ($total*0.6) - $total_cliente}}">
                @endif
                <input type="submit" class="btn btn-sm btn-success" value="Guardar Datos y Cerrar Contrato">
                {!! Form::close() !!}
                @endif
                @if ($agreement->status == 'cerrado' && $agreement->utilidad )
                <br>
                <a class="btn btn-sm btn-info" href="{{ route ('reporte.contrato_gestor') }}"> Ver Contratos y gestores </a>
                @endif
                @endif
            </div>
        </div>
        <hr>
        {{-- <h3 class="text-center">SALDOS A FAVOR Y EN CONTRA</h3>
        <div class="row">
            @if ($agreement->name === 'estandar')
            <div class="col-md-12 col-sm-12">
                <div class="text-left">
                    <h5> Importe cobrado por cliente <span class="text-warning"> {{$total_cliente}} <i class="fa fa-euro"></i> </span>
                    </h5>
                    <h5>
                        @if ($total_cliente >= ($total*0.75))
                        saldo en contra <span class="text-warning"> {{$total_cliente - ($total*0.75)}} <i class="fa fa-euro"></i> </span>
                        @else
                        saldo a favor<span class="text-warning"> {{ ($total*0.75) - $total_cliente}} <i class="fa fa-euro"></i> </span>
                        @endif
                    </h5>
                </div>
                <div class="text-right">
                    <h5> Importe cobrado por hometohome <span class="text-success"> {{$total_gestor}} <i class="fa fa-euro"></i> </span> </h5>
                    </span>
                    </h5>
                    <h5>
                        @if ($total_gestor >= ($total*0.1))
                        saldo en contra <span class="text-success"> {{$total_gestor - ($total*0.1)}} <i class="fa fa-euro"></i> </span>
                        @else
                        saldo a favor<span class="text-success"> {{ ($total*0.1)- $total_gestor}} <i class="fa fa-euro"></i> </span>
                        @endif
                    </h5>
                    <br>
                    <h4>Importe Final para HOMETOHOME : <span class="text-red"> {{($total*0.25) - $total*0.1}} <i class="fa fa-euro"></i>
                        </span> </h4>
                </div>
            </div>
            @else
            <div class="col-md-12 col-sm-12">
                <div class="text-left">
                    <h5> Importe cobrado por cliente <span class="text-warning"> {{$total_cliente}} <i class="fa fa-euro"></i> </span></h5>
                    <h5>
                        @if ($total_cliente >= ($total*0.6))
                        saldo en contra <span class="text-warning"> {{$total_cliente - ($total*0.6)}} <i class="fa fa-euro"></i> </span>
                        @else
                        saldo a favor<span class="text-warning"> {{ ($total*0.6) - $total_cliente}} <i class="fa fa-euro"></i> </span>
                        @endif
                    </h5>
                </div>
                <div class="text-right">
                    <h5> Importe cobrado por Gestor <span class="text-success"> {{$total_gestor}} <i class="fa fa-euro"></i> </span> </h5>
                    </span>
                    </h5>
                    <h5>
                        @if ($total_gestor >= ($total*0.1))
                        saldo en contra <span class="text-success"> {{$total_gestor - ($total*0.1)}} <i class="fa fa-euro"></i> </span>
                        @else
                        saldo a favor<span class="text-success"> {{ ($total*0.1)- $total_gestor}} <i class="fa fa-euro"></i> </span>
                        @endif
                    </h5>
                    <br>
                    <h4>Importe Final para HOMETOHOME : <span class="text-red"> {{($total*0.4) - $total*0.1}} <i class="fa fa-euro"></i>
                        </span> </h4>
                </div>
            </div>
            @endif
        </div> --}}
    </div>
</section>
@endsection
