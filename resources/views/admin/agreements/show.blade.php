@extends('layouts.admin')
@section('styles')
<link href="{{ asset('adm/assets/vendor_components/Magnific-Popup-master/dist/magnific-popup.css') }}" rel="stylesheet">
@endsection

@section('content')
<!-- Popup CSS -->
<section class="content-header">
    <h1>
        Contrato N°
        @if ($agreement->name == 'estandar')
        E-000{{ $agreement->id }}
        @else
        P-000{{ $agreement->id }}
        @endif
        Solicitado por : <a href="{{ route ('profile', $agreement->client->user)}}">
            {{$agreement->client->user->name}}</a>
    </h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('admin')}}"><i class="fa fa-dashboard"></i> admin</a></li>
        @if ($agreement->visit)
        <li class="breadcrumb-item"><a href="{{ route('visits.cotizar',$agreement->visit )}}">Contizacion</a></li>
        <li class="breadcrumb-item"><a href="{{ route('visits.show', $agreement->visit)}}">Detalle</a></li>
        @endif
        <li class="breadcrumb-item active">Contrato: 000{{ $agreement->id}} </li>
    </ol>
    <hr>
    <div class="col-lg-12">
        <div class="row no-space text-center">
            <div class="col-2">
                <div class="pull-up rounded bg-purple bg-banknote-white">
                    <a href="{{ route ('profile', $agreement->client->user)}}">
                        <div class="">
                            <div class="p-15 text-center">
                                <div class="vertical-align-middle">
                                    <span class="font-size-20 text-white">{{$agreement->client->user->name}}</span>
                                    <div class="font-size-10 text-white">Cliente</div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>

            @if ($agreement->visit)
            <div class="col-2">
                <div class="weather-day vertical-align">
                    <div class="vertical-align-middle font-size-16">
                        <div class="mb-10">FECHA Y HORA</div>
                        <div class="text-info">{{ date('d-m-Y H:i', strtotime($agreement->visit->date_visit)) }}
                            <br>
                            <span class="font-size-12">{{$agreement->visit->hour_visit}}</span>
                        </div>
                    </div>
                </div>
            </div>
            @endif

            <div class="col-2">
                <div class="weather-day vertical-align">
                    <div class="vertical-align-middle font-size-16">
                        <div class="mb-10">CANTIDAD</div>
                        <i class="wi-day-cloudy font-size-24 mb-10"></i>
                        <div>
                            <span class="font-size-30 text-info">{{$agreement->products->count()}}</span>
                        </div>
                    </div>
                </div>
            </div>
            @if ($agreement->visit)
            <div class="col-2">
                <div class="weather-day vertical-align">
                    <div class="vertical-align-middle font-size-16">
                        <div class="mb-10">CODIGO POSTAL</div>
                        <i class="wi-day-sunny font-size-24 mb-10"></i>
                        <div class="text-info">{{$agreement->visit->cp}}
                            <span class="font-size-12"></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-2">
                <div class="weather-day vertical-align">
                    <div class="vertical-align-middle font-size-16">
                        <div class="mb-10">DIRECCION</div>
                        <i class="wi-day-cloudy-gusts font-size-24 mb-10"></i>
                        <div>
                            <span class="font-size-12 text-info"> {{$agreement->visit->address}}</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-2">
                <div class="weather-day vertical-align">
                    <div class="vertical-align-middle font-size-16">
                        <div class="mb-10">TELEFONO</div>
                        <i class="wi-day-lightning font-size-24 mb-10"></i>
                        <div class="text-info">{{$agreement->visit->phone}}
                            <span class="font-size-12"></span>
                        </div>
                    </div>
                </div>
            </div>
            @endif
        </div>
    </div>

</section>
<br>
<div class="row">
    <div class="col-lg-12">
        <div class="box box-solid bg-dark">
            <div class="box-header with-border flex text-center">
                <label class="mr-5">Estado de Contrato : <span class="text-warning">{{$agreement->status}}</span></label>
                @if ($agreement->status =='aprobado')
                <div class="col-12 my-2">
                    <a href="{{ route ('agreements.export', $agreement)}}" class="btn btn-sm btn-info">Descargar Excel
                    </a>
                    <a href="{{ route ('agreements.resendContratoToUser', $agreement)}}" class="btn btn-sm btn-success">Volver a Enviar Contrato
                        a Email</a>
                </div>
                {!! Form::open(['route' => ['agreements.import', $agreement], 'files' =>true, 'class'=>['bg-light
                rounded text-dark py-3']]) !!}
                <input type="file" name="excel" accept=".xlsx" required="true" />
                <button type="submit" class="btn btn-sm btn-warning"> Cargar Doc Excel</button>
                {!! Form::close() !!}
                @else
                @if ($agreement->visit && $agreement->visit->handler && $agreement->products->count() > 0)
                    @if ($agreement->status == 'publicado')
                        <label><span class="text-warning">Los Articulos de este Contrato se Encuentran publicados en el
                                sitio</span></label>
                        {!! Form::open(['route' =>['agreements.concluirContrato', $agreement], 'method' =>
                        'PUT', 'class' => ['float-right inline'] ]) !!}
                        <button class="btn btn-sm btn-warning"> Concluir contrato</button>
                        {!! Form::close() !!}
                    @else
                        @if ($agreement->status === 'concluido'||$agreement->status === 'cerrado' )
                            <a class="btn btn-sm btn-primary" href="{{ route ('agreements.detail', $agreement) }}"> Ver Contrato</a>
                        @else
                            <a href="{{ route ('agreements.sendAgrementEmail', $agreement)}}" class="btn btn-success">Enviar Contrato</a>
                        @endif
                    @endif
                @else
                    @if (!$agreement->handler_id)
                        <p>EL CONTRATO NO TIENE GESTOR ASIGNADO Ó EL CONTRATO AUN NO ESTA COTIZADO</p>
                    @endif
                    @if ($agreement->visit)
                        <p>
                            <a class="btn btn-sm btn-info" href="{{ route ('visits.show', $agreement->visit)}}"><i class="fa fa-edit"></i> Ver Detalle de
                                Visita</a>
                        </p>
                        @endif
                @endif
                @endif
                @if ($agreement->products->count() > 0 && $agreement->status == 'publicado')
                <a href="{{ route ('agreements.print_qr', $agreement)}}" class="btn btn-sm btn-info">Imprimir Codigos QR</a>
                @endif
            </div>
            <div class="box-body rounded flex">
                <table class="table table-striped">
                    <tbody>
                        @foreach($agreement->products as $product)
                        <tr>
                            <td>{{$product->id}}</td>
                            <td class="text-center min-with-50">
                                <a href="{{ route ('products.show', $product) }}">
                                    @if ($product->img)
                                    <img class="rounded" src="{{ asset($product->img) }}" alt="" height="50" />
                                    @else
                                    <img src="{{ asset('assets/images/product-1.png') }}" alt="" height="50" />
                                    @endif
                                </a>
                            </td>
                            <td> <a href="{{ route ('products.show', $product) }}"> {{ $product->name }} </a> </td>
                            <td> {{ $product->category->name }} </td>
                            <td class="text-warning min-with-100"> {{ $product->code }} </td>
                            <td>
                                @if ($product->reduced_price)
                                {{ $product->normal_price}}
                                @else
                                {{ $product->normal_price}}
                                @endif
                                <i class="fa fa-euro"></i>
                            </td>
                            <td>
                                @if ($agreement->status <> 'concluido' && $agreement->status <> 'cerrado')
                                    @if ($product->status == 'VENDIDO')
                                        @if ($product->compra)
                                        <span class="text-success">VENDIDO  A :</span> {{$product->compra->pago}} <i class="fa fa-euro"></i>
                                        <br />
                                        Fecha: {{ date('d-m-Y', strtotime($product->compra->updated_at)) }}
                                        @endif
                                    @else
                                        {!! Form::open(['route' =>['products.more_images', $product], 'files' => true,'class'=>['inline']]) !!}
                                        <label class="fileContainer">
                                            Click, Subir Fotos!
                                            <input type="file" id="images" name="images[]" multiple="multiple" accept='image/*' required="true"
                                                class="" />
                                        </label>
                                        <br>
                                        <button type="submit" class="btn btn-sm btn-warning"><i class="fa  fa-qrcode"></i> Adjuntar</button>
                                        {!! Form::close() !!}
                                    @endif
                                @endif
                            </td>
                            <td>
                                <div class="py-3">
                                    {{ $product->images->count()}}
                                </div>
                            </td>
                            @if ($agreement->status <> 'cerrado' && $agreement->status <> 'concluido' && $product->status <> 'VENDIDO')
                                    <td>
                                        <a class="btn btn-sm btn-default ma-2" href="{{ route ('products.edit', $product) }}">
                                            <i class="fa  fa-pencil"></i> Editar</a>
                                        <br>
                                        {!! Form::open(['route' =>['contrato.products.destroy', $product], 'method' =>'delete',
                                         'class'=>['inline'], 'onclick' => 'return confirm("Estas seguro de eliminar el artículo?")' ]) !!}
                                        <button type="submit" class="btn btn-sm btn-danger"> <i class="fa fa-close"></i> Eliminar
                                            Articulo</button>
                                        {!! Form::close() !!}
                                    </td>
                                    @endif
                        </tr>
                        @endforeach
                        <tr>
                            <td colspan="3"></td>
                            <td>TOTAL:</td>
                            <td> {{ $total}} <i class="fa fa-euro"></i></td>
                            <td colspan="3"></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="box-footer box-solid bg-dark text-center rounded">
                <div class="row">
                    <div class="col-12 text-center">
                        @if ($agreement->status == 'publicado')
                        <label><span class="text-warning">Los Articulos de este Contrato se Encuentran publicados en el
                                sitio</span></label>
                        @else
                        @if ($agreement->status == 'aprobado')
                        <a href="{{ route ('agreements.publicar', $agreement) }}" class="btn btn-sm btn-success pull-up"> Publicar Articulos de
                            Contrato</a>
                        @endif
                        @endif
                        @if ($agreement->status == 'enviado' || $agreement->status == 'aprobado' || $agreement->status == 'rechazado' ||
                        $agreement->status == 'pendiente')
                        {{ Form::open(['route' =>['agreements.destroy', $agreement], 'method' => 'delete', 'class' =>
                            ['inline'],'onclick' => 'return confirm("Estas seguro de eliminar el contrato?")' ]) }}
                        <button type="submit" class="btn btn-sm btn-danger"> Eliminar Completamente el contrato</button>
                        {{ Form::close() }}
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style>
    table {
        /* min-width: ; */
        display: block;
        overflow-x: auto;
        white-space: nowrap;
    }

    .cursor {
        cursor: copy;
    }

    .fileContainer {
        padding: 5px;
        border-radius: 4px;
        background: rgba(226, 125, 30, 0.4);
        font-size: 1rem;
        overflow: hidden;
        position: relative;
        vertical-align: center;
    }

    .fileContainer:hover {
        background: rgba(230, 117, 11, 0.795);
    }

    .fileContainer [type=file] {
        display: block;
        font-size: 999px;
        filter: alpha(opacity=0);
        min-height: 100%;
        min-width: 100%;
        opacity: 0;
        position: absolute;
        right: 0;
        text-align: right;
        top: 0;
    }
</style>
@endsection
