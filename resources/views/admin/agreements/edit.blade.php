@extends('layouts.admin')

@section('content')
<div class="row">
    <div class="col-12">
        <div class="box box-solid bg-dark">
            <div class="box-header with-border flex">
                <a href="{{ route ('agreements.index')}}" class="btn btn-info float-left"> Listar Contratos</a>
                <h3 class="mx-2 box-title"> Cambiar de Gestor Al Contrato</h3>
                <a href="{{ route ('agreements.index')}}" class="btn btn-danger float-right">Cancelar</a>
            </div>
            <div class="box-body">
                <div class="row">
                    @if ($agreement->handler)
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-6 text-right">Gestor Actual</div>
                            <div class="col-6 text-left text-success">
                                <span class=""> {{ $agreement->handler->user->name }} </span>
                            </div>
                        </div>
                    </div>
                    @endif

                    @if ($agreement->client)
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-6 text-right">Cliente Actual</div>
                            <div class="col-6 text-left text-success">
                                <span class=""> {{ $agreement->client->user->name }} </span>
                            </div>
                        </div>
                    </div>
                    @endif

                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-6 text-right">Estado de Contrato</div>
                            <div class="col-6 text-left text-red"> <strong>{{ $agreement->status }} </strong> </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-6 text-right">Tipo de Contrato</div>
                            <div class="col-6 text-left text-success"><strong>{{ $agreement->name }} </strong>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-6 text-right">Creado </div>
                            <div class="col-6 text-left text-info"><strong>
                                    <span class=""> {{ date('d-m-Y', strtotime($agreement->created_at)) }} </span>
                                </strong>
                            </div>
                        </div>
                    </div>
                </div>
                <br>
                {!! Form::model($agreement,['route' => ['agreements.update', $agreement->id], 'method'=> 'PUT', 'files' =>true]) !!}
                <div class="form-group">
                    <label>Seleccione el nuevo Gestor del contrato</label>
                    <select class="form-control select2 w-p100" name="handler_id" id="handler_id">
                        @foreach ($handlers as $hand)
                        <option selected="selected" value="{{ $hand->id }}"> {{ $hand->user->name }} </option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    {{ Form::submit('Guardar Cambios', ['class' => 'btn btn-sm btn-primary']) }}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection
