@extends('layouts.admin')
@section('styles')
<link rel="stylesheet" media="all" href="{{ asset('css/furniture-icons.css') }}" />
<style type="text/css">
    table {
        border-spacing: 0px;
        border-collapse: separate;
        page-break-inside: auto
    }

    tr {
        page-break-inside: avoid;
        page-break-after: auto
    }

    @media print {
        thead {
            display: table-header-group;
        }

        tfoot {
            display: table-footer-group;
        }
    }

    @media screen {
        thead {
            display: block;
        }

        tfoot {
            display: block;
        }
    }

    @print {
        .ocultar {
            width: 0px;
            visibility: hidden;
            display: none;
        }

        table,
        th,
        td {
            border: 0px
        }

        button {
            display: none;
        }

        span {
            display: none;
        }

        .page-break {
            page-break-after: always;
        }


        thead {
            display: table-header-group;
        }

        tfoot {
            display: table-footer-group;
        }

        table {
            page-break-inside: auto
        }

        tr {
            page-break-inside: avoid;
            page-break-after: auto
        }
    }

    .v_align {
        font-size: 1.3em;
        font-weight: bold;
        padding-left: auto;
        padding-right: auto;
    }

    .table td,
    .table th {
        padding: 0;
        vertical-align: middle;
        border-top: 1px solid #dee2e6;
    }

    .table-bordered,
    .table-bordered>tbody>tr>td,
    .table-bordered>tbody>tr>th,
    .table-bordered>tfoot>tr>td,
    .table-bordered>tfoot>tr>th,
    .table-bordered>thead>tr>td,
    .table-bordered>thead>tr>th {
        border: 0px;
    }

    /* .table-bordered,
    .table-bordered>tbody>tr>td,
    .table-bordered>tbody>tr>th,
    .table-bordered>tfoot>tr>td,
    .table-bordered>tfoot>tr>th,
    .table-bordered>thead>tr>td,
    .table-bordered>thead>tr>th {
        border: 0 !important;
    } */

    svg:not(:root) {
        overflow: hidden;
        /* margin: 5px; */
        /* padding: 5px; */
    }
</style>
@endsection
@section('content')
<div class="row">
    <div class="col-12">
        <div class="box box-solid bg-dark">
            <div class="box-header with-border flex">
                <h3 class="box-title">Codigos QR de contrato</h3>
                <a href="{{ route('agreements.show', $agreement)}}" class="btn btn-info float-right">Detalle de Contrato</a>
                @if ($products->count() > 0 && $agreement->status == 'publicado')
                <button class="btn btn-warning mx-2 float-right" type="button" onclick="Imprimir();">
                    Imprimir Todos
                </button>
                @endif
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="table-responsive">
                    <table id="table" class="data-table table table-bordered table-hover display nowrap margin-top-10 w-p100 page-break"
                        style="width: 100%;">
                        <tbody id="code_qr">
                            @foreach ($products as $key => $item)
                            <tr class="v_align ac" id="id_{{$item->id}}">
                                <td class="v_align pl-1 bold">{{$item->name}}</td>
                                <td class="bold item-code">{{$item->code}}</td>
                                <td>
                                    <div class="ma-2">
                                        {!! QrCode::size(105)->generate(route('page.detail_product',$item)) !!}
                                    </div>
                                </td>
                                <td class="ocultar text-center">
                                    <span>{{$item->status}} </span>
                                    @if ($item->status == 'PUBLICADO' || $item->status == 'RESERVADO')
                                    <button class="btn btn-info" type="button" onclick="onePrint({{$item->id}})"> Imprimir</button>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>
</div>
<script>
    function Imprimir(){

        // // Getting the table
        // var tble = document.getElementById('table');

        // // Getting the rows in table.
        // var row = tble.rows;

        // // Removing the column at index(1).
        // var i = 3;
        // for (var j = 0; j < row.length; j++) {

        //     // Deleting the ith cell of each row.
        //     row[j].deleteCell(i);
        // }

        // .table td, .table th { padding-top: 50%; vertical-align: middle; border-top: 1px solid #dee2e6; }
        printJS({
            printable: 'code_qr',
            type: 'html',
            style: `.data-table{ width:100%;} .pl-1{ padding-left: 1rem; }
                .bold{ font-size: 1.5em; font-weight: bold; padding-left: 2em;}
                .ocultar{ display: none;width:0px;}
                .item-code{ padding-left:1em;}
                .ma-2{ margin: 10px;}
                thead   {display: table-header-group;   }
                tfoot   {display: table-footer-group;   }
                table { page-break-inside:auto }
                tr { page-break-inside:avoid; page-break-after:auto }
                table, th, td {
                    border: 0px
                }
                button {
                    display: none;
                }
                span {
                    display: none;
                }
                `
         })
    }
    function onePrint(id){
        printJS({
            printable: 'id_'+id ,
            type: 'html',
            style: `.ac {width:100%;} .pl-1{ padding-left: 1rem; }
            .bold { font-size: 1.7em; font-weight: bold; vertical-align: middle; padding-left: 2em; }
            .ma-2{ margin: 10px;}
            .ocultar { display: none; width:0px;} `
        });
    }
</script>
@endsection
