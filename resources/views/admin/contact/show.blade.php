@extends('layouts.admin')

@section('content')
<div class="row">
    <div class="col-12">
        <div class="box box-solid bg-dark">
            <div class="box-header with-border flex">
                <h3 class="box-title">Contacto</h3>
                <a href="{{ route ('contactos.index')}}" class="btn btn-danger float-right">Listar Contactos</a>
            </div>
            <div class="box-body">
                <p><strong>Nombre : </strong> {{ $contacto->name }} </p>
                <p><strong>Correo Electronico : </strong> <span class="text-warning">{{$contacto->email}}</span>
                    <a class="btn btn-primary btn-xs mx-2 text-white hover-info" href="mailto: {{ $contacto->email }}" target="_blank"><i
                            class="fa fa-envelope"></i>
                        responder</a>

                </p>
                <p><strong>Telefono : </strong>
                    <a class="btn btn-success btn-xs mx-2 text-white hover-info"
                        href="https://web.whatsapp.com/send?phone={{ $contacto->phone_number }}" target="_blank"><i class="fa fa-whatsapp"></i>
                        Mensaje a {{$contacto->phone_number}}</a>
                </p>
                <p><strong>Ciudad:</strong> {{ $contacto->city }} </p>
                <hr>
                <p><strong>Asunto de Correo</strong> <br> {{ $contacto->subject }} </p>
                <p><strong>Mensaje</strong> </p>
                <p>{{ $contacto->description }}</p>
            </div>
        </div>
    </div>
</div>
@endsection
