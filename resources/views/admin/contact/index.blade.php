@extends('layouts.admin')
@section('content')
<div class="row">
    <div class="col-12">
        <div class="box box-solid bg-dark">
            <div class="box-header with-border flex">
                <h3 class="box-title">Lista de Contactos Interesados </h3>
            </div>
            <div class="box-body">
                <div class="table-responsive">
                    <table id="example" class="data-table table table-bordered table-hover display nowrap margin-top-10 w-p100">
                        <thead>
                            <tr>
                                <th>Nombre</th>
                                <th>Email</th>
                                <th>Telefono</th>
                                <th>Fecha</th>
                                <th>Accion</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($contactos as $contacto)
                            <tr>
                                <td>{{$contacto->name}}</td>
                                <td>
                                    <a class="btn btn-primary btn-xs mx-2 text-white hover-info" href="mailto: {{ $contacto->email }}"
                                        target="_blank"><i class="fa fa-envelope"></i>
                                        {{$contacto->email}}</a>
                                </td>
                                <td>
                                    <a class="btn btn-success btn-xs mx-2 text-white hover-info"
                                        href="https://web.whatsapp.com/send?phone={{ $contacto->phone_number }}" target="_blank"><i
                                            class="fa fa-whatsapp"></i>
                                        {{$contacto->phone_number}}</a>
                                </td>
                                <td>{{ date('d-m-Y', strtotime($contacto->created_at)) }}</td>
                                <td class="text-center">
                                    <a class="btn btn-sm my-2 btn-default" href="{{ route ('contactos.show', $contacto) }}"> Ver</a>
                                    {!! Form::open(['route' =>['contactos.destroy', $contacto], 'method' => 'delete', 'class' =>
                                    ['inline'],'onclick' => 'return confirm("Estas seguro de eliminar el contacto?")' ])
                                    !!}
                                    {!! Form::token() !!}
                                    <button class="btn btn-sm btn-danger"> Eliminar </button>
                                    {!! Form::close() !!}
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>No</th>
                                <th>Nombre</th>
                                <th>Correo</th>
                                <th>Telfono</th>
                                <th>Accion</th>
                            </tr>
                        </tfoot>
                    </table>
                    <nav class="mt-15 pb-10">
                        <ul class="pagination justify-content-center">
                            {{ $contactos->links() }}
                        </ul>
                    </nav>
                </div>
            </div>

        </div>
    </div>
</div>
@endsection
