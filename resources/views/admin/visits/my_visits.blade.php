@extends('layouts.admin')

@section('content')
@section('styles')
<link rel="stylesheet" href="{{ asset('adm/assets/vendor_components/select2/dist/css/select2.min.css') }}">
@endsection
<div class="row">
    <div class="col-md-12 col-lg-12">
        <div class="box">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs nav-tabs-light-mode nav-justified" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" href="#my_visits" role="tab">Mis visitas</a>
                </li>
            </ul>

            <!-- Tab panes -->
            <div class="box-body tab-content">
                {{-- en caso de que sea um promotot --}}
                <div class="tab-pane fade active show" id="my_visits">
                    <div class="box-body">
                        <div class="table-responsive">
                            <table id="employeelist" class="table table-hover no-wrap" data-page-size="10">
                                <thead>
                                    <tr>
                                        <th>N</th>
                                        <th>Usuario</th>
                                        <th>Contrato</th>
                                        <th>Fecha de Visita</th>
                                        <th>horario</th>
                                        <th>Codigo Postal</th>
                                        <th>Direccion</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($visits as $visit)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>
                                            @if ($visit->client->user->profile)
                                            <a href="{{ route('profile', $visit->client->user)}}"><img
                                                    src="{{ asset($visit->client->user->profile->avatar) }}" alt="user"
                                                    class="avatar avatar-sm mr-5" />
                                                {{ $visit->client->user->name}}</a>
                                            @else

                                            <a href="{{ route('profile', $visit->client->user)}}"><img
                                                    src="{{ asset('assets/images/user-2.jpg') }}" alt="user" class="avatar avatar-sm mr-5" />
                                                {{ $visit->client->user->name}}</a>
                                            @endif
                                        </td>
                                        @if ($visit->client_agreement->name == 'premium')
                                        <td> {{$visit->client_agreement->name}} - P000{{ $visit->client_agreement->id }} </td>
                                        @else
                                        <td> {{$visit->client_agreement->name}} - E000{{ $visit->client_agreement->id }} </td>
                                        @endif

                                        <td><a href="{{ route ('visits.show', $visit)}}">
                                                @if ($visit->date_visit == date('Y-m-d'))
                                                <span class="label label-danger">{{ date('d-m-Y', strtotime($visit->date_visit)) }}</span>
                                                @elseif($visit->date_visit < date('Y-m-d')) <span class="label label-success">
                                                    {{ date('d-m-Y', strtotime($visit->date_visit)) }}</span>
                                                    @else
                                                    {{ date('d-m-Y', strtotime($visit->date_visit)) }}
                                                    @endif
                                            </a>
                                        </td>
                                        <td><a href="{{ route ('visits.show', $visit)}}">{{ $visit->hour_visit}}</a></td>
                                        <td><a href="{{ route ('visits.show', $visit)}}"><span class="label label-warning">{{ $visit->cp}}</span>
                                            </a></td>
                                        <td><a href="{{ route ('visits.show', $visit)}}"> {{ $visit->address}}</a></td>
                                        <td>
                                            <a class="btn btn-xs btn-info" href="{{ route ('visits.show', $visit)}}"><i class="fa fa-edit"></i>
                                                Detalle</a>
                                            <a class="btn btn-xs btn-success"
                                                href="https://web.whatsapp.com/send?phone={{ $visit->client->user->profile->phone_number }}"
                                                target="_blank"><i class="fa fa-whatsapp"></i></a>
                                            <button type="button" onclick="asignar({{$visit->id}});" id="btn-assign"
                                                class="btn btn-xs btn-warning" data-toggle="modal" data-target="#reprogrmar">Reprogramar
                                                Visita</button>
                                            @if ($visit->date_visit < date('Y-m-d'))
                                                @if ($visit->client_agreement->status == 'pendiente')
                                                    {!! Form::open(['route' =>['visits.destroy', $visit], 'method' =>'delete', 'class' =>['inline'], 'onclick' => 'return confirm("Estas seguro de eliminar?")' ]) !!}
                                                    <button class="btn btn-sm btn-danger-outline" data-toggle="tooltip" data-original-title="Eliminar"><i class="ti-trash"></i></button>
                                                    {!! Form::close() !!}
                                                @else
                                                    {!! Form::open(['route' =>['visits.destroy', $visit], 'method' => 'delete', 'class' => ['inline'], 'onclick' => 'return confirm("Estas seguro de concluir la visita?")' ]) !!}
                                                    <button class="btn btn-sm btn-danger-outline" data-toggle="tooltip" data-original-title="Concluir Visita"><i class="ti-trash"></i></button>
                                                    {!! Form::close() !!}
                                                @endif
                                            @endif
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td colspan="7">
                                            <div class="text-right">
                                                <ul class="pagination"> </ul>
                                            </div>
                                        </td>
                                    </tr>
                                </tfoot>
                            </table>
                            <div id="reprogrmar" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2"
                                aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title" id="myModalLabel2">Reprogramar Visita</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                        </div>
                                        {!! Form::open(['route' => 'visits.reprograming']) !!}
                                        <div class="modal-body">
                                            <div class="col-md-12 m-b-20">
                                                <div class="form-group">
                                                    {{ Form::label('date_visit', 'Nueva Fecha de visita') }}
                                                    {{ Form::date('date_visit', null, [ 'class' => 'form-control', 'id' => 'date_visit', 'required'=>'true' ]) }}
                                                </div>
                                                <div class="form-group">
                                                    {{ Form::label('hour_visit', 'Hora de visita') }}
                                                    {{ Form::time('hour_visit', null, [ 'class' => 'form-control', 'id' => 'hour_visit' ,'title'=>"Ejemplo: \n 11:05 AM", 'data-toggle'=> "tooltip", 'data-placement' => "left",
                                                                        'required'=>'true']) }}
                                                </div>
                                            </div>
                                            <input type="hidden" name="id_visit" id="id_visit">
                                        </div>
                                        <div class="modal-footer">
                                            {{-- <button type="submit" class="btn btn-warning" data-dismiss="modal">Asignar</button> --}}
                                            {{ Form::submit('Asignar', ['class' => 'btn btn-warning']) }}
                                            <button type="button" class="btn btn-default float-right" data-dismiss="modal">Cancelar</button>
                                        </div>
                                        {!! Form::close() !!}
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <nav class="mt-15 pb-10">
                            <ul class="pagination justify-content-center">
                                {{ $visits->links() }}
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@section('scripts')
<script src="{{ asset('adm/assets/vendor_components/select2/dist/js/select2.full.js') }}"></script>
<script>
    function asignar(id) {
        $('#id_visit').val(id);
    }
</script>
@endsection
@endsection
