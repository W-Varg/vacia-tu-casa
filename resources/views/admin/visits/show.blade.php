@extends('layouts.admin')

@section('styles')
<link rel="stylesheet" href="{{ asset('adm/assets/vendor_components/select2/dist/css/select2.min.css') }}">
<!-- gallery -->
<link rel="stylesheet" href="{{ asset('adm/assets/vendor_components/gallery/css/animated-masonry-gallery.css') }}" />
<!-- fancybox -->
<link rel="stylesheet" href="{{ asset('adm/assets/vendor_components/lightbox-master/dist/ekko-lightbox.css') }}" />
@endsection

@section('breadcrumb')

<section class="content-header">
    <h1>
        Visita N° {{ $visit->id}} Solicitado por : <a href="{{ route ('profile', $visit->client->user)}}">
            {{$visit->client->user->name}}</a>
    </h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('admin')}}"><i class="fa fa-dashboard"></i> admin</a></li>
        <li class="breadcrumb-item"><a href="{{ route('visits.index')}}">Visitas</a></li>
        <li class="breadcrumb-item active">Detalle {{ $visit->id}} </li>
    </ol>
</section>
@endsection

@section('content')
<style>
.btn-absolute-show-visit{
    position: absolute;
    left: 0px;
    top: 0px;
}
</style>
<div class="row">
    <div class="col-lg-12">
        <!-- Default box -->
        <div class="box">
            <!-- /.box-body -->
            <div class="box-body">
                <div class="row align-items-center">
                    <div class="col-lg-12">
                        <div class="row no-space text-center">
                            <div class="col-2">
                                <div class="pull-up rounded bg-purple bg-banknote-white">
                                    <a href="{{ route ('profile', $visit->client->user)}}">
                                        <div class="">
                                            <div class="p-15 text-center">
                                                <div class="vertical-align-middle">
                                                    <span
                                                        class="font-size-20 text-white">{{$visit->client->user->name}}</span>
                                                    <div class="font-size-10 text-white">Cliente</div>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>

                            <div class="col-2">
                                <div class="weather-day vertical-align">
                                    <div class="vertical-align-middle font-size-16">
                                        <div class="mb-10">FECHA Y HORA</div>
                                        <div class="text-info">{{date('d-m-Y H:i', strtotime($visit->date_visit)) }}
                                            <br>
                                            <span class="font-size-12">{{$visit->hour_visit}}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-1">
                                <div class="weather-day vertical-align">
                                    <div class="vertical-align-middle font-size-16">
                                        <div class="mb-10">CANTIDAD</div>
                                        <i class="wi-day-cloudy font-size-24 mb-10"></i>
                                        <div>
                                            <span class="font-size-30 text-info">{{$visit->quantity_product}}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-1">
                                <div class="weather-day vertical-align">
                                    <div class="vertical-align-middle font-size-16">
                                        <div class="mb-10">CODIGO POSTAL</div>
                                        <i class="wi-day-sunny font-size-24 mb-10"></i>
                                        <div class="text-info">{{$visit->cp}}
                                            <span class="font-size-12"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-2">
                                <div class="weather-day vertical-align">
                                    <div class="vertical-align-middle font-size-16">
                                        <div class="mb-10">DIRECCION</div>
                                        <i class="wi-day-cloudy-gusts font-size-24 mb-10"></i>
                                        <div>
                                            <span class="font-size-12 text-info"> {{$visit->address}}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-2">
                                <div class="weather-day vertical-align">
                                    <div class="vertical-align-middle font-size-16">
                                        <div class="mb-10">TELEFONO</div>
                                        <i class="wi-day-lightning font-size-24 mb-10"></i>
                                        <div class="text-info">{{$visit->phone}}
                                            <span class="font-size-12"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            @if ($visit->client_agreement->id)
                            <div class="col-2">
                                <div class="weather-day vertical-align">
                                    <div class="vertical-align-middle font-size-16">
                                        <div class="mb-10">CONTRATO</div>
                                        <i class="wi-day-storm-showers font-size-24 mb-10"></i>
                                        <div class="text-info">
                                            @if ($visit->client_agreement->name == 'estandar')
                                            <a class="font-size-12 btn-sm btn-info" href="{{route('agreements.show',$visit->client_agreement)}}">E-000{{ $visit->client_agreement->id }}</a>
                                            @else
                                            <a class="font-size-12 btn-sm btn-info" href="{{route('agreements.show',$visit->client_agreement)}}">P-000{{ $visit->client_agreement->id }}</a>
                                            @endif <br>
                                            <span class="font-size-12">{{$visit->client_agreement->status}}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            {{-- modal --}}
            <div id="assign" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2"
                aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="myModalLabel2">Asignar Visita</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        </div>
                        {!! Form::open(['route' => 'visits.assign']) !!}
                        <div class="modal-body">
                            <div class="col-md-12 m-b-20">
                                <div class="form-group">
                                    <label>Seleccione el usuario Promotor al cual se va asignar la visita</label>
                                    <select class="form-control select2 w-p100" name="handler_id" id="handler_id">
                                        @foreach ($handlers as $hand)
                                        <option selected="selected" value="{{ $hand->id }}"> {{ $hand->user->name }}
                                        </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <input type="hidden" name="id_visit" id="id_visit">
                        </div>
                        <div class="modal-footer">
                            {{-- <button type="submit" class="btn btn-warning" data-dismiss="modal">Asignar</button> --}}
                            {{ Form::submit('Asignar', ['class' => 'btn btn-warning']) }}

                            <button type="button" class="btn btn-default float-right"
                                data-dismiss="modal">Cancelar</button>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<div class="box">
    <div class="box-header text-center bg-dark">
        <div class="row">
            <div class="col-12">
                @if ($visit->handler)
                    @if ($visit->handler->user->id == auth()->user()->id)
                        @if ($visit->client_agreement->products->count() > 0)
                            Articulo Valorado: <a href="{{ route ('visits.cotizar', $visit)}}" class="p-1 text-center  btn btn-success pull-up"> <span>Revisar Valoración</span></a> |
                        @else
                            <a href="{{ route ('visits.cotizar', $visit)}}" class="p-1 text-center  btn btn-success pull-up"> <span>Valorar Articulos</span></a> |
                        @endif
                    @else
                        <a href="{{ route ('visits.cotizar', $visit)}}" class="p-1 text-center  btn btn-success disabled"> <span>Solo Gestor {{$visit->handler->user->name}} puede crear la Valoración</span></a> |
                    @endif
                    Visita Asignada : <a href="{{ route ('profile', $visit->handler->user)}}" class="p-1 text-center  btn btn-primary pull-up"> <span>Gestor :  {{$visit->handler->user->name}} </span></a> |
                @else
                    <button type="button" onclick="asignar({{$visit->id}});" id="btn-assign" class="btn btn-warning" data-toggle="modal" data-target="#assign">Asignar a Gestor</button>
                @endif
                @if ($visit->client_agreement->status == 'pendiente')
                        <a class="btn btn-sm btn-danger pull-up"
                        href="{{ route ('visits.regected', $visit) }}">Rechazar Articulo</a>

                    {{-- {!! Form::open(['route' =>['visits.destroy', $visit], 'method' => 'delete', 'class' => ['inline'],'onclick' => 'return confirm("Estas seguro de rechazar el contrato?")' ]) !!}
                    <button class="btn btn-sm btn-danger pull-up"> Rechazar el Contrato </button>
                    {!! Form::close() !!} --}}
                @else
                    {!! Form::open(['route' =>['visits.destroy', $visit], 'method' => 'delete', 'class' => ['inline'],'onclick' => 'return confirm("Estas seguro de concluir la visita?")' ]) !!}
                    <button class="btn btn-sm btn-danger pull-up"> concluir visita </button>
                    {!! Form::close() !!}
                @endif
            </div>
        </div>
    </div>
    <div class="box-body">
        <div id="gallery-content">
            <div id="gallery-content-center">
                <div class="row">
                    @forelse ($visit->images as $img)
                    {{-- <div class="col-6"> --}}
                        <a href="{{ asset($img->path) }}" data-toggle="lightbox" data-gallery="multiimages"
                            data-title="Imagen"><img src="{{ asset($img->path) }}" class="all studio" alt="gallery" />
                        </a>
                        {{-- <button class="btn btn-warning btn-absolute-show-visit">agregar mas articulos</button>
                    </div> --}}
                        @empty
                        <p> No tiene fotografias</p>
                        @endforelse
                </div>
            </div>
        </div>
    </div>
    <div class="box-footer">
        <div class="text-center">
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script src="{{ asset('adm/assets/vendor_components/select2/dist/js/select2.full.js') }}"></script>
<!-- gallery -->
<script src="{{ asset('adm/assets/vendor_components/gallery/js/animated-masonry-gallery.js') }}"></script>
<script src="{{ asset('adm/assets/vendor_components/gallery/js/jquery.isotope.min.js') }}"></script>

<!-- fancybox -->
<script src="{{ asset('adm/assets/vendor_components/lightbox-master/dist/ekko-lightbox.js') }}"></script>
<!-- Crypto_Admin for demo purposes -->
<script src="{{ asset('adm/js/pages/gallery.js') }}"></script>

<script>
    function asignar(id) {
        $('#id_visit').val(id);
    }
</script>
@endsection
