@extends('layouts.admin')

@section('styles')
<link rel="stylesheet" href="{{ asset('adm/assets/vendor_components/select2/dist/css/select2.min.css') }}">
<!-- gallery -->
<link rel="stylesheet" href="{{ asset('adm/assets/vendor_components/gallery/css/animated-masonry-gallery.css') }}" />
<!-- fancybox -->
<link rel="stylesheet" href="{{ asset('adm/assets/vendor_components/lightbox-master/dist/ekko-lightbox.css') }}" />
@endsection

@section('breadcrumb')
<section class="content-header">
    <h1>
        Visita N° {{ $visit->id}} Solicitado por : <a href="{{ route ('profile', $visit->client->user)}}">
            {{$visit->client->user->name}}</a>
    </h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('admin')}}"><i class="fa fa-dashboard"></i> admin</a></li>
        <li class="breadcrumb-item"><a href="{{ route('visits.index')}}">Visitas</a></li>
        <li class="breadcrumb-item"><a href="{{ route('visits.show', $visit)}}">Detalle de Visita</a></li>
        <li class="breadcrumb-item active">Valoracion {{ $visit->id}} </li>
    </ol>
</section>
@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
        <!-- Default box -->
        <div class="box">
            <!-- /.box-body -->
            <div class="box-body">
                <div class="row align-items-center">
                    <div class="col-lg-12">
                        <div class="row no-space text-center">
                            <div class="col-2">
                                <div class="pull-up rounded bg-purple bg-banknote-white">
                                    <a href="{{ route ('profile', $visit->client->user)}}">
                                        <div class="">
                                            <div class="p-15 text-center">
                                                <div class="vertical-align-middle">
                                                    <span
                                                        class="font-size-20 text-white">{{$visit->client->user->name}}</span>
                                                    <div class="font-size-10 text-white">Cliente</div>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>

                            <div class="col-2">
                                <div class="weather-day vertical-align">
                                    <div class="vertical-align-middle font-size-16">
                                        <div class="mb-10">FECHA Y HORA</div>
                                        <div class="text-info">{{date('d-m-Y H:i', strtotime($visit->date_visit)) }}
                                            <br>
                                            <span class="font-size-12">{{$visit->hour_visit}}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-1">
                                <div class="weather-day vertical-align">
                                    <div class="vertical-align-middle font-size-16">
                                        <div class="mb-10">CANTIDAD</div>
                                        <i class="wi-day-cloudy font-size-24 mb-10"></i>
                                        <div>
                                            <span class="font-size-30 text-info">{{$visit->images->count()}}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-1">
                                <div class="weather-day vertical-align">
                                    <div class="vertical-align-middle font-size-16">
                                        <div class="mb-10">CODIGO POSTAL</div>
                                        <i class="wi-day-sunny font-size-24 mb-10"></i>
                                        <div class="text-info">{{$visit->cp}}
                                            <span class="font-size-12"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-2">
                                <div class="weather-day vertical-align">
                                    <div class="vertical-align-middle font-size-16">
                                        <div class="mb-10">DIRECCION</div>
                                        <i class="wi-day-cloudy-gusts font-size-24 mb-10"></i>
                                        <div>
                                            <span class="font-size-12 text-info"> {{$visit->address}}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-2">
                                <div class="weather-day vertical-align">
                                    <div class="vertical-align-middle font-size-16">
                                        <div class="mb-10">TELEFONO</div>
                                        <i class="wi-day-lightning font-size-24 mb-10"></i>
                                        <div class="text-info">{{$visit->phone}}
                                            <span class="font-size-12"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            @if ($visit->client_agreement_id)
                            <div class="col-2">
                                <div class="weather-day vertical-align">
                                    <div class="vertical-align-middle font-size-16">
                                        <div class="mb-10">CONTRATO</div>
                                        <i class="wi-day-storm-showers font-size-24 mb-10"></i>
                                        <div class="text-info"> {{$visit->client_agreement->name}}
                                            <span class="font-size-12"> 000{{ $visit->id }}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="box">
    <div class="box-header text-center bg-dark">
        <div class="row">
            <div class="col-12">
                @if ($visit->handler)
                Visita Asignada : <a href="{{ route ('profile', $visit->handler->user)}}"
                    class="p-1 text-center  btn btn-primary pull-up"> <span>Gestor : {{$visit->handler->user->name}}
                    </span></a> |
                @endif
                @if ($visit->client_agreement->status == 'pendiente')
                    {!! Form::open(['route' =>['visits.destroy', $visit], 'method' => 'delete', 'class' => ['inline'], 'onclick' => 'return confirm("Estas seguro de rechazar el contrato ?")' ]) !!}
                    <button class="btn btn-sm btn-danger pull-up"> Rechazar el Contrato </button>
                    {!! Form::close() !!}
                @else
                    {!! Form::open(['route' =>['visits.destroy', $visit], 'method' => 'delete', 'class' => ['inline'], 'onclick' => 'return confirm("Estas seguro de concluir la visita ?")' ]) !!}
                    <button class="btn btn-sm btn-danger pull-up"> concluir visita </button>
                    {!! Form::close() !!}
                @endif
            </div>
        </div>
    </div>
    <div class="box-body">
        <script>
            var lista = [];
            var contador = 0;
        </script>
        @forelse ($visit->images as $img)
        {{-- ----------------formulario N Campos  para cotizar --}}
        @if($img->product_id)
        <div class="row">
            <div class="col-md-2 col-sm-12">
                <div id="gallery-content">
                    <div id="gallery-content-center">
                        <a href="{{ asset($img->path) }}" data-toggle="lightbox" data-gallery="multiimages"
                            data-title="Imagen"><img src="{{ asset($img->path) }}" class="all studio" alt="gallery" /></a>
                    </div>
                </div>
            </div>
            <div class="col-md-8 col-sm-12">
                <div class="row">
                    <div class="form-group col-md-5">
                        {{ Form::label('name', 'Nombre del Artículo') }}
                        {{ Form::text('name', $img->product->name, [ 'class' => 'form-control','disabled'=>'true', 'id' => 'name','maxlength' => 26 ]) }}
                    </div>
                    <div class="form-group col-md-4">
                        {{ Form::label('category_id', 'Estancia') }}
                        {{ Form::text('category_id', $img->product->category->name, [ 'class' => 'form-control','disabled'=>'true', 'id' => 'category_id' ]) }}
                    </div>

                    <div class="form-group col-md-3">
                        {{ Form::label('normal_price', 'Precio') }}
                        {{ Form::text('normal_price', $img->product->normal_price, [ 'class' => 'form-control', 'disabled'=>'true', 'id' => 'normal_price' ]) }}
                    </div>
                    <div class="form-group col-md-12">
                        {{ Form::hidden('description', $img->product->description, [ 'class' => 'form-control', 'disabled'=>'true', 'placeholder'=>' Descripción del Articulo', 'id' => 'description']) }}
                    </div>
                </div>
            </div>
            <div class="col-md-2 col-sm-12">
                <a href="{{ route ('products.show', $img->product) }}" class="btn btn-sm btn-warning pull-up">Ver Producto Valorado</a>
            </div>
        </div>
        @else
        {{-- sino no esta cotizado entonces se muestra un formulario --}}
        <div id="formulario_{{$img->id}}">
        {!! Form::open(['route' => ['visit.products.store', $visit->client_agreement ]]) !!}
            <div class="row">
                <div class="col-md-2 col-sm-12">
                    <div id="gallery-content">
                        <div id="gallery-content-center">
                            <a href="{{ asset($img->path) }}" data-toggle="lightbox" data-gallery="multiimages"
                                data-title="Imagen"><img src="{{ asset($img->path) }}" class="all studio" alt="gallery" /></a>
                        </div>
                    </div>
                </div>
                <div class="col-md-8 col-sm-12">
                    <div class="row">
                        <div class="form-group col-md-5">
                            {{ Form::label('name'.$img->id, 'Nombre del Artículo') }}
                            {{ Form::text('name'.$img->id, null, [ 'class' => 'form-control','required'=>'true','maxlength' => 26, 'id' => 'name'.$img->id ]) }}
                        </div>
                        <div class="form-group col-md-4">
                            {{ Form::label('category_id'.$img->id, 'Estancia') }}
                            {{ Form::select('category_id'.$img->id, ['' => 'Selecciona tu estancia'] + $categories->all(), null, [ 'class' => 'form-control','required'=>'true', 'id' => 'category_id'.$img->id ]) }}
                        </div>

                        <div class="form-group col-md-3">
                            {{ Form::label('normal_price'.$img->id, 'Precio') }}
                            {{ Form::text('normal_price'.$img->id, 10, [ 'class' => 'form-control', 'required'=>'true', 'id' => 'normal_price'.$img->id ]) }}
                        </div>
                        <div class="form-group col-md-12">
                            {{ Form::hidden('description'.$img->id, null, [ 'class' => 'form-control', 'required'=>'true', 'placeholder'=>' Descripción del Articulo', 'id' => 'description'.$img->id ]) }}
                        </div>
                    </div>
                </div>
                <div class="col-md-2 col-sm-12">
                    <button id="form_{{$img->id}}" class="btn btn-sm pull-up btn-success my-1">Añadir A Valoracion</button>
                    <a href="{{route('visits.removeArticle', ['id' => $img->id])}}" class="btn btn-sm pull-up btn-danger my-1">Eliminar Articulo</a>
                </div>
            </div>
            {!! Form::close() !!}
            {{-- {!! Form::open(['route' =>['visits.removeArticle', $img], 'method' =>'delete',
             'class'=>['inline', 'f-right'],'onclick' => 'return confirm("Estas seguro de eliminar?")' ]) !!}
                <button class="btn btn-sm btn-danger"> <i class="fa fa-close"></i> Eliminar Articulo</button>
            {!! Form::close() !!} --}}
        </div>

        <script>
            lista.push('{{$img->id}}')
            contador++;
        </script>
        {{-- ----------------formulario N --}}
        @endif
        <hr>
        @empty
        <p> No tiene Articulos</p>
        @endforelse
        <div id="ShowButton" class="col-sm-12 text-center">
            <a href="{{ route ('agreements.show', $visit->client_agreement) }}" class="btn btn-sm btn-success pull-up">Ver Contrato Generado</a>
        </div>
    </div>
    <div class="box-footer">
        <div class="text-center">
            {!! Form::open(['route' =>['visits.addArticle'], 'files' => true, 'method' => 'post', 'class' => ['inline'], 'id' => 'load_file' ]) !!}
            <label class="fileContainer">
                Agregar mas Articulos
                <input type="hidden" name="visit_id" value="{{$visit->id}}">
                <input type="file" id="images" name="images[]"  multiple="multiple" accept='image/*' required="true"  class="">
            </label>
            {!! Form::close() !!}
        </div>
    </div>
</div>
<style>
    .f-right{
        float: right;
        margin-top: 0px;
    }
    table {
        /* min-width: ; */
        display: block;
        overflow-x: auto;
        white-space: nowrap;
    }
    .cursor{
        cursor: copy;
    }
    .fileContainer {
        padding: 5px;
        border-radius: 4px;
        background:rgba(226, 125, 30, 0.4);
        font-size: 1rem;
        overflow: hidden;
        position: relative;
        vertical-align: center;
    }
    .fileContainer:hover {
        background:rgba(230, 117, 11, 0.795);
    }

.fileContainer [type=file] {
    display: block;
    font-size: 999px;
    filter: alpha(opacity=0);
    min-height: 100%;
    min-width: 100%;
    opacity: 0;
    position: absolute;
    right: 0;
    text-align: right;
    top: 0;
}
</style>
@endsection

@section('scripts')
<script src="{{ asset('adm/assets/vendor_components/select2/dist/js/select2.full.js') }}"></script>
<!-- gallery -->
<script src="{{ asset('adm/assets/vendor_components/gallery/js/animated-masonry-gallery.js') }}"></script>
<script src="{{ asset('adm/assets/vendor_components/gallery/js/jquery.isotope.min.js') }}"></script>

<!-- fancybox -->
<script src="{{ asset('adm/assets/vendor_components/lightbox-master/dist/ekko-lightbox.js') }}"></script>
<!-- Crypto_Admin for demo purposes -->
<script src="{{ asset('adm/js/pages/gallery.js') }}"></script>
<script src="{{ asset('vendor/bootstrap-notify/bootstrap-notify.min.js') }}"></script>

<script>
document.getElementById("images").onchange = function () {
    document.getElementById("load_file").submit();
};

function cargando(color = 'bg-gd-sea') {
    document.getElementById("form_archivos").style.display = "none";
    ldld.on();
}
console.log(lista, contador);
$('#ShowButton').hide();
showbutton();
function asignar(id) {
    $('#id_visit').val(id);
}
// ============ jqqury submit
for (let i = 0; i < lista.length; i++) {
    // $lastYear = date("Y", strtotime("-2000 years"));
    //     $product->code = 'B'.$lastYear.'/'.$product->id;
    //     $product->save();

    jQuery(document).ready(function () {
        jQuery('#form_'+lista[i]).click(function (e) {
            e.preventDefault();
            $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
            jQuery.ajax({
                type: 'POST',
                url: "{{ route('visit.products.store', $visit->client_agreement) }}",
                method: 'post',
                data: {
                    numero: i+1,
                    image_id: lista[i],
                    name: jQuery('#name'+lista[i]).val(),
                    category_id: jQuery('#category_id'+lista[i]).val(),
                    normal_price: jQuery('#normal_price'+lista[i]).val(),
                    description: jQuery('#description'+lista[i]).val(),
                    user_id: '{{$visit->client->user->id}}'
                },
                success: function (result) {
                    mostrarNotify(result.msg);
                    if (result.status) { // if save is true then hide div
                        contador--;
                        $('#formulario_'+lista[i]).hide()
                        showbutton();
                    }
                }
            });
        });
    });
}
function showbutton(){
    if (contador==0) {
        $('#ShowButton').show()
    }
    console.log('contador: en showbutton: ',contador);
}
function mostrarNotify(msg) {
    var nFrom = 'top';
    var nAlign = 'right';
    var nIcons = 'fa fa-comments';
    var nType = 'inverse';
    var nAnimIn = undefined;
    var nAnimOut = undefined;
    notify(nFrom, nAlign, nIcons, nType, nAnimIn, nAnimOut, msg);
};
function notify(from, align, icon, type, animIn, animOut, msg) {
        $.notify({
            icon: icon,
            title: 'MENSAJE: ',
            message: msg,
            url: ''
        }, {
                element: 'body',
                type: type,
                allow_dismiss: true,
                placement: {
                    from: from,
                    align: align
                },
                offset: {
                    x: 15, // Keep this as default
                    y: 15  // Unless there'll be alignment issues as this value is targeted in CSS
                },
                spacing: 20,
                z_index: 1031,
                delay: 2500,
                timer: 3000,
                url_target: '_blank',
                mouse_over: false,
                animate: {
                    enter: animIn,
                    exit: animOut
                },
                template: `<div data-notify="container" class="alert alert-success alert-dismissible alert-{0} alert--notify" role="alert">
                        <span data-notify="icon"></span>
                        <span data-notify="title">{1}</span>
                        <span data-notify="message">{2}</span>
                        <div class="progress" data-notify="progressbar">
                            <div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>
                        </div>
                    </div>`
            });
    }




</script>
@endsection
