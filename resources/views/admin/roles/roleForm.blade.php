<div class="row">
    <div class="form-group col-md-12">
        {{ Form::label('name', 'Nombre del Rol') }}
        {{ Form::text('name', null, [ 'class' => 'form-control', 'id' => 'name' ]) }}
    </div>
    <div class="form-group">
        {{-- {{ Form::label('slug', 'URL Amigable') }} --}}
        {{ Form::hidden('slug',null, [ 'class' => 'form-control', 'id' => 'slug' ]) }}
    </div>
    <div class="form-group col-md-12">
        {{ Form::label('description', 'Descripción del Rol') }}
        {{ Form::textarea('description', null, [ 'class' => 'form-control', 'id' => 'description' ]) }}
    </div>
    <h3 class="col-12">Acceso especial</h3>
    <div class="form-group">
        <div class="radio">
            {{ Form::radio('special','all-access',false,['id'=>'10']) }}
            <label for="10">Acceso Total (solo para administradores)</label>
        </div>
        <div class="radio">
            {{ Form::radio('special','no-access',false,['id'=>'20']) }}
            <label for="20"> {{ Form::radio('special','no-access') }}Ningun Acceso (Solo para usuarios suspendidos)</label>
        </div>
    </div>

    <h3 class="col-12">Lista de Permisos</h3>
    <div class="form-group">
            @foreach ($permisos as $key=>$permiso)
            <div class="checkbox">
                {{ Form::checkbox('permisos[]', $permiso->id, null, ['id'=> ++$key ]) }}
                <label for="{{ $key }}">{{ $permiso->name }}<em>( {{ $permiso->description ?: 'S/N' }} )</em></label>                    
            </div>
            @endforeach
    </div>
    <div class="form-group col-md-12 text-center">
        {{ Form::submit('Guardar Rol', ['class' => 'btn btn-dark']) }}
        {{ Form::reset('Limpiar', ['class' => 'btn btn-warning']) }}
    </div>
</div>
@section('scripts')
<script src="{{ asset('js/jquery.stringToSlug.min.js') }}"></script>
<script>
    $(document).ready(()=>{
        $("#name").stringToSlug({
            callback: (text)=>{ $('#slug').val(text); }
        });
    });
</script>
@endsection