@extends('layouts.admin')

@section('content')

<div class="box box-solid bg-dark">
	<div class="box-header with-border">
		<h3 class="box-title">Crear Nuevo Rol</h3>
        {{-- <h6 class="box-subtitle"> Registrar un articulo para la venta</h6> --}}
        <a href="{{ route ('roles.index')}}" class="btn btn-danger float-right">Cancelar</a>
	</div>
	<!-- /.box-header -->
	<div class="box-body">
		{!! Form::open(['route' => 'roles.store']) !!}
	
			@include('admin.roles.roleForm')

		{!! Form::close() !!}
	</div>
</div>

@endsection