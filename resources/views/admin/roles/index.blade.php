@extends('layouts.admin')

@section('content')
<div class="row">
    <div class="col-12">
        <div class="box box-solid bg-dark">
            <div class="box-header with-border flex">
                <h3 class="box-title">Roles</h3>
                {{-- <h6 class="box-subtitle"> Listado de Roles</h6> --}}
            <a href="{{ route ('roles.create') }}" class="btn btn-info float-right">Crear Rol</a>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="table-responsive">
                    <table id="example"
                        class="data-table table table-bordered table-hover display nowrap margin-top-10 w-p100">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nombre</th>
                                <th>Slug</th>
                                <th>Descripcion</th>
                                <th>Accion</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($roles as $item)
                            <tr>
                                <td>{{$item->id}}</td>
                                <td>{{$item->name}}</td>
                                <td>{{$item->slug}}</td>
                                <td>{{$item->description}}</td>
                                <td>
                                    <a class="btn btn-sm btn-default" href="{{ route ('roles.show', $item) }}"> Ver</a>
                                    <a class="btn btn-sm btn-info" href="{{ route ('roles.edit', $item) }}"> Editar</a>
                                    {!! Form::open(['route' =>['roles.destroy', $item], 'method' => 'delete', 'class' => ['inline'], 'onclick' => 'return confirm("Estas seguro de eliminar?")' ]) !!}
                                    {{-- {!! Form::token() !!} --}}
                                        <button class="btn btn-sm btn-danger"> Eliminar</button>
                                    {!! Form::close() !!}
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                        {{-- <tfoot>
                            <tr>
                                <th>No</th>
                                <th>Nombre</th>
                                <th>Slug</th>
                                <th>Icon</th>
                                <th>Accion</th>
                            </tr>
                        </tfoot> --}}
                    </table>
                    <div class="text-center">
                        {{ $roles->render() }}
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
@endsection
