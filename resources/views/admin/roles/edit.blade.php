@extends('layouts.admin')

@section('content')
<div class="row">
    <div class="col-12">
        <div class="box box-solid bg-dark">
            <div class="box-header with-border flex">
                <h3 class="box-title">Editar Rol</h3>
                {{-- <h6 class="box-subtitle"> Listado de Rols</h6> --}}
                <a href="{{ route ('roles.index')}}" class="btn btn-danger float-right">Cancelar</a>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                {!! Form::model($role,['route' => ['roles.update', $role->id], 'method'=> 'PUT']) !!}
                
                    @include('admin.roles.roleForm')
                
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection
