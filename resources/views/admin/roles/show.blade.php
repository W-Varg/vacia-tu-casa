@extends('layouts.admin')

@section('content')
<div class="row">
    <div class="col-12">
        <div class="box box-solid">
            <div class="box-header bg-dark with-border flex">
                <h3 class="box-title">Roles</h3>
                {{-- <h6 class="box-subtitle"> Listado de Roles</h6> --}}
                <a href="{{ route ('roles.index')}}" class="btn btn-danger float-right">Listar todos los Roles</a>
            </div>
            <div class="box-body">
                <p><strong>Nombre</strong> {{ $role->name }} </p>
                <p><strong>descripcion</strong> {{ $role->description }} </p>
            </div>
            <div class="box-footer text-center">
                    <a href="{{ route ('roles.edit', $role)}}" class="btn btn-info">Editar Rol</a>
            </div>
        </div>
    </div>
</div>
@endsection
