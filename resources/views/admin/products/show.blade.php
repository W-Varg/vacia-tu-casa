@extends('layouts.admin')
@section('styles')
<link href="{{ asset('adm/assets/vendor_components/Magnific-Popup-master/dist/magnific-popup.css') }}" rel="stylesheet">
@endsection
@section('content')
<!-- Popup CSS -->
<section class="content-header">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('admin')}}"><i class="fa fa-dashboard"></i> Admin</a></li>
        <li class="breadcrumb-item"><a href="{{ route('products.index')}}">Lista de Artículos</a></li>
        <li class="breadcrumb-item active">Detalle de Articulo</li>
    </ol>
    <h2 style="text-transform: capitalize;" class="my-4">
        Detalle de Articulo
        <span class="text-success"> NO: {{$product->code}}</span>
        <span class="text-primary">
            @if ($product->client_agreement)
            Contrato {{$product->client_agreement->name}} 000{{ $product->client_agreement->id }}
            @else
            @if ($product->beliani)
            Beliani
            @elseif ($product->importance==1 && !$product->beliani )
            outled
            @else
            Basico
            @endif
            @endif
        </span>
    </h2>
</section>
<br>
<div class="row">
    <div class="col-lg-12">
        <div class="box box-solid bg-dark">
            <div class="box-header with-border flex">
                <h3 class="box-title">Nombre: {{ $product->name }}</h3>
                <a href="{{ URL::previous() }}" class="btn btn-danger float-right"> Retornar </a>

            </div>
            <div class="box-body rounded flex">
                <div class="popup-gallery">
                    <div class="row">
                        @foreach ($product->images as $img)
                        <div class="col-2">
                            <a href="{{ asset($img->path) }}">
                                <img src="{{ asset($img->path) }}" alt="{{$product->name}}" />
                            </a>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer box-solid bg-dark text-center rounded">
                <div class="row">
                    <div class="col-12 col-sm-6">
                        <div class="ribbon bg-warning bg-status-{{$product->status}}">{{ $product->status }}</div>
                        <br>
                        <br>
                        <p class="lead"><b>Codigo QR:</b></p>
                        <div id="code_qr">
                            {!!QrCode::size(150)->generate(route('page.detail_product',$product)) !!}
                        </div>
                        <button class="btn btn-primary" type="button" onclick="printJS({ printable: 'code_qr', type: 'html'})">
                            Imprimir
                        </button>
                        <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
                            {!! $product->description !!}
                        </p>

                    </div>
                    <!-- /.col -->
                    <div class="col-12 col-sm-6 text-right">
                        <p class="lead"><b>Articulo Registrado </b><span class="text-warning">
                                {{ date('d-M-Y / H:i', strtotime($product->created_at)) }} </span></p>
                        <div>
                            <h4> Estancia: <span class="text-success">{{ $product->category->name }}</span></h4>
                            <h5> Cantidad: {{ $product->quantity }}</h5>
                            <p>Largo : {{ $product->long }} cm.</p>
                            <p>Ancho : {{ $product->width}} cm. </p>
                            <p>Alto : {{ $product->height}} cm.</p>
                            <p>Peso : {{ $product->weight}} Kg.</p>
                        </div>
                        <div class="total-payment">
                            @if ($product->reduced_price == null || $product->reduced_price <= 0) <h3><b>Precio :</b> {{ $product->normal_price}}
                                <i class="fa fa-euro"></i> </h3>
                                @else
                                <h3><b>Precio Reducido :</b> <span class="text-danger"><i class="fa fa-eur"></i>
                                        {{ $product->reduced_price}} </span></h3>
                                @endif
                        </div>

                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        @if ($product->status == 'PENDIENTE')
                        {{-- <a href="{{ route ('products.publicar', $product)}}" class="btn btn-success pull-right"><i
                                class="fa fa-external-link"></i> Publicar Articulo</a> --}}
                        {!! Form::open(['route' =>['products.publicar', $product], 'method' => 'PUT', 'class' =>
                        ['inline'] ]) !!}
                        <button class="btn btn-success pull-right mx-3 my-3"> <i class="fa fa-external-link"></i>
                            Publicar Articulo</button>
                        {!! Form::close() !!}
                        <a class="btn btn-sm btn-outline-danger" href="{{ route ('products.regected', $product) }}">Rechazar Articulo</a>

                        {{-- {!! Form::open(['route' =>['products.rechazar', $product],
                        'method'=>'PUT','class' => ['inline pull-right mx-3 my-3'] ]) !!}
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Motivo</span>
                            </div>
                            {{ Form::select('razon',[
                            'El Articulo no cumple los criterios de calidad de Vacía Tu Casa' => 'El Articulo no cumple los criterios de calidad
                            de Vacía Tu Casa' ,
                            'Por mala calidad de imágenes, se necesita más imágenes, descripción o medidas' => 'Por mala calidad de imágenes, se
                            necesita más imágenes, descripción o medidas'],
                            null, [ 'class' => '', 'id' => 'razon' ]) }}
                            <div class="input-group-append">
                                <button class="btn btn-sm btn-outline-danger"> Rechazar
                                    Articulo</button>
                            </div>
                        </div>
                        {!! Form::close() !!} --}}
                        @endif

                        @if (($product->importance==1 && !$product->beliani) && ($product->status == 'PUBLICADO' || $product->status ==
                        'PENDIENTE'))
                        <a href="{{ route ('products.toBeliani', $product)}}" class="btn btn-secondary pull-right mx-3 my-3">
                            <i class="fa fa-edit"> </i>
                            Convertir a Beliani
                        </a>
                        @endif
                        @if ($product->status == 'PUBLICADO' || $product->status == 'PENDIENTE')
                        <a href="{{ route ('products.edit', $product)}}" class="btn btn-info pull-right mx-3 my-3"> <i
                                class="fa fa-edit"></i>Editar
                            Artículo</a>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script src="{{ asset('adm/assets/vendor_components/Magnific-Popup-master/dist/jquery.magnific-popup.min.js') }}">
</script>
<script src="{{ asset('adm/assets/vendor_components/Magnific-Popup-master/dist/jquery.magnific-popup-init.js') }}">
</script>
@endsection
