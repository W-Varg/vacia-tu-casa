@extends('layouts.admin')

@section('content')
<div class="row">
    <div class="col-md-12 col-lg-12">
        <div class="box box-solid bg-dark">
            <div class="box-header with-border">
                <h3 class="box-title">Mis Artículos Publicados</h3>
                {!! Form::open(['route' => 'products.index', 'method' => 'GET', 'class'=>['inline']])
                !!}
                <div class="form-group">
                    <input class="form-control" placeholder="Buscar por Nombre o Código" value="{{$name}}" name="name" />
                </div>
                {!! Form::close() !!}

                <a href="{{ route ('products.create')}}" class="btn btn-info float-right">Registrar Nuevo</a>
                <a href="{{ route ('products.createSinDecuento')}}" class="btn btn-info mx-2 float-right">Registrar Oulet</a>
                <a href="{{ route ('products.createBeliani')}}" class="btn btn-info mx-2 float-right">Registrar Beliani</a>
                <h6 class="box-subtitle"> Listado de mis Artículos Publicados en la Web</h6>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#pending" role="tab">
                            <span> <i class="fa fa-clock-o"></i></span> publicados </a>
                    </li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content tabcontent-border">
                    <div class="tab-pane active" id="published" role="tabpanel">
                        <div class="pad">
                            <table class="table table-striped">
                                <tbody>
                                    @foreach($published as $product)
                                    <tr>
                                        <td class="text-center">
                                            <a href="{{ route ('products.show', $product) }}">
                                                @if ($product->img)
                                                <img class="rounded" src="{{ asset($product->img) }}" alt="{{ $product->name }}" height="50" />
                                                @else
                                                <img src="{{ asset('assets/images/product-1.png') }}" alt="{{ $product->name }}" height="50" />
                                                @endif
                                            </a>
                                        </td>
                                        <td> <a href="{{ route ('products.show', $product) }}"> {{ $product->name }}
                                            </a> </td>
                                        <td> {{ $product->normal_price }} <i class="fa fa-euro"></i></td>
                                        <td> {{ $product->category->name }} </td>
                                        <td> {{ $product->code }} </td>

                                        <td>
                                            <a href="{{ route ('compras.create', $product) }}" class="btn btn-sm btn-success">
                                                <i class="fa  fa-check"></i> Vendido??
                                            </a>
                                        </td>
                                        <td>
                                            @if (!$product->compra)
                                            {!! Form::open(['route' =>['products.destroy.debaja', $product], 'method' => 'delete', 'class' =>
                                            ['inline'], 'onclick' => 'return confirm("Estas seguro de eliminar?")' ]) !!}
                                            <button class="btn btn-sm btn-danger"> <i class="fa fa-trash"></i> Eliminar</button>
                                            {!! Form::close() !!}
                                            @endif
                                        </td>
                                        <td>
                                            {!! Form::model($product,['route' => ['products.reservar', $product],
                                            'method'=> 'PUT', 'class' =>[''] ]) !!}
                                            <input name="have_reserva" value="GESTOR" type="radio" id="radio_{{$product->id}}" checked>
                                            <label for="radio_{{$product->id}}">Gestor</label>

                                            <input name="have_reserva" value="CLIENTE" type="radio" id="radio_{{$product->id}}101">
                                            <label for="radio_{{$product->id}}101">Cliente</label>

                                            <div class="input-group">
                                                <input type="number" class="form-control" style="width: 120px;" placeholder="Importe de Euros"
                                                    required="true" id="monto_reserva" name="monto_reserva">
                                                <div class="input-group-append">
                                                    <span class="input-group-text"><i class="fa fa-euro"></i></span>
                                                    <button type="submit" class="btn btn-outline-secondary">Reservar</button>
                                                </div>
                                            </div>
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <nav class="mt-15 pb-10">
                    <ul class="pagination justify-content-center">
                        {{ $published->appends($pagelinks)->links() }}
                    </ul>
                </nav>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
</div>

@endsection
