@extends('layouts.admin')
@section('styles')
<link href="{{ asset('adm/assets/vendor_components/Magnific-Popup-master/dist/magnific-popup.css') }}" rel="stylesheet">
@endsection
@section('content')
<div class="box box-solid bg-dark">
    <div class="box-header with-border">
        <h3 class="box-title">Rechazar Articulo </h3>

        <a href="{{ URL::previous()}}" class="btn btn-danger float-right">Cancelar</a>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        {!! Form::open(['route' =>['products.rechazar', $product], 'method'=>'PUT','class' => ['inline'] ]) !!}

        <div class="row">
            {{ Form::hidden('user_id', auth()->user()->id) }}

            {{-- <div class="form-group col-md-8">
                {{ Form::label('subject', 'Titulo de correo') }}
            {{ Form::text('subject', null, [ 'class' => 'form-control','required'=>'true', 'id' => 'subject', 'maxlength' => 26 ]) }}
        </div> --}}

        <div class="form-group col-md-12">
            {{ Form::label('razon', 'Razon') }}
            {{ Form::textarea('razon', '<strong>El artículo fue rechazado para su publicacion por la siguiente razon:<br>Mala Calidad de imágenes, o número insuficiente de fotografías.</strong><br> <br> Deben ser un mínimo de 6 imágenes.<br> <br> Te animamos a que vuelvas a intentarlo.', [ 'class' => 'form-control', 'id' => 'razon','required'=>'true', "maxlength" => 800 ]) }}
        </div>

        <div class="form-group col-md-12 text-center">
            {{ Form::submit('Completar Rechazo', ['class' => 'btn btn-success']) }}
        </div>
    </div>
    {!! Form::close() !!}
</div>
</div>
@endsection
@section('scripts')
<script src="{{ asset('vendor/ckeditor/ckeditor.js') }}"></script>
<script>
    CKEDITOR.config.height = 200;
    CKEDITOR.config.width = 'auto';
    CKEDITOR.config.language = 'es';
    CKEDITOR.replace('razon');
</script>
@endsection
