@extends('layouts.admin')
@section('content')

<div class="row">
    <div class="col-md-12 col-lg-12">
        <div class="box box-solid bg-dark">
            <div class="box-header with-border">
                {!! Form::open(['route' => 'products.pendientes', 'method' => 'GET', 'class'=>['inline']])
                !!}
                <div class="form-group">
                    <input class="form-control" placeholder="Buscar por Nombre o Código" value="{{ $name}}" name="name" />
                </div>
                {!! Form::close() !!}

                <a href="{{ route ('products.create')}}" class="btn btn-info float-right">Registrar Nuevo</a>
                <h6 class="box-subtitle"> Listado de mis Artículos Pendientes</h6>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#pending" role="tab">
                            <span> <i class="fa fa-clock-o"></i></span> Pendientes </a>
                    </li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content tabcontent-border">
                    <div class="tab-pane active" id="pending" role="tabpanel">
                        <div class="pad">
                            <table class="table table-striped">
                                <tbody>
                                    @foreach($pending as $product)
                                    <tr>
                                        <td class="text-center">
                                            <a href="{{ route ('products.show', $product) }}">
                                                @if ($product->img)
                                                <img class="rounded" src="{{ asset($product->img) }}" alt="" height="50" />
                                                @else
                                                <img src="{{ asset('assets/images/product-1.png') }}" alt="" height="50" />
                                                @endif
                                            </a>
                                        </td>
                                        <td> <a href="{{ route ('products.show', $product) }}"> {{ $product->name }} | {{$product->user->name}}
                                            </a> </td>
                                        <td> {{ $product->normal_price }} <i class="fa fa-euro"></i></td>
                                        <td> {{ $product->category->name }} </td>
                                        <td> {{ $product->code }} </td>
                                        <td>
                                            <a class="btn btn-sm btn-primary" href="{{ route ('products.show', $product) }}"> Ver</a>
                                            <a class="btn btn-sm btn-default" href="{{ route ('products.edit', $product) }}"> Editar</a>
                                        </td>
                                        <td>
                                            <div class="input-group-append">
                                                <a class="btn btn-sm btn-outline-danger"
                                                    href="{{ route ('products.regected', $product) }}">Rechazar Articulo</a>

                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <nav class="mt-15 pb-10">
                    <ul class="pagination justify-content-center">
                        {{ $pending->appends($pagelinks)->links() }}
                    </ul>
                </nav>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
</div>

@endsection
