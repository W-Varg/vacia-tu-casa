@extends('layouts.admin')

@section('content')
<div class="row">
    <div class="col-12">
        <div class="box box-solid bg-dark">
            <div class="box-header with-border flex">
                <a href="{{ route ('categories.index')}}" class="btn btn-info float-left"> Listar Instancias</a>
                <h3 class="box-title"> Editar Estancia</h3>
                {{-- <h6 class="box-subtitle"> Listado de Estancias</h6> --}}
                <a href="{{ route ('categories.index')}}" class="btn btn-danger float-right">Cancelar</a>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <img src="{{ asset('adm/icons/'.$category->icon) }}" alt="logo">
                {!! Form::model($category,['route' => ['categories.update', $category->id], 'method'=> 'PUT', 'files' =>true]) !!}

                    @include('admin.category.categoryForm')

                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection
