@extends('layouts.admin')

@section('content')
<div class="row">
    <div class="col-12">
        <div class="box box-solid bg-dark">
            <div class="box-header with-border flex">
                <h3 class="box-title">Estancia</h3>
                {{-- <h6 class="box-subtitle"> Listado de Estancias</h6> --}}
                <a href="{{ route ('categories.index')}}" class="btn btn-danger float-right">Listar Estancias</a>
            </div>
            <div class="box-body">
                <p><strong>Nombre</strong> {{ $category->name }} </p>
                <p><strong>Slug</strong> {{ $category->slug }} </p>
                <p><strong>icon</strong> {{ $category->icon }} </p>
            </div>
            <div class="box-footer text-center">
                    <a href="{{ route ('categories.edit', $category)}}" class="btn btn-info">Editar Estancia</a>
            </div>
        </div>
    </div>
</div>
@endsection
