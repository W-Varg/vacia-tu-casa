@extends('layouts.admin')

@section('content')
<div class="row">
    <div class="col-12">
        <div class="box box-solid bg-dark">
            <div class="box-header with-border flex">
                <h3 class="box-title">Crear Nueva Estancia</h3>
                {{-- <h6 class="box-subtitle"> Listado de Estancias</h6> --}}
                <a href="{{ route ('categories.index')}}" class="btn btn-danger float-right">Cancelar</a>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                {!! Form::open(['route' => 'categories.store','files' =>true]) !!}

                    @include('admin.category.categoryForm')
                
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection
