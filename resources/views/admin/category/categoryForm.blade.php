<div class="form-group">
    {{ Form::label('name', 'Nombre de la etiqueta') }}
    {{ Form::text('name',null, [ 'class' => 'form-control', 'id' => 'name' ]) }}
</div>
<div class="form-group">
    {{-- {{ Form::label('slug', 'URL Amigable') }} --}}
    {{ Form::hidden('slug',null, [ 'class' => 'form-control', 'id' => 'slug' ]) }}
</div>
<div class="form-group">
    {{ Form::label('icon', 'Icono De Instancia') }}
    <!-- {{ Form::select('icon', array('f-icon f-icon-dining-table' => 'Mesas de comedor', 'f-icon f-icon-accessories' => 'Accesorios', 'f-icon f-icon-armchair' => 'Sillones', 'f-icon f-icon-bar-set' => 'Bar', 'f-icon f-icon-bathroom' => 'Baño', 'f-icon f-icon-bedroom' => 'Dormitorio', 'f-icon f-icon-bookcase' => 'Librero', 'f-icon f-icon-carpet' => 'Alfombra', 'f-icon f-icon-chair' => 'Silla', 'f-icon f-icon-children-room' => 'Habitación de niños', 'f-icon f-icon-kitchen' => 'Cocina', 'f-icon f-icon-lightning' => 'Lampara', 'f-icon f-icon-media-cabinet' => 'Gabinete', 'f-icon f-icon-nightstand' => 'Mesa de noche', 'f-icon f-icon-office' => 'Oficina', 'f-icon f-icon-retro' => 'Retro', 'f-icon f-icon-shoe-cabinet' => 'Gabinete de zapatos', 'f-icon f-icon-sofa' => 'Sofa', 'f-icon f-icon-table' => 'Mesa', 'f-icon f-icon-wardrobe' => 'Guardaropa'), [ 'class' => 'form-control', 'id' => 'icon' ]) }} -->
    <input type="file" class="form-control" id="icon"  accept="image/*" name="icon" autofocus>
</div>
    
<div class="form-group">
    {{ Form::submit('Guardar', ['class' => 'btn btn-sm btn-primary']) }}
</div>

@section('styles')
    <link rel="stylesheet" href="{{ asset('adm/css/jquery.fonticonpicker.css') }}">
    <link rel="stylesheet" href="{{ asset('css/furniture-icons.css') }}">
@endsection

@section('scripts')
    <script src="{{ asset('js/jquery.stringToSlug.min.js') }}"></script>
    <script src="{{ asset('adm/js/jquery.fonticonpicker.js') }}"></script>
    <script>
        $(document).ready(()=>{
            $("#name").stringToSlug({
                callback: (text)=>{ $('#slug').val(text); }
            });
        });
    </script>
@endsection

