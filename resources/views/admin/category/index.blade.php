@extends('layouts.admin')
@section('styles')
<link rel="stylesheet" media="all" href="{{ asset('css/furniture-icons.css') }}" />
@endsection
@section('content')
<div class="row">
    <div class="col-12">
        <div class="box box-solid bg-dark">
            <div class="box-header with-border flex">
                <h3 class="box-title">Estancias</h3>
                {{-- <h6 class="box-subtitle"> Listado de Estancias</h6> --}}
                <a href="{{ route ('categories.create')}}" class="btn btn-info float-right">Crear nueva Estancia</a>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="table-responsive">
                    <table id="example"
                        class="data-table table table-bordered table-hover display nowrap margin-top-10 w-p100">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nombre</th>
                                <th>Icon</th>
                                <th>Accion</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($categories as $item)
                            <tr>
                                <td>{{$item->id}}</td>
                                <td>{{$item->name}}</td>
                                <td> <img src="{{ asset('adm/icons/'.$item->icon) }}" alt="{{$item->icon}}" width="75"> </td>
                                <td class="text-center">
                                    <a class="btn btn-sm my-2 btn-default" href="{{ route ('categories.show', $item) }}"> Ver</a>
                                    <a class="btn btn-sm btn-info" href="{{ route ('categories.edit', $item) }}"> Editar</a>
                                    {!! Form::open(['route' =>['categories.destroy', $item], 'method' => 'delete', 'class' => ['inline'],'onclick' => 'return confirm("Estas seguro de eliminar?")' ]) !!}
                                        {!! Form::token() !!}
                                        @if ($item->products_count)
                                            <button class="btn btn-sm btn-danger" disabled> Eliminar ({{ $item->products_count}}) </button>
                                        @else
                                            <button class="btn btn-sm btn-danger"> Eliminar ({{ $item->products_count}}) </button>
                                        @endif
                                    {!! Form::close() !!}
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>No</th>
                                <th>Nombre</th>
                                <th>Icon</th>
                                <th>Accion</th>
                            </tr>
                        </tfoot>
                    </table>
                    <div class="text-center">
                        {{ $categories->render() }}
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
@endsection
