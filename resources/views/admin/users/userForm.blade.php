    
    <div class="form-group row">
            {{ Form::label('name', 'Nombre',[ 'class' => 'col-sm-2 col-form-label']) }}
            <div class="col-sm-10">
            {{ Form::text('name', null, [ 'class' => 'form-control', 'id' => 'name' ]) }}
            </div>
        </div>
    <div class="form-group row">
        {{ Form::label('last_name', 'Apellidos',[ 'class' => 'col-sm-2 col-form-label']) }}
        <div class="col-sm-10">
        {{ Form::text('last_name', null, [ 'class' => 'form-control', 'id' => 'last_name' ]) }}
        </div>
    </div>
    <div class="form-group row">
        {{ Form::label('email', 'Email',[ 'class' => 'col-sm-2 col-form-label']) }}
        <div class="col-sm-10">
        {{ Form::email('email', null, [ 'class' => 'form-control', 'id' => 'email' ]) }}
        </div>
    </div>
    <div class="form-group row">
        {{ Form::label('phone_number', 'Telefono',[ 'class' => 'col-sm-2 col-form-label']) }}
        <div class="col-sm-10">
        {{ Form::tel('phone_number', null, [ 'class' => 'form-control', 'id' => 'phone_number' ]) }}
        </div>
    </div>

    <div class="form-group row">
        {{ Form::label('role_id', 'Tipo de Usuario',[ 'class' => 'col-sm-2 col-form-label']) }}
        <div class="col-sm-10">
        {{ Form::select('role_id', \App\Role::pluck('name', 'id'), $user->role_id, [ 'class' => 'form-control', 'id' => 'role_id' ]) }}
        </div>
    </div>

    <div class="form-group row">
        <label class="col-sm-2 col-form-label"></label>
        <div class="col-sm-10">
          {{ Form::submit('Editar', ['class' => 'btn btn-yellow']) }}
        </div>
    </div>