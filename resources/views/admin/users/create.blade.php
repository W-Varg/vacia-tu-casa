@extends('layouts.admin')

@section('breadcrumb')
<section class="content-header">
    <h1> Registrar Gestor
        <a href="{{ route ('admin')}}" class="btn btn-sm pull-up btn-danger">Cancelar</a>
    </h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route ('admin')}}"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li class="breadcrumb-item active">Agregar gestor</li>
    </ol>
</section>
@endsection

@section('content')
<div class="box box-solid bg-dark">
    <div class="box-body">
        {!! Form::open(['route' => 'handlers.store', 'class'=>'form-horizontal form-element col-12', 'files' =>true ]) !!}
        @csrf
        <div class="row">
            {{-- name --}}
            <div class="col-md-6 col-sm-12">
                <div class="form-group row">
                    <label for="name" class="col-sm-3 control-label">Nombre</label>
                    {{ Form::text('name', null, [ 'class' => 'form-control col-8', 'id' => 'name' ]) }}
                    @error('name')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
            </div>
            {{-- apellido --}}
            <div class="col-md-6 col-sm-12">
                <div class="form-group row">
                    <label for="last_name" class="col-sm-3 control-label">Apellidos</label>
                    {{ Form::text('last_name', null, [ 'class' => 'form-control col-8', 'id' => 'last_name' ]) }}
                    @error('last_name')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
            </div>
        </div>

        <div class="row pt-8">
            {{-- codigo postal --}}
            <div class="col-md-6 col-sm-12">
                <div class="form-group row">
                    <label for="dni" class="col-sm-3 control-label">DNI</label>
                    {{ Form::text('dni', null, [ 'class' => 'form-control col-8', 'id' => 'dni' ]) }}
                    @error('dni')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
            </div>
            <div class="col-md-6 col-sm-12">
                {{-- codigo postal --}}
                <div class="form-group row">
                    <label for="code_postal" class="col-sm-3 control-label">Código Postal</label>
                    {{ Form::number('code_postal', null, [ 'class' => 'form-control col-8', 'id' => 'code_postal' ]) }}
                    @error('code_postal')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
            </div>
        </div>

        <div class="row">
            {{-- phone number --}}
            <div class="col-md-6 col-sm-12">
                <p class="control-label" style="display: block;text-align: initial;margin-bottom: 5px;">Telefono</p>
                <div class="form-control-2 row" id="phone_number">
                    <div class="col-sm-5 py-0">
                        <select class="form-control" name="countryCode">
                            <option value="+34" style="background-image:url({{ asset('images/flags/es.svg') }})"> España (+34) </option>
                            <option value="+351">Portugal (+351) </option>
                            <option value="+33">Francia (+33) </option>
                            <option value="+49">Alemania (+49) </option>
                            <option value="+39">Italia (+39) </option>
                            <optgroup label="Otros Paises">
                                <option value="+30">Grecia (+30) </option>
                                <option value="+31">Países Bajos (+31) </option>
                                <option value="+32">Bélgica (+32) </option>
                                <option value="+350">Gibraltar (+350) </option>
                                <option value="+352">Luxemburgo (+352) </option>
                                <option value="+353">Irlanda (+353) </option>
                                <option value="+354">Islandia (+354) </option>
                                <option value="+355">Albania (+355) </option>
                                <option value="+356">Malta (+356) </option>
                                <option value="+357">Chipre (+357) </option>
                                <option value="+358">Finlandia (+358) </option>
                                <option value="+359">Bulgaria (+359) </option>
                                <option value="+36">Hungría (+36) </option>
                                <option value="+370">Lituania (+370) </option>
                                <option value="+371">Letonia (+371) </option>
                                <option value="+372">Estonia (+372) </option>
                                <option value="+373">Moldavia (+373) </option>
                                <option value="+374">Armenia (+374) </option>
                                <option value="+375">Bielorrusia (+375) </option>
                                <option value="+376">Andorra (+376) </option>
                                <option value="+377">Mónaco (+377) </option>
                                <option value="+378">San Marino (+378) </option>
                                <option value="+379">Ciudad del Vaticano (+379) </option>
                                <option value="+380">Ucrania (+380) </option>
                                <option value="+381">Serbia (+381) </option>
                                <option value="+382">Montenegro (+382) </option>
                                <option value="+385">Croacia (+385) </option>
                                <option value="+386">Eslovenia (+386) </option>
                                <option value="+387">Bosnia (+387) </option>
                                <option value="+389">República de Macedonia (+389) </option>
                                <option value="+40">Rumania Rumania (+40) </option>
                                <option value="+41">Suiza (+41) </option>
                                <option value="+42">Checoslovaquia (+42) </option>
                                <option value="+420">República Checa (+420) </option>
                                <option value="+421">Eslovaquia (+421) </option>
                                <option value="+422">no asignado (+422) </option>
                                <option value="+423">Liechtenstein (+423) </option>
                                <option value="+43">Austria (+43) </option>
                                <option value="+44">Reino Unido (+44) </option>
                                <option value="+45">Dinamarca (+45) </option>
                                <option value="+46">Suecia (+46) </option>
                                <option value="+47">Noruega (+47) </option>
                                <option value="+48">Polonia (+48) </option>
                            </optgroup>
                        </select>
                    </div>
                    <div class="col-sm-7 py-0">
                        {{ Form::text('phone_number', null, [ 'class' => 'form-control col-8', 'id' => 'phone_number' ]) }}
                        @error('phone_number')
                        <span class="invalid-feedback" role="alert"> <strong>{{ $message }}</strong> </span>
                        @enderror
                    </div>
                </div>
            </div>

            {{-- email --}}
            <div class="col-md-6 col-sm-12">
                <div class="form-group row">
                    <label for="email" class="col-sm-3 control-label">Correo Electronico</label>
                    {{ Form::email('email', null, [ 'class' => 'form-control col-8', 'id' => 'email' ]) }}
                    @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-6 col-sm-12">
                {{-- password --}}
                <div class="form-group row">
                    <label for="password" class="col-sm-3 control-label">Contraseña</label>
                    {{ Form::password('password', null, [ 'class' => 'form-control col-8', 'id' => 'password' ]) }}
                    @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
            </div>
            <div class="col-md-6 col-sm-12">
                {{-- password 2--}}
                <div class="form-group row">
                    <label for="password-confirm" class="col-sm-4 control-label">Confirmar
                        Contraseña</label>
                    {{ Form::password('password_confirmation', null, [ 'class' => 'form-control col-8', 'id' => 'password_confirmation' ]) }}
                    @error('password_confirmation')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
            </div>
        </div>

        {{-- photo --}}
        <div class="form-group row">
            <label for="avatar" class="col-sm-3 control-label">Fotografia (Avatar)</label>
            <input type="file" class="form-control" id="avatar" name="avatar" accept='image/*' autofocus>
            @error('avatar')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>

        <div class="row">
            <div class="col-12">
                <div id="foto"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-12 text-center">
                <div class="form-group">
                    <div class="checkbox">
                        <input type="checkbox" id="condiciones">
                        <label for="condiciones"> Aceptar Condiciones</label>
                        <a href="#"></a>
                    </div>
                </div>
                <button type="submit" class="btn btn-success">Registrar Gestor</button>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
    <div class="box-footer">
        <div class="col-md-12 text-center">
            <div class="gallery" id="galeria"></div>
        </div>

    </div>
</div>
<style scoped>
    .gallery img {
        width: 150px;
        margin-top: 0.5em;
        margin-left: 0.5em;
        margin-bottom: 0.5em;
        margin-right: 0.5em;
    }
</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script>
    $(function () { // Multiple images preview in browser
        var imagesPreview = function (input, placeToInsertImagePreview) {
            if (input.files) {
                $('div#galeria > img').remove();
                for (i = 0; i < input.files.length; i++) {
                    var reader = new FileReader();
                    reader.onload = function (event) {
                        $($.parseHTML('<img>')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
                    }
                    reader.readAsDataURL(input.files[i]);
                }
            }
        };

        $('#avatar').on('change', function () {
            imagesPreview(this, 'div.gallery');
        });
    });
</script>
@endsection
