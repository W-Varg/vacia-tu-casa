<form class="form-horizontal form-element col-12" method="POST" action="{{ route('handlers.store') }}">
    @csrf
    <div class="row">
        {{-- name --}}
        <div class="col-md-6 col-sm-12">
            <div class="form-group row">
                <label for="name" class="col-sm-3 control-label">Nombre</label>
                {{ Form::text('name', $user->name, [ 'class' => 'form-control col-8', 'id' => 'name' ]) }}
                @error('name')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
        </div>
        {{-- apellido --}}
        <div class="col-md-6 col-sm-12">
            <div class="form-group row">
                <label for="last_name" class="col-sm-3 control-label">Apellidos</label>
                {{ Form::text('last_name', $user->profile->last_name, [ 'class' => 'form-control col-8', 'id' => 'last_name' ]) }}
                @error('last_name')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
        </div>
    </div>

    <hr>
    <div class="row pt-8">
        <div class="col-md-6 col-sm-12">
            {{-- codigo postal --}}
            <div class="form-group row">
                <label for="code_postal" class="col-sm-4 control-label">Código Postal</label>
                {{ Form::number('code_postal', $user->profile->code_postal, [ 'class' => 'form-control col-8', 'id' => 'code_postal' ]) }}
                @error('code_postal')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
        </div>
        <div class="col-md-6 col-sm-12">
            {{-- phone number --}}
            <div class="form-group row">
                <label for="phone_number" class="col-sm-3 control-label">Telefono</label>
                {{ Form::tel('phone_number', $user->profile->phone_number, [ 'class' => 'form-control col-8', 'id' => 'phone_number' ]) }}
                @error('phone_number')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
        </div>
    </div>

    {{-- email --}}
    <div class="form-group row">
        <label for="email" class="col-sm-2 control-label">Correo Electronico</label>
        <div class="col-sm-10">
            {{ Form::email('email', $user->email, [ 'class' => 'form-control col-8', 'id' => 'email' ]) }}
        </div>
        @error('email')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
    <hr>
    <div class="row">
        <div class="col-md-6 col-sm-12">
            {{-- password --}}
            <div class="form-group row">
                <label for="password" class="col-sm-3 control-label">Nueva Contraseña</label>
                <input type="password" class="form-control col-8" id="password" name="password" required autocomplete="new-password" autofocus
                    value="{{ $user->password }}">
                {{-- {{ Form::password('password', $user->password, [ 'class' => 'form-control col-8', 'id' => 'password' ]) }} --}}
                @error('password')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
        </div>
        <div class="col-md-6 col-sm-12">
            {{-- password 2--}}
            <div class="form-group row">
                <label for="password-confirm" class="col-sm-3 control-label">Confirmar
                    Contraseña</label>
                <input type="password" class="form-control col-8" id="password-confirm" name="password_confirmation" required
                    autocomplete="new-password" autofocus>
                @error('password_confirmation')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
        </div>
    </div>

    {{-- photo --}}
    <div class="form-group row">
        <label for="avatar" class="col-sm-3 control-label">Fotografia (Avatar)</label>
        <input type="file" class="form-control" id="avatar" name="avatar" autofocus>
        @error('avatar')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>

    <div id="foto"></div>

    <div class="form-group row">
        <div class="ml-auto col-sm-10">
            <div class="checkbox">
                <input type="checkbox" id="basic_checkbox_1" checked="">
                <label for="basic_checkbox_1"> Aceptar Condiciones</label>
                <a href="#"></a>
            </div>
        </div>
    </div>

    <div class="form-group row">
        <div class="ml-auto col-sm-10">
            <button type="submit" class="btn btn-success">Registrar Gestor</button>
        </div>
    </div>
</form>
