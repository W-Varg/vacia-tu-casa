@extends('layouts.admin')

@section('breadcrumb')
<section class="content-header">
    <h1>
        {{ $title }}
        @if ($add_gestor)
        <a href="{{ route ('handlers.create')}}" class="btn btn-sm pull-up btn-warning"> Agregar Nuevo Gestor</a>
        @endif
    </h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route ('admin')}}"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li class="breadcrumb-item active">Usuarios</li>
    </ol>
</section>
@endsection

@section('content')
<div class="row">
    <div class="col-md-12 col-12">
        <div class="box bg-inverse bg-dark">
            <div class="box-header with-border">
                {!! Form::open(['route' => 'users.index', 'method' => 'GET', 'class'=>['inline']]) !!}
                <div class="form-group">
                    <input class="form-control" placeholder="Buscar por Nombre o Código" value="{{$name}}" name="name" />
                </div>
                {!! Form::close() !!}
                <p class="box-subtitle"> cantidad de registros {{count($withAgrement) + count($outAgrement)}}</p>
            </div>
            <div class="box-body">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#withContrato" role="tab"><span><i
                                    class="fa  fa-file"></i></span> Con Contrato </a> </li>

                    <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#outContrato" role="tab"><span><i
                                    class="fa  fa-file-powerpoint-o"></i></span> Sin Contrato </a> </li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content tabcontent-border">

                    <div class="tab-pane active" id="withContrato" role="tabpanel">
                        <div class="pad">
                            <div class="media-list media-list-divided media-list-hover">
                                @foreach ($withAgrement as $user)
                                <div class="media">
                                    <a class="avatar avatar-lg" href="#">
                                        <img src="{{ asset($user->profile->avatar) }}" alt="...">
                                    </a>

                                    <div class="media-body">
                                        <p>
                                            <a class="text-white hover-info" href="#"><strong>{{ $user->name }} {{ $user->profile->last_name
                                                    }}</strong></a>
                                            @if ($user->handler)
                                            <span class="text-warning float-right"> Clientes:
                                                {{$user->handler->num_clientes($user->id)}}
                                            </span>
                                            @endif
                                        </p>

                                        <nav class="nav mt-2">
                                            <a class="btn btn-info btn-xs mx-2 text-white hover-info" href="{{ route ('profile', $user) }}"><i
                                                    class="fa fa-fw fa-user"></i> Ver Perfil</a>
                                            <a class="btn btn-success btn-xs mx-2 text-white hover-info"
                                                href="https://web.whatsapp.com/send?phone={{ $user->profile->phone_number }}" target="_blank"><i
                                                    class="fa fa-whatsapp"></i> Mensaje</a>
                                            {{-- @if ($add_gestor && auth()->user()->id != $user->id )
                                            {!! Form::open(['route' =>['handlers.destroy', $user], 'method' => 'delete',
                                            'class' => ['inline'],
                                            'onclick' =>'return confirm("Estas seguro de eliminar al gestor?")' ]) !!}
                                            {!! Form::token() !!}
                                            <button class="btn btn-xs mx-2 btn-danger"> <i class="fa fa-trash"></i> Eliminar a </button>
                                            {!! Form::close() !!}
                                            @endif --}}
                                            @if (auth()->user()->id != $user->id )
                                            {!! Form::open(['route' =>['users.disabled', $user], 'method' => 'delete', 'class' => ['inline'],
                                            'onclick' =>
                                            'return confirm("Estas seguro de eliminar al gestor?")' ]) !!}
                                            {!! Form::token() !!}
                                            <button class="btn btn-xs mx-2 btn-danger"> <i class="fa fa-trash"></i> Eliminar</button>
                                            {!! Form::close() !!}
                                            @endif
                                            <small class="sidetitle">{{$user->email}}</small>
                                            <small class="sidetitle"> CONTRATOS : {{ $user->agrements() }} </small>
                                        </nav>
                                    </div>

                                    <div class="media-right gap-items">
                                        <div class="btn-group">
                                            <a class="media-action lead" href="#" data-toggle="dropdown"><i
                                                    class="ion-android-more-vertical"></i></a>
                                            <div class="dropdown-menu dropdown-menu-right">
                                                <a class="dropdown-item" href="{{ route ('profile', $user) }}"><i class="fa fa-fw fa-user"></i>
                                                    ver Perfil</a>
                                                <a class="dropdown-item"
                                                    href="https://web.whatsapp.com/send?phone={{ $user->profile->phone_number }}"
                                                    target="_blank"><i class="fa fa-fw fa-comments"></i> Messages</a>
                                                <div class="dropdown-divider"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                            @if (count($withAgrement) > 0)
                            <nav class="mt-15 pb-10">
                                <ul class="pagination justify-content-center">
                                    {{ $withAgrement->appends($pagelinks)->links() }}
                                </ul>
                            </nav>
                            @endif
                        </div>
                    </div>


                    <div class="tab-pane pad" id="outContrato" role="tabpanel">
                        <div class="pad">
                            <div class="media-list media-list-divided media-list-hover">
                                @foreach ($outAgrement as $user)
                                <div class="media">
                                    <a class="avatar avatar-lg" href="#">
                                        <img src="{{ asset($user->profile->avatar) }}" alt="...">
                                    </a>

                                    <div class="media-body">
                                        <p>
                                            <a class="text-white hover-info" href="#"><strong>{{ $user->name }} {{ $user->profile->last_name
                                                    }}</strong></a>
                                            @if ($user->handler)
                                            <span class="text-warning float-right"> Clientes:
                                                {{$user->handler->num_clientes($user->id)}}
                                            </span>
                                            @endif
                                        </p>

                                        <nav class="nav mt-2">
                                            <a class="btn btn-info btn-xs mx-2 text-white hover-info" href="{{ route ('profile', $user) }}"><i
                                                    class="fa fa-fw fa-user"></i> Ver Perfil</a>
                                            <a class="btn btn-success btn-xs mx-2 text-white hover-info"
                                                href="https://web.whatsapp.com/send?phone={{ $user->profile->phone_number }}" target="_blank"><i
                                                    class="fa fa-whatsapp"></i> Mensaje</a>
                                            @if ($add_gestor && auth()->user()->id != $user->id )
                                            {!! Form::open(['route' =>['handlers.destroy', $user], 'method' => 'delete',
                                            'class' => ['inline'],
                                            'onclick' =>'return confirm("Estas seguro de eliminar al gestor?")' ]) !!}
                                            {!! Form::token() !!}
                                            <button class="btn btn-xs mx-2 btn-danger"> <i class="fa fa-trash"></i> Eliminar</button>
                                            {!! Form::close() !!}
                                            @endif
                                            @if (auth()->user()->id != $user->id )
                                            {!! Form::open(['route' =>['users.disabled', $user], 'method' => 'delete', 'class' => ['inline'],
                                            'onclick' =>
                                            'return confirm("Estas seguro de eliminar al gestor?")' ]) !!}
                                            {!! Form::token() !!}
                                            <button class="btn btn-xs mx-2 btn-danger"> <i class="fa fa-trash"></i> Eliminar</button>
                                            {!! Form::close() !!}
                                            @endif
                                            <small class="sidetitle">{{$user->email}}</small>
                                            <small class="sidetitle"> CONTRATOS : {{ $user->agrements() }} </small>
                                        </nav>
                                    </div>

                                    <div class="media-right gap-items">
                                        <div class="btn-group">
                                            <a class="media-action lead" href="#" data-toggle="dropdown"><i
                                                    class="ion-android-more-vertical"></i></a>
                                            <div class="dropdown-menu dropdown-menu-right">
                                                <a class="dropdown-item" href="{{ route ('profile', $user) }}"><i class="fa fa-fw fa-user"></i>
                                                    ver Perfil</a>
                                                <a class="dropdown-item"
                                                    href="https://web.whatsapp.com/send?phone={{ $user->profile->phone_number }}"
                                                    target="_blank"><i class="fa fa-fw fa-comments"></i> Messages</a>
                                                <div class="dropdown-divider"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                            </div>


                            @if (count($outAgrement) > 0)
                            <nav class="mt-15 pb-10">
                                <ul class="pagination justify-content-center">
                                    {{ $outAgrement->appends($pagelinks)->links() }}
                                </ul>
                            </nav>
                            @endif
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>

</div>
@endsection
