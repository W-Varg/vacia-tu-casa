@extends('layouts.admin')

@section('content')
<div class="row">
    <div class="col-12">
        <div class="box box-solid bg-dark">
            <div class="box-header with-border flex">
                <h3 class="box-title">Editar Cliente (Usuario)</h3>
                {{-- <h6 class="box-subtitle"> Listado de Clientes</h6> --}}
                <a href="{{ route ('users.index')}}" class="btn btn-danger float-right">Cancelar</a>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                {!! Form::model($user,['route' => ['users.update', $user->id], 'method'=> 'PUT']) !!}
                
                    @include('admin.users.userForm')
                
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection
