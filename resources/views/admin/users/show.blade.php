@extends('layouts.admin')

@section('breadcrumb')
<section class="content-header">
        <h1>
          Usuario: {{ $user->name }}
        </h1>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="{{ route ('admin')}}"><i class="fa fa-dashboard"></i> Admin</a></li>
          <li class="breadcrumb-item"><a href="{{ route ('users.index')}}">Usuarios</a></li>
          <li class="breadcrumb-item active">Perfil de Usuario</li>
        </ol>
      </section>  
@endsection

@section('content')
<div class="row">
        <div class="col-xl-4 col-lg-5">

          <!-- Profile Image -->
          <div class="box bg-yellow bg-deathstar-dark">
            <div class="box-body box-profile">
              <img class="profile-user-img rounded img-fluid mx-auto d-block" src="{{ asset($user->profile->avatar) }}" alt="User profile picture">

              <h2 class="profile-username text-center mb-0">{{ $user->name }}</h2>

              <h4 class="text-center mt-0"><i class="fa fa-envelope-o mr-10"></i> {{ $user->email }}</h4>
				
              <div class="row social-states">
				  <div class="col-6 text-right"><a href="#" class="link text-white"><i class="ion ion-ios-people-outline"></i> N° Clientes</a></div>
				  <div class="col-6 text-left"><a href="#" class="link text-white"><i class="ion ion-images"></i> N° Articulos</a></div>
			  </div>
            
              <div class="row">
				<h2 class="title w-p100 mt-10 mb-0 p-20">Ultimas Publicaciones</h2>
				<div class="col-12">
					<div class="media-list media-list-hover w-p100 mt-0">
						<h5 class="media media-single py-10 px-0 w-p100 justify-content-between">
							  <p>
							  <i class="fa fa-circle text-red pr-10 font-size-12"></i>Deal number 1548
							  <span class="subtitle pl-20 mt-10">by<span class="text-red">Johen Doe</span></span>						  
							  </p>
							  <p class="text-right pull-right"><span class="badge badge-sm badge-danger mb-10">sell</span><br>0.12458921 BTC</p>
						</h5>
						<h5 class="media media-single py-10 px-0 w-p100 justify-content-between">
							  <p>
							  <i class="fa fa-circle text-success pr-10 font-size-12"></i>Deal number 1548
							  <span class="subtitle pl-20 mt-10">by<span class="text-success">Johen Doe</span></span>						  
							  </p>
							  <p class="text-right pull-right"><span class="badge badge-sm badge-success mb-10">sell</span><br>0.12458921 BTC</p>
						</h5>
						<h5 class="media media-single py-10 px-0 w-p100 justify-content-between">
							  <p>
							  <i class="fa fa-circle text-success pr-10 font-size-12"></i>Deal number 1548
							  <span class="subtitle pl-20 mt-10">by<span class="text-success">Johen Doe</span></span>						  
							  </p>
							  <p class="text-right pull-right"><span class="badge badge-sm badge-success mb-10">sell</span><br>0.12458921 BTC</p>
						</h5>
						<h5 class="media media-single py-10 px-0 w-p100 justify-content-between">
							  <p>
							  <i class="fa fa-circle text-red pr-10 font-size-12"></i>Deal number 1548
							  <span class="subtitle pl-20 mt-10">by<span class="text-red">Johen Doe</span></span>						  
							  </p>
							  <p class="text-right pull-right"><span class="badge badge-sm badge-danger mb-10">sell</span><br>0.12458921 BTC</p>
						</h5>
						<h5 class="media media-single py-10 px-0 w-p100 justify-content-between">
							  <p>
							  <i class="fa fa-circle text-success pr-10 font-size-12"></i>Deal number 1548
							  <span class="subtitle pl-20 mt-10">by<span class="text-success">Johen Doe</span></span>						  
							  </p>
							  <p class="text-right pull-right"><span class="badge badge-sm badge-success mb-10">sell</span><br>0.12458921 BTC</p>
						</h5>
					</div>
				</div>
              </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
        <div class="col-xl-8 col-lg-7">
          <div class="box box-solid bg-black">
			<div class="box-header with-border">
			  <h3 class="box-title">Datos Personales</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
			  <div class="row">
				<div class="col-12">
                    {!! Form::model($user,['route' => ['users.update', $user->id], 'method'=> 'PUT']) !!}
                
                        @include('admin.users.userForm')
                    
                    {!! Form::close() !!}
				</div>
				<!-- /.col -->
			  </div>
			  <!-- /.row -->
			</div>
			<!-- /.box-body -->
		  </div>
		  <!-- /.box -->
          <div class="box box-solid bg-black">
			<div class="box-header with-border">
			  <h3 class="box-title">Dirección Personal</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
			  <div class="row">
				<div class="col-12">
					<div class="form-group row">
					  <label class="col-sm-2 col-form-label">Calle</label>
					  <div class="col-sm-10">
						<input class="form-control" type="text" placeholder="A-458, Lorem Ipsum, city">
					  </div>
					</div>
					<div class="form-group row">
					  <label class="col-sm-2 col-form-label">Ciudad</label>
					  <div class="col-sm-10">
						<input class="form-control" type="text" placeholder="Your City">
					  </div>
					</div>
					<div class="form-group row">
					  <label class="col-sm-2 col-form-label">Estado</label>
					  <div class="col-sm-10">
						<input class="form-control" type="text" placeholder="Your State">
					  </div>
					</div>
					<div class="form-group row">
					  <label class="col-sm-2 col-form-label">Codigo Postal</label>
					  <div class="col-sm-10">
						<input class="form-control" type="number" placeholder="123456">
					  </div>
					</div>
					<div class="form-group row">
					  <label class="col-sm-2 col-form-label"></label>
					  <div class="col-sm-10">
						<button type="submit" class="btn btn-yellow">Editar</button>
					  </div>
					</div>
				</div>
				<!-- /.col -->
			  </div>
			  <!-- /.row -->
			</div>
			<!-- /.box-body -->
		  </div>
        </div>
        <!-- /.col -->
      </div>
@endsection
