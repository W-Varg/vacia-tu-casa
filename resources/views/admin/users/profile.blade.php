@extends('layouts.admin')

@section('breadcrumb')
<section class="content-header">
    <h1>Perfil de Usuario @if ($user->handler) Gestor @endif </h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route ('admin')}}"><i class="fa fa-dashboard"></i> Admin</a></li>
        <li class="breadcrumb-item"><a href="{{ route ('users.index')}}">Usuarios</a></li>
        <li class="breadcrumb-item active"> Perfil de Usuario
            @if ($user->handler) Gestor @endif
        </li>
    </ol>
</section>
@endsection

@section('content')
<div class="row">
    <div class="col-lg-3 col-12">

        <!-- Profile Image -->
        <div class="box bg-inverse bg-dark bg-hexagons-white">
            <div class="box-body box-profile">
                <img class="profile-user-img rounded-circle img-fluid mx-auto d-block" src="{{ asset($user->profile->avatar) }}"
                    alt="User profile picture">

                <h3 class="profile-username text-center">{{ $user->name }} {{ $user->profile->last_name}} </h3>

                <div class="row social-states">
                    @if ($user->handler)
                    <p class="col-6 text-center"> Clientes</p>
                    <div class="col-6 text-center"><i class="ion ion-ios-people-outline"></i> {{$user->handler->num_clientes($user->id)}}</div>
                    @endif
                </div>

                <div class="row">
                    <div class="col-12 pt-0">
                        <div class="profile-user-info mt-0">
                            <p>Correo Electronico </p>
                            <h6 class="margin-bottom"> {{$user->email}} </h6>
                            <p>Telefono</p>
                            <h6 class="margin-bottom">
                                <a class="btn btn-success btn-xs mx-2 text-white hover-info"
                                    href="https://web.whatsapp.com/send?phone={{$user->profile->phone_number}}"
                                    target="_blank">{{$user->profile->phone_number}} <i class="fa fa-whatsapp"></i></a>
                            </h6>
                            <p>Código Postal</p>
                            <h6 class="margin-bottom"> {{$user->profile->code_postal}} </h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.col -->
    <div class="col-lg-9 col-12">
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li><a class="active" href="#activity" data-toggle="tab">Clientes</a></li>
                <li><a class="" href="#contratos" data-toggle="tab">Contratos</a></li>
                @if (auth()->user()->id == $user->id)
                <li><a href="#settings" data-toggle="tab">Datos Personales</a></li>
                @endif
            </ul>

            <div class="tab-content">
                <!-- /.tab-pane -->

                <div class="active tab-pane" id="activity">
                    <!-- Post -->
                    <div class="post clearfix">
                        @forelse ($clients as $client)
                        <div class="user-block">
                            <img class="img-bordered-sm rounded-circle" src="{{ asset($client->profile->avatar) }}" alt="user image">
                            <span class="username">
                                <a href="">{{$client->name}} {{$client->profile->last_name}}</a>
                                <a href="{{ route('profile', $client)}}" class="btn btn-info pull-right btn-sm"> Detalle</a>
                                <a class="btn btn-success btn-xs mx-2 text-white hover-info"
                                    href="https://web.whatsapp.com/send?phone={{ $client->profile->phone_number }}" target="_blank"><i
                                        class="fa fa-whatsapp"></i> Mensaje</a>
                            </span>
                            <span class="description"> {{ $client->email }} </span>
                        </div>
                        @empty
                        <p>...</p>
                        @endforelse
                    </div>
                </div>
                <div class="tab-pane" id="contratos">
                    <!-- Post -->
                    <div class="post clearfix">
                        @forelse ($contratos as $agreement)
                        {{-- <p>
                            CONTRATO: <span> {{ $agreement->name }} </span> <span class=""> {{ $agreement->status }} </span> <a
                                href="{{ route('agreements.detail', $agreement)}}" class="ml-2 btn btn-sm btn-warning">{{ __('ver de Contrato')
                                }} </a>
                        </p> --}}
                        <div class="user-block">
                            <span class="username">
                                <a href="{{ route('agreements.detail', $agreement)}}"> Contrato: {{$agreement->name}}</a>
                                <a href="{{ route('agreements.detail', $agreement)}}" class="btn btn-info pull-right btn-sm"> Ver Contrato</a>
                                {{-- <a class="btn btn-success btn-xs mx-2 text-white hover-info"
                                    href="https://web.whatsapp.com/send?phone={{ $client->profile->phone_number }}" target="_blank"><i
                                        class="fa fa-whatsapp"></i> Mensaje</a> --}}
                                <button class="btn btn-sm mx-2 btn-default" disabled>
                                    <i class="fa fa-caret-right"></i> Publicados {{ $agreement->published() }}
                                </button>
                                <button class="btn btn-sm mx-2 btn-default" disabled>
                                    <i class="fa fa-caret-right"></i> Vendidos {{ $agreement->sold() }}
                                </button>
                            </span>
                            <span class="description"> Estado : {{ $agreement->status }} </span>
                        </div>
                        @empty
                        <p>...</p>
                        @endforelse
                    </div>
                </div>
                <!-- Configuraciones, gestor datos -->
                @if (auth()->user()->id == $user->id)

                <div class="tab-pane" id="settings">
                    {!! Form::model($user,['route' => ['handlers.update', $user], 'method'=> 'PUT', 'class'=> 'form-horizontal form-element
                    col-12', 'files' => true]) !!}
                    @csrf
                    <div class="row">
                        {{-- name --}}
                        <div class="col-sm-12">
                            <div class="form-group row">
                                <label for="name" class="col-sm-3 control-label">Nombre</label>
                                {{ Form::text('name', $user->name, [ 'class' => 'form-control col-8', 'id' => 'name' ]) }}
                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>

                        </div>
                    </div>
                    <div class="row">
                        {{-- apellido --}}
                        <div class="col-sm-12">
                            <div class="form-group row">
                                <label for="last_name" class="col-sm-3 control-label">Apellidos</label>
                                {{ Form::text('last_name', $user->profile->last_name, [ 'class' => 'form-control col-8', 'id' => 'last_name' ])
                                }}
                                @error('last_name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        {{-- apellido --}}
                        <div class="col-sm-12">
                            <div class="form-group row">
                                <label for="dni" class="col-sm-3 control-label">DNI</label>
                                {{ Form::text('dni', $user->profile->dni, [ 'class' => 'form-control col-8', 'id' => 'dni' ]) }}
                                @error('dni')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="row pt-8">
                        <div class="col-sm-12">
                            {{-- codigo postal --}}
                            <div class="form-group row">
                                <label for="code_postal" class="col-sm-3 control-label">Código Postal</label>
                                {{ Form::number('code_postal', $user->profile->code_postal, [ 'class' => 'form-control col-8', 'id' =>
                                'code_postal' ]) }}
                                @error('code_postal')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            {{-- phone number --}}
                            <div class="form-group row">
                                <label for="phone_number" class="col-sm-3 control-label">Telefono</label>
                                {{ Form::tel('phone_number', $user->profile->phone_number, [ 'class' => 'form-control col-8', 'id' =>
                                'phone_number' ]) }}
                                @error('phone_number')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <hr>
                    {{-- photo --}}
                    <div class="form-group row">
                        <label for="avatar" class="col-sm-3 control-label">Fotografia (Avatar)</label>
                        <input type="file" class="form-control" id="avatar" name="avatar" autofocus>
                        @error('avatar')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>

                    <div id="foto"></div>

                    <div class="form-group row">
                        <div class="ml-auto col-sm-10">
                            <button type="submit" class="btn btn-success">Guardar Cambios</button>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
                @endif
                <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
        </div>
        <!-- /.nav-tabs-custom -->
    </div>
    <!-- /.col -->
</div>
@endsection
