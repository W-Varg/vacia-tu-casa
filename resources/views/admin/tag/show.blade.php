@extends('layouts.admin')

@section('content')
<div class="row">
    <div class="col-12">
        <div class="box box-solid bg-dark">
            <div class="box-header with-border flex">
                <h3 class="box-title">Ver Material</h3>
                {{-- <h6 class="box-subtitle"> Listado de Etiquetas</h6> --}}
                <a href="{{ route ('tags.index')}}" class="btn btn-danger float-right">Listar Materiales</a>
            </div>
            <div class="box-body">
                <p><strong>Nombre</strong> {{ $tag->name }} </p>
                <p><strong>Slug</strong> {{ $tag->slug }} </p>
            </div>
            <div class="box-footer text-center">
                    <a href="{{ route ('tags.edit', $tag)}}" class="btn btn-info">Editar Material</a>
            </div>
        </div>
    </div>
</div>
@endsection
