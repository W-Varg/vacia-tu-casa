<div class="form-group">
    {{ Form::label('name', 'Nombre del Material') }}
    {{ Form::text('name',null, [ 'class' => 'form-control', 'id' => 'name' ]) }}
</div>
<div class="form-group">
    {{-- {{ Form::label('slug', 'URL Amigable') }} --}}
    {{ Form::hidden('slug',null, [ 'class' => 'form-control', 'id' => 'slug' ]) }}
</div>
<div class="form-group">
    {{ Form::submit('Guardar', ['class' => 'btn btn-sm btn-primary']) }}
</div>

@section('scripts')
<script src="{{ asset('js/jquery.stringToSlug.min.js') }}"></script>
<script>
    $(document).ready(()=>{
        $("#name").stringToSlug({
            callback: (text)=>{ $('#slug').val(text); }
        });
    });
</script>
@endsection

