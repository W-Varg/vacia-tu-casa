@extends('layouts.admin')

@section('content')
<div class="row">
    <div class="col-12">
        <div class="box box-solid bg-dark">
            <div class="box-header with-border flex">
                <h3 class="box-title">Crear Nuevo Material</h3>
                {{-- <h6 class="box-subtitle"> Listado de Materiales</h6> --}}
                <a href="{{ route ('tags.index')}}" class="btn btn-danger float-right">Cancelar</a>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                {!! Form::open(['route' => 'tags.store']) !!}

                    @include('admin.tag.tagForm')
                
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection
