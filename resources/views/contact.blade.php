@extends('layouts.web')
@section('content')
{!! NoCaptcha::renderJs() !!}
<section class="main-header" style="background-image:url(assets/images/contacto.jpg)">
    <header>
        <div class="container text-center">
            <h2 class="h2 title">Contáctanos</h2>
            <ol class="breadcrumb breadcrumb-inverted">
                <li><a href="/"><span class="icon icon-home"></span></a></li>

                <li><a class="active" href="">Formulario de Contacto</a></li>

            </ol>
        </div>
    </header>
</section>

<section class="contact">
    <!-- === Goolge map === -->
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                @include('client.messages')
            </div>
        </div>
    </div>

    <div id="map"></div>
    <div class="container">
        <div class="row">
            <div class="col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1">
                <div class="contact-block">
                    <div class="contact-info">
                        <div class="row">
                            <div class="col-sm-6">
                                <figure class="text-center">
                                    <span class="icon icon-map-marker"></span>
                                    <figcaption>
                                        <strong>Donde encontrarnos</strong>

                                        <span>{{$company->address}}</span>
                                    </figcaption>
                                </figure>
                            </div>
                            <div class="col-sm-6">
                                <figure class="text-center">
                                    <span class="icon icon-envelope"></span>
                                    <figcaption>
                                        <strong>Correo</strong>
                                        <span>
                                            <strong>{{$company->email}}</strong>
                                        </span>
                                    </figcaption>
                                </figure>
                            </div>
                        </div>
                    </div>

                    <div class="banner">
                        <div class="row">
                            <div class="col-md-offset-1 col-md-10 text-center">
                                <h2 class="title">Escr&iacute;benos un correo</h2>
                                <p>
                                    En Vacíatucasa estamos a tu disposición para atender todas tus consultas y comentarios. Puedes utilizar el
                                    formulario que tienes más abajo para contactar con nosotros.
                                </p>

                                <div class="contact-form-wrapper">

                                    <a class="btn btn-clean open-form" data-text-open="Contacta con nostros"
                                        data-text-close="Cerrar Formulario">Contacta con nosotros</a>

                                    <div class="contact-form clearfix">
                                        {!! Form::open(['route' => 'contact.store' , 'id' => 'formContacto' ]) !!}
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <input id="name" name="name" type="text" value="" class="form-control" required
                                                        placeholder="Nombre">
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <input id="email" name="email" type="email" value="" class="form-control"
                                                        placeholder="Correo" required>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <input id="phone_number" name="phone_number" type="phone" class="form-control"
                                                        placeholder="Teléfono" required>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <input id="city" name="city" type="text" value="" class="form-control" placeholder="Ciudad">
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <input id="subject" name="subject" type="text" value="" class="form-control"
                                                        placeholder="Asunto" required>
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <textarea id="description" name="description" class="form-control"
                                                        placeholder="Consultas y comentarios" rows="10"></textarea>
                                                </div>
                                            </div>
                                            <div class="col-md-12 text-center">
                                                <div class="form-group{{ $errors->has('g-recaptcha-response') ? ' has-error' : '' }}">
                                                    {!! app('captcha')->display() !!}
                                                    @if ($errors->has('g-recaptcha-response'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('g-recaptcha-response') }}</strong>
                                                    </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-md-12 text-center">
                                                <input type="submit" class="btn btn-clean" value="Enviar" />
                                            </div>
                                        </div>
                                        {!! Form::close() !!}
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>


            </div>
            <!--col-sm-8-->
        </div>
        <!--/row-->
    </div>
    <!--/container-->
</section>

@endsection

@section('scripts')
<script>
    function initMap() {
                var contentString =
                '<div class="map-info-window">' +
                '<p><img src="{{ asset('assets/images/logo.png') }}" alt=""></p>' +
                '<p><strong> </strong></p>' +
                '<p><i class="fa fa-map-marker"></i> Calle Asturias 9, Leganés, 28913</p>' +
                '<p><i class="fa fa-phone"></i> </p>' +
                '<p><i class="fa fa-clock-o"></i> </p>' +
                '</div>';

                var infowindow = new google.maps.InfoWindow({
                    content: contentString
                });
                //set default pposition
                var myLatLng = { lat: 40.3313089, lng: -3.7709906 };
                var map = new google.maps.Map(document.getElementById('map'), {
                    zoom: 12,
                    center: myLatLng,
                    styles: [{ "featureType": "administrative", "elementType": "all", "stylers": [{ "visibility": "on" }, { "saturation": -100 }, { "lightness": 20 }] }, { "featureType": "road", "elementType": "all", "stylers": [{ "visibility": "on" }, { "saturation": -100 }, { "lightness": 40 }] }, { "featureType": "water", "elementType": "all", "stylers": [{ "visibility": "on" }, { "saturation": -10 }, { "lightness": 30 }] }, { "featureType": "landscape.man_made", "elementType": "all", "stylers": [{ "visibility": "simplified" }, { "saturation": -60 }, { "lightness": 10 }] }, { "featureType": "landscape.natural", "elementType": "all", "stylers": [{ "visibility": "simplified" }, { "saturation": -60 }, { "lightness": 60 }] }, { "featureType": "poi", "elementType": "all", "stylers": [{ "visibility": "off" }, { "saturation": -100 }, { "lightness": 60 }] }, { "featureType": "transit", "elementType": "all", "stylers": [{ "visibility": "off" }, { "saturation": -100 }, { "lightness": 60 }] }]
                });
                //set marker
                var image = 'assets/images/map-icon.png';
                var marker = new google.maps.Marker({
                    position: myLatLng,
                    map: map,
                    title: "Vacia tu casa!",
                    icon: image
                });
                marker.addListener('click', function () {
                    infowindow.open(map, marker);
                });
            }
</script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCOAO9oWWZN1fOVHBaWP1vHAnTfU4zRV4Q&callback=initMap"></script>
@endsection
