@extends('layouts.web')
@section('content')
<style>
    /* .owl-carousel .owl-item img {
        width: 100%;
        height: 500px;
    } */
    .oportunidad {
        color: #235952;
    }

    .vendido {
        color: #ffff;
        background: #ec275d;
    }

    .label {
        font-weight: bold;
    }

</style>
<span sds="{{ $company = \App\Company::get()->first() }}"></span>
<section class="header-content">
    <div class="owl-slider">
        <div class="item" style="background-image:url({{ asset($company->img1) }})">
            <div class="box">
                <div class="container">
                    <h2 class="title animated h1" data-animation="fadeInDown">{{$company->texto1}}</h2>
                    <h1 class="h2 title">
                        <div class="row">
                            <div class="col-md-4 col-sm-12">
                                {!! Form::open(['route' => 'page.more_product', 'method' => 'GET', 'class'=>['inline']])
                                !!}
                                <div class="form-group">
                                    <input class="form-control" placeholder="Buscar por Nombre" name="name" />
                                </div>
                                {!! Form::close() !!}
                            </div>
                            <div class="col-md-4 col-sm-12">
                                {!! Form::open(['route' => 'page.more_product', 'method' => 'GET', 'class'=>['inline']])
                                !!}
                                <div class="form-group">
                                    <input class="form-control" placeholder="Buscar por Estancias... " name="estancia" />
                                </div>
                                {!! Form::close() !!}
                            </div>
                            <div class="col-md-4 col-sm-12">
                                {!! Form::open(['route' => 'page.more_product', 'method' => 'GET', 'class'=>['inline']])
                                !!}
                                <div class="form-group">
                                    <input class="form-control" placeholder="Buscar Ciudad o Código postal... " name="ciudad" />
                                </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </h1>
                    <div class="animated" data-animation="fadeInUp">
                        <a href="{{ route('quiero-vender') }}" class="btn btn-main">Quiero Vender</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="item" style="background-image:url({{ asset($company->img2) }})">
            <div class="box">
                <div class="container">
                    <h2 class="title animated h1" data-animation="fadeInDown"> {{$company->texto2}}</h2>
                    <h1 class="h2 title">
                        <div class="row">
                            <div class="col-md-4 col-sm-12">
                                {!! Form::open(['route' => 'page.more_product', 'method' => 'GET', 'class'=>['inline']])
                                !!}
                                <div class="form-group">
                                    <input class="form-control" placeholder="Buscar por Nombre" name="name" />
                                </div>
                                {!! Form::close() !!}
                            </div>
                            <div class="col-md-4 col-sm-12">
                                {!! Form::open(['route' => 'page.more_product', 'method' => 'GET', 'class'=>['inline']])
                                !!}
                                <div class="form-group">
                                    <input class="form-control" placeholder="Buscar por Estancias... " name="estancia" />
                                </div>
                                {!! Form::close() !!}
                            </div>
                            <div class="col-md-4 col-sm-12">
                                {!! Form::open(['route' => 'page.more_product', 'method' => 'GET', 'class'=>['inline']])
                                !!}
                                <div class="form-group">
                                    <input class="form-control" placeholder="Buscar Ciudad o Código postal... " name="ciudad" />
                                </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </h1>
                    <div class="animated" data-animation="fadeInUp">
                        <a href="{{ route('quiero-vender') }}" class="btn btn-main">Quiero Vender</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="item" style="background-image:url({{ asset($company->img3) }})">
            <div class="box">
                <div class="container">
                    <h2 class="title animated h1" data-animation="fadeInDown">{{$company->texto3}}</h2>
                    <h1 class="h2 title">
                        <div class="row">
                            <div class="col-md-4 col-sm-12">
                                {!! Form::open(['route' => 'page.more_product', 'method' => 'GET', 'class'=>['inline']])
                                !!}
                                <div class="form-group">
                                    <input class="form-control" placeholder="Buscar por Nombre" name="name" />
                                </div>
                                {!! Form::close() !!}
                            </div>
                            <div class="col-md-4 col-sm-12">
                                {!! Form::open(['route' => 'page.more_product', 'method' => 'GET', 'class'=>['inline']])
                                !!}
                                <div class="form-group">
                                    <input class="form-control" placeholder="Buscar por Estancias... " name="estancia" />
                                </div>
                                {!! Form::close() !!}
                            </div>
                            <div class="col-md-4 col-sm-12">
                                {!! Form::open(['route' => 'page.more_product', 'method' => 'GET', 'class'=>['inline']])
                                !!}
                                <div class="form-group">
                                    <input class="form-control" placeholder="Buscar Ciudad o Código postal... " name="ciudad" />
                                </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </h1>
                    <div class="animated" data-animation="fadeInUp">
                        <a href="{{ route('quiero-vender') }}" class="btn btn-main">Quiero Vender</a>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!--/owl-slider-->
</section>

<!-- ======================== Menu Icons slider ======================== -->

@include('partials/category/menu-slide')

<!-- ========================  Products widget ======================== -->

<section class="products pb-1e">

    <div class="ma-2">
        @if (session('info')) {{--para mostrar los sms --}}
        <div class="mt-2">
            <div class="col-12">
                <div class="alert alert-success alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h5><i class="fa fa-check"></i> {{ session('info') }} </h5>
                </div>
            </div>
        </div>
        @endif

        <header>
            <div class="row">
                <div class="col-md-offset-2 col-md-8 text-center">
                    <h2 class="title">HOME TO HOME</h2>
                    <div class="otamendi s-60">
                        <p> Vacia tu casa y Decora tu hogar</p>
                    </div>
                </div>
            </div>
        </header>

        <!-- === product-item === -->
        <div class="row">
            @foreach ($products as $item)
            <div class="col-md-3 col-xs-12 d-flex align-items-stretch my-1">
                <article>
                    <div class="fondo">
                        <div class="info">
                            <span class="add-favorite bg-sucess">
                                @if ($item->client_agreement)
                                {{-- tiene contrato --}}
                                <a data-title="Iniciar Chat" data-title-added="Enviar sms" href="https://web.whatsapp.com/send?phone={{ $item->client_agreement->handler->user->profile->phone_number }}" target="_blank"><i class="fa fa-whatsapp"></i>
                                </a>
                                @else
                                {{-- basico --}}
                                <a data-title="Iniciar Chat" data-title-added="Enviar sms" href="https://web.whatsapp.com/send?phone={{ $item->user->profile->phone_number }}" target="_blank"><i class="fa fa-whatsapp"></i>
                                </a>
                                @endif
                            </span>

                            <span class="view">
                                <a href="#productid{{ $item->id }}" class="mfp-open" data-title="Ver Detalles"><i class="icon icon-eye"></i></a>
                            </span>
                            <span class="shared" onclick="copyToClipboard('{{Request::url()}}/detalle/{{$item->id}}')">
                                @if ($item->client_agreement)
                                {{-- tiene contrato --}}
                                <a data-title="Copiar Enlace para compartir" data-title-added="Clipboard" id="Clipboard"><i class="fa fa-share"></i>
                                </a>
                                @else
                                {{-- basico --}}
                                <a data-title="Copiar Enlace para compartir" data-title-added="Clipboard" id="Clipboard"><i class="fa fa-share"></i>
                                </a>
                                @endif
                            </span>
                        </div>
                        <div class="btn btn-add">
                            <a class="" href=" {{route('page.comprar', $item)}} "><i class="icon icon-cart"></i></a>
                        </div>
                        <div class="figure-grid">
                            @if ($item->variant == 'NOVEDAD')
                            <span class="label label-info">{{$item->variant}} </span>
                            @endif
                            @if ($item->variant == 'OFERTA')
                            <span class="label label-primary">{{$item->variant}}</span>
                            @endif
                            @if ($item->variant == 'OPORTUNIDAD')
                            <span class="label label-danger oportunidad">{{$item->variant}}</span>
                            @endif
                            @if ($item->variant == 'VENDIDO')
                            <span class="label vendido">{{$item->variant}}</span>
                            @endif
                            <div class="image pt-2">
                                <a href="#productid{{ $item->id }}" class="mfp-open">
                                    <div class="image-container">
                                        <img src="{{ asset($item->img) }}" alt="" class="img-responsive center-block" />
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="text-center bg-white">
                            <p class="box-title title-article"><a href="{{ route('page.detail_product', $item->id) }}">{{ $item->name }}</a></p>
                            <hr class="borde-grueso">
                            <p class="text-center fs-1-4 flexbox-price">
                                @if ($item->reduced_price)
                                <sub class="precio-normal"> {{ $item->reduced_price}} <i class="fa fa-euro"></i></sub>
                                <sub class="tachado"> {{ $item->normal_price}} <i class="fa fa-euro"></i></sub>
                                @else
                                <sub class="precio-normal">{{ $item->normal_price}} <i class="fa fa-euro"></i></sub>
                                @endif
                            </p>
                        </div>
                    </div>
                </article>
            </div>
            @endforeach

        </div>
        <!--/row-->
        <!-- === button more === -->
        <div class="row">
            <div class="pagination-wrapper">
                {{-- {{ $products->links() }} --}}
            </div>
        </div>


        <div class="wrapper-more">
            <a href="{{ route('page.more_product', 'page=2') }}" class="btn btn-main">Ver mas art&iacute;culos</a>
        </div>

        <!-- ========================  Product info popup - quick view ======================== -->

        @foreach ($products as $item)
        <div class="popup-main mfp-hide" id="productid{{ $item->id }}">
            <!-- === product popup === -->
            <div class="product">
                <!-- === popup-title === -->
                <div class="popup-title" style="padding-right: 50px">
                    <div class="h1 title">{{ $item->name }}<small> <a class="text-red" href="{{ route('page.category', $item->category->slug )}}">{{ $item->category->name }}</a></small>
                    </div>
                </div>
                <!-- === product gallery === -->
                <div class="owl-product-gallery">
                    @foreach ($item->images as $img)
                    <img src="{{ $img->path }}" alt="" />
                    @endforeach
                </div>

                <!-- === product-popup-info === -->

                <div class="popup-content">
                    <div class="product-info-wrapper">
                        <div class="row">

                            <!-- === left-column === -->
                            <div class="col-sm-6">
                                <div class="info-box">
                                    <strong>Contacto:</strong>
                                    <span>{{ $item->user->name }}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--/row-->
                </div>
                <!--/product-info-wrapper-->
            </div>
            <!--/popup-content-->
            <!-- === product-popup-footer === -->

            <div class="popup-table">
                <div class="popup-cell">
                    <div class="price">
                        @if ($item->reduced_price)
                        <span class="h3"> {{$item->reduced_price}} <i class="fa fa-euro"></i> <small> <i class="fa fa-euro"></i>
                                {{$item->normal_price}}</small></span>
                        @else
                        <span class="h3"> {{$item->normal_price}} <i class="fa fa-euro"></i> <small></small></span>
                        @endif
                    </div>
                </div>
                <div class="popup-cell">
                    <div class="popup-buttons">
                        <a href="{{ route('page.detail_product', $item->id) }}"><span class="icon icon-eye"></span>
                            <span class="hidden-xs">Mas Detalles</span></a>
                        <a href="{{ route('page.comprar', $item)}}"> <span class="icon icon-cart"></span> <span class="hidden-xs">Comprar</span></a>
                    </div>
                </div>
            </div>

        </div>
        <!--/product-->
        @endforeach
        @if (!Session::get('show-modal'))
        <a href="#modalActivate" class="mfp-open" id="modalActivateclick"> </a>
        @endif
        <div class="popup-main-modal mfp-hide" id="modalActivate">
            <!-- === product popup === -->
            <div class="product">
                <div class="owl-product-gallery">
                    <img src="{{ asset('images/modalPhoto.png') }}" class="img-responsive center-block" alt="" />
                </div>
                <div class="popup-content">
                    <div class="text-center">
                        <h1 class="px92">ENVÍO GRATIS</h1>
                        <h1 class="px57">EN TU PRIMERA COMPRA</h1>
                        <p class="px37">APLICABLE A NOVEDADES. </p>
                        <p class="px37">SOLO EN LA COMUNIDAD DE MADRID</p>
                        <a class="btn btn-main px42" href="{{ route('page.more_product','close=true&ofertas=premium') }}">COMPRAR AHORA</a>
                        {{-- <p class="px20">Aplican términos y condiciones </p> --}}
                    </div>
                </div>
            </div>

        </div>
    </div>
    <!--/container-->
</section>

<section class="instagram">
    <header>
        <div class="row">
            <div class="col-md-offset-2 col-md-8 text-center">
                <div class="otamendi text s-60">
                    <p> Muebles de casa a casa</p>
                </div>
            </div>
        </div>
    </header>
    <div class="gallery clearfix">
        <a href="{{ route('page.more_product', 'ofertas=premium') }}" class="item"><img src="{{ asset('assets/banners/secciones-01.jpg') }}" alt="Alternate Text" /></a>
        <a href="{{ route('page.more_product', 'ofertas=nuevo') }}" class="item"><img src="{{ asset('assets/banners/secciones-02.jpg') }}" alt="Alternate Text" /></a>
        <a href="{{ route('page.more_product', 'ofertas=oportunidad') }}" class="item"><img src="{{ asset('assets/banners/secciones-03.jpg') }}" alt="Alternate Text" /></a>
        <a href="{{ route('page.more_product', 'beliani=true') }}" class="item"><img src="{{ asset('assets/banners/secciones-04.jpg') }}" alt="Alternate Text" /></a>
        <a href="{{ route('page.more_product', 'ofertas=oulet') }}" class="item"><img src="{{ asset('assets/banners/secciones-05.jpg') }}" alt="Alternate Text" /></a>
    </div>
</section>

<!-- ========================  Blog Block ======================== -->
<section class="blog blog-block py-30 background-white">
    <div class="container">
        <!-- === blog header === -->
        <header>
            <div class="row">
                <div class="col-md-offset-2 col-md-8 text-center">
                    {{-- <h2 class="title h2">Los muebles ya no se tiran</h2> --}}
                    <div class="otamendi text s-60">
                        <p> Los muebles ya no se tiran</p>
                    </div>
                </div>
            </div>
        </header>

        <div class="row">
            <!-- === blog item === -->
            <div class="col-sm-3">
                <article>
                    <a href="{{route ('homestaging')}}">
                        <div class="image">
                            <img src="{{ asset('assets/images/home_staging.jpg') }}" alt="" />
                        </div>
                        <div class="entry entry-block">
                            <div class="title">
                                {{-- <h2 class="h4">HOME-STAGING. DECORA PARA VENDER</h2> --}}
                            </div>
                            <div class="description">
                            </div>
                        </div>
                        <div class="show-more">
                            <span class="btn btn-main btn-block">Ver detalles</span>
                        </div>
                    </a>
                </article>
            </div>
            <!-- === blog item === -->
            <div class="col-sm-3">
                <article>
                    <a href="{{route ('saldos')}}">
                        <div class="image">
                            <img src="{{ asset('assets/images/saldos.jpg') }}" alt="" />
                        </div>
                        <div class="entry entry-block">
                            <div class="title">
                                {{-- <h2 class="h4">INSPIRACIÓN. EN CASA DE NUESTROS CLIENTES </h2> --}}
                            </div>
                            <div class="description"> </div>
                        </div>
                        <div class="show-more">
                            <span class="btn btn-main btn-block">Ver detalles</span>
                        </div>
                    </a>
                </article>
            </div>

            <!-- === blog item === -->
            <div class="col-sm-3">
                <article>
                    <a href="#EXPERIENCIAS" class="mfp-open" data-title="Detalles">
                        <div class="image">
                            <img src="{{ asset('assets/images/project-3.jpg') }}" alt="" />
                        </div>
                        <div class="entry entry-block">
                            <div class="title">
                                {{-- <h2 class="h4">EXPERIENCIAS. LO QUE DICEN DE NOSOTROS</h2> --}}
                            </div>
                            <div class="description">

                            </div>
                        </div>
                        <div class="show-more">
                            <span class="btn btn-main btn-block">Ver detalles</span>
                        </div>
                    </a>
                </article>
            </div>
            <div class="popup-main mfp-hide" style="margin-top: 5%;" id="EXPERIENCIAS">
                <div class="product">
                    <div class="popup-title">
                        <div class="fs-2 text-center text-main">Experiencias</div>
                    </div>
                    <div class="popup-content text-center text-black">
                        <p>¿ Qué dicen nuestros clientes sobre</p>
                        <p> nuestros servicios y artículos ?</p>
                        <p> ¡Descúbrelo y únete a nosotros! </p>
                        <div class="text-center pa-05">
                            <div class="pa-2 bg-white">
                                <img src="{{ asset('assets/images/experiencias.png') }}" alt="" class="img-responsive center-block" />
                            </div>
                        </div>
                        <div class="btn__container">
                            <p class="text-center">
                                <a href="https://www.instagram.com/vaciatucasa.es/" target="_blank" class="btn-i">
                                    <span>instagram</span>
                                </a>
                                <a href="https://g.co/kgs/nR7XsA" target="_blank" class="btn-g">
                                    <span>Google +</span>
                                </a>
                                <a href="https://www.facebook.com/vaciatucasa.es/?ref=py_c" target="_blank" class="btn-f">
                                    <span>facebook</span>
                                </a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-3">
                <article>
                    <a href="{{route ('restauracion')}}">
                        <div class="image">
                            <img src="{{ asset('assets/images/restauracion.jpg') }}" alt="" />
                        </div>
                        <div class="entry entry-block">
                            <div class="title">
                                {{-- <h2 class="h4">RESTAURACIÓN DE MUEBLES. NUESTRO TALLER </h2> --}}
                            </div>
                            <div class="description"> </div>
                        </div>
                        <div class="show-more">
                            <span class="btn btn-main btn-block">Ver detalles</span>
                        </div>
                    </a>
                </article>
            </div>
        </div>
    </div>
    <!--/row-->
    </div>
    <!--/container-->
</section>

<section class="blog blog-block pt-2">
    <header class="">
        <div class="container">
            <div class="row">
                <div class="col-md-offset-2 col-md-8 text-center">
                    <h1 class="h5 title">Vende y compra artículos de segunda mano de forma sencilla cómoda y profesional
                    </h1>
                    <div class="text">
                        <p></p>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <article>
                    <a href="{{ route('page.to_buy')}}">
                        <div class="image">
                            <img src="{{ asset('assets/images/comprar.jpg') }}" alt="" />
                        </div>
                        <div class="entry entry-block">
                            {{-- <div class="title"><h2 class="h3">Muebles de segunda mano</h2></div> --}}
                        </div>
                        <div class="show-more">
                            <span class="btn btn-main btn-block">Quiero Comprar</span>
                        </div>
                    </a>
                </article>
            </div>
            <div class="col-md-6">
                <article>
                    <a href="{{ route('page.to_sell')}}">
                        <div class="image">
                            <img src="{{ asset('assets/images/vender.jpg') }}" alt="" />
                        </div>
                        <div class="entry entry-block">
                            {{-- <div class="title"><h2 class="h3">Muebles de segunda mano</h2></div> --}}
                        </div>
                        <div class="show-more">
                            <span class="btn btn-main btn-block">Quiero Vender</span>
                        </div>
                    </a>
                </article>
            </div>
        </div>
    </div>
</section>
<!-- ========================  Instagram ======================== -->
<section class="instagram">

    <!-- === instagram header === -->

    <header>
        <div class="container">
            <div class="row">
                <a href="{{$company->instagram}}">
                    <div class="col-md-offset-2 col-md-8 text-center">
                        <h4 class="h5 title">MUEBLES ÚNICOS COMO TÚ</h4>
                        <div class="text">
                            <p>
                                {{-- @vaciatucasa_ --}}
                            </p>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </header>

    <!-- === instagram gallery === -->
    <div class="container-fluid">
        {{-- <img src="{{ asset('assets/banners/ECO-01.jpg') }}" alt="" class="img-responsive center-block" /> --}}
        <img src="{{ asset('assets/banners/ECO-01.jpg') }}" alt="" class="img-responsive eco-freendly center-block" />
        {{-- <a class="item"><img src="{{ asset('assets/photos_eva/662-2-(1).jpg') }}" alt="Alternate Text" /></a>
        <a class="item"><img src="{{ asset('assets/photos_eva/662-27-(1).jpg') }}" alt="Alternate Text" /></a>
        <a class="item"><img src="{{ asset('assets/photos_eva/662-4-(1).jpg') }}" alt="Alternate Text" /></a>
        <a class="item"><img src="{{ asset('assets/photos_eva/662-5-(1).jpg') }}" alt="Alternate Text" /></a> --}}
        {{-- <a class="item"><img src="{{ asset('assets/photos_eva/662-30-(1).jpg') }}" alt="Alternate Text" /></a> --}}
    </div>

</section>

<section>
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-6">
                <ul>
                    <li style="margin-top: 10px !important;">
                        <h3 class="h4 text-primary title">#DECORACIÓN GREEN</h3> Crear ambientes en armonía con la naturaleza. Ese es el objetivo
                        de la decoración eco-green. Se trata de crear ambientes naturales y orgánicos, respetando siempre la naturaleza e
                        introduciéndola en nuestro hogar.
                    </li>
                    <li style="margin-top: 10px !important;">
                        <h3 class="h4 text-primary title">#MUEBLESREUTILIZADOS</h3> Reciclar muebles antiguos es una de las mejores formas de
                        reducir el impacto medioambiental, ya que evitarás la contaminación que supone la creación de cada pieza, desde la tala
                        al proceso de producción.
                    </li>
                </ul>
            </div>
            <div class="col-sm-12 col-md-6">
                <ul>
                    <li style="margin-top: 10px !important;">
                        <h3 class="h4 text-primary title">#ECO-FRIENDLY</h3> Convertir en tendencia la decoración con muebles reciclados es vital
                        para el planeta.
                    </li>
                    <li style="margin-top: 10px !important;">
                        <h3 class="h4 text-primary title">#MÁSMADERA</h3> Aunque los materiales reciclados son el nuevo reclamo, <b>la madera
                            nunca pasa de moda</b>, ya que es uno de los más duraderos y permite infinidad de usos. Una de las nuevas tendencias
                        consiste en <b>reutilizar madera antigua</b> y tratarla al mínimo para que conserve su aspecto natural,
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>

<section class="blog">
    <header class="">
        <div class="container">
            <div class="row">
                <div class="text-center">
                    <h3 class="h3 title">Apariciones en medios</h3>
                </div>
            </div>
        </div>
    </header>
    <div class="container">
        <div class="col-md-3 col-sm-3 ">
            <a class="item" href="https://elpais.com/economia/2016/06/17/actualidad/1466154025_586327.html" target="_blank"><img height="90px" src="{{ asset('assets/images/el-pais.png') }}" alt="Alternate Text" /></a>
        </div>
        <div class="col-md-3 col-sm-3 ">
            <a class="item" href="" target="_blank"><img height="90px" src="{{ asset('assets/images/antena-3.png') }}" alt="Alternate Text" /></a>
        </div>
        <div class="col-md-3 col-sm-3 ">
            <a class="item" href="https://www.youtube.com/watch?v=7Em8DGJiU8k" target="_blank"><img height="90px" src="{{ asset('assets/images/emprendedores.png') }}" alt="Alternate Text" /></a>
        </div>
        <div class="col-md-3 col-sm-3 ">
            <a class="item" href="" target="_blank"><img height="90px" src="{{ asset('assets/images/2.png') }}" alt="Alternate Text" /></a>
        </div>
        <div class="col-md-3 col-sm-3 ">
            <a class="item" href="" target="_blank"><img height="90px" src="{{ asset('assets/images/ad-info.png') }}" alt="Alternate Text" /></a>
        </div>
        <div class="col-md-3 col-sm-3 ">
            <a class="item" href="https://www.telemadrid.es/programas/mi-camara-y-yo/Muebles-buscan-nuevos-hogares-2-2196100415--20200117111500.html" target="_blank"><img height="90px" src="{{ asset('assets/images/telemadrid.png') }}" alt="Alternate Text" /></a>
        </div>
        <div class="col-md-3 col-sm-3 ">
            <a class="item" href="https://www.efe.com/efe/espana/efeemprende/vaciatucasa-una-opcion-para-vender-muebles-de-segunda-mano/50000911-3137878" target="_blank"><img height="90px" src="{{ asset('assets/images/efe.png') }}" alt="Alternate Text" /></a>
        </div>
        <div class="col-md-3 col-sm-3 ">
            <a class="item" href="" target="_blank"><img height="90px" src="{{ asset('assets/images/lainformacion.png') }}" alt="Alternate Text" /></a>
        </div>
    </div>
</section>
@endsection

@section('scripts')
<script>
    $(function() {
        $('#modalActivateclick').trigger('click');
    });
    // let a = document.getElementById("modalActivateclick")[0].getAttribute("href").click();

    // document.getElementById("modalActivateclick").click();


    function copyToClipboard(text) {
        const elem = document.createElement('textarea');
        elem.value = text;
        document.body.appendChild(elem);
        elem.select();
        document.execCommand('copy');
        document.body.removeChild(elem);
        alert("Enlace Copiado");
    }

    // <!-- Facebook Pixel Code -->
    ! function(f, b, e, v, n, t, s) {
        if (f.fbq) return;
        n = f.fbq = function() {
            n.callMethod ?
                n.callMethod.apply(n, arguments) : n.queue.push(arguments)
        };
        if (!f._fbq) f._fbq = n;
        n.push = n;
        n.loaded = !0;
        n.version = '2.0';
        n.queue = [];
        t = b.createElement(e);
        t.async = !0;
        t.src = v;
        s = b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t, s)
    }(window, document, 'script',
        'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '912971325964263');
    fbq('track', 'PageView');
</script>
<noscript>
    <img height="1" width="1" src="https://www.facebook.com/tr?id=912971325964263&ev=PageView
    &noscript=1" />
</noscript>
<!-- End Facebook Pixel Code -->

@endsection
