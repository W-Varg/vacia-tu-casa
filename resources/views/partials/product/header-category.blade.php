<!-- ======================== Main header ======================== -->
<span name="{{ $company = \App\Company::get()->first() }}"></span>
<section class="main-header" style="background-image:url({{ asset($company->img2) }})">
    <header>
        <div class="container">
            <h1 class="h2 title text-center">
                <div class="row">
                    <div class="col-md-4 col-sm-12">
                        {!! Form::open(['route' => 'page.more_product', 'method' => 'GET', 'class'=>['inline']]) !!}
                        <div class="form-group">
                        <input class="form-control" placeholder="Buscar por Nombre" name="name" />
                        </div>
                        {{-- <button type="submit" class="btn btn-main btn-search"><i class="fa fa-search"></i></button> --}}
                        {!! Form::close() !!}
                    </div>
                    <div class="col-md-4 col-sm-12">
                        {!! Form::open(['route' => 'page.more_product', 'method' => 'GET', 'class'=>['inline']]) !!}
                        <div class="form-group">
                        <input class="form-control" placeholder="Buscar por Estancias... " name="estancia" />
                        </div>
                        {{-- <button type="submit" class="btn btn-main btn-search"><i class="fa fa-search"></i></button> --}}
                        {!! Form::close() !!}
                    </div>
                    <div class="col-md-4 col-sm-12">
                        {!! Form::open(['route' => 'page.more_product', 'method' => 'GET', 'class'=>['inline']]) !!}
                        <div class="form-group">
                        <input class="form-control" placeholder="Buscar Ciudad o Código postal... " name="ciudad" />
                        </div>
                        {{-- <button type="submit" class="btn btn-main btn-search"><i class="fa fa-search"></i></button> --}}
                        {!! Form::close() !!}
                    </div>
                </div>
            </h1>
            <h1 class="h2 title">Art&iacute;culos</h1>
            <ol class="breadcrumb breadcrumb-inverted">
                {{-- <li><a href="index.html"><span class="icon icon-home"></span></a></li>
                <li>
                </li> --}}
            </ol>
        </div>
    </header>
</section>

<!-- ========================  Menu Icons slider ======================== -->
@include('partials.category.menu-slide')
<style scoped>
    .owl-carousel figure .categories {
        width: 100px;
        height: 100px;
    }
</style>
