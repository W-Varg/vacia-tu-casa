<ul class="sidebar-menu" data-widget="tree">
    <li class="nav-devider"></li>
    <li class="header nav-small-cap">ADMINISTRACI&Oacute;N</li>
    @if (auth()->user()->role->id == \App\Role::ADMIN)
    <li><a href="{{ route ('company.show')}} "><i class="ti-settings"></i><span>Vacia tu Casa</span></a></li>
    @endif
    <li class="treeview">
        <a href="/admin">
            <i class="icon-home"></i> <span>Estancias</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-right pull-right"></i>
            </span>
        </a>
        <ul class="treeview-menu">
            <li><a href="{{ route ('categories.index')}}">Mis Estancias</a></li>
            <li><a href="{{ route ('categories.create')}}">Crear Nueva Estancia</a></li>
        </ul>
    </li>
    <li class="treeview">
        <a href="#">
            <i class="icon-grid"></i>
            <span>Artículos</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-right pull-right"></i>
            </span>
        </a>
        <ul class="treeview-menu">
            <li><a href="{{ route ('products.index') }}">Mis Artículos</a></li>
            <li><a href="{{ route ('products.outet') }}">Artículos Oulet</a></li>
            {{-- <li><a href="{{ route ('products.create') }}">Registrar Artículo</a></li> --}}
            <li><a href="{{ route ('products.pendientes') }}">Artículos Pendientes</a></li>
            <li><a href="{{ route ('products.debaja') }}">Artículos Rechazados</a></li>
            <li><a href="{{ route ('products.reservados') }}">Artículos Reservados</a></li>
            <li><a href="{{ route ('products.reservadosBasicos') }}">Reservas Básicas</a></li>
            <li><a href="{{ route ('reporte.vendidos') }}">Artículos Vendidos</a></li>
        </ul>
    </li>

    <li><a href="{{ route('visits.index')}}"><i class="icon-chart"></i><span>Visitas</span></a></li>
    <li><a href="{{ route('agreements.index')}}"><i class="fa fa-list"></i><span>Contratos</span></a></li>
    <li class="treeview">
        <a href="#">
            <i class="icon-wallet"></i>
            <span>Compras</span>
            <span class="pull-right-container"><i class="fa fa-angle-right pull-right"></i></span>
        </a>
        <ul class="treeview-menu">
            <li><a href="{{ route('compras.index')}}">Compras Solicitadas</a></li>
            <li><a href="{{ route('compras.reservados')}}">Compras Reservados</a></li>
            <li><a href="{{ route('compras.solds')}}">Ventas Realizadas</a></li>
        </ul>
    </li>

    <li class="treeview">
        <a href="#">
            <i class="icon-people"></i>
            @if (auth()->user() && auth()->user()->role->id == \App\Role::ADMIN)
            <span>Clientes y Gestores</span>
            @else
            <span>Clientes</span>
            @endif
            <span class="pull-right-container">
                <i class="fa fa-angle-right pull-right"></i>
            </span>
        </a>
        <ul class="treeview-menu">
            <li><a href="{{ route('clients.index')}}">Mis Clientes</a></li>
            @if (auth()->user() && auth()->user()->role->id == \App\Role::ADMIN)
            <li><a href="{{ route('handlers.index')}}">Mis Gestores</a></li>
            <li><a href="{{ route('clients.contrato')}}">Clientes de Contrato</a></li>
            @endif
        </ul>
    </li>

    <li class="treeview">
        <a href="#">
            <i class="icon-chart"></i>
            <span>Reportes</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-right pull-right"></i>
            </span>
        </a>
        <ul class="treeview-menu">
            <li><a href="{{route('reporte.vendidos')}}">Vendidos</a></li>
            <li><a href="{{route('reporte.no_vendidos')}}">No Vendidos</a></li>
            <li><a href="{{route('reporte.reservados')}}">Reservados</a></li>
            <li><a href="{{route('reporte.contratos_active')}}">Contratos en Vigencia</a></li>
            <li><a href="{{route('reporte.contratos_close')}}">Contratos Concluidos</a></li>
            @if (auth()->user()->role->id == \App\Role::ADMIN)
            <li><a href="{{route('reporte.contrato_gestor')}}">Contratos Y Gestores</a></li>
            @endif
        </ul>
    </li>
    <li><a href="{{ route('contactos.index')}}"><i class="icon-people"></i><span>Contactos</span></a></li>

</ul>
