<div class="user-panel">
    <div class="ulogo">
        <a href="{{ route('admin') }}">
            <span><img src="{{ asset('/assets/images/logo.png') }}" class="" alt="HomeToHome"></span>
        </a>
    </div>
    <div class="info">
        @if (auth()->user()->role->id == \App\Role::ADMIN)
            <p>Administrador</p>
        @endif
        @if (auth()->user()->role->id == \App\Role::HANDLER)
            <p>Gestor/a</p>
        @endif
        @if (auth()->user()->role->id == \App\Role::CLIENT)
            <p>Cliente</p>
        @endif
        <p>{{ auth()->user()->name }}</p>
        <a href="/" class="link" data-toggle="tooltip" title="Página Principal" data-original-title="web"><i class="fa fa-reply"></i></a>
        <a href="{{ route('profile', auth()->user()) }}" class="link" data-toggle="tooltip" title="Ver Perfil" data-original-title="profile"><i
                class="fa  fa-user-circle"></i></a>
        <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="link"
            data-toggle="tooltip" title="Cerrar Sesión" data-original-title="Salir"><i class="ion ion-power"></i></a>
    </div>
</div>
