<header class="main-header">
    <!-- Logo -->
    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <!-- User Account -->
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="{{ asset(auth()->user()->profile->avatar) }}" class="user-image rounded-circle" alt="Usuario">
                    </a>
                    <ul class="dropdown-menu scale-up">
                        <!-- User image -->
                        <li class="user-header">
                            <img src="{{ asset(auth()->user()->profile->avatar) }}" class="float-left rounded-circle" alt="Usuario">
                            @if (auth()->user()->role->id == \App\Role::ADMIN)
                            <p>Administrador</p>
                            @endif
                            @if (auth()->user()->role->id == \App\Role::HANDLER)
                            <p>Gestor/a</p>
                            @endif
                            @if (auth()->user()->role->id == \App\Role::CLIENT)
                            <p>Cliente</p>
                            @endif
                            <p>
                                {{ auth()->user()->name }}
                                <small class="mb-5">{{ auth()->user()->email }}</small>
                            </p>
                        </li>
                        <!-- Menu Body -->
                        <li class="user-body">
                            <div class="row no-gutters">
                                @if (auth()->user() && auth()->user()->role->id == \App\Role::HANDLER)
                                <div role="separator" class="divider col-12"></div>
                                <div class="col-12 text-left">
                                    <a href="https://web.whatsapp.com/send?phone={{ $company->phone_number }}" target="_blank"><i
                                            class="fa fa-whatsapp"></i> Iniciar chat con Administrador</a>
                                </div>
                                @endif
                                <div role="separator" class="divider col-12"></div>
                                <div class="col-12 text-left">
                                    <a href="{{ route('profile', auth()->user()) }}"><i class="ti-settings"></i> Cuenta</a>
                                </div>
                                <div role="separator" class="divider col-12"></div>
                                <div class="col-12 text-left">
                                    <a href="{{ route('logout') }}"
                                        onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i
                                            class="fa fa-power-off"></i> Cerrar Sesi&oacute;n</a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </div>
                            <!-- /.row -->
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>
