<div class="login-wrapper">
    @if(auth()->check())
    <div class="">
        <div class="h4">Mis Datos</div>
        <p><small><a class="btn btn-primary" href="{{ route('profile.edit')}}">Mi Perfil</a></small></p>
        <p><small>{{ auth()->user()->name }}</small></p>
        <p><small>{{ auth()->user()->email }}</small></p>
        <a class="btn btn-main" href="{{ route('logout') }}"
            onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Cerrar Sesi&oacute;n</a>
        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            @csrf
        </form>
    </div>
    @else
    {!! Form::open(['route' => 'login', 'method' => 'POST']) !!}
    @csrf
    <div class="h4">Iniciar Sesi&oacute;n</div>
    <div class="form-group">
        <input type="email" class="form-control @error('email') is-invalid @enderror" id="exampleInputEmail1" value="{{ old('email') }}"
            placeholder="Correo" required autocomplete="email" name="email">
        @error('email')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
    <div class="form-group">
        <input type="password" class="form-control @error('password') is-invalid @enderror" id="exampleInputPassword1" placeholder="Contraseña"
            name="password" required autocomplete="current-password">

        @error('password')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
    <button type="submit" class="btn btn-block btn-main">Entrar</button>
    <div class="form-group">
        <a href="/register" class="open-popup btn btn-link">Registrarse</a>
        @if (Route::has('password.request'))
        <a class="btn btn-link" href="{{ route('password.request') }}">
            {{ __('Recuperar Contraseña?') }}
        </a>
        @endif
    </div>
    {!! Form::close() !!}
    @endif


</div>
