<nav class="navbar-fixed">

    <div class="container">

        <!-- ==========  Top navigation ========== -->

        <div class="navigation navigation-top clearfix">
            <ul>
                <!--add active class for current page-->
                <li><a href="https://web.whatsapp.com/send?phone={{ $company->phone_number }}" target="_blank"><i class="fa fa-whatsapp"></i></a></li>
                @if ($company->facebook)
                <li><a href="{{ $company->facebook }}" target="_blank" class=""><i class="fa fa-facebook"></i></a></li>
                @endif
                @if ($company->twitter)
                <li><a href="{{ $company->twitter }}" target="_blank" class=""><i class="fa fa-twitter"></i></a></li>
                @endif
                @if ($company->instagram)
                <li><a href="{{ $company->instagram }}" target="_blank" class=""><i class="fa fa-instagram"></i></a>
                </li>
                @endif
                @if ($company->pinterest)
                <li><a href="{{ $company->pinterest }}" target="_blank" class=""><i class="fa fa-pinterest"></i></a>
                </li>
                @endif
                <li><a class="" href="mailto: {{$company->email}}" target="_blank"><i class="fa fa-envelope"></i></a></li>
                @if (auth()->user() && auth()->user()->role->id == \App\Role::CLIENT)
                <li class="hidden-xs"><a href="{{ route('client_products') }}" class="btn btn-primary "> Mis Artículos</a></li>
                <li class="hidden-xs"><a href="{{ route('quiero-vender') }}" class="btn btn-primary ">Quiero Vender</a></li>
                @endif
                <li><a href="javascript:void(0);" class="open-login"><i class="icon icon-user"></i></a></li>
                <li><a href="javascript:void(0);" class="open-login"></a></li>
                @if (auth()->user() && auth()->user()->role->id == \App\Role::ADMIN)
                <li><a href="{{ route('admin') }}" class="btn btn-primary ">Administrador</a></li>
                @endif
                @if (auth()->user() && auth()->user()->role->id == \App\Role::HANDLER)
                <li><a href="{{ route('admin') }}" class="btn btn-primary ">Administrador</a></li>
                @endif
            </ul>
        </div>
        <!--/navigation-top-->

        <!-- ==========  Main navigation ========== -->

        <div class="navigation navigation-main">

            <!-- Setup your logo here-->

            <a href="{{ url('/') }}" class="logo"><img src="{{ asset('assets/images/logo.png') }}" alt="" /></a>

            <!-- Mobile toggle menu -->

            <a href="#" class="open-menu"><i class="icon icon-menu"></i></a>

            <!-- Convertible menu (mobile/desktop)-->

            <div class="floating-menu">
                <!-- Mobile toggle menu trigger-->
                <div class="close-menu-wrapper">
                    <span class="close-menu"><i class="icon icon-cross"></i></span>
                </div>

                <ul>
                    <li><a href="{{ url('/') }}">Inicio</a></li>
                    {{-- <li><a href="#popup-comprar" class="mfp-open" data-title="Quiero comprar">Comprar</a></li>
                    <li><a href="#popup-vender" class="mfp-open" data-title="Quiero vender">Vender</a></li> --}}
                    <li><a href="{{ route('page.to_sell')}}">Vender</a></li>
                    <li><a href="{{ route('page.to_buy')}}">Comprar</a></li>
                    {{-- <li><a href="#decoracion">Decoración</a></li> --}}

                    {{-- <li><a href="">Noticias</a></li> --}}
                    {{-- <li><a href="{{ route('how_func')}}">Como Funciona</a></li> --}}
                    <li><a href="{{ route('nosotros') }}">Nosotros</a></li>
                    <li><a href="{{ route('contact') }}">Contacto</a></li>
                    @if(auth()->check())
                    <li class="visible-xs-inline hidden-md hidden-lg"><a class="btn btn-sm btn-primary" href="{{ route('profile.edit')}}">Mi Perfil</a></li>
                    <li class="visible-xs-inline hidden-md hidden-lg">
                        <a class="btn btn-sm btn-primary" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Cerrar Sesi&oacute;n</a>
                    </li>
                    {{-- =============== --}}
                    @if (auth()->user() && auth()->user()->role->id == \App\Role::CLIENT)
                    <li class="visible-xs-inline hidden-md hidden-lg"><a href="{{ route('client_products') }}"> Mis Artículos</a></li>
                    <li class="visible-xs-inline hidden-md hidden-lg"><a href="{{ route('quiero-vender') }}">Quiero Vender</a></li>
                    @endif
                    @if (auth()->user() && auth()->user()->role->id == \App\Role::ADMIN)
                    <li class="visible-xs-inline hidden-md hidden-lg"><a href="{{ route('admin') }}">Administrador</a></li>
                    @endif
                    @if (auth()->user() && auth()->user()->role->id == \App\Role::HANDLER)
                    <li class="visible-xs-inline hidden-md hidden-lg"><a href="{{ route('admin') }}">Administrador</a></li>
                    @endif
                    {{-- =============== --}}
                    @else
                    <li class="visible-xs-inline hidden-md hidden-lg"><a href="{{ route('login') }}">Iniciar Sesión</a></li>
                    <li class="visible-xs-inline hidden-md hidden-lg"><a href="{{ route('register') }}">Registrarse</a></li>
                    @endif

                </ul>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </div>
            <!--/floating-menu-->
        </div>
        <!--/navigation-main-->

        <!-- ==========  Search wrapper ========== -->

        <div class="search-wrapper">
            <!-- Search form -->
            {!! Form::open(['route' => 'page.more_product', 'method' => 'GET']) !!}
            <input class="form-control" placeholder="Buscar..." name="name" />
            <button type="submit" class="btn btn-main btn-search"><i class="fa fa-search"></i></button>
            {!! Form::close() !!}
        </div>

        <!-- ==========  Login wrapper ========== -->

        @include('partials.login.form-login')

    </div>
    <!--/container-->
</nav>

<a href="https://web.whatsapp.com/send?phone={{ $company->phone_number }}" target="_blank" class="float">
    <i class="fa fa-whatsapp my-float"></i>
</a>
