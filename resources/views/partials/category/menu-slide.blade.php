<section class="owl-icons-wrapper">
    <style scoped>
        .owl-carousel figure .categories {
            width: 100px !important;
            height: 100px !important;
        }
    </style>
    <!-- === header === -->
    <header class="hidden">
        <h2>Instancias</h2>
    </header>

    <div class="">
        <div class="owl-icons">
            <?php $categories = \App\Category::All() ?>

            @foreach ($categories as $category)
            <a href="{{ route('page.category', ['slug' => $category->slug]) }}">
                <figure>
                    <img class="categories" src="{{ asset('adm/icons/'.$category->icon) }}" width="100" height="100" alt="{{$category->name}}">
                    <figcaption>{{ $category->name }}</figcaption>
                </figure>
            </a>
            @endforeach
        </div>
    </div>
</section>
