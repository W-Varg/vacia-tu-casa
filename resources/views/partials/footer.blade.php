<footer>
    <div class="container">
        <!--footer links-->
        <div class="footer-links" style="padding-top: 15px;">
            <div class="row">
                <div class="col-sm-4">
                    <h3>{{ $company->company_name }}</h3>
                    <p><i class="fa fa-home"></i> {{ $company->address }}</p>
                    <p><i class="fa fa-envelope"></i> {{ $company->email }}</p>
                </div>
                <div class="col-sm-4">
                    {{-- <a href="#" class="btn btn-clean"><span class="icon icon-map-marker"></span> Nuestras direcciones</a>
                    <div class="call-us h4"><span class="icon icon-phone-handset"></span> {{ $company->phone_number }}</div>
                    <div class="call-us h4"><a data-title="Iniciar Chat" data-title-added="Enviar sms" href="https://web.whatsapp.com/send?phone={{ $company->phone_number }}" target="_blank"><i class="fa fa-whatsapp"></i> Iniciar Chat con Administrador</a></div> --}}
                </div>
                <div class="col-sm-12 col-md-4">
                    {{-- <h6>Siguenos en instagram</h6> --}}
                    <h4 class="h4 title">S&iacute;guenos en nuestra redes sociales </h4>

                        {{-- <input class="form-control" type="text" name="email" value="" placeholder="Correo electrónico" />
                        <input type="submit" class="btn btn-clean btn-sm" value="Suscribete" /> --}}
                        <p><a href="{{ $company->facebook }}" ><i class="fa fa-facebook fa-2x w-30"></i> Facebook</a></p>
                        <p><a href="{{ $company->twitter }}" ><i class="fa fa-twitter fa-2x w-30"></i> Twitter</a></p>
                        <p><a href="{{ $company->instagram }}"  ><i class="fa fa-instagram fa-2x w-30"></i> Instagram</a></p>
                        <p><a href="{{ $company->pinterest }}"  ><i class="fa fa-pinterest fa-2x w-30"></i> Pinterest </a></p>
                </div>
            </div>
        </div>

        <!--footer social-->

        <div class="footer-social">
            <div class="row">
                <div class="col-sm-6">
                <a href="{{ route('politicas')}}" target="_blank"> Pol&iacute;tica de Privacidad</a> &nbsp; | &nbsp; <a href="{{ route('politicas')}}">Pol&iacute;tica de cookies</a> &nbsp;
                </div>
                {{-- <div class="col-sm-6 links">
                    <ul>
                        <li><a href="{{ $company->facebook }}" ><i class="fa fa-facebook"></i></a></li>
                        <li><a href="{{ $company->twitter }}" ><i class="fa fa-twitter"></i></a></li>
                        <li><a href="{{ $company->instagram }}"  ><i class="fa fa-instagram"></i></a></li>
                        <li><a href="{{ $company->pinterest }}"  ><i class="fa fa-pinterest"></i></a></li>
                    </ul>
                </div> --}}
            </div>
        </div>
    </div>
</footer>
