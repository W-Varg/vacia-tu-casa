@extends('layouts.web')
@section('content')
<style>
    .image-container-home {
        height: auto;
        /* margin-top: -30vh;
        margin-bottom: -35vh; */
    }

    .img-text {
        display: block;
        position: relative;
        width: 100%;
    }
</style>
<section class="main-header" style="background-image:url({{ asset('assets/banners/homestaging.jpg') }})">
    <header>
        <div class="container text-center">
            <h2 class="h2 title">HOME STAGING</h2>
        </div>
    </header>
</section>
<div class="container">
    <div class="row my-1">
        <div class="image-container-home">
            <div class="row  overflow-y">
                <div class="col-md-5 col-sm-12 text-center pa-05">
                    <div class="pa-2 bg-white">
                        <div>
                            <img src="{{ asset('images/homestaging2.png') }}" alt="" class="img-responsive center-block" />
                        </div>
                    </div>
                </div>
                <div class="col-md-7 col-sm-12">
                    <img src="{{ asset('images/homestaging2.jpg') }}" alt="" class="img-responsive center-block img-text" />
                    <div class="restauracion-text">
                        <p class="text-center homes-text" style="margin-bottom: -20px;margin-left: -240px;">DECORA PARA</p>
                        <p class="fs-6 otamendi text-center text-black"> Vender</p>
                        <p class="text-justify">
                            Es una técnica utilizada para acelerar la venta o alquiler de tu vivienda. Una puesta a punto estratégica y económica
                            puede agilizar la venta de una casa hasta ocho veces. Te ofrecemos nuestros muebles y te asesoramos para que vendas o
                            alquiles antes y mejor.
                        </p>
                        <p class="text-justify">
                            La primera impresión cuenta: El objetivo de esta técnica es convertir la vivienda en un sitio neutro y acogedor en el
                            que
                            la mayoría de los visitantes se sientan cómodos.
                        </p>
                        <p>
                        </p>
                        <br>
                        <p class="text-center otamendi fs-2 text-black">
                            ¿Vendemos juntos?
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
