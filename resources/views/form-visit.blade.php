@extends('layouts.web')
@section('content')
<span name="{{ $company = \App\Company::get()->first() }}"></span>
<section class="main-header" style="background-image:url({{ asset($company->img2) }}">
    <header>
        <div class="container text-center">
            <h2 class="h2 title">Solicita una Visita</h2>
            <p>Si quieres que vendamos tus muebles rellena el formulario</p>
            <ol class="breadcrumb breadcrumb-inverted">
                <li><a href="/"><span class="icon icon-home"></span></a></li>
                <li><a href="{{route('quiero-vender')}}">quiero vender</a></li>
                <li><a class="active" href="">Formulario de solicitud visita</a></li>
            </ol>
        </div>
    </header>
</section>


<section class="login-wrapper login-wrapper-page">
    <div class="container">

        <div class="row">
            <!-- === left content === -->
            <div class="col-md-6 col-md-offset-3">
                <!-- === login-wrapper === -->
                <div class="login-wrapper">
                    <div class="white-block">
                        <!--signin-->
                        <div class="login-block login-block-signup">
                            <div class="h4">Datos de la solicitud</div>
                            <hr />
                            <div class="row"><div class="col-md-12">@include('client.messages')</div></div>

                            <form action="{!! route('visits.store') !!}" method="POST"  enctype="multipart/form-data" id="my-form">
                            {{-- {!! Form::open(['route' => 'visits.store', 'files' =>true ]) !!} --}}
                            @csrf
                            {{ Form::hidden('client_agreement_id', $agreement->id) }}
                            {{ Form::hidden('status', 'NORMAL') }}
                            <div class="row">
                                <div class="form-group col-md-6">
                                    {{ Form::label('date_visit', 'Fecha de visita') }}
                                    {{ Form::date('date_visit', null, [ 'class' => 'form-control', 'id' => 'date_visit', 'required'=>'true' ]) }}
                                </div>
                                <div class="form-group col-md-6">
                                    {{ Form::label('hour_visit', 'Mañana/Tarde') }}
                                    {{-- {{ Form::time('hour_visit', null, [ 'class' => 'form-control', 'id' => 'hour_visit' ,'title'=>"Ejemplo: \n 11:05 AM", 'data-toggle'=> "tooltip", 'data-placement' => "right", 'required'=>'true']) }} --}}
                                    {{ Form::select('hour_visit',['Mañana' => 'Mañana', 'Tarde' => 'Tarde'], null, [ 'class' => 'form-control', 'id' => 'hour_visit' ]) }}
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-6">
                                    {{ Form::label('cp', 'C.P.') }}
                                    {{ Form::number('cp', $profile->code_postal, [ 'class' => 'form-control', 'id' => 'cp' , 'required'=>'true', 'min'=>'1'  ]) }}
                                </div>
                                <div class="form-group col-md-6">
                                    {{ Form::label('phone', 'Teléfono') }}
                                    {{ Form::number('phone', $profile->phone_number, [ 'class' => 'form-control', 'id' => 'phone', 'required'=>'true' ]) }}
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-12">
                                    {{ Form::label('address', 'Dirección') }}
                                    {{ Form::text('address', $profile->address, [ 'class' => 'form-control', 'id' => 'address', 'required'=>'true' ,'title'=>"Barcelona, calle:San Diego,  #25", 'data-toggle'=> "tooltip", 'data-placement' => "top"]) }}
                                </div>
                            </div>

                            <div class="row">
                                <li type="square" class="py-4"> CANTIDAD DE ARTICULOS Y FOTOS A SUBIR</li>
                                <p></p>
                                <div class="form-group col-md-6">
                                    {{ Form::label('quantity_product', 'Cantidad') }}
                                    {{ Form::number('quantity_product', 1, [ 'class' => 'form-control', 'id' => 'quantity_product', 'min'=>'1' ]) }}
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Imágenes</label>
                                    <input type="file" name="images[]" id="images" class="form-control"   multiple="multiple" accept='image/*' />
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="gallery" id="galeria"></div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 text-center">
                                    <input type="submit" id="btn-submit" class="btn btn-primary" value="Enviar Solicitud" />
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<style scoped>
    .gallery img {
        width: 45%;
        margin-top: 0.5em;
        margin-left: 0.5em;
        margin-bottom: 0.5em;
        margin-right: 0.5em;
    }
</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script>
    $("#date_visit").keydown(function (event) {
        event.preventDefault();
    });
    $(function () { // Multiple images preview in browser
        var imagesPreview = function (input, placeToInsertImagePreview) {
            if (input.files) {
                $('div#galeria > img').remove();
                let cantidad = document.getElementById('quantity_product').value *1;
                if (cantidad>= 1) {
                    var filesAmount = input.files.length;
                    if (filesAmount == cantidad) {

                        for (i = 0; i < filesAmount && i<cantidad; i++) {
                            var reader = new FileReader();

                            reader.onload = function (event) {
                                $($.parseHTML('<img>')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
                            }

                            reader.readAsDataURL(input.files[i]);
                        }
                    }
                    else{
                        alert('Mínimo 4 imágenes por artículo..!!')
                    }
                }else{
                    alert('Fotografias minimas 4, y máxima ilimitado..!!')
                }
            }
        };

        $('#images').on('change', function () {
            imagesPreview(this, 'div.gallery');
        });

        $("#my-form").submit(function (e) {
            $("#btn-submit").attr("disabled", true); return true;
        });

    });

    let today = new Date().toISOString().split('T')[0];
    let tomorrow = new Date(today);
    tomorrow.setDate(tomorrow.getDate() + 1);
    tomorrow =  tomorrow.toISOString().split('T')[0];

    document.getElementsByName("date_visit")[0].setAttribute('min', tomorrow);
</script>

@endsection
