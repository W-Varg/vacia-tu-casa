@extends('layouts.web')

@section('content')
@include('partials.header.header-register')

{!! NoCaptcha::renderJs() !!}
<section class="login-wrapper login-wrapper-page">
    <div class="container">

        <header class="hidden">
            <h3 class="h3 title">Registrarse</h3>
        </header>

        <div class="row">
            <!-- === left content === -->
            <div class="col-md-6 col-md-offset-3">
                <!-- === login-wrapper === -->

                <div class="login-wrapper">
                    <div class="white-block">
                        <!--signin-->
                        <div class="login-block login-block-signup">
                            <div class="h4">Datos de Registro</div>
                            <hr />
                            <form method="POST" action="{{ route('register') }}" enctype="multipart/form-data">
                                @csrf
                                <div class="form-group row">
                                    <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Nombre') }}</label>

                                    <div class="col-md-6">
                                        <input id="name" title="Introdusca su nombre personal" data-toggle="tooltip" data-placement="top"
                                            type="text" class="form-control @error('name') is-invalid @enderror" name="name" required
                                            autocomplete="name" autofocus>

                                        @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="last_name" class="col-md-4 col-form-label text-md-right">{{ __('Apellidos') }}</label>

                                    <div class="col-md-6">
                                        <input id="last_name" type="text" class="form-control @error('last_name') is-invalid @enderror"
                                            name="last_name" required autocomplete="last_name" autofocus>

                                        @error('last_name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('Correo') }}</label>

                                    <div class="col-md-6">
                                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email"
                                            value="{{ old('email') }}" required autocomplete="email">

                                        @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Contraseña') }}</label>

                                    <div class="col-md-6">
                                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror"
                                            name="password" required autocomplete="new-password">

                                        @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="password_confirmation"
                                        class="col-md-4 col-form-label text-md-right">{{ __('Confirmar Contraseña') }}</label>

                                    <div class="col-md-6">
                                        <input id="password_confirmation" type="password" class="form-control" name="password_confirmation"
                                            required autocomplete="new-password">
                                    </div>
                                    <span class="checkbox checkbox-inline">
                                        <input id="ver" type="checkbox" onclick="myFunction()">
                                        <label for="ver">Ver Contraseñas</label>
                                    </span>
                                </div>
                                <div class="row well">
                                    <div class="col-sm-12 text-center">
                                        <span class="checkbox checkbox-inline">
                                            <input type="checkbox" id="estandar" required>
                                            <label for="estandar">Aceptar Terminos</label>
                                        </span>
                                    </div>
                                    <div class="text-center">
                                        <a href="{{route('politicas')}}" target="_blank">
                                            <div class="show-more">
                                                <span class="btn btn-sm btn-clean-dark ">Leer Terminos Y Condiciones</span>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('g-recaptcha-response') ? ' has-error' : '' }}">
                                    <div class="col-md-6 col-centered">
                                        {!! app('captcha')->display() !!}
                                        @if ($errors->has('g-recaptcha-response'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('g-recaptcha-response') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row mb-0">
                                    <div class="col-md-12 text-center">
                                        <button type="submit" class="btn btn-primary">{{ __('Registrarse') }}</button>
                                    </div>
                                </div>
                            </form>
                            <hr>
                            <div class="text-center">
                                <a class="btn btn-link" href="/login"><i class="fa  fa-location-arrow"></i> Ingresar
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <!--/login-wrapper-->
            </div>
            <!--/col-md-6-->

        </div>

    </div>
</section>
<style scoped>
    .gallery img {
        width: 40%;
        margin-top: 0.5em;
        margin-left: 0.5em;
        margin-bottom: 0.5em;
        margin-right: 0.5em;
    }
</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script>
    // $(function () { // Multiple images preview in browser
    //     var imagesPreview = function (input, placeToInsertImagePreview) {
    //         if (input.files) {
    //             $('div#galeria > img').remove();
    //                 for (i = 0; i < input.files.length ; i++) {
    //                     var reader = new FileReader();
    //                     reader.onload = function (event) {
    //                         $($.parseHTML('<img>')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
    //                     }
    //                     reader.readAsDataURL(input.files[i]);
    //                 }
    //         }
    //     };

    //     $('#avatar').on('change', function () {
    //         imagesPreview(this, 'div.gallery');
    //     });
    // });
    function myFunction() {
        var pass = document.getElementById("password");
        if (pass.type === "password") {
            pass.type = "text";
        } else {
            pass.type = "password";
        }
        var confir = document.getElementById("password_confirmation");
        if (confir.type === "password") {
            confir.type = "text";
        } else {
            confir.type = "password";
        }
    }
</script>
@endsection
