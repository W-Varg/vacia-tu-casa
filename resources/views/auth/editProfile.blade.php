@extends('layouts.web')
@section('styles')
{{-- https://flagicons.lipis.dev/?continent=Europe --}}
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/3.5.0/css/flag-icon.min.css"
    integrity="sha512-Cv93isQdFwaKBV+Z4X8kaVBYWHST58Xb/jVOcV9aRsGSArZsgAnFIhMpDoMDcFNoUtday1hdjn0nGp3+KZyyFw==" crossorigin="anonymous" />
@endsection
@section('content')
<span name="{{ $company = \App\Company::get()->first() }}"></span>
<section class="main-header" style="background-image:url({{ asset($company->img4) }}">
</section>
<section class="product">
    <div class="info">
        <div class="container text-center">
            @include('client.messages')
            <header>
                <div class="container text-center">
                    <h2 class="h2 title" style="text-transform: capitalize;">
                        MI PERFIL
                    </h2>
                    <p> <a href="{{ route('client_products')}}">Mis articulos</a></p>
                    <p>Cuenta: <a class="active text-main"> {{$user->email}} </a></p>
                </div>
            </header>

            <h3>Editar Perfil de Usuario</h3>

            <div class="products">
                <div class="container">
                    <div class="row">

                        <div class="col-md-12">
                            {!! Form::model($user,['route' => ['profile.update', $user], 'method'=> 'PUT', 'files' => true]) !!}
                            @csrf
                            <div class="row">
                                {{-- name --}}
                                <div class="col-md-6 col-sm-12">
                                    <div class="form-group row">
                                        <label for="name" class="col-sm-3 control-label">Nombre</label>
                                        {{ Form::text('name', $user->name, [ 'class' => 'form-control col-8', 'id' => 'name' ]) }}
                                        @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                                {{-- apellido --}}
                                <div class="col-md-6 col-sm-12">
                                    <div class="form-group row">
                                        <label for="last_name" class="col-sm-3 control-label">Apellidos</label>
                                        {{ Form::text('last_name', $user->profile->last_name, [ 'class' => 'form-control col-8', 'id' => 'last_name' ]) }}
                                        @error('last_name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                                {{-- dni --}}
                                <div class="col-md-6 col-sm-12">
                                    <div class="form-group row">
                                        <label for="dni" class="col-sm-3 control-label">DNI</label>
                                        {{ Form::text('dni', $user->profile->dni, [ 'class' => 'form-control col-8', 'id' => 'dni', 'required' => 'required' ]) }}
                                        @error('dni')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="col-md-6 col-sm-12">
                                    {{-- codigo postal --}}
                                    <div class="form-group row">
                                        <label for="code_postal" class="col-sm-4 control-label">Código Postal</label>
                                        {{ Form::number('code_postal', $user->profile->code_postal, [ 'class' => 'form-control col-8', 'id' => 'code_postal', 'required' => 'required' ]) }}
                                        @error('code_postal')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-12">
                                    <p class="control-label" style="display: block;text-align: initial;margin-bottom: 5px;">Telefono</p>
                                    {{-- phone number --}}
                                    <div class="form-control-2 row" id="phone_number">
                                        <div class="col-sm-5 py-0">
                                            <select class="form-control" name="countryCode">
                                                <option value="+34"  style="background-image:url({{ asset('images/flags/es.svg') }})"> España (+34) </option>
                                                <option value="+351">Portugal (+351) </option>
                                                <option value="+33">Francia (+33) </option>
                                                <option value="+49">Alemania (+49) </option>
                                                <option value="+39">Italia (+39) </option>
                                                <optgroup label="Otros Paises">
                                                    <option value="+30">Grecia (+30) </option>
                                                    <option value="+31">Países Bajos (+31) </option>
                                                    <option value="+32">Bélgica (+32) </option>
                                                    <option value="+350">Gibraltar (+350) </option>
                                                    <option value="+352">Luxemburgo (+352) </option>
                                                    <option value="+353">Irlanda (+353) </option>
                                                    <option value="+354">Islandia (+354) </option>
                                                    <option value="+355">Albania (+355) </option>
                                                    <option value="+356">Malta (+356) </option>
                                                    <option value="+357">Chipre (+357) </option>
                                                    <option value="+358">Finlandia (+358) </option>
                                                    <option value="+359">Bulgaria (+359) </option>
                                                    <option value="+36">Hungría (+36) </option>
                                                    <option value="+370">Lituania (+370) </option>
                                                    <option value="+371">Letonia (+371) </option>
                                                    <option value="+372">Estonia (+372) </option>
                                                    <option value="+373">Moldavia (+373) </option>
                                                    <option value="+374">Armenia (+374) </option>
                                                    <option value="+375">Bielorrusia (+375) </option>
                                                    <option value="+376">Andorra (+376) </option>
                                                    <option value="+377">Mónaco (+377) </option>
                                                    <option value="+378">San Marino (+378) </option>
                                                    <option value="+379">Ciudad del Vaticano (+379) </option>
                                                    <option value="+380">Ucrania (+380) </option>
                                                    <option value="+381">Serbia (+381) </option>
                                                    <option value="+382">Montenegro (+382) </option>
                                                    <option value="+385">Croacia (+385) </option>
                                                    <option value="+386">Eslovenia (+386) </option>
                                                    <option value="+387">Bosnia (+387) </option>
                                                    <option value="+389">República de Macedonia (+389) </option>
                                                    <option value="+40">Rumania Rumania (+40) </option>
                                                    <option value="+41">Suiza (+41) </option>
                                                    <option value="+42">Checoslovaquia (+42) </option>
                                                    <option value="+420">República Checa (+420) </option>
                                                    <option value="+421">Eslovaquia (+421) </option>
                                                    <option value="+422">no asignado (+422) </option>
                                                    <option value="+423">Liechtenstein (+423) </option>
                                                    <option value="+43">Austria (+43) </option>
                                                    <option value="+44">Reino Unido (+44) </option>
                                                    <option value="+45">Dinamarca (+45) </option>
                                                    <option value="+46">Suecia (+46) </option>
                                                    <option value="+47">Noruega (+47) </option>
                                                    <option value="+48">Polonia (+48) </option>
                                                </optgroup>
                                            </select>
                                        </div>
                                        <div class="col-sm-7 py-0">
                                            {{ Form::tel('phone_number', $user->profile->phone_number, [ 'class' => 'form-control', 'required' => 'required' ]) }}
                                            @error('phone_number')
                                            <span class="invalid-feedback" role="alert"> <strong>{{ $message }}</strong> </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-12">
                                    {{-- address --}}
                                    <div class="form-group row">
                                        <label for="address" class="col-sm-3 control-label">Direccion</label>
                                        {{ Form::text('address', $user->profile->address, [ 'class' => 'form-control col-8', 'id' => 'address', 'required' => 'required' ]) }}
                                        @error('address')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-12">
                                    {{-- provincia --}}
                                    <div class="form-group row">
                                        <label for="provincia" class="col-sm-3 control-label">Provincia</label>
                                        {{ Form::text('provincia', $user->profile->provincia, [ 'class' => 'form-control col-8', 'id' => 'provincia', 'required' => 'required' ]) }}
                                        @error('provincia')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-12">
                                    {{-- localidad --}}
                                    <div class="form-group row">
                                        <label for="localidad" class="col-sm-3 control-label">Localidad</label>
                                        {{ Form::text('localidad', $user->profile->localidad, [ 'class' => 'form-control col-8', 'id' => 'localidad', 'required' => 'required' ]) }}
                                        @error('localidad')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>

                            {{-- photo --}}
                            <div class="form-group row">
                                <label for="avatar" class="col-sm-3 control-label">Nueva Fotografia (Avatar)</label>
                                <input type="file" accept='image/*' class="form-control"  id="avatar" name="avatar" autofocus>
                                @error('avatar')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>

                            <div class="col-md-12 text-center">
                                <div class="gallery" id="galeria"></div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="text-center">
                                    <h4>Desea modificar la contraseña? (OPCIONAL)</h4>
                                </div>
                                <div class="col-md-6 col-sm-12">
                                    {{-- password --}}
                                    <div class="form-group row">
                                        <label for="password" class="col-sm-3 control-label">Nueva Contraseña</label>
                                        <input type="password" class="form-control col-8" id="password" name="password"
                                            autocomplete="new-password" autofocus>
                                        @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 text-center">
                                <input type="submit" class="btn btn-primary" value="Guardar Cambios" />
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<style scoped>
    .gallery img {
        width: 150px;
        margin-top: 0.5em;
        margin-left: 0.5em;
        margin-bottom: 0.5em;
        margin-right: 0.5em;
    }

    .form-control-2 {
        display: block;
        height: 46px;
        /* padding: 12px 18px; */
    }

    .py-0 {
        padding: 0px;
    }
</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script>
    $(function () { // Multiple images preview in browser
        var imagesPreview = function (input, placeToInsertImagePreview) {
            if (input.files) {
                $('div#galeria > img').remove();
                for (i = 0; i < input.files.length; i++) {
                    var reader = new FileReader();
                    reader.onload = function (event) {
                        $($.parseHTML('<img>')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
                    }
                    reader.readAsDataURL(input.files[i]);
                }
            }
        };

        $('#avatar').on('change', function () {
            imagesPreview(this, 'div.gallery');
        });
    });
</script>
@endsection
