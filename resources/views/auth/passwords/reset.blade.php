@extends('layouts.web')

@section('content')

<section class="main-header" style="background-image:url({{ asset('assets/images/gallery-2.jpg') }})">
    <header>
        <div class="container text-center">
            <h2 class="h2 title">Cambiar Contraseña</h2>
            <ol class="breadcrumb breadcrumb-inverted">
                <li><a href="index.html"><span class="icon icon-home"></span></a></li>
                <li><a class="active" href="login.html">Cambiar contraseña</a></li>
            </ol>
        </div>
    </header>
</section>

<section class="login-wrapper login-wrapper-page">
    <div class="container">
        <header class="hidden">
            <h3 class="h3 title">Cambiar Contraseña</h3>
        </header>
        <div class="row">
            <!-- === left content === -->
            <div class="col-md-6 col-md-offset-3">                        
                <div class="login-wrapper">
                    <div class="white-block">
                        <!--signin-->
                        <div class="login-block login-block-signup">
                            <div class="h4">Cambiar Contraseña</div>
                            <hr />
                            <form method="POST" action="{{ route('password.update') }}">
                                @csrf
                                <input type="hidden" name="token" value="{{ $token }}">
                                <div class="form-group row">
                                    <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('Correo') }}</label>
                                    <div class="col-md-6">
                                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus>

                                        @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Contraseña') }}</label>

                                    <div class="col-md-6">
                                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                        @error('password')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirmar Contraseña') }}</label>

                                    <div class="col-md-6">
                                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                                    </div>
                                </div>

                                <div class="form-group row mb-0">
                                    <div class="col-md-12 text-center">
                                        <button type="submit" class="btn btn-primary">
                                            {{ __('Cambiar Contraseña') }}
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>                                 
                    </div>
                </div> <!--/login-wrapper-->
            </div> <!--/col-md-6-->

        </div>

    </div>
</section>

@endsection
