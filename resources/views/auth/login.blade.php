@extends('layouts.web')

@section('content')
@include('partials.header.header-login')


<section class="login-wrapper login-wrapper-page">
    <div class="container">

        <header class="hidden">
            <h3 class="h3 title">Iniciar Sesi&oacute;n</h3>
        </header>

        <div class="row">
            <!-- === left content === -->
            <div class="col-md-6 col-md-offset-3">
                <!-- === login-wrapper === -->

                <div class="login-wrapper">
                    <div class="white-block">
                        <!--signin-->
                        <div class="login-block login-block-signup">
                            <div class="h4">Iniciar Sesi&oacute;n</div>
                            <hr />
                            {!! Form::open(['route' => 'login', 'method' => 'POST']) !!}
                            <meta name="csrf-token" content="{{ csrf_token() }}">

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <input type="text" class="form-control @error('email') is-invalid @enderror" name="email" id="email"
                                            value="{{ old('email') }}" placeholder="Correo">

                                        @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror"
                                            name="password" required autocomplete="current-password" placeholder="Contraseña">

                                        @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <span class="checkbox checkbox-inline">
                                        <input id="ver" type="checkbox" onclick="myFunction()">
                                        <label for="ver">Ver Contraseña</label>
                                    </span>
                                </div>

                                {{-- <div class="col-xs-6">
                                    <span class="checkbox">
                                        <input type="checkbox" id="checkBoxId3">
                                        <label for="checkBoxId3">Recordarme</label>
                                    </span>
                                </div> --}}

                                <div class="col-xs-12 text-center">
                                    <button type="submit" class="btn btn-main">Iniciar Sesión</button>
                                    <br>
                                    @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('Recuperar Contraseña?') }}
                                    </a>
                                    @endif
                                    <a class="btn btn-link" href="/register">{{ __('Registrarse?') }}</a>
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                        <!--/signin-->
                    </div>
                </div>
                <!--/login-wrapper-->
            </div>
            <!--/col-md-6-->

        </div>

    </div>
</section>
<script>
    function myFunction() {
        var pass = document.getElementById("password");
        if (pass.type === "password") {
            pass.type = "text";
        } else {
            pass.type = "password";
        }
        var confir = document.getElementById("password_confirmation");
        if (confir.type === "password") {
            confir.type = "text";
        } else {
            confir.type = "password";
        }
    }
</script>

@endsection
