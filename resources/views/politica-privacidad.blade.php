@extends('layouts.web')
@section('content')
<section class="main-header" style="background-image:url(assets/images/gallery-3.jpg)">
    <header>
        <div class="container text-center">
            <h2 class="h2 title">Políticas de Privacidad</h2>

            <ol class="breadcrumb breadcrumb-inverted">
                <li><a href="/"><span class="icon icon-home"></span></a></li>
                <li><a class="active" href="">Políticas de privacidad</a></li>
            </ol>
        </div>
    </header>
</section>
<section class="our-team">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="post-formatting">
                    <article class="cuerpo">
                        <div class="vc_row row">
                            <div class="wpb_column vc_column_container vc_col-sm-12">
                                <div class="vc_column-inner ">
                                    <div class="wpb_wrapper">
                                        <div class="wpb_text_column wpb_content_element ">
                                            <div class="wpb_wrapper">
                                                <p><strong class="title h6">PRIVACIDAD</strong></p>
                                                <p>Denominación Social: Silvia Yolanda Robledo Armentera. (en adelante
                                                    LA COMPAÑÍA)</p>
                                                <p>Nombre Comercial: <strong class="title h6"><span
                                                            style="color: #89ccbc;">HomeToHome</span></strong></p>
                                                <p>Domicilio Social: Calle Asturias nº 9; 28914 Leganés; Madrid; España
                                                </p>
                                                <p>DNI: 52092577-S</p>
                                                <p>e-mail: <a
                                                        href="mailto:info.hometohomedecor@gmail.com">info.hometohomedecor@gmail.com</a>
                                                </p>
                                                <p>Nombre de dominio: <a
                                                        href="http://hometohomedecor.com/">hometohomedecor.com</a> (en
                                                    adelante la Web)</p>
                                                <p>De conformidad con lo dispuesto en la Ley Orgánica 15/1999, de
                                                    Protección de Datos de Carácter Personal (LOPD), el Reglamento
                                                    General de Protección de Datos Reglamento 2016/679, de 27 de abril
                                                    de 2016 y su normativa de desarrollo, el responsable del sitio web,
                                                    en cumplimiento de lo dispuesto en el art. 5 y 6 de la LOPD, informa
                                                    a todos los usuarios del sitio web que faciliten o vayan a facilitar
                                                    sus datos personales, que estos serán incorporados en un fichero
                                                    automatizado de titularidad de LA COMPAÑÍA, que es la responsable de
                                                    su tratamiento.</p>
                                                <p>Los usuarios, mediante el envío de solicitud de información, interés
                                                    por algún artículo, inscripción en la newsletter, etc. aceptan
                                                    expresamente y de forma libre e inequívoca que sus datos personales
                                                    sean tratados por parte de LA COMPAÑÍA para realizar las siguientes
                                                    finalidades:</p>
                                                <ol>
                                                    <li>Remisión de comunicaciones comerciales publicitarias por e-mail,
                                                        fax, SMS, MMS, comunidades sociales o cualesquier otro medio
                                                        electrónico o físico, presente o futuro, que posibilite realizar
                                                        comunicación comerciales. Dichas comunicaciones comerciales
                                                        serán relacionadas sobre productos o servicios ofrecidos por el
                                                        prestador, así como por parte de los colaboradores o partners
                                                        con los que éste hubiera alcanzado algún acuerdo de promoción
                                                        comercial entre sus clientes. En este caso, los terceros nunca
                                                        tendrán acceso a los datos personales. En todo caso las
                                                        comunicaciones comerciales serán realizadas por parte del
                                                        prestador y serán de productos y servicios relacionados con el
                                                        sector del prestador. </li>
                                                    <li>Realizar estudios estadísticos.</li>
                                                    <li>Tramitar encargos, solicitudes o cualquier tipo de petición que
                                                        sea realizada por el usuario a través de cualquiera de las
                                                        formas de contacto que se ponen a disposición del usuario en el
                                                        sitio web de la compañía.</li>
                                                    <li>Remitir el boletín de noticias de la página web, newsletteres,
                                                        etc.</li>
                                                    <li>Realizar solicitudes o reservas de artículos.</li>
                                                    <li>Compra de artículos</li>
                                                </ol>
                                                <p>LA COMPAÑÍA informa y garantiza expresamente a los usuarios que sus
                                                    datos personales no serán cedidos en ningún caso a terceras
                                                    compañías, y que siempre que fuera a realizarse algún tipo de cesión
                                                    de datos personales, de forma previa, se solicitaría el
                                                    consentimiento expreso, informado, e inequívoco por parte de los
                                                    titulares.</p>
                                                <p>Todos los datos solicitados a través del sitio web son obligatorios,
                                                    ya que son necesarios para la prestación de un servicio óptimo al
                                                    usuario. En caso de que no sean facilitados todos los datos, el
                                                    prestador no garantiza que la información y servicios facilitados
                                                    sean completamente ajustados a sus necesidades.</p>
                                                <p>LA COMPAÑÍA garantiza en todo caso al usuario el ejercicio de los
                                                    derechos de acceso, rectificación, cancelación, información y
                                                    oposición, en los términos dispuestos en la legislación vigente. Por
                                                    ello, de conformidad con lo dispuesto en la Ley Orgánica 15/1999, de
                                                    Protección de Datos de Carácter Personal (LOPD) y el Reglamento
                                                    General de Protección de Datos Reglamento 2016/679, de 27 de abril
                                                    de 2016 podrá ejercer sus derechos remitiendo una solicitud expresa,
                                                    junto a una copia de su DNI, a través de los siguientes medios:</p>
                                                <ol>
                                                    <li>E-mail: <a
                                                            href="mailto:info.hometohomedecor@gmail.com">info.hometohomedecor@gmail.com</a>
                                                    </li>
                                                    <li>Correo Postal Calle Asturias nº 9; 28914 Leganés; Madrid; España
                                                    </li>
                                                </ol>
                                                <p>Del mismo modo, el usuario podrá darse de baja de cualquiera de los
                                                    servicios de suscripción facilitados haciendo clic en el apartado
                                                    darse de baja de los correos electrónicos remitidos por parte del
                                                    prestador.</p>
                                                <p>LA COMPAÑÍA ha adoptado todas las medidas técnicas y de organización
                                                    necesarias para garantizar la seguridad e integridad de los datos de
                                                    carácter personal que trate, así como para evitar su pérdida,
                                                    alteración y/o acceso por parte de terceros no autorizados.</p>
                                            </div>
                                        </div>
                                        <div class="wpb_text_column wpb_content_element ">
                                            <div class="wpb_wrapper">
                                                <p><strong class="title h6">POLÍTICA DE COOKIES Y DEL FICHERO DE ACTIVIDAD</strong></p>
                                                <p>Una cookie es un archivo que se descarga en su equipo (ordenador o
                                                    dispositivo móvil) con la finalidad de almacenar datos que podrán
                                                    ser actualizados y recuperados por la entidad responsable de su
                                                    instalación.</p>
                                                <p>La Web utiliza una tecnología denominada “cookies” con la finalidad
                                                    de poder recabar información acerca del uso del citado portal de
                                                    internet. La información recabada a través de las cookies puede
                                                    incluir la fecha y hora de visitas a la Web, las páginas visionadas,
                                                    el tiempo que ha estado en la Web y los sitios visitados justo antes
                                                    y después del mismo.</p>
                                                <p>Las cookies utilizadas por el sitio web se asocian únicamente con un
                                                    usuario anónimo y su ordenador, y no proporcionan por sí mismas los
                                                    datos personales del usuario.</p>
                                                <p>Las cookies utilizadas en este sitio web tienen, en todo caso,
                                                    carácter temporal con la única finalidad de hacer más eficaz su
                                                    transmisión ulterior. En ningún caso se utilizarán las cookies para
                                                    recoger información sensible de identificación personal como puede
                                                    ser su dirección, contraseña o datos de su tarjeta de débito o
                                                    crédito.</p>
                                                <p>Le informamos de que podemos utilizar cookies con la finalidad de
                                                    facilitar su navegación a través de la Web, distinguirle de otros
                                                    usuarios, proporcionarle una mejor experiencia en el uso de la
                                                    misma, e identificar problemas para mejorar la Web</p>
                                                <p>Al navegar y continuar en la Web estará consintiendo el uso de las
                                                    cookies, si bien, puede bloquearlas o deshabilitarlas activando la
                                                    configuración de su navegador que le permite rechazar la instalación
                                                    de todas las cookies o de algunas de ellas. La práctica mayoría de
                                                    los navegadores permiten advertir de la presencia de cookies o
                                                    rechazarlas automáticamente. Si las rechaza podrá seguir usando la
                                                    Web , aunque el uso de algunos de sus servicios podrá ser limitado y
                                                    por tanto su experiencia en la misma menos satisfactoria.</p>
                                            </div>
                                        </div>
                                        <div class="wpb_text_column wpb_content_element ">
                                            <div class="wpb_wrapper">
                                                <p><strong class="title h6">DIRECCIONES IP</strong></p>
                                                <p>Los servidores del sitio web podrán detectar de manera automática la
                                                    dirección IP y el nombre de dominio utilizados por el usuario. Una
                                                    dirección IP es un número asignado automáticamente a un ordenador
                                                    cuando ésta se conecta a Internet. Toda esta información es
                                                    registrada en un fichero de actividad del servidor debidamente
                                                    inscrito que permite el posterior procesamiento de los datos con el
                                                    fin de obtener mediciones únicamente estadísticas que permitan
                                                    conocer el número de impresiones de páginas, el número de visitas
                                                    realizadas a los servicios web, el orden de visitas, el punto de
                                                    acceso, etc.</p>
                                            </div>
                                        </div>
                                        <div class="wpb_text_column wpb_content_element ">
                                            <div class="wpb_wrapper">
                                                <p><strong class="title h6">SEGURIDAD</strong></p>
                                                <p>La web utiliza técnicas de seguridad de la información generalmente
                                                    aceptadas en la industria, tales como firewalls, procedimientos de
                                                    control de acceso y mecanismos criptográficos, todo ello con el
                                                    objeto de evitar el acceso no autorizado a los datos. Para lograr
                                                    estos fines, el usuario acepta que el prestador obtenga datos para
                                                    efectos de la correspondiente autenticación de los controles de
                                                    acceso.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
