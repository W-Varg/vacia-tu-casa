@extends('layouts.web')
@section('content')
<style>
    .image-container-home {
        height: auto;
        /* margin-top: -30vh;
        margin-bottom: -35vh; */
    }

    .img-text {
        display: block;
        position: relative;
        width: 100%;
    }
</style>
<section class="main-header" style="background-image:url({{ asset('assets/banners/restauracion.png') }})">
    <header>
        <div class="container text-center">
            <h2 class="h2 title">RESTAURACION</h2>
        </div>
    </header>
</section>
<div class="container">
    <div class="row my-1">
        <div class="image-container-home">
            <div class="row  overflow-y">
                <div class="col-sm-12 col-md-5 text-center pa-05">
                    <div class="pa-2 bg-white">
                        <div>
                            <img src="{{ asset('images/restauracion.png') }}" alt="" class="img-responsive center-block" />
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-7">
                    <img src="{{ asset('images/restauracion1.png') }}" alt="" class="img-responsive center-block img-text" />
                    <div class="restauracion-text">
                        <p class="text-center otamendi text-black">Segunda</p>
                        <p class="fs-6 otamendi text-center text-black ">Vida</p>
                        <p class="text-justify">
                            La vida está llena de segundas oportunidades, así que.. ¿Por qué no dársela también a
                            tus muebles y accesorios viejos, olvidados y con mala cara? El reciclaje está de moda, no se puede discutir.
                            Restaurar y transformar es una forma de conservar nuestra historia. La contemplación de estos objetos provoca en
                            nosotros un sentimiento especial de
                            nostalgia y romanticismo.
                        </p>
                        <p class="text-justify">
                            En home to home, te damos claves geniales para que puedas renovar tus muebles y darles un toque mas actual a esa mesa
                            de comedor que compraste con tanta ilusión; el aparador del salón que te parecía tan bonito, la cómoda de la abuela..
                        </p>
                        <p class="text-justify">
                            Si los muebles de tu casa han perdido el encanto que tenían antes, es hora de cambiarlos. Y no, no se trata de
                            cambiarlos por otros, sino a darle otro aspecto diferente
                            y mucho más atractivo.
                        </p>
                        <br>
                        <p class="text-center otamendi fs-2 text-main">
                            ¿Te animas a renovar tus muebles?
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
