@extends('layouts.web')
@section('content')
<style>
    .image-container-home {
        height: auto;
        /* margin-top: -30vh;
        margin-bottom: -35vh; */
    }

    .img-text {
        display: block;
        position: relative;
        width: 100%;
    }
</style>
<section class="main-header" style="background-image:url({{ asset('assets/banners/saldos2.jpg') }})">
    <header>
        <div class="container text-center">
            <h2 class="h2 title">Saldos</h2>
        </div>
    </header>
</section>
<div class="container">
    <div class="row my-1">
        <div class="image-container-home">
            <div class="row overflow-y">
                <div class="col-sm-12 col-md-5 text-center pa-05">
                    <div class="pa-2 bg-white">
                        <div>
                            <img src="{{ asset('images/saldos1.png') }}" alt="" class="img-responsive center-block" />
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-7">
                    <img src="{{ asset('images/saldos.jpg') }}" alt="" class="img-responsive center-block img-text" />
                    <div class="restauracion-text">
                        <p class="text-center"></p>
                        <p class=" otamendi text-center text-black">Somos</p>
                        <p class="fs-6 otamendi text-center text-black pb-05em">Ecofriendly</p>
                        <p class="text-justify">
                            Aquí podrás ver y comprar muebles en liquidación expuestos en nuestra tienda montados o sin montar. Muebles en stock
                            procedentes de fábricas o devoluciones a precios de saldo.
                        </p>
                        <p class="text-justify">
                            Muchas veces se compran muebles por impulso, o sin haber tomado medidas correctamente y cuando el artículo llega a
                            casa
                            nos damos cuenta de nuestro error. Las fábricas con las que trabajamos aceptan la devolución sin coste y para que
                            esos
                            muebles no se destruyan, desde hometohome volvemos a gestionar su venta al 50% o menos de su precio original.
                        </p>
                        <p>
                            Si quieres te lo llevamos directamente a tu domicilio.
                        </p>
                        <br>
                        <p class="text-center otamendi fs-2">
                        </p>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection
