@extends('layouts.pdf')
@section('content')
<div class="container">

    {{-- <div class="pt-5 text-right">En Madrid {{ date('d-m-Y', strtotime($agreement->created_at)) }}</div> --}}
<div class="pt-3 text-justify">
    DE UNA PARTE <span class="text-capitalize font-weight-bold"> {{ $agreement->client->user->name }}
        {{ $agreement->client->user->profile->last_name }}</span>, con DNI
    <span class=" font-weight-bold">{{ $agreement->client->user->profile->dni }}</span> y domicilio en <span
        class="font-weight-bold">{{ $agreement->client->user->profile->address }}</span>
    y reconociendo la siguiente relación de objetos, mencionada en dicho contrato y recogida en el ANEXO 1 de su propiedad.
</div>
<div class="pt-3 text-justify">
    DE OTRA PARTE vaciatucasa & hometohome con domicilio comercial en Leganés Calle Asturias, 9 28913
</div>

<div class="text-justify my-5">
    <h5 class="pt-2"> CONDICIONES </h5>

    {{-- <h5 class="pt-2"> PRIMERA </h5> --}}

    <p>
        Vaciatucasa es una empresa dedicada a la gestión de la venta de todos los artículos (ANEXO 1) que el cliente quiere vender. En ningún
        momento vaciatucasa es propietaria de ninguno de los objetos recogidos en el ANEXO 1.
    </p>

    {{-- <h5 class="pt-2"> SEGUNDA </h5> --}}
    <p class="my-2">
        Gestiones a realizar:
    </p>

    <p class="my-2">
        1) Inventariado, fotografiado y/o grabación de todos los artículos que el cliente desea vender.
    </p>
    <p class="my-2">
        2) Valoración de todos los artículos. Dicha valoración figura en el (ANEXO 1)
    </p>
    <p class="my-2">
        3) Realizar las gestiones oportunas para la venta de los artículos del cliente,tales como:
    </p>
    <ul class="ml-5">
        <li>Realización de los anuncios delos artículos en nuestra Web.</li>
        <li>Publicación de dichos anuncios en Webs especializadas</li>
        <li>Publicidad en redes sociales.</li>
        <li>Realizar una política de descuento para favorecer la venta:</li>
        <ul>
            <dl> 1. A las 6 semanas se aplicará el 15%.</dl>
            <dl> 2. A las 8 semanas se aplicará el 30%.</dl>
            <dl> 3. A las 12 semanas se aplicará el 50%.</dl>
            <dl> 4. A las 16 semanas, los artículos que no se hayan vendidos se darán de baja de la Web, adémas de concluirse su contrato.
            </dl>

            <dl>-Al término del contrato (12 semanas), excepcionalmente se podrá ampliar el contrato hasta un máximo de 16 semanas y se aplicará un descuento del 50% del precio original.</dl>
        </ul>
        <li>Intermediar con todos los posibles compradores, encargándose de la coordinación de la visita.</li>
        <li>Gestionar y cerrar con los posibles compradores el precio de cada artículo, para lo cual el cliente le
            confiere expresa representación por medio de este documento</li>
    </ul>
    <p class="my2">
        4) Vaciatucasa dispone de una capacidad de negociación sobre el precio fijado con el cliente de un máximo del 15% de descuento sin previa
        consulta
    </p>

    {{-- <h5 class="pt-2"> TERCERA </h5> --}}

    La duración del presente contrato es de 16 semanas, <span class="font-weight-bold font-italic"> iniciándose el día de publicación de los
        artículos en la Web.</span>

    La comisión para HOMETOHOME aplicada a dicha gestión será del 40% del precio final de todos los artículos vendidos.

    El cliente otorga exclusividad a vaciatucasa para gestionar la venta de todos los artículos desde el momento de la firma del contrato hasta
    la finalización del mismo.

    Durante la duración del contrato, en caso de que el cliente quiera retirar de la venta alguno o algunos de los artículos que figuran en el
    ANEXO 1 pagará una penalización a HOMETOHOME, del 40% del valor de tasación inicial de cada artículo o artículos anulados.

    Por el presente documento se consiente de forma expresa que los datos de carácter personal facilitados sean transmitidos a terceros con la
    exclusiva finalidad del cumplimiento del presente contrato.

    <div class="text-justify">
        <h5 class="pt-2"> CLÁUSULAS DEPÓSITO Y EXPOSICIÓN MOBILIARIO </h5>
        <p>
            1. El depósito tiene por objeto la exposición y venta de dichos artículos en el plazo de 16 semanas
        </p>
        <p>
            2. En el caso de que algún artículo quede sin vender, el depósito será gratuito respecto de aquél.
        </p>
        <p>3. El transporte y montaje de los artículos hasta el local de depósito corre a cargo del depositante, al igual que, en su caso, los
            gastos de retirada y desmontaje. </p>
        <p>4. El local de depósito está situado en Calle Asturias nº 9 de Leganés, C.P. 28913 (Madrid), lugar donde permanecerán los artículos el
            tiempo que dure el depósito. </p>
        <p>5. Este cuidará conforme a criterio profesional de la exposición de los artículos, atendiendo a los potenciales compradores y
            percibiendo el importe de la venta, para su posterior liquidación con el cliente. </p>
        <p>6. Para la RETIRADA de los artículos no vendidos, el depositante remitirá correo electrónico a la dirección
            silvia.vaciatucasa@gmail.com a fin de concertar cita. </p>

        <p>
            MUY IMPORTANTE: EL PLAZO DE RETIRADA DE ARTÍCULOS NO VENDIDOS ES DE 7 DÍAS HÁBILES. EN EL CASO DE NO HABERLOS RETIRADO EN ESE PLAZO
            SE CONSIDERARÁN ABANDONADOS Y PASARÁN A PROPIEDAD DEL DEPOSITARIO. </p>

        {{-- <p>
      Por el presente documento el comitente consiente de forma expresa en que los datos de carácter personal
      facilitados
      a hometohome sean transmitidos a terceros con la exclusiva finalidad del cumplimiento del presente contrato.
    </p>
    <p>
      Y en prueba de conformidad con lo expuesto y acordado, firman el presente Acuerdo por duplicado en todas sus
      páginas, en el lugar y fechas señalados en el encabezamiento.
    </p> --}}
    </div>

    <div class="page-break">
        <h5 class="pt-2">
            A N E X O 1
        </h5>
        <p class="text-justify">
            Relación de artículos que el cliente: <span class="font-weight-bold font-italic">{{ $agreement->client->user->name }}
                {{ $agreement->client->user->profile->last_name }}</span> desea vender:
        </p>
        <div class="pt-5">
            <table class="table table-striped" width="100%" style="width:100%" border="0">
                <tr>
                    <th>#</th>
                    <th>Imagen</th>
                    <th>Nombre</th>
                    <th>Categoria</th>
                    <th>Estado</th>
                    <th>Precio</th>
                </tr>
                {{-- <tbody> --}}
                @foreach($agreement->products as $product)
                <tr>
                    <td>{{$loop->iteration}}</td>
                    <td class="text-center">
                        <img src="{{ $product->images->first()->path }}" alt="" height="50">
                    </td>
                    <td> <a href="{{ route ('products.show', $product) }}"> {{ $product->name }} </a> </td>
                    <td> {{ $product->category->name }} </td>
                    <td class="text-warning"> {{ $product->status }} </td>
                    <td> {{ $product->normal_price }} <i class="fa fa-euro"></i> </td>
                </tr>
                @endforeach
                <tr>
                    <td colspan="5" class="text-right">TOTAL EUROS</td>
                    <td> {{ $total}} </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
