@extends('layouts.web')
@section('content')
@include('partials.product.header-category')
<!-- ========================  Product ======================== -->
<style scoped>
    .products article .label {
        position: inherit !important;
    }

    .owl-carousel .owl-item img {
        /* max-width: 100% !important; */
        max-height: 500px;
    }

    .img-responsive {
        /* max-height: 275px */
        margin-left: auto;
        margin-right: auto;
    }

    :root {
        /* Not my favorite that line-height has to be united, but needed */
        --lh: 1.4rem;
    }

    html {
        line-height: var(--lh);
    }

    .paddding-avatar {
        padding-left: 4em;
        padding-right: 4em;
    }
    .min-height{
        min-height: 100px;
    }
</style>
<section class="product">
    <div class="main">
        <div class="container">
            <div class="row">
                <div class="col-md-12">@include('client.messages')</div>
            </div>

            <div class="row product-flex">
                <div class="col-md-4 col-sm-12 product-flex-info">
                    <div class="clearfix">

                        <!-- === product-title === -->
                        <div class="clearfix">
                            <h1 class="title" data-title="{{$product->name}}"> {{ $product->name }}</h1>
                            @if ($product->code)
                            <h4 class=""> {{ $product->code }}</h4>
                            @endif
                            <div class="price text-bold text-black">
                                @if ($product->reduced_price)
                                <span class="h4"> {{$product->reduced_price}} <i class="fa fa-euro"></i> <small> <i class="fa fa-euro"></i>
                                        {{$product->normal_price}}</small></span>
                                @else
                                <span class="h4"> {{$product->normal_price}} <i class="fa fa-euro"></i>
                                    <small></small></span>
                                @endif
                            </div>
                            <hr />

                            <!-- === info-box === -->

                            <div class="info-box">
                                <span><strong> Estancia</strong></span>
                                <span class=""> {{$product->category->name}} </span>
                            </div>

                            @if ($product->long)

                            <div class="info-box info-box-addto">
                                <span><strong>Largo</strong></span>
                                <span> {{ $product->long }} </span>
                            </div>
                            @endif

                            @if ($product->width)
                            <div class="info-box info-box-addto">
                                <span><strong>Ancho</strong></span>
                                <span> {{ $product->width }} </span>
                            </div>
                            @endif

                            @if ($product->height)
                            <div class="info-box info-box-addto added">
                                <span><strong>Alto</strong></span>
                                <span> {{ $product->height }} </span>
                            </div>
                            @endif

                            @if ($product->height)
                            <div class="info-box info-box-addto added">
                                <span><strong>Peso</strong></span>
                                <span> {{ $product->weight }} </span>
                            </div>
                            @endif

                            @if (auth()->user() && $product->status == 'RESERVADO' && $product->user && $product->user->id = auth()->user()->id)
                            <a class="btn btn-warning form-control disabled" style="margin-top:1em">Reservado con <i class="fa fa-euro"></i>
                                {{ $product->payment }}, <span class="text-details">saldo <i class="fa fa-euro"></i>
                                    @if ($product->reduced_price)
                                    {{$product->reduced_price - $product->payment }}
                                    @else
                                    {{$product->normal_price - $product->payment}}
                                    @endif
                                </span>
                            </a>
                            <a class="btn btn-primary form-control" style="margin-top:1em"
                                href="{{ route('articulo.PublishedProduct', $product)}}">Volver a publicar</a>
                            @endif

                            @if ( auth()->user() && $product->user->id != auth()->user()->id)
                            <a class="btn btn-primary form-control" href="{{ route('page.comprar', $product)}}">Solicitar Compra</a>
                            @else
                            <a class="btn btn-primary form-control" href="{{ route('page.comprar', $product)}}">Solicitar Compra</a>
                            @endif
                            <div class="info-box text-center pa-2">
                                <span>
                                    {{-- {!!QrCode::size(200)->generate(route('page.detail_product',$product)) !!} --}}
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- === product item gallery === -->
                <div class="col-md-7 col-sm-12 product-flex-gallery">
                    <!-- === product gallery === -->
                    <div class="container-fluid">
                        <div class="owl-product-gallery open-popup-gallery">
                            @foreach ($product->images as $img)
                            <a href="{{ asset($img->path) }}">
                                <img id="imagenslider" src="{{ asset($img->path) }}" alt="sdf" class="img-responsive center-block" /></a>
                            @endforeach
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <!-- === product-info === -->

    <div class="info">
        <div class="container">
            <div class="row">
                @if (!$product->client_agreement)
                <div class="col-md-4">
                    <div class="designer">
                        <div class="box">
                            <div class="container-fluid bg-white paddding-avatar">
                                <img src="{{ asset($user->profile->avatar) }}" alt="Alternate Text" class="img-responsive rounded-img min-height" />
                            </div>
                            <div class="name">
                                <div class="h3 title"> {{ $user->name }} <small> {{-- Arhitect --}}</small></div>
                                <hr />
                                {{-- <p><a><i class="icon icon-envelope"></i>{{ $user->email }}</a> </p> --}}
                                <p>
                                    <a href="https://web.whatsapp.com/send?phone={{ $user->profile->phone_number }}">
                                        <i class="fa fa-whatsapp"></i> {{ $user->profile->phone_number }}
                                    </a>
                                </p>
                                <p class="h3 title">
                                    Iniciar Chat
                                    <a href="https://web.whatsapp.com/send?phone={{ $user->profile->phone_number }}" target="_blank"
                                        class="btn btn-xs">
                                        <img src="{{ asset('assets/WhatsApp.svg') }}" alt="" height="50">
                                    </a>
                                </p>
                            </div>
                            <!--/name-->
                        </div>
                        <!--/box-->
                        <div class="btn btn-add">
                            <i class="icon icon-phone-handset"></i>
                        </div>
                    </div>
                    <!--/designer-->
                </div>
                @endif
                <!--/col-md-4-->
                <!-- === nav-tabs === -->

                <div class="col-md-12">
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" >
                            <a href="#Articles" aria-controls="Articles" role="tab" data-toggle="tab">
                                <i class="icon icon-user"></i>
                                <span>Articulos</span>
                            </a>
                        </li>
                        <li role="presentation" class="active">
                            <a href="#design" aria-controls="design" role="tab" data-toggle="tab">
                                <span>Detalles</span>
                            </a>
                        </li>
                    </ul>

                    <!-- === tab-panes === -->

                    <div class="tab-content">

                        <div role="tabpanel" class="tab-pane " id="Articles">
                            <div class="content">

                                <!-- === Articles collection title === -->

                                <h3>Otros Artículos</h3>

                                <div class="products">
                                    <div class="row">
                                        <!-- === product-item === -->
                                        @foreach ($articles as $item)
                                        <div class="col-md-4 col-sm-6 col-xs-12 my-1">
                                            <article>
                                                <div class="fondo">
                                                    <div class="figure-grid">
                                                        <div class="image pt-2">
                                                            <a href="{{ route('page.detail_product', $item->id) }}">
                                                                <div class="image-container">
                                                                    <img src="{{ asset($item->images->first()->path) }}" alt=""
                                                                        class="img-responsive center-block" />
                                                                </div>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="text-center bg-white">
                                                        <p class="box-title"><a
                                                                href="{{ route('page.detail_product', $item->id) }}">{{ $item->name }}</a></p>
                                                        <hr class="borde-grueso">
                                                        <p class="text-center fs-1-4">
                                                            @if ($item->reduced_price)
                                                            <sub class="tachado"> {{ $item->normal_price}} <i class="fa fa-euro"></i></sub>
                                                            <sub> <b> {{ $item->reduced_price}} </b> <i class="fa fa-euro"></i></sub>
                                                            @else
                                                            <sub class="tachado"></sub>
                                                            <sub>{{ $item->normal_price}} <i class="fa fa-euro"></i></sub>
                                                            @endif
                                                        </p>
                                                    </div>
                                                </div>
                                            </article>
                                        </div>
                                        @endforeach
                                    </div>
                                    <!--/row-->
                                </div>
                                <!--/products-->
                            </div>
                            <!--/content-->
                        </div>
                        <!--/tab-pane-->
                        <!-- ============ tab #2 ============ -->

                        <div role="tabpanel" class="tab-pane active" id="design">
                            <div class="content">
                                <div class="row">
                                    <div class="col-md-12">
                                        <h3>Descripcion del artículo</h3>
                                        <p>
                                            {!! $product->description !!}
                                        </p>
                                    </div>
                                </div>
                                <!--/row-->
                            </div>
                        </div>
                    </div>
                    <!--/tab-content-->
                </div>
            </div>
            <!--/row-->
        </div>
        <!--/container-->
    </div>
    <!--/info-->
</section>
@endsection
