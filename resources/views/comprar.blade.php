@extends('layouts.web')
@section('content')
{!! NoCaptcha::renderJs() !!}
<span name="{{ $company = \App\Company::get()->first() }}"></span>
<section class="main-header" style="background-image:url({{ asset($company->img1) }})">
    <header>
        <div class="container text-center">
            {{-- <h2 class="h2 title">Vende tus Artículos de Segunda Mano</h2>
            <p>Si quieres que vendamos tus muebles rellena el formulario</p> --}}
            <ol class="breadcrumb breadcrumb-inverted">
                <li><a href="/"><span class="icon icon-home"></span></a></li>
                <li><a class="active" href="">Formulario de compra</a></li>
            </ol>
        </div>
    </header>
</section>
<style scoped>
    /* .owl-carousel .owl-item img {
        width: 100%;
        height: 500px;
    } */
    .owl-carousel .owl-item img {
        width: 100%;
        max-height: 50rem;
        padding: 0.5em;
    }

    .owl-theme .owl-controls .owl-pagination {
        position: inherit;
        width: 100%;
        bottom: 0;
    }

    .owl-theme .owl-controls .owl-buttons div {
        position: absolute;
        top: 100% !important;
        right: auto;
        left: auto;
    }

    .descripcion-details {
        line-height: 1.5em;
        letter-spacing: 1px
    }

    .banner {
        color: #3a3d45;
        background-color: transparent;
    }
</style>
<section class="product">
    <div class="main">
        <div class="container">
            <div class="row">
                <div class="col-md-12">@include('client.messages')</div>
            </div>
            <div class="row product-flex">
                <div class="col-md-4 col-sm-12">
                    <div class="clearfix">
                        <!-- === product-title === -->
                        <h1 class="title" data-title="{{$product->name}}"> {{ $product->name }}</h1>

                        <div class="clearfix">
                            <div class="price">
                                @if ($product->reduced_price)
                                <span class="h3"> {{$product->reduced_price}} <i class="fa fa-euro"></i> <small> <i class="fa fa-euro"></i>
                                        {{$product->normal_price}}</small></span>
                                @else
                                <span class="h3"> {{$product->normal_price}} <i class="fa fa-euro"></i>
                                    <small></small></span>
                                @endif
                            </div>
                            <hr />
                            <!-- === info-box === -->
                            <div class="info-box">
                                <span><strong> Estancia</strong></span>
                                <span class="text-primary"> {{$product->category->name}} </span>
                            </div>

                            <hr />

                            <div class="info-box info-box-addto added">
                                <span><strong>Peso</strong></span>
                                <span> {{ $product->weight }} </span>
                            </div>

                            <div class="info-box info-box-addto">
                                <span><strong>Largo</strong></span>
                                <span> {{ $product->long }} </span>
                            </div>

                            <div class="info-box info-box-addto">
                                <span><strong>Ancho</strong></span>
                                <span> {{ $product->width }} </span>
                            </div>
                            <div class="info-box info-box-addto added">
                                <span><strong>Alto</strong></span>
                                <span> {{ $product->height }} </span>
                            </div>
                            <div class="info-box info-box-addto added">
                                <span><strong>Telefono</strong></span>
                                <span>
                                    @if ($product->client_agreement)
                                    <a href="https://web.whatsapp.com/send?phone={{ $product->client_agreement->handler->user->profile->phone_number }}"
                                        class="btn btn-primary btn-sm" target="_blank"><i class="fa fa-whatsapp"></i>
                                        {{$product->client_agreement->handler->user->profile->phone_number}}</a>
                                    @else
                                    <a href="https://web.whatsapp.com/send?phone={{ $product->user->profile->phone_number }}"
                                        class="btn btn-primary btn-sm" target="_blank"><i class="fa fa-whatsapp"></i>
                                        {{$product->user->profile->phone_number}}</a>
                                    @endif
                                </span>
                            </div>
                        </div>
                        {{-- <p class="descripcion-details"> <label class="text-primary"> Descripcion: </label> {!!$product->description!!}</p>
                        --}}
                        <!--/clearfix-->
                    </div>
                    <!--/product-info-wrapper-->
                </div>
                <!--/col-md-4-->
                <!-- === product item gallery === -->
                <div class="col-md-7 col-sm-12">
                    <!-- === product gallery === -->
                    <div class="owl-product-gallery open-popup-gallery image-container">
                        @foreach ($product->images as $img)
                        <a href="{{ asset($img->path) }}">
                            <img id="imagenslider" src="{{ asset($img->path) }}" alt="sdf" class="img-responsive center-block" />
                        </a>
                        @endforeach
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>
@if ($product->user != auth()->user())
<section class="contact">
    <div class="container">
        <div class="row">
            <div class="col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1">
                <div class="contact-block">
                    <div class="banner">
                        <div class="row">
                            @if ($product->importance > 0)
                            <div class="col-md-offset-1 col-md-10 text-center">
                                <img id="imagenslider" src="{{ asset('images/solicita_compra.png') }}" alt="sdf"
                                    class="img-responsive center-block" />

                                <p>Una persona de nuestro equipo se pondrá </p>
                                <p>en contacto contigo para finalizar la compra</p>
                                <div class="contact-form-wrapper">
                                    {!! Form::open(['route' => ['comprar.store', $product]]) !!}
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                @if (auth()->user())
                                                <input id="name" name="name" type="text" value="{{ auth()->user()->name }}" required
                                                    class="form-control" placeholder="Nombre">
                                                @else
                                                <input id="name" name="name" type="text" required class="form-control" placeholder="Nombre">
                                                @endif
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                @if (auth()->user())
                                                <input id="email" name="email" type="email" value="{{ auth()->user()->email }}" required
                                                    class="form-control" placeholder="Correo">
                                                @else
                                                <input id="email" name="email" type="email" required class="form-control" placeholder="Correo">
                                                @endif
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                @if (auth()->user())
                                                <input id="phone" name="phone" type="phone" value="{{ auth()->user()->profile->phone_number }}"
                                                    required class="form-control" placeholder="Teléfono">
                                                @else
                                                <input id="phone" name="phone" value="+34" type="phone" required class="form-control" placeholder="Teléfono con código de pais">
                                                @endif
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                @if (auth()->user())
                                                <input id="city" name="city" type="text" value="{{ auth()->user()->profile->code_postal }}"
                                                    required class="form-control" placeholder="Ciudad">
                                                @else
                                                <input id="city" name="city" type="text" required class="form-control" placeholder="Ciudad">
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <textarea id="comments" name="comments" class="form-control"
                                                    placeholder="Consultas y comentarios" rows="3"></textarea>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="text-center">
                                            </div>
                                        </div>
                                        <div class="col-md-12 text-center">
                                            <div class="form-group{{ $errors->has('g-recaptcha-response') ? ' has-error' : '' }}">
                                                {!! app('captcha')->display() !!}
                                                @if ($errors->has('g-recaptcha-response'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('g-recaptcha-response') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-md-12 text-center">
                                            <input type="submit" class="btn btn-primary" value="Solicitar Compra" />
                                        </div>
                                    </div>
                                    {!! Form::close() !!}
                                </div>
                            </div>
                            @else
                            <div class="col-md-offset-1 col-md-10">
                                <h2 class="title text-center">Datos de Vendedor</h2>
                                {{-- <p class="text-center">Una persona de nuestro equipo se pondrá en contacto contigo para finalizar la compra,
                                    gracias</p> --}}
                                <hr>
                                <p> Nombre del Vendedor: {{$product->user->name}}</p>
                                <p> Telefono: {{$product->user->profile->phone_number}}
                                    <a href="https://web.whatsapp.com/send?phone={{ $product->user->profile->phone_number }}"
                                        class="btn-primary btn-sm" target="_blank"><i class="fa fa-whatsapp"></i>
                                        Iniciar Chat </a>
                                </p>
                                <p> Email: {{$product->user->email}}</p>
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endif

@endsection
