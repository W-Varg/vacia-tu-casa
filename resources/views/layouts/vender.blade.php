@extends('layouts.web')
@section('content')
<span name="{{ $company = \App\Company::get()->first() }}"></span>
<section class="main-header" style="background-image:url({{ asset($company->img2) }}">
</section>
<style scoped>
    .pasos-img {
        max-height: 35vh;
        /* margin-top: -5%;
        margin-bottom: -5% */
    }

    .register {
        max-height: 400px;
        /* margin-top: -5%;
        margin-bottom: -5% */
    }

    .select {
        max-height: 550px;
        /* margin-top: -10%;
        margin-bottom: -10% */
    }

    .middle-line {
        margin-top: -7vh;
    }
</style>
<section class="bg-white">
    <div class="info bg-white">
        <div class="container">
            <div class="row">
                <header>
                    <div class="container text-center">
                        <div class="otamendi text s-70"> ¿ Quiéres Vender ? </div>
                        <p> </p>
                    </div>
                </header>
                <div class="col-md-12">
                    <p class="fs-1-4 text-justify">
                        Ofrecemos todas las soluciones para que tú solo te ocupes de lo importante. Tanto si solo quieres vender un artículo que
                        ya no usas o no te encaja en la nueva decoración, como si te trasladas a otro lugar y necesitas que gestionemos la venta
                        de todo tu mobiliario, aquí nos tienes para ayudarte de una manera sencilla y profesional.
                    </p>
                    <hr class="hr-green my-3em">
                </div>
                <div class="col-md-12">
                    <div class="text-center fs-1-3">
                        <p>Vender con nosotros </p>
                        <p>es fácil y rápido</p>
                        <p><span class="text-main">Muebles de casa a casa.</span></p>
                        <div class="container-fluid ma-3">
                            <img src="{{ asset('images/01.png') }}" class="img-responsive center-block pasos-img" />
                        </div>
                        <div class="">
                            <p class="otamendi text s-60">
                                <span class="bg-white pay-1">Registrarse</span>
                            </p>
                        </div>
                        <hr class="hr-green middle-line">
                        <div class="container-fluid ma-4">
                            <img src="{{ asset('images/register.png') }}" class="img-responsive center-block register" />
                        </div>
                        <p> Para acceder al servicio de venta debes registrarte en nuestra web indicando un correo electrónico válido.</p>
                        <div class="container-fluid ma-3">
                            <img src="{{ asset('images/02.png') }}" class="img-responsive center-block pasos-img" />
                        </div>
                        <p class="otamendi text s-60">
                            <span class="bg-white pay-1">Selecciona tu Método de venta</span>
                        </p>
                        <hr class="hr-green middle-line">
                    </div>
                </div>
                <div class="col-md-12 ma-2">
                    <div class="text-center">
                        <div class="row">
                            <div class="col-12">
                                <div class="row fs-1-3">
                                    <div class="col-md-4 col-sm-12">
                                        <div class="container-fluid">
                                            <img src="{{ asset('images/venta_1.png') }}" class="img-responsive center-block select" />
                                        </div>
                                        Tú mismo gestionas tus artículos. Selecciona las mejores fotos, describe y pon un precio razonable.
                                    </div>
                                    <div class="col-md-4 col-sm-12">
                                        <div class="container-fluid">
                                            <img src="{{ asset('images/venta_2.png') }}" class="img-responsive center-block select" />
                                        </div>

                                        Buscamos un comprador por ti. Visitamos tu domicilio, valoramos, describimos y
                                        fotografiamos tus muebles. Estos permanecen en tu casa.
                                    </div>
                                    <div class="col-md-4 col-sm-12">
                                        <div class="container-fluid">
                                            <img src="{{ asset('images/venta_3.png') }}" class="img-responsive center-block select" />
                                        </div>
                                        Si además necesitas trasladarlos a un espacio de exposición dónde profesionales
                                        gestionan su venta.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="text-center">
                        <div class="container-fluid ma-3">
                            <img src="{{ asset('images/03.png') }}" class="img-responsive center-block pasos-img" />
                        </div>
                        <p class="otamendi text s-60">
                            <span class="bg-white pay-1">Transporte</span>
                        </p>
                        <hr class="hr-green middle-line">
                        <div class="container-fluid ma-3">
                            <img src="{{ asset('images/transporte.png') }}" class="img-responsive center-block register" />
                        </div>
                        <p class="fs-1-3">¡Contamos con empresas de transporte de confianza con precios muy competitivos que se encargan de todo!</p>
                    </div>
                    <h4 class="text-center pt-2e">
                        <span class="bg-white pay-1 fs-1-5">VENDE CON VACIA TU CASA</span>
                    </h4>
                    {{-- <hr class="hr-green middle-line"> --}}
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
