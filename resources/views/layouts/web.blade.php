<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- Mobile Web-app fullscreen -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="mobile-web-app-capable" content="yes">
    <!-- Meta tags -->

    <meta name="author" content="Vacia Tu Casa" />
    <meta name="description"
        content="Sofas y muebles de segunda mano Madrid. Descubre una nueva forma sencilla, comoda, barata y profesional de comprar y vender" />
    <!-- Google / Search Engine Tags -->
    <meta itemprop="name" content="HomeToHomeDecor">
    <meta itemprop="image" content="{{ asset('assets/images/logo.png') }}">
    <meta itemprop="description" content="Sofás y muebles de segunda mano Madrid">

    <!-- Facebook Meta Tags -->
    <meta property="og:url" content="http://hometohomedecor.com/">
    <meta property="og:type" content="website">
    <meta property="og:title" content="HomeToHomeDecor">
    <meta property="og:description" content="Sofás y muebles de segunda mano Madrid">
    <meta property="og:image" content="{{ asset('assets/images/logo.png') }}">

    <!-- Twitter Meta Tags -->
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="HomeToHomeDecor">
    <meta name="twitter:description" content="Sofás y muebles de segunda mano Madrid">
    <meta name="twitter:image" content="{{ asset('assets/images/logo.png') }}">

    <link rel="icon" href="{{ asset('assets/images/icon.png') }}" />

    <!--Title-->
    <title> Sofás y muebles de segunda mano Madrid</title>

    <link href="https://fonts.googleapis.com/css?family=Dancing+Script:400,700|Open+Sans:300,300i,400,400i,600,600i,700,700i" rel="stylesheet">

    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600&amp;subset=latin-ext" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Comfortaa:wght@400;500;600&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Work+Sans:wght@300;400;500;600&display=swap" rel="stylesheet">
    <!--CSS styles-->
    <link rel="stylesheet" media="all" href="{{ asset('css/bootstrap.css') }}" />
    <link rel="stylesheet" media="all" href="{{ asset('css/animate.css') }}" />
    <link rel="stylesheet" media="all" href="{{ asset('css/font-awesome.css') }}" />
    <link rel="stylesheet" media="all" href="{{ asset('css/furniture-icons.css') }}" />
    <link rel="stylesheet" media="all" href="{{ asset('css/linear-icons.css') }}" />
    <link rel="stylesheet" media="all" href="{{ asset('css/magnific-popup.css') }}" />
    <link rel="stylesheet" media="all" href="{{ asset('css/owl.carousel.css') }}" />
    <link rel="stylesheet" media="all" href="{{ asset('css/ion-range-slider.css') }}" />
    <link rel="stylesheet" media="all" href="{{ asset('css/theme.css') }}" />

    @yield('styles')

    <!--Google fonts-->
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->


    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-87Q638ZHGY"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'G-87Q638ZHGY');

        !function(f,b,e,v,n,t,s)
        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};
        if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
        n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t,s)}(window,document,'script',
        'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '912971325964263');
        fbq('track', 'PageView');
    </script>

</head>

<body>

    <div class="page-loader"></div>

    <div class="wrapper">

        <!-- ======================== Navigation ======================== -->

        @include('partials.navigation', ['company'=> \App\Company::get()->first()])

        <!-- ======================== Content ======================== -->

        @yield('content')

        <!-- ================== Footer  ================== -->

        @include('partials.footer', ['company'=> \App\Company::get()->first()])

    </div>
    <!--/wrapper-->

    <!--JS files-->
    <script src="{{ asset('js/jquery.min.js') }}"></script>
    <script src="{{ asset('js/jquery.bootstrap.js') }}"></script>
    <script src="{{ asset('js/jquery.magnific-popup.js') }}"></script>
    <script src="{{ asset('js/jquery.owl.carousel.js') }}"></script>
    <script src="{{ asset('js/jquery.ion.rangeSlider.js') }}"></script>
    <script src="{{ asset('js/jquery.isotope.pkgd.js') }}"></script>
    <script src="{{ asset('js/main.js') }}"></script>
    <script src="{{ asset('vendor/bootstrap-notify/bootstrap-notify.min.js') }}"></script>
    @yield('scripts')
</body>

</html>
