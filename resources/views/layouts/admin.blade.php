<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="{{ $company = App\Company::get()->first() }}">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <link rel="icon" href="{{ asset('assets/images/icon.png') }}">

    <title>Admin - Vacia tu casa</title>

    <!-- Bootstrap 4.0-->
    <link rel="stylesheet" href="{{ asset('adm/assets/vendor_components/bootstrap/dist/css/bootstrap.min.css') }}">

    <!--amcharts -->
    <link href="https://www.amcharts.com/lib/3/plugins/export/export.css" rel="stylesheet" type="text/css" />

    <!-- Bootstrap-extend -->
    <link rel="stylesheet" href="{{ asset('adm/css/bootstrap-extend.css') }}">

    <!-- theme style -->
    <link rel="stylesheet" href="{{ asset('adm/assets/vendor_components/font-awesome/css/font-awesome.css') }}">
    <link rel="stylesheet" href="{{ asset('adm/assets/vendor_components/Ionicons/css/ionicons.css') }}">
    <link rel="stylesheet" href="{{ asset('adm/assets/vendor_components/themify-icons/themify-icons.css') }}">
    <link rel="stylesheet" href="{{ asset('adm/assets/vendor_components/linea-icons/linea.css') }}">
    <link rel="stylesheet" href="{{ asset('adm/assets/vendor_components/glyphicons/glyphicon.css') }}">
    <link rel="stylesheet" href="{{ asset('adm/assets/vendor_components/flag-icon/css/flag-icon.css') }}">
    <link rel="stylesheet" href="{{ asset('adm/assets/vendor_components/material-design-iconic-font/css/materialdesignicons.css') }}">
    <link rel="stylesheet" href="{{ asset('adm/assets/vendor_components/simple-line-icons-master/css/simple-line-icons.css') }}">
    <link rel="stylesheet" href="{{ asset('adm/assets/vendor_components/cryptocoins-master/webfont/cryptocoins.css') }}">


    <link rel="stylesheet" href="{{ asset('adm/css/master_style.css') }}">

    <!-- HomeToHome_Admin skins -->
    <link rel="stylesheet" href="{{ asset('adm/css/skins/_all-skins.css') }}">
    @yield('styles')
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
    <link href="https://printjs-4de6.kxcdn.com/print.min.css" rel="stylesheet" type="text/css" />
    <script src="https://printjs-4de6.kxcdn.com/print.min.js"></script>
</head>

<body class="hold-transition skin-black sidebar-mini">
    <div class="wrapper">

        @include('partials.admin.navigation-top')

        <!-- Left Menu -->
        <aside class="main-sidebar">
            <!-- sidebar -->
            <section class="sidebar">
                <!-- Sidebar user panel -->
                @include('partials.admin.user-info')

                <!-- sidebar menu -->
                @include('partials.admin.navigation')

            </section>
        </aside>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            @yield('breadcrumb')

            @if (session('info')) {{--para mostrar los sms --}}
            <div class="mt-3">
                <div class="col-12">
                    <div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <h5><i class="icon fa fa-check"></i> {{ session('info') }} </h5>
                    </div>
                </div>
            </div>
            @endif
            @if (session('error')) {{--para mostrar los sms --}}
            <div class="mt-3">
                <div class="col-12">
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <h5> {{ session('error') }} </h5>
                    </div>
                </div>
            </div>
            @endif
            @if (count($errors)) {{--para mostrar los sms --}}
            <div class="mt-3">
                <div class="col-12">
                    <div class="alert alert-danger alert-dismissible">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li> {{ $error }} </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
            @endif

            <!-- Main content -->
            <section class="content">
                @yield('content')
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->

        <footer class="main-footer">
            <div class="pull-right d-none d-sm-inline-block">

            </div>
            &copy; 2019 Vacia tu Casa. Todos los derechos reservados.
        </footer>

        <!-- Add the sidebar's background. This div must be placed immediately after the control sidebar -->
        <div class="control-sidebar-bg"></div>

    </div>
    <!-- ./wrapper -->

    <!-- jQuery 3 -->
    <script src="{{ asset('adm/assets/vendor_components/jquery/dist/jquery.min.js') }}"></script>

    <!-- popper -->
    <script src="{{ asset('adm/assets/vendor_components/popper/dist/popper.min.js') }}"></script>

    <!-- Bootstrap 4.0-->
    <script src="{{ asset('adm/assets/vendor_components/bootstrap/dist/js/bootstrap.js') }}"></script>

    <!-- Slimscroll -->
    <script src="{{ asset('adm/assets/vendor_components/jquery-slimscroll/jquery.slimscroll.js') }}"></script>

    <!-- FastClick -->
    <script src="{{ asset('adm/assets/vendor_components/fastclick/lib/fastclick.js') }}"></script>

    <!--amcharts charts -->
    <script src="http://www.amcharts.com/lib/3/amcharts.js" type="text/javascript"></script>
    <script src="http://www.amcharts.com/lib/3/gauge.js" type="text/javascript"></script>
    <script src="http://www.amcharts.com/lib/3/serial.js" type="text/javascript"></script>
    <script src="http://www.amcharts.com/lib/3/amstock.js" type="text/javascript"></script>
    <script src="http://www.amcharts.com/lib/3/pie.js" type="text/javascript"></script>
    <script src="http://www.amcharts.com/lib/3/plugins/animate/animate.min.js" type="text/javascript"></script>
    <script src="http://www.amcharts.com/lib/3/plugins/export/export.min.js" type="text/javascript"></script>
    <script src="http://www.amcharts.com/lib/3/themes/patterns.js" type="text/javascript"></script>
    <script src="http://www.amcharts.com/lib/3/themes/light.js" type="text/javascript"></script>

    <!-- webticker -->
    <script src="{{ asset('adm/assets/vendor_components/Web-Ticker-master/jquery.webticker.min.js') }}"></script>

    <!-- EChartJS JavaScript -->
    <script src="{{ asset('adm/assets/vendor_components/echarts-master/dist/echarts-en.min.js') }}"></script>
    <script src="{{ asset('adm/assets/vendor_components/echarts-liquidfill-master/dist/echarts-liquidfill.min.js') }}"></script>

    <!-- This is data table -->
    <script src="{{ asset('adm/assets/vendor_plugins/DataTables-1.10.15/media/js/jquery.dataTables.min.js') }}"></script>

    <!-- HomeToHome_Admin App -->
    <script src="{{ asset('adm/js/template.js') }}"></script>

    <!-- HomeToHome_Admin dashboard demo (This is only for demo purposes) -->
    <script src="{{ asset('adm/js/pages/dashboard.js') }}"></script>
    <script src="{{ asset('adm/js/pages/dashboard-chart.js') }}"></script>

    <!-- HomeToHome_Admin for demo purposes -->
    <script src="{{ asset('adm/js/demo.js') }}"></script>

    <!-- Form validator JavaScript -->
    <script src="{{ asset('adm/js/pages/validation.js') }}"></script>
    @yield('scripts')

    <script>
        ! function (window, document, $) {
          "use strict";
          $("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
        }(window, document, jQuery);
    </script>

</body>

</html>
