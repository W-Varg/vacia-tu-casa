@extends('layouts.web')
@section('content')
<span name="{{ $company = \App\Company::get()->first() }}"></span>
<section class="main-header" style="background-image:url({{ asset($company->img2) }}">
    {{-- <header>
        <div class="container text-center">
            <ol class="breadcrumb breadcrumb-inverted">
                <li><a href="/"><span class="icon icon-home"></span></a></li>
                <li><a href="{{ route('client_products')}}">Mis articulos</a></li>
    <li><a class="active"> Compra </a></li>
    </ol>
    </div>
    </header> --}}
</section>
<style scoped>
    .pasos-img {
        max-height: 35vh;
        /* margin-top: -5%;
        margin-bottom: -5% */
    }

    .register {
        max-height: 400px;
        /* margin-top: -5%;
        margin-bottom: -5% */
    }

    .select {
        max-height: 60vh;
        /* margin-top: -10%;
        margin-bottom: -10% */
    }

    .middle-line {
        margin-top: -7vh;
    }
</style>
<section class="bg-white">
    <div class="info bg-white">
        <div class="container">
            <div class="row">
                <header>
                    <div class="container text-center">
                        <div class="otamendi text s-60"> ¿ Quiéres Comprar ? </div>
                    </div>
                </header>
                <div class="col-md-12 text-justify fs-1-3">
                    <p> Decorar es divertido y puede ser una manera de cuidar nuestro entorno. Comprar muebles de segunda mano es una forma de
                        consumo responsable, con ello reciclas y reduces el impacto ambiental. </p>
                    <p> Si eres de los que piensa que los muebles merecen una segunda oportunidad, te encanta renovar tu casa por poco dinero,
                        esta es tu tienda!</p>
                    <hr class="hr-green">
                </div>
                <div class="col-md-12">
                    <div class="text-center">
                        <div class="container-fluid">
                            <img src="{{ asset('images/01-02.png') }}" class="img-responsive center-block select" />
                        </div>
                        <p class="fs-1-3">Compra directamente desde nuestra web, <br> recogemos el mueble de casa del vendedor <br> y te lo
                            entregamos en tu domicilio.</p>
                        <div class="container-fluid ma-3">
                            <img src="{{ asset('images/01.png') }}" class="img-responsive center-block pasos-img" />
                        </div>
                        <p class="otamendi text s-60">
                            <span class="bg-white pay-1">Verificados</span>
                        </p>
                        <hr class="hr-green middle-line">
                        <div class="container-fluid ma-2">
                            <img src="{{ asset('images/verificados.png') }}" class="img-responsive center-block select" />
                        </div>

                        <p class="fs-1-3">Todos nuestros artículos han sido verificados. <br> Deben superar un control de calidad realizado por
                            nuestro equipo.</p>
                        <div class="container-fluid">
                            <img src="{{ asset('images/02.png') }}" class="img-responsive center-block pasos-img" />
                        </div>
                        <p class="otamendi text s-60">
                            <span class="bg-white pay-1">Exposición Permanente</span>
                        </p>
                        <hr class="hr-green middle-line">
                        <div class="container-fluid  ma-2">
                            <img src="{{ asset('images/maximos.png') }}" class="img-responsive center-block register" />
                        </div>
                        <p class="fs-1-3">En nuestra exposición podrás elegir entre todos los estilos. <br>
                            Descubrirás un mundo singular dónde encontrar verdaderos tesoros. <br>
                            Solicita tu cita. Estaremos encantados de atenderte
                        </p>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="text-center">
                        <div class="container-fluid">
                            <img src="{{ asset('images/03.png') }}" class="img-responsive center-block pasos-img" />
                        </div>
                        <p class="otamendi text s-60">
                            <span class="bg-white pay-1">Transporte</span>
                        </p>
                        <hr class="hr-green middle-line">
                        <div class="container-fluid ma-2">
                            <img src="{{ asset('images/transporte.png') }}" class="img-responsive center-block register" />
                        </div>
                        <p class="fs-1-3">Contamos con empresas de confianza <br> a precios muy competitivos que <br> se encargan de todo.</p>
                    </div>
                    <h4 class="text-center pt-2e">
                        <span class="bg-white pay-1 fs-1-5">COMPRA EN HOME TO HOME</span>
                    </h4>
                    {{-- <hr class="hr-green middle-line"> --}}
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
