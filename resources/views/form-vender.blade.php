@extends('layouts.web')
@section('content')
<span name="{{ $company = \App\Company::get()->first() }}"></span>
<section class="main-header" style="background-image:url({{ asset($company->img2) }}">
    <header>
        <div class="container text-center">
            <h2 class="h2 title">Vende tus Artículos de Segunda Mano</h2>
            <p>Si quieres que vendamos tus muebles rellena el formulario</p>
            <ol class="breadcrumb breadcrumb-inverted">
                <li><a href="/"><span class="icon icon-home"></span></a></li>
                <li><a class="active" href="">Formulario de venta</a></li>
            </ol>
        </div>
    </header>
</section>

<section class="our-team">
    <div class="container">
        <div class="row">
            <div class="col-md-12">@include('client.messages')</div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <h2>Datos del Art&iacute;culo</h2>
            </div>
        </div>

        <div class="row">
            <div class="col-md-10">
                {!! Form::open(['route' => 'articulo.store', 'files' =>true , 'id' => 'formVender' ]) !!}

                <div class="row">
                    {{ Form::hidden('user_id', auth()->user()->id) }}
                    {{-- {{ Form::hidden('code', rand()) }} --}}
                    {{ Form::hidden('status', 'PENDIENTE') }}
                    {{ Form::hidden('contrato', 'basico') }}
                    {{ Form::hidden('quantity', 1) }}

                    <div class="form-group col-md-8">
                        {{ Form::label('name', 'Nombre del Articulo') }}
                        {{ Form::text('name', null, [ 'class' => 'form-control', 'id' => 'name', 'maxlength' => 26 ]) }}
                    </div>
                    <div class="form-group col-md-4">
                        {{ Form::label('category_id', 'Estancia') }}
                        {{ Form::select('category_id',['' => 'Selecciona tu estancia'] + $categories->all(), null, [ 'class' => 'form-control', 'id' => 'category_id', 'required' => 'true' ]) }}
                    </div>

                    <div class="form-group col-md-6">
                        {{ Form::label('normal_price', 'Precio') }}
                        {{ Form::text('normal_price', null, [ 'class' => 'form-control', 'id' => 'normal_price','min'=>'10' ]) }}
                    </div>
                    <div class="form-group col-md-6">
                        <label>Im&aacute;genes</label>
                        <input type="file" name="images[]" id="images" accept="image/*" class="form-control" multiple="multiple" />
                    </div>
                    <hr>
                    <div class="col-md-12">
                        <div class="gallery" id="galeria"></div>
                    </div>
                    <div class="form-group col-md-12">
                        {{ Form::label('description', 'Descripción') }}
                        {{ Form::textarea('description', null, [ 'class' => 'form-control', 'id' => 'description' ]) }}
                    </div>

                    <div class="form-group col-md-3">
                        {{ Form::label('weight', 'Peso Aproximado (kg)') }}
                        {{ Form::text('weight', null, [ 'class' => 'form-control', 'id' => 'weight' ]) }}
                    </div>
                    <div class="form-group col-md-3">
                        {{ Form::label('long', 'Largo (cm)') }}
                        {{ Form::text('long', null, [ 'class' => 'form-control', 'id' => 'long' ]) }}
                    </div>
                    <div class="form-group col-md-3">
                        {{ Form::label('width', 'Ancho (cm)') }}
                        {{ Form::text('width', null, [ 'class' => 'form-control', 'id' => 'width' ]) }}
                    </div>
                    <div class="form-group col-md-3">
                        {{ Form::label('height', 'Alto (cm)') }}
                        {{ Form::text('height', null, [ 'class' => 'form-control', 'id' => 'height' ]) }}
                    </div>

                    {{ Form::hidden('type', 'NORMAL') }}

                    <div class="col-md-12 text-center">
                        <span class="checkbox checkbox-inline">
                            <input type="checkbox" class="checkbox-inline" required id="checkbox">
                            <label for="checkbox">Contrato Basico </label>
                            <div class="show-more">
                                <span class="btn btn-sm btn-clean-dark " data-toggle="modal" data-target="#exampleModalCenter"> Leer Contrato
                                    Basico</span>
                            </div>
                        </span>
                    </div>
                    <div class="col-md-12 text-center">
                        <input type="hidden" name="new_article" id="new_article" value="false">
                        <input type="submit" class="btn btn-primary" value="Enviar Solo Este Artículo" />
                        <a href="javascript: enviarForm()" class="btn btn-primary"> Enviar y añadir más Artículos </a>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
            <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
                aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document" style="margin-top: 100px;width: 90%;">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <h5 class="modal-title" id="exampleModalLongTitle">Aceptar Contrato Básico</h5>
                        </div>
                        <div class="modal-body">
                            De una parte {{ $user->name }} con DNI {{ $user->profile->dni }} y domicilio en
                            {{ $user->profile->address }} y reconociendo los artículos de
                            su propiedad.
                            <br>
                            <p> De otra parte vaciatucasa & hometohome y con domicilio comercial en Leganés
                                Calle Asturias, 9 28913
                            </p>
                            <h6>
                                ACUERDAN:
                            </h6>
                            <p>Las gestiones que HOMETOHOME realizará serán las siguientes:
                            </p> Verificación de los artículos que el cliente quiere vender.
                            <p>Realizar las gestiones oportunas para la venta de dichos artículos</p>
                            <ul>
                                <li> Diseño de los anuncios de cada artículo </li>
                                <li> Publicación de dichos anuncios en nuestra Web </li>
                                <li> Búsqueda de otros medios para publicar y encontrar potenciales compradores.
                                </li>
                                <li> Realizar una política de descuento para favorecer la venta: </li>
                            </ul>

                            <p> 1. A las 6 semanas se aplicará el 15%.</p>
                            <p> 2. A las 8 semanas se aplicará el 30%.</p>
                            <p> 3. A las 12 semanas se aplicará el 50%.</p>
                            <p> 4. A las 16 semanas, los artículos que no se hayan vendidos se darán de baja de la Web.</p>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary btn-sm" data-dismiss="modal">Aceptar</button>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>
<style scoped>
    .gallery img {
        height: 150px;
        margin-top: 0.5em;
        margin-left: 0.5em;
        margin-bottom: 0.5em;
        margin-right: 0.5em;
    }
</style>
<script src="{{ asset('vendor/ckeditor/ckeditor.js') }}"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script>
    CKEDITOR.config.height = 200;
    CKEDITOR.config.width = 'auto';
    CKEDITOR.config.language = 'es';
    CKEDITOR.replace('description');

    function enviarForm(){
        document.getElementById('new_article').value = true;
        let value = document.getElementById('new_article').value;
        document.forms["formVender"].submit();
    }

    $(function() {
    // Multiple images preview in browser
    var imagesPreview = function(input, placeToInsertImagePreview) {

        if (input.files) {
            $('div#galeria > img').remove();
            if (input.files.length>= 4 && input.files.length <=10 ) {
                var filesAmount = input.files.length;
                for (i = 0; i < filesAmount; i++) {
                    var reader = new FileReader();
                    reader.onload = function(event) {
                        $($.parseHTML('<img>')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
                    }
                    reader.readAsDataURL(input.files[i]);
                }
            }else{
                alert('Fotografias minimas 4, y máxima 10..!!')
            }
        }

    };

    $('#images').on('change', function() {
        imagesPreview(this, 'div.gallery');
    });
});
</script>
@endsection
