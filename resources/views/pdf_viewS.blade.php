@extends('layouts.pdf')
@section('content')
<div class="container">

  {{-- <div class="pt-5 text-right">En Madrid {{ date('d-m-Y', strtotime($agreement->created_at)) }}</div>--}}
  <div class="pt-3 text-justify">
    DE UNA PARTE <span class="text-capitalize font-weight-bold"> {{ $agreement->client->user->name }}
      {{ $agreement->client->user->profile->last_name }}</span>, con DNI
    <span class=" font-weight-bold">{{ $agreement->client->user->profile->dni }}</span> y domicilio en <span class="font-weight-bold">{{ $agreement->client->user->profile->address }}</span>
    y reconociendo la siguiente relación de objetos, mencionada en dicho contrato y recogida en el ANEXO 1 de su propiedad, ubicados en su domicilio
    </div>
  <div class="pt-3 text-justify">
    DE OTRA PARTE vaciatucasa & hometohome  con domicilio comercial en Leganés Calle Asturias, 9 28913
  </div>

  <div class="text-justify my-5">
    <h5 class="pt-2"> ACUERDAN </h5>

    {{-- <h5 class="pt-2"> PRIMERA </h5> --}}
    <p>
      Vaciatucasa es una empresa dedicada a la gestión de la venta de los artículos que el cliente quiere vender.  En ningún momento  vaciatucasa es propietaria de ninguno de los objetos recogidos en el ANEXO 1.
    </p>

    {{-- <h5 class="pt-2"> SEGUNDA </h5> --}}

    <p class="my-2">
      Las gestiones que hometohome realizará serán las siguientes:
    </p>
    <p class="my-2">
      1) Inventariado, fotografiado y/o grabación de todos los artículos que el cliente desea vender.</p>
    <p class="my-2">
      2) Valoración de todos los artículos. Dicha valoración figura en el (ANEXO 1)</p>

    <p class="my-2">
      3) Realizar las gestiones oportunas para la venta de los artículos del cliente,tales como:
    </p>
    <ul class="ml-5">

      <li>Realización de los anuncios de los artículos en nuestra Web
      </li>
      <li>
        Publicación de dichos anuncios en webs especializadas
      </li>
      <li>Publicidad en redes sociales.      </li>
      <li> Realizar una política de descuento para favorecer la venta:
      </li>
      <ul>
        <dl> 1. A las 6 semanas se aplicará el 15%.</dl>
        <dl> 2. A las 8 semanas se aplicará el 30%.</dl>
        <dl> 3. A las 12 semanas se aplicará el 50%.</dl>
        <dl> 4. A las 16 semanas, los artículos que no se hayan vendidos se darán de baja de la Web, adémas de concluirse su contrato.
        </dl>

        <dl>-Al término del contrato (12 semanas), excepcionalmente se podrá ampliar el contrato hasta un máximo de 16 semanas y se aplicará un descuento del 50% del precio original.</dl>

      </ul>
      <li>	Intermediar con todos los posibles compradores, encargándose de la coordinación de la visita.
      </li>
      <li>	Gestionar y cerrar el precio de cada artículo, para lo cual el cliente le confiere expresa representación por medio de este documento.
      </li>

    </ul>
    <p class="my-2">
      4)	Vaciatucasa dispone de una capacidad de negociación sobre el precio fijado con el cliente de un máximo del 15% de descuento sin previa consulta.
    </p>

    {{-- <h5 class="pt-2"> TERCERA </h5> --}}

    La duración del presente contrato es de 16 semanas, <span class="font-weight-bold font-italic"> iniciándose el día de publicación de los artículos en la Web.</span>

<p>    La comisión para HOMETOHOME aplicada a dicha gestión será del 25 % del precio final de todos los artículos vendidos.
</p>

<p>    Dado que el cobro de la venta de los bienes será realizado por el cliente o/y vaciatucasa, las partes acuerdan reunirse de forma presencial o telemáticamente en el día de finalización del contrato, con el objeto de liquidar por completo los importes cobrados fruto de la venta de los enseres en el porcentaje arriba descrito.</p>
        {{-- y finalizando el <span
      class="font-weight-bold font-italic">{{ date('d-m-Y', strtotime($agreement->created_at)) }}</span> , dichos días inclusive, fecha esta
    última en la que expira el mandato conferido a Vaciatucasa salvo que ambas partes, de forma
    expresa, hayan acordado la prórroga del contrato.

    <h5 class="pt-2"> CUARTA </h5>

    La comisión para HOMETOHOME aplicada a dicha gestión será del **25 % (+ IVA)** del precio final de venta de todos los
    artículos vendidos.

    Dado que el cobro de la venta de los bienes será realizado por el cliente o/y HOMETOHOME, las partes acuerdan reunirse de
    forma presencial o/y telemáticamente en el día de finalización del contrato, o en los tres días hábiles posteriores,
    o cuando las partes pacten de mutuo acuerdo, con el objeto de liquidar por completo en esa reunión los importes
    cobrados fruto de la venta de los enseres en el porcentaje arriba descrito.

    <h5 class="pt-2"> QUINTA </h5> --}}

    El cliente otorga exclusividad a vaciatucasa para gestionar la venta de todos los artículos desde el momento de la firma del contrato hasta la finalización del mismo.

    Durante la duración del contrato, en caso de que el cliente quiera retirar de la venta alguno o algunos de los artículos que figuran en el ANEXO 1 pagará una penalización a HOMETOHOME, del 15%  del valor de tasación inicial de cada artículo o artículos anulados.

    Por el presente documento se consiente de forma expresa que los datos de carácter personal facilitados sean transmitidos a terceros con la exclusiva finalidad del cumplimiento del presente contrato.

    {{-- <h5 class="pt-2"> SEXTA </h5>

    Durante la duración del contrato, en caso de que el cliente quiera retirar de la venta alguno o algunos de los
    artículos que figuran en el ANEXO 1 pagará una penalización a HOMETOHOME, del **15%** del valor de tasación inicial de cada
    artículo o artículos anulados.

    Y en prueba de conformidad con lo expuesto y acordado, firman el presente acuerdo por duplicado en todas sus
    páginas, en el lugar y fechas señalados en el encabezamiento.

    Por el presente documento el comitente consiente de forma expresa en que los datos de carácter personal facilitados
    a HOMETOHOME sean transmitidos a terceros con la exclusiva finalidad del cumplimiento del presente contrato.
  </div>

  <p class="text-right pt-3">
    {{ $agreement->handler->user->name }} {{ $agreement->handler->user->profile->last_name }}
  </p> --}}

  <div class="page-break">
    <h5 class="pt-2">
      A N E X O 1
    </h5>
    <p class="text-justify">
      Relación de artículos que el cliente: <span
        class="font-weight-bold font-italic">{{ $agreement->client->user->name }}
        {{ $agreement->client->user->profile->last_name }}</span> desea vender:
    </p>
    <div class="pt-5">
      <table class="table table-striped" width="100%" style="width:100%" border="0">
        <tr>
          <th>#</th>
          <th>Imagen</th>
          <th>Nombre</th>
          <th>Categoria</th>
          <th>Estado</th>
          <th>Precio</th>
        </tr>
        {{-- <tbody> --}}
        @foreach($agreement->products as $product)
        <tr>
          <td>{{$loop->iteration}}</td>
          <td class="text-center">
            @if ($product->images->first())
            <img src="{{ $product->images->first()->path }}" alt="" height="50">
            @endif
          </td>
          <td> <a href="{{ route ('products.show', $product) }}"> {{ $product->name }} </a> </td>
          <td> {{ $product->category->name }} </td>
          <td class="text-warning"> {{ $product->status }} </td>
          <td>  {{ $product->normal_price }} <i class="fa fa-euro"></i></td>
        </tr>
        @endforeach
        <tr>
          <td colspan="5" class="text-right">TOTAL EUROS</td>
          <td> {{ $total}} </td>
        </tr>
        </tbody>
      </table>
    </div>
  </div>
</div>
@endsection
