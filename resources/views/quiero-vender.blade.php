@extends('layouts.web')
@section('content')
<span name="{{ $company = \App\Company::get()->first() }}"></span>
    <section class="main-header" style="background-image:url({{ asset($company->img3) }}">
        <header>
            <div class="container text-center">
                <h2 class="h2 title">Vende tus Artículos</h2>
                <p>Si quieres que vendamos tus muebles seleccione el tipo de contrato</p>
                <ol class="breadcrumb breadcrumb-inverted">
                    <li><a href="/"><span class="icon icon-home"></span></a></li>
                    <li><a class="active" href="">Quiero Vender</a></li>
                </ol>
            </div>
        </header>
    </section>


    <section class="our-team">
        <div class="container">
            <div class="row"><div class="col-md-12">@include('client.messages')</div></div>
            <div class="row text-center">
                <div class="col-md-12">
                    <h2>Seleccionar tipo de contrato</h2>
                </div>
            </div>

            <section class="info-icons" style="background: transparent !important;">
                <div class="row">
                    <div class="col-xs-12 col-md-3 col-md-offset-2">
                        <figure>
                            <figcaption>
                                <span><i class="icon icon-gift"></i></span>
                                <span>
                                    <strong>Contrato Básico</strong>
                                    <small><a href="{{ route('terminos-condiciones')}}" class="btn btn-link" target="_blank">Términos y condiciones</a></small>
                                </span>
                            </figcaption>
                            <a href="{{ route('form-vender') }}" class="btn btn-lg btn-block btn-primary">Seleccionar</a>
                        </figure>
                    </div>
                    <div class="col-xs-12 col-md-3">
                        <figure>
                            <figcaption>
                                <span><i class="icon icon-flag"></i></span>
                                <span>
                                    <strong>Contrato Estándar</strong>
                                    <small><a href="{{ route('terminos-condiciones')}}" class="btn btn-link" target="_blank">Términos y condiciones</a></small>
                                </span>
                            </figcaption>
                            <a href="{{ route('form-visit', ['plan' => 'estandar']) }}" class="btn btn-lg btn-block btn-primary">Seleccionar</a>
                        </figure>
                    </div>
                    <div class="col-xs-12 col-md-3 ">
                        <figure>
                            <figcaption>
                                <span><i class="icon icon-star"></i></span>
                                <span>
                                    <strong>Contrato Premium</strong>
                                    <small><a href="{{ route('terminos-condiciones')}}" class="btn btn-link" target="_blank">Términos y condiciones</a></small>
                                </span>
                            </figcaption>
                            <a href="{{ route('form-visit', ['plan' => 'premium']) }}" class="btn btn-lg btn-block btn-primary">Seleccionar</a>
                        </figure>
                    </div>
                </div>
            </section>

            @if ($client_agreements->count())
            <div class="row text-center">
                <div class="col-md-12">
                    <h2>Contratos Vigentes</h2>
                </div>
            </div>
            <section class="info-icons" style="background: transparent !important;">
                <div class="row text-center">
                    @forelse($client_agreements as $agreement)
                        <div class="well well-sm col-md-3 px-3 mx-4" style="margin-right: 2em;">
                            <h6 style="margin-bottom: 0px;">{{ $agreement->name }} : {{ $agreement->status }}<br> <small>Registrado en ({{ date('d-m-Y', strtotime(date('d-m-Y', strtotime($agreement->created_at)))) }})</small></h6>
                            @if ($agreement->status == 'enviado')
                                <small class="text-primary">Se debe Aprobar la Valoracion</small>
                                <a href="{{ route('contrato.detalle', $agreement)}}" class="btn btn-xs btn-warning">{{ __('Detalle de artículos en contrato') }}</a>
                            @else
                                <a href="{{ route('contrato.detalle', $agreement)}}" class="btn btn-xs btn-primary">{{ __('Detalle de artículos en contrato') }}</a>
                            @endif
                        </div>
                    @empty
                        <div class="alert alert-info" role="alert">
                            {{ __("No existen contratos vigentes") }}
                        </div>
                    @endforelse
                </div>
            </section>
            @endif
            @if ($concluidos->count())
            <div class="row text-center">
                <div class="col-md-12">
                    <h2>Contratos Concluidos</h2>
                </div>
            </div>
            <section class="info-icons" style="background: transparent !important;">
                <div class="row text-center">
                    @forelse($concluidos as $agreement)
                        <div class="well well-sm col-md-3 px-3 mx-4" style="margin-right: 2em;">
                            <h6 style="margin-bottom: 0px;" >{{ $agreement->name }} : {{ $agreement->status }}<br> <small>Registrado en ({{ date('d-m-Y', strtotime(date('d-m-Y', strtotime($agreement->created_at)))) }})</small></h6>
                            <p></p>
                            <a href="{{ route('contrato.detalle', $agreement)}}" class="btn btn-xs btn-primary">{{ __('Detalle de artículos en contrato') }}</a>
                        </div>
                    @empty
                        <div class="alert alert-info" role="alert">
                            {{ __("No existen contratos concluidos") }}
                        </div>
                    @endforelse
                </div>
            </section>
            @endif
        </div>
    </section>
@endsection
