@extends('layouts.web')
@section('content')
<style scoped>
    .products article .label {
        position: inherit !important;
    }

    .owl-carousel .owl-item img {
        /* max-width: 100% !important; */
        max-height: 500px;
    }

    .img-responsive {
        /* max-height: 275px */
        margin-left: auto;
        margin-right: auto;
    }


    .breadcrumb.breadcrumb-inverted>li>a {
        color: #039192;
        text-shadow: 0 3px 20px rgba(0, 0, 0, 1);
        font-weight: bold;
    }

    /* ------------------ */
    /* pretty radio */
    label>input[type="radio"] {
        display: none;
    }

    label>input[type="radio"]+*::before {
        content: "";
        display: inline-block;
        vertical-align: bottom;
        width: 1rem;
        height: 1rem;
        margin-right: 0.3rem;
        border-radius: 50%;
        border-style: solid;
        border-width: 0.1rem;
        border-color: gray;
    }

    label>input[type="radio"]:checked+* {
        color: teal;
    }

    label>input[type="radio"]:checked+*::before {
        background: radial-gradient(teal 0%, teal 40%, transparent 50%, transparent);
        border-color: teal;
    }

    /* basic layout */
    fieldset {
        margin: 20px;
        max-width: 400px;
    }

    label>input[type="radio"]+* {
        display: inline-block;
        padding: 0.5rem 1rem;
    }

    .lista {
        display: flex;
        flex-wrap: wrap;
    }

    .lista-item {
        height: 38vh;
        flex-grow: 1;
        padding: 0.3rem;
        margin: 0.3rem;
        border: 2px solid #039192;
        border-radius: 0.3em;
    }

    .img-edit {
        max-height: 70%;
        min-width: 0;
        object-fit: cover;
        vertical-align: bottom;
    }

    .my-0-3 {
        margin-top: 0.3em;
        margin-bottom: 0.3em;
    }

    :root {
        /* Not my favorite that line-height has to be united, but needed */
        --lh: 1.4rem;
    }

    html {
        line-height: var(--lh);
    }
</style>
<span name="{{ $company = \App\Company::get()->first() }}"></span>
<section class="main-header" style="background-image:url({{ asset($company->img4) }}">
    <header>
        <div class="container text-center">
            <h2 class="h2 title" style="text-transform: capitalize;">
                Artículo
                @if ($product->client_agreement)
                {{$product->client_agreement->name}}
                @endif
                {{$product->code}}
            </h2>
            <p> </p>
        </div>
    </header>
</section>
<section class="product">
    <div class="main">
        <div class="container">
            <div class="row product-flex">
                <div class="col-md-4 col-sm-12 product-flex-info">
                    <div class="clearfix">
                        <!-- === product-title === -->

                        <div class="clearfix">
                            <div class="price">
                                @if ($product->reduced_price)
                                <span class="h3"> {{$product->reduced_price}} <i class="fa fa-euro"></i> <small> <i class="fa fa-euro"></i>
                                        {{$product->normal_price}}</small></span>
                                @else
                                <span class="h3"> {{$product->normal_price}} <i class="fa fa-euro"></i>
                                    <small></small></span>
                                @endif
                            </div>
                            <hr />

                            <!-- === info-box === -->

                            <div class="info-box">
                                <span><strong> Estancia</strong></span>
                                <span class="text-primary"> {{$product->category->name}} </span>
                            </div>

                            <!-- === info-box === -->
                            <div class="info-box info-box-addto added">
                                <span><strong>Peso</strong></span>
                                <span> {{ $product->weight }} </span>
                            </div>

                            <div class="info-box info-box-addto">
                                <span><strong>Largo</strong></span>
                                <span> {{ $product->long }} </span>
                            </div>

                            <div class="info-box info-box-addto">
                                <span><strong>Ancho</strong></span>
                                <span> {{ $product->width }} </span>
                            </div>
                            <div class="info-box info-box-addto added">
                                <span><strong>Alto</strong></span>
                                <span> {{ $product->height }} </span>
                            </div>
                            @if ($product->status == 'RESERVADO')
                            <a class="btn btn-warning form-control disabled" style="margin-top:1em">Reservado con <i class="fa fa-euro"></i>
                                {{ $product->payment }}, <span class="text-primary">saldo <i class="fa fa-euro"></i>
                                    @if ($product->reduced_price)
                                    {{$product->reduced_price - $product->payment }}
                                    @else
                                    {{$product->normal_price - $product->payment}}
                                    @endif
                                </span>
                            </a>
                            <a class="btn btn-primary form-control" style="margin-top:1em"
                                href="{{ route('articulo.PublishedProduct', $product)}}">Volver a publicar</a>
                            @endif
                            @if ($product->user->id != auth()->user()->id)
                            <a class="btn btn-primary form-control" href="{{ route('page.comprar', $product)}}">Solicitar Compra</a>
                            @endif
                            <div class="info-box">
                                <span>
                                    {!!QrCode::size(100)->generate($product->code) !!}
                                </span>
                            </div>
                        </div>
                        <h1 class="title" data-title="{{$product->name}}"> {{ $product->name }}</h1>
                        <label class="text-details"> Descripcion: </label>
                        <div class="descripcion-details"> {!!$product->description!!}</div>
                    </div>
                </div>

                <!-- === product item gallery === -->
                <div class="col-md-7 col-sm-12 product-flex-gallery">
                    <div class="owl-product-gallery open-popup-gallery">
                        @foreach ($product->images as $img)
                        <a href="{{ asset($img->path) }}">
                            <img src="{{ asset($img->path) }}" alt="dfs" id="imagenslider" class="img-responsive center-block" />
                        </a>
                        @endforeach
                    </div>
                </div>

            </div>
        </div>
    </div>
    @include('client.messages')
    @if (!$product->client_agreement)
    <div class="info">
        <div class="container text-center">
            <h3>Formulario de Edicion</h3>

            <div class="products">
                <div class="row">
                    <div class="col-md-12">
                        {{-- <form id="form_article" name="form_article" action="#" method="post"> --}}
                        {!! Form::model($product,['route' => ['articulo.update', $product], 'method'=> 'PUT', 'files' => true]) !!}
                        {{-- <form action=""> --}}
                        <div class="row">
                            {{ Form::hidden('user_id', auth()->user()->id) }}
                            {{-- {{ Form::hidden('code', rand()) }} --}}
                            {{ Form::hidden('status') }}

                            <div class="form-group col-md-8">
                                {{ Form::label('name', 'Nombre del Articulo') }}
                                {{ Form::text('name', null, [ 'class' => 'form-control', 'id' => 'name','maxlength' => 26 ]) }}
                            </div>
                            <div class="form-group col-md-4">
                                {{ Form::label('category_id', 'Estancia') }}
                                {{ Form::select('category_id',$categories, null, [ 'class' => 'form-control', 'id' => 'category_id' ]) }}
                            </div>
                            <div class="form-group col-md-12">
                                {{ Form::label('description', 'Descripción') }}
                                {{ Form::textarea('description', null, [ 'class' => 'form-control', 'id' => 'description' ]) }}
                            </div>

                            <div class="form-group col-md-3">
                                {{ Form::label('weight', 'Peso (kg)') }}
                                {{ Form::text('weight', null, [ 'class' => 'form-control', 'id' => 'weight' ]) }}
                            </div>
                            <div class="form-group col-md-3">
                                {{ Form::label('long', 'Largo (cm)') }}
                                {{ Form::text('long', null, [ 'class' => 'form-control', 'id' => 'long' ]) }}
                            </div>
                            <div class="form-group col-md-3">
                                {{ Form::label('width', 'Ancho (cm)') }}
                                {{ Form::text('width', null, [ 'class' => 'form-control', 'id' => 'width' ]) }}
                            </div>
                            <div class="form-group col-md-3">
                                {{ Form::label('height', 'Alto (cm)') }}
                                {{ Form::text('height', null, [ 'class' => 'form-control', 'id' => 'height' ]) }}
                            </div>
                        </div>

                        {{ Form::hidden('type', 'NORMAL') }}

                        <div class="col-md-12 text-center">
                            <input type="submit" class="btn btn-primary" value="Guardar Datos de Artículo" />
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
            <div class="container text-center">
                <h3>Opciones de Imagen</h3>
                <div class="lista">
                    @foreach ($product->images as $img)
                    <div class="lista-item text-center">
                        @if ($product->img == $img->path)
                        {!! Form::open(['route' =>['removeImage', $img], 'method' =>'delete', 'class'=>['inline', 'f-right my-0-3'] ]) !!}
                        <button type="submit" title="Eliminar Imagen" disabled class="btn btn-sm pull-up btn-danger"> <i class="fa fa-close"></i>
                            Eliminar Imagen </button>
                        {!! Form::close() !!}

                        <p class="my-0-3">Imagen de Portada</p>
                        @else

                        {!! Form::open(['route' =>['removeImage', $img], 'method' =>'delete', 'class'=>['inline', 'f-right my-0-3'] ]) !!}
                        <button type="submit" class="btn btn-sm btn-danger"> <i class="fa fa-close">Eliminar Imagen</i></button>
                        {!! Form::close() !!}

                        {!! Form::open(['route' =>['products.assign_portada', $product], 'method' =>'post', 'class'=>['inline', 'f-right my-0-3']
                        ])
                        !!}
                        <input type="hidden" value="{{ $img->path }}" name="img" />
                        <button type="submit" class="btn btn-sm btn-success"> <i class="fa fa-check"></i> Asignar Portada</button>
                        {!! Form::close() !!}
                        @endif
                        <img src="{{ asset($img->path) }}" alt="image" class="img-edit" />
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    @endif
</section>

<script src="{{ asset('vendor/ckeditor/ckeditor.js') }}"></script>
<script>
    CKEDITOR.config.height = 200;
    CKEDITOR.config.width = 'auto';
    CKEDITOR.config.language = 'es';
    CKEDITOR.replace('description');
</script>
@endsection
