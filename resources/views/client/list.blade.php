@extends('layouts.web')
@section('content')
<span name="{{ $company = \App\Company::get()->first() }}"></span>
<section class="main-header" style="background-image:url({{ asset($company->img3) }}">
    <header>
        <div class="container text-center">
            <h2 class="h2 title">Mi lista de Artículos</h2>
            {{-- <p>Si quieres que vendamos tus muebles seleccione el tipo de contrato</p> --}}
            <ol class="breadcrumb breadcrumb-inverted">
                <li><a href="/"><span class="icon icon-home"></span></a></li>
                <li><a class="active" href="">Mi lista de articulos</a></li>
            </ol>
        </div>
    </header>
</section>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            @include('client.messages')
        </div>
    </div>
</div>

<section class="product">
    <div class="main">
        <div class="container">
            <div class="row product-flex">
            </div>
        </div>
    </div>

    <div class="info">
        <div class="container">
            <div class="row">

                <!-- === product-designer === -->

                <div class="col-md-3">
                    <div class="designer">
                        <div class="box">
                            <div class="image pa-2">
                                <img src="{{ asset($user->profile->avatar) }}" alt="Alternate Text" class="img-responsive rounded-img " />
                            </div>
                            <div class="name">
                                <div class="h3 title"> {{ $user->name }} <small> {{-- Arhitect --}}</small></div>
                                <hr />
                                <p><a class="btn btn-primary form-control" href="{{ route('profile.edit')}}">Mi Perfil</a></p>
                                {{-- <p><a href="{{ $user->email }}"><i class="icon icon-envelope"></i>{{ $user->email }}</a> --}}
                                </p>
                                <p>
                                    <a href="javascript: void(0);"><i class="fa fa-whatsapp"></i>
                                        {{ $user->profile->phone_number }}</a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- === nav-tabs === -->

                <div class="col-md-9 col-sm-12">
                    <ul class="nav nav-tabs pa-1" role="tablist">
                        <li class="active"><a href="#agreements" aria-controls="Articles" role="tab" data-toggle="tab">
                                <span>Contratos</span></a></li>
                        <li><a href="#published" aria-controls="Articles" role="tab" data-toggle="tab">
                                <span>Publicados</span></a></li>
                        <li><a href="#basicos" aria-controls="Articles" role="tab" data-toggle="tab">
                                <span>Básicos</span></a></li>
                        <li><a href="#pending" aria-controls="design" role="tab" data-toggle="tab">
                                <span>Pendientes</span></a></li>
                        <li><a href="#sold" aria-controls="design" role="tab" data-toggle="tab">
                                <span>Vendidos</span></a></li>
                        <li><a href="#reservas" aria-controls="design" role="tab" data-toggle="tab">
                                <span>Reservados</span></a></li>
                        <!-- <li><a href="#solicitados" aria-controls="design" role="tab" data-toggle="tab">
                                <span>Solicitados</span></a></li> -->
                    </ul>

                    <!-- === tab-panes === -->
                    <div class="tab-content pa-1">
                        <div role="tabpanel" class="tab-pane active" id="agreements">
                            <div class="content">
                                @if ($agreements)
                                @include('client.mis_productos.contratos')
                                @else
                                <h3>No hay Artículos Publicados de Contratos</h3>
                                @endif
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="published">
                            <div class="content">
                                <!-- === Articles collection title === -->
                                @if (count($published)>0)
                                @include('client.mis_productos.publicados')
                                @else
                                <h3>No hay Artículos Publicados por el momento</h3>
                                @endif
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="basicos">
                            <div class="content">
                                <!-- === Articles collection title === -->
                                @if (count($basicos)>0)
                                @include('client.mis_productos.basicos')
                                @else
                                <h3>No hay Artículos Básicos en el sitio </h3>
                                @endif
                            </div>
                        </div>

                        <div role="tabpanel" class="tab-pane" id="pending">
                            <div class="content">
                                @if (count($pending)>0)
                                @include('client.mis_productos.pendientes')
                                @else
                                <h3>No tiene Articulos Pendientes por el momento</h3>
                                @endif
                            </div>
                        </div>

                        <div role="tabpanel" class="tab-pane" id="sold">
                            <div class="content">
                                @if (count($sold)>0)
                                @include('client.mis_productos.vendidos')
                                @else
                                <h3>No tiene Articulos vendidos por el momento</h3>
                                @endif
                            </div>
                        </div>

                        <div role="tabpanel" class="tab-pane" id="reservas">
                            <div class="content">
                                @if (count($reservados)>0)
                                @include('client.mis_productos.reservados')
                                @else
                                <h3>No hay Articulos Reservados por el momento</h3>
                                @endif
                            </div>
                        </div>

                        <div role="tabpanel" class="tab-pane" id="solicitados">
                            <div class="content">
                                @if ($solicitados)
                                @include('client.mis_productos.solicitados')
                                @else
                                <h3>No hay Articulos solicitados por el momento</h3>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
