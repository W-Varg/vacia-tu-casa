@if (session('info') || count($errors) || session('error') )
    {{--para mostrar los sms --}}
    @if (session('info'))
    <div class="row">
        <div class="col-12">
            <div class="alert alert-info" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <i class="icon icon-checkmark-circle"></i> {{ session('info') }}
            </div>
        </div>
    </div>
    @endif
    @if (session('error'))
    <div class="mt-3 row">
        <div class="col-12">
            <div class="alert alert-danger">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <i class="icon icon-checkmark-circle"></i> {{ session('error') }}
            </div>
        </div>
    </div>
    @endif
    {{--para mostrar los sms --}}
    @if (count($errors))
    <div class="row">
        <div class="col-12">
            <div class="alert alert-warning" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <ul>
                    @foreach ($errors->all() as $error)
                    <li> {{ $error }} </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
    @endif
<style>
    .step-wrapper {
        padding: 60px 0 0 0;
    }

    section {
        padding-top: 20px;
        padding-bottom: 50px;
    }
</style>
@endif