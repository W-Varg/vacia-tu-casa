<h3>Articulos Reservados</h3>
<div class="products">
    <div class="row">
        @foreach ($reservados as $item)
        <div class="col-md-12">
            <article>
                <div class="fondo-blanco">
                    <div class="info" style="background-color: initial;">
                        <span class="add-favorite">
                            <a href="{{ route ('products.qr_products', $item) }}" data-title="Codigo QR">
                                <i class="icon icon-qrcode"></i></a>
                        </span>
                    </div>
                    <div class="figure-list">
                        <div class="image">
                            <a href="{{ route('page.detail_edit_product', $item->id) }}">
                                @if ($item->img)
                                    <img src="{{ $item->img }}" alt="" height="130" />
                                @else
                                    <img src="{{ asset('assets/images/product-1.png') }}" alt="" height="130" />
                                @endif
                            </a>
                        </div>
                        <div class="text">
                            <h2 class="title h4 pt-1 mb-0 text-center"><a href="{{ route('page.detail_edit_product', $item->id) }}">{{ $item->name }}</a></h2>
                            <div class="text-center bg-white">
                                @if ($item->reduced_price)
                                <sub>  {{ $item->reduced_price}} <i class="fa fa-euro"></i> </sub>
                                <sub class="tachado">  {{ $item->normal_price}} <i class="fa fa-euro"></i> </sub>
                                @else
                                <sub>  {{ $item->normal_price}} <i class="fa fa-euro"></i> </sub>
                                @endif
                                <br>
                                <p>
                                    <a class="btn btn-primary btn-sm"
                                    href="{{ route('articulo.PublishedProduct', $item)}}">Volver a publicar</a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </article>
        </div>
        @endforeach
    </div>
    <div class="pagination-wrapper">
        {{-- {{ $sold->links() }} --}}
    </div>
</div>
