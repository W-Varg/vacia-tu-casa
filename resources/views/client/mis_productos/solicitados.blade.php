<h3>Articulos solicitados</h3>
<div class="products">
    <div class="row">
        @foreach ($solicitados as $compra)
        <div class="col-md-12">
            <article>
                <div class="fondo-blanco">

                    <div class="info" style="background-color: initial;">
                        <span class="add-favorite">
                            <a href="{{ route ('products.qr_products', $compra->product) }}" data-title="Codigo QR">
                                <i class="icon icon-heart"></i></a>
                        </span>
                    </div>
                    <div class="figure-list">
                        <div class="image">
                            <a href="{{ route('page.detail_edit_product', $compra->product) }}">
                                <img src="{{ $compra->product->images->first()->path }}" alt="" height="130" />
                            </a>
                        </div>
                        <div class="text row">
                            <div class="my-10 col-md-6 col-sm-12">
                                <h6 class="title h4 pt-1 mb-0 text-center"><a
                                        href="{{ route('page.detail_edit_product', $compra->product) }}">{{ $compra->product->name }}</a>
                                </h6>
                                <div class="text-center bg-white">
                                    @if ($compra->product->reduced_price)
                                    <sub> {{ $compra->product->reduced_price}} <i class="fa fa-euro"></i> </sub>
                                    <sub class="tachado"> {{ $compra->product->normal_price}} <i class="fa fa-euro"></i> </sub>
                                    @else
                                    <sub> {{ $compra->product->normal_price}} <i class="fa fa-euro"></i> </sub>
                                    @endif
                                    @if ($compra->product->client_agreement)
                                    <p><span class="text-info"> Articulo Administrado por Vacía Tu Casa</span></p>
                                    @else
                                    <p class="">
                                        {!! Form::open(['route' =>['articulo.soldproduct', $compra->product], 'class' =>
                                        [' vendido']]) !!}
                                        <div class="">
                                            <input class="form-control form-control-sm" type="number" name="pago" placeholder="Importe en Euros"
                                                required="true" />
                                            <input type="submit" class="btn btn-info btn-xs m-5y" value="Vendido" />
                                        </div>
                                        {!! Form::close() !!}
                                    </p>
                                    <p>
                                        {!! Form::open(['route' =>['article.destroy', $compra->product],
                                        'method' =>'delete', 'class'=>['inline'],'onclick' => 'return confirm("Estas seguro de dar de baja el articulo?")' ]) !!}
                                        <button type="submit" class="btn btn-sm btn-warning"> <i class="fa fa-close"></i> Dar de Baja</button>
                                        {!! Form::close() !!}
                                    </p>
                                    @endif
                                </div>
                            </div>
                            @if (!$compra->product->client_agreement)
                            <div class=" my-10 col-md-6 col-sm-12">
                                <div class="text-center bg-white">
                                    <h6 class="title h6">Cliente Interesado</h6>
                                    <p class="mb-1">NOMBRE: {{ $compra->nameClient }} </p>
                                    <p class="mb-1">TELEFONO: {{ $compra->phone }} </p>
                                    <p class="mb-1">EMAIL: {{ $compra->email }} </p>
                                    <p class="mb-1">CITY: {{ $compra->city }} </p>
                                </div>
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
            </article>
        </div>
        @endforeach
    </div>
    <div class="pagination-wrapper">
        {{-- {{ $solicitados->links() }} --}}
    </div>
</div>
