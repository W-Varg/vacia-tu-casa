<h3>Mis Artículos Básicos</h3>
<div class="products">
    <div class="row">
        @foreach ($basicos as $product)
        <div class="col-md-12">
            <article>
                <div class="fondo-blanco">
                    <div class="info" style="background-color: initial;">
                        <span class="view">
                            <a href="{{ route('page.detail_edit_product', $product->id) }}" data-title="Mas detalles"><i class="icon icon-eye"></i></a>
                        </span>
                    </div>
                    <div class="figure-list bg-white">
                        <div class="image">
                            <a href="{{ route('page.detail_edit_product', $product->id) }}">
                                @if ($product->img)
                                <img src="{{ $product->img }}" alt="" height="130" />
                                @else
                                <img src="{{ asset('assets/images/product-1.png') }}" alt="" height="130" />
                                @endif
                            </a>
                        </div>
                        <div class="">
                            <h2 class="title h4 pt-1 mb-0 text-center"><a href="{{ route('page.detail_edit_product', $product->id) }}">{{ $product->name }}</a>
                            </h2>
                            <div class="text-center bg-white">
                                @if ($product->reduced_price)
                                <sub> {{ $product->reduced_price}} <i class="fa fa-euro"></i> </sub>
                                <sub class="tachado"> {{ $product->normal_price}} <i class="fa fa-euro"></i> </sub>
                                @else
                                <sub> {{ $product->normal_price}} <i class="fa fa-euro"></i> </sub>
                                @endif
                                @if (!$product->client_agreement)
                                <p>
                                    {!! Form::open(['route' =>['articulo.soldproduct', $product], 'class' =>
                                    ['vendido']]) !!}
                                <div class="">
                                    <input class="form-control-sm" type="number" name="pago" placeholder="Importe en Euros" required="true" />
                                    <br>
                                    <input type="submit" class="btn btn-info btn-xs m-5y" value="Marcar Como Vendido" />
                                </div>
                                {!! Form::close() !!}
                                </p>
                                <p>
                                    <a href="{{route('articulo.reservar', $product)}}" class="btn btn-sm btn-primary">Marcar como
                                        Reservado</a>
                                    {{-- {!! Form::open(['route' =>['article.destroy', $product], 'method' => 'delete', 'class' => ['inline'], 'onclick' => 'return confirm("Estas seguro de eliminar?")' ]) !!}
                                    {{ Form::submit('Eliminar', ['class' => 'btn btn-sm btn-warning']) }}
                                    {!! Form::close() !!} --}}
                                </p>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </article>
        </div>
        @endforeach
    </div>
    <div class="pagination-wrapper">
    </div>
</div>
