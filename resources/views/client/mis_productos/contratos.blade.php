<h3>Contratos Vigentes</h3>
<div class="products">
    <div class="row">
        @foreach ($agreements as $agreement)
        <div class="col-md-12">
            <div class="panel panel-default" id="typography">
                <div class="panel-heading">
                    <h5 class="mb-0">
                        @if ($agreement->name == 'premium') P-000{{ $agreement->id }}
                        @else E-000{{ $agreement->id }} @endif
                        <small>
                            Creado el
                            {{ date('d-m-Y', strtotime($agreement->created_at)) }}
                        </small>
                    </h5>
                </div>
                <div class="panel-body row">
                    <div class="col-md-4">
                        <h6 class="lead mb-0">{{ $agreement->name }}</h6>
                    </div>
                    <div class="col-md-4">
                        @if ($agreement->status == 'enviado')
                            <h6 class="text-warning mb-0">{{ $agreement->status }}</h6>
                            <small class="text-primary">Se debe Aprobar la Valoracion</small>
                        @else
                          <h6 class="text-info mb-0">{{ $agreement->status }}</h6>
                        @endif
                    </div>
                    <div class="col-md-4 col-sm-12">
                        @if ($agreement->status == 'enviado')
                            <a href="{{ route('contrato.detalle', $agreement)}}" class="btn btn-sm btn-warning">{{ __('Detalle de Contrato') }}</a>
                        @else
                            <a href="{{ route('contrato.detalle', $agreement)}}" class="btn btn-sm btn-primary">{{ __('Detalle de Contrato') }}</a>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        @endforeach
    </div>
    <div class="pagination-wrapper">
        {{-- {{ $published->links() }} --}}
    </div>
</div>
