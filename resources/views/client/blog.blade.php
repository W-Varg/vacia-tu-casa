@extends('layouts.web')
@section('content')
<span name="{{ $company = \App\Company::get()->first() }}"></span>
<section class="main-header" style="background-image:url({{ asset($company->img1) }}">
    <header>
        <div class="container text-center">
            <ol class="breadcrumb breadcrumb-inverted">
                <li><a href="/"><span class="icon icon-home"></span></a></li>
                <li><a class="active" href="">Blog de experiencias</a></li>
            </ol>
        </div>
    </header>
</section>
<section class="product">
    <div class="main">
        <div class="container">
            <div class="row">
                <div class="col-md-12">@include('client.messages')</div>
            </div>

        </div>
    </div>
</section>
@endsection