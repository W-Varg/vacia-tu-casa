@extends('layouts.web')
@section('content')
<section class="not-found">
    <div class="container">
        <h1 class="title" data-title="Página no encontrada!">404</h1>
        <div class="h4 subtitle">Página no encontrada</div>
        <p>El contenido que buscas no se encuentra aqui.</p>
        <p>Click <a class="subtitle" href="/">aqui</a> para ir a home </p>
    </div>
</section>
@endsection
