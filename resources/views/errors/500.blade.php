<section class="not-found">
    <div class="container">
        <h1 class="title" data-title="Page not found!">500</h1>
        <div class="h4 subtitle">Error de Servidor.</div>
        <p>La URL solicitada no se encontró en este servidor. Eso es todo lo que sabemos.</p>
        <p>Click <a class="subtitle" href="/">aqui</a> para ir al inicio de la pagina </p>
    </div>
</section>

