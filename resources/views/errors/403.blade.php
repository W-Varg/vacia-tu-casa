@extends('layouts.web')
@section('content')
<section class="not-found">
    <div class="container">
        <h1 class="title" data-title="Acceso no permitido!">403</h1>
        <div class="h4 subtitle">Acceso no permitido</div>
        <p>Por Seguridad este usuario no tiene acceso a esta pagina.</p>
        <p>Click <a class="subtitle" href="/">aqui</a> para ir a home </p>
    </div>
</section>
@endsection
