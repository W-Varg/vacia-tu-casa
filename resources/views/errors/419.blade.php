@extends('layouts.web')
@section('content')
<section class="not-found">
    <div class="container">
        <h1 class="title" data-title="Page not found!">419</h1>
        <div class="h4 subtitle">Sesión Expirada.</div>
        <p>Por Seguridad Se ha actualizado el token de acceso.</p>
        <p>Click <a class="subtitle" href="/login">aqui</a> para loguearse nuevamente </p>
    </div>
</section>
@endsection
