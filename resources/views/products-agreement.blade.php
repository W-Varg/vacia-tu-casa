@extends('layouts.web')
@section('content')
<span name="{{ $company = \App\Company::get()->first() }}"></span>
<section class="main-header" style="background-image:url({{ asset($company->img1) }}">
    <header>
        <div class="container text-center">
            <h2 class="h2 title">Detalle de contrato</h2>
            <ol class="breadcrumb breadcrumb-inverted">
                <li><a href="/"><span class="icon icon-home"></span>home</a></li>
                <li><a href="{{ route('client_products') }} ">Contratos</a></li>
                <li class="active">Detalle de contrato</li>
            </ol>
        </div>
    </header>
</section>

<section class="our-team">
    <div class="container">
        <div class="row">
            <div class="col-md-12">@include('client.messages')</div>
        </div>
        <div class="row">
            <div class="col-md-6 col-sm-12">
                <div class="box-header with-border flex text-center">
                    <p class="h4 title">Tipo de contrato - {{ $agreement->name }}</p>
                    <p>Contrato creado el ({{ date('d-m-Y', strtotime($agreement->created_at)) }})</p>
                    <p class="title h4"> Estado de Contrato : <span class="text-warning">{{$agreement->status}}</span> </p>
                    @if ($agreement->status == 'aprobado')
                    <div class="col-12 my-2">
                        <a href="{{ route ('contrato.download', $agreement)}}" class="btn btn-sm btn-info">Descargar
                            Contrato en Pdf </a>
                    </div>
                    @endif
                    @if ($agreement->status =='enviado')
                    <br>
                    <div class="row mt-3">
                        <div class="col-md-4"></div>
                        <div class="col-md-6">
                            <span class="checkbox">
                                <input type="checkbox" id="checkBoxId3">
                                <label for="checkBoxId3">Acepto las condiciones de Contrato</label>
                            </span>
                        </div>
                    </div>
                    <a href="{{ route('pass.contrato', $agreement->verifyAgreement->token)}}" class="btn btn-success">Aprobar Contrato</a>
                    @endif
                    <p class="mt-3">
                        <small class="text-info">La validez del contrato empieza desde la fecha de publicación de los articulos</small>
                    </p>
                </div>
            </div>
            <div class="col-md-6 col-sm-12">
                <div class="box-header with-border flex text-center">
                    <p class="h4 title">N° de Contrato: <label class="text-primary mb-0"> {{ $agreement->id }} </label> </p>
                    <p>Cliente: <label class="text-primary"> {{ $agreement->client->user->name}}</label></p>
                    @if ($agreement->handler)
                    <p class="h4 title"> Gestor Asignado: <span class="text-primary mb-0"> {{ $agreement->handler->user->name }}</span> </p>
                    <span class="bg-primary text-primary mb-0">
                        <a data-title="Iniciar Chat" class="btn btn-sm btn-info" data-title-added="Enviar sms"
                            href="https://web.whatsapp.com/send?phone={{ $agreement->handler->user->profile->phone_number }}" target="_blank"><i
                                class="fa fa-whatsapp"></i>
                            {{ $agreement->handler->user->profile->phone_number }}</a>
                    </span>
                    @else
                    <div class="info-box info-box-addto">
                        <span><strong><label class="title text-warning"> NUESTRO EQUIPO SE PONDRÁ EN CONTACTO CONTIGO EN
                                    BREVE</label></strong></span>
                    </div>
                    @endif
                    @if($agreement->status =='publicado')
                    <div class="info-box info-box-addto">
                        <span><strong>Fecha de publicacion: </strong></span><span> <label class="text-primary">
                                {{ date('d-m-Y', strtotime($agreement->date_start)) }}</label> </span>
                    </div>
                    @endif
                </div>
            </div>
        </div>
        <hr>
        <section class="info-icons" style="background: transparent !important;">
            <div class="row">
                <div class="col-md-12">
                    <h3>Artículos Valorados</h3>
                </div>
                <div class="box">
                    <div class="box-body rounded flex">
                        <table class="table table-striped">
                            <thead>
                                <th>Codigo</th>
                                <th>Imagen</th>
                                <th>Nombre</th>
                                <th>Categoria</th>
                                <th>Estado</th>
                                <th>Precio</th>
                                <th>Precio Reducido</th>
                            </thead>
                            <tbody>
                                @foreach($agreement->products as $product)
                                <tr>
                                    <td>{{ $product->code}} </td>
                                    <td class="text-center">
                                        <a href="{{ route ('page.detail_product', $product) }}">
                                            @if ($product->img)
                                            <img class="rounded" src="{{ asset($product->img) }}" alt="" height="50" />
                                            @else
                                            <img src="{{ asset('assets/images/product-1.png') }}" alt="" height="50" />
                                            @endif
                                        </a>
                                    </td>
                                    <td> <a href="{{ route ('page.detail_product', $product) }}"> {{ $product->name }} </a>
                                    </td>
                                    <td> {{ $product->category->name }} </td>
                                    <td class="text-warning">
                                        {{ $product->status }}
                                    </td>
                                    <td>
                                        @if ($product->status === 'VENDIDO')
                                        {{ $product->compra->pago }}
                                        @else
                                        {{ $product->normal_price }}
                                        @endif
                                        <i class="fa fa-euro"></i>
                                    </td>
                                    <td>
                                        @if ($product->status === 'VENDIDO')
                                        @else
                                        {{ $product->reduced_price }}
                                        <i class="fa fa-euro"></i>
                                        @endif
                                    </td>
                                    <td>
                                        {{-- <a class="btn btn-sm btn-default" href="{{ route ('products.edit', $product) }}"><i
                                                class="fa  fa-pencil"></i> Editar</a> --}}
                                    </td>
                                </tr>
                                @endforeach
                                <tr>
                                    <td colspan="4"></td>
                                    <td>TOTAL:</td>
                                    <td class="h6"> {{ $total}} <i class="fa fa-euro"></i> </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </section>
    </div>
</section>
@endsection
