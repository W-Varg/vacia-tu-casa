<?php

namespace App;

use App\Http\Controllers\AgreementController;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
        'user_id', 'category_id', 'name', 'variant', 'normal_price', 'reduced_price', 'quantity', 'code', 'client_agreement_id',
        // 'images', // 'slug', img
        'description', 'type', 'weight', 'long', 'width', 'height', 'status', 'style', 'img', 'created_at', 'updated_at', 'descuento',
        'importance', 'beliani'
    ];
    public function getNormalPriceAttribute($value)
    {
        return str_replace('.', ',', $value);
    }
    public function getReducedPriceAttribute($value)
    {
        return str_replace('.', ',', $value);
    }
    public function getWeightAttribute($value)
    {
        return str_replace('.', ',', $value);
    }
    public function getLongAttribute($value)
    {
        return str_replace('.', ',', $value);
    }
    public function getWidthAttribute($value)
    {
        return str_replace('.', ',', $value);
    }
    public function getHeightAttribute($value)
    {
        return str_replace('.', ',', $value);
    }
    // setter

    public function setNormalPriceAttribute($value)
    {
        $this->attributes['normal_price'] = str_replace(',', '.', $value);
    }
    public function setReducedPriceAttribute($value)
    {
        $this->attributes['reduced_price'] = str_replace(',', '.', $value);
    }
    public function setWeightAttribute($value)
    {
        $this->attributes['weight'] = $value ? str_replace(',', '.', $value) : null;
    }
    public function setLongAttribute($value)
    {
        $this->attributes['long'] = $value ? str_replace(',', '.', $value) : null;
    }
    public function setWidthAttribute($value)
    {
        $this->attributes['width'] = $value ? str_replace(',', '.', $value) : null;
    }
    public function setHeightAttribute($value)
    {
        $this->attributes['height'] = $value ? str_replace(',', '.', $value) : null;
    }

    public function category()
    { //pertenece a una categoria, (relacion inversa)
        return $this->belongsTo(Category::class);
    }

    public function user()
    { //pertenece a un usuario, (relacion inversa)
        return $this->belongsTo(User::class);
    }

    public function tags()
    { //pertenecen muchos etiquetas a este producto
        return $this->belongsToMany(Tag::class);
    }

    public function images()
    { //many images belong to a product
        return $this->hasMany(Image::class);
    }

    public function client_agreement()
    {
        return $this->belongsTo(ClientAgreement::class);
    }

    public function comments()
    {
        return $this->morphMany(Comment::class, 'commentable');
    }

    public function compra()
    { // a user can have many products
        return $this->hasOne(Compra::class);
    }

    // querysets
    public function scopeName($query, $name)
    {
        if ($name)
            return $query->where('name', 'iLIKE', "%$name%")
                ->orWhere('code', 'iLIKE', "%$name%");
    }
    public function scopeBeliani($query, $beliani)
    {
        if ($beliani)
            return $query->where('beliani', true)
                ->where('reduced_price', null)
                ->orWhere('reduced_price', '<=', 0);
    }

    public function scopeDescuento($query, $descuento, $precio)
    { //descuento
        if ($descuento && $precio) {
            $precio = explode(";", $precio);
            switch ($descuento) {
                case 'all':
                    // articulos que tiene descuento..
                    return $query->where(function ($qr) use ($precio) {
                        $qr->where('normal_price', '>=', $precio[0])->where('normal_price', '<=', $precio[1]);
                    })->orWhere(function ($qr) use ($precio) {
                        $qr->where('reduced_price', '>=', $precio[0])->where('reduced_price', '<=', $precio[1]);
                    });
                    break;
                case 'nuevo':
                    // // return $query->where('reduced_price', '=', null)->where('normal_price', '>=',$precio[0])->where('normal_price', '<=',$precio[1]);
                    return $query->where('reduced_price', '=', null)
                        ->where('normal_price', '>=', $precio[0])
                        ->where('normal_price', '<=', $precio[1]);
                    break;

                case 'regular':
                    // // return $query->where('reduced_price', '=', null)->where('normal_price', '>=',$precio[0])->where('normal_price', '<=',$precio[1]);
                    return $query->where('reduced_price', '=', null);
                    break;

                case 'premium': // los que tengan
                    $contratos =  ClientAgreement::select('id')->where('name', '=', 'premium')->get();
                    return $query->whereIn('client_agreement_id', $contratos);
                    break;

                case 'descuento':
                    // $contratos =  ClientAgreement::select('id')->where('name','=','estandar')->get();
                    // return $query->whereIn('client_agreement_id', $contratos);
                    return $query->where('reduced_price', '<>', null) // articulos que tiene descuento..
                        ->where('reduced_price', '>=', $precio[0])
                        ->where('reduced_price', '<=', $precio[1]);
                    break;
                default:
                    return;
                    break;
            }
        }
    }

    public function scopeOfertas($query, $ofertas)
    { // nuevo, premium, estandar
        if ($ofertas && $ofertas != 'all') {
            switch ($ofertas) {
                case 'nuevo':
                    return $query->where('reduced_price', '=', null)->where('variant', '=', 'NOVEDAD');
                    break;
                case 'oulet':
                    return $query->where('code', 'like', 'O%');
                    break;
                case 'oportunidad':
                    // tienen q estar estar los estandar premiuns q tengan descuento, pero q no sean outled ni beliani
                    return $query->where('reduced_price', '<>', null)
                        // ->where('code', 'not like', 'O%')
                        ->whereIn('variant', ['OFERTA', 'OPORTUNIDAD']);
                    break;
                case 'descuento':
                    return $query->where('reduced_price', '<>', null);
                    break;
                case 'destacados':
                    return $query->where('variant', '=', 'OFERTA');
                    break;
                case 'premium': // los que tengan
                    $contratos =  ClientAgreement::select('id')->where('name', '=', 'premium')->get();
                    return $query->whereIn('client_agreement_id', $contratos);
                    break;

                case 'estandar':
                    $contratos =  ClientAgreement::select('id')->where('name', '=', 'estandar')->get();
                    return $query->whereIn('client_agreement_id', $contratos);
                    break;
                default:
                    return;
                    break;
            }
        }
    }
    public function scopeVariante($query)
    {
        return $query->where('variant', 'OPORTUNIDAD')
            ->orWhere('variant', 'OFERTA')
            ->orWhere('variant', 'NOVEDAD');
    }
    public function scopeOrderImportance($query)
    {
        return $query->orderBy('importance', 'desc');
    }
    public function scopeSinDescuento($query, $sindescuento)
    {
        if ($sindescuento) {
            $bool_value = ($sindescuento === "true");
            $produtcs = $query->where('descuento', '=', !$bool_value);
            // dd($sindescuento, $bool_value, $produtcs->get());
            return $produtcs;
        }
    }
    public function scopeOnlynovedad($query)
    {
        return $query->where('variant', 'NOVEDAD')->where('reduced_price', '=', null)->where('client_agreement_id', '!=', null);
    }
    public function scopePrecio($query)
    {
        $query2 = $query;
        $products = $query2->get();
        $hoy = Carbon::today();
        $products = $products->filter(function ($p) use ($hoy) {
            if ($p->descuento == false) {
                return $p;
            }
            if ($p->status == 'PUBLICADO') {
                switch ($p) {
                    default:
                        $fecha112 = $p->created_at;
                        $fecha84 = $p->created_at;
                        $fecha56 = $p->created_at;
                        $fecha42 = $p->created_at;
                        $fechaNow = $p->created_at;

                        if ($fecha112->addDays(112) < $hoy) { // 16  semanas, se cierrar contrato, aplica a todos los articulos
                            // si el producto es premium entra en una zona q solo estan los premiun y aquellos q superaron los 16 semanas
                            // y son basicos quedaron rechazados y finaliza su contrato
                            /** descomentar line 151 to157, and comment 158,159 */
                            if ($p->client_agreement) { // FIXME: FALTA EXCEPCCION PARA LOS ARTICULOS QUE SE CONCLUYERON
                                if ($p->client_agreement->date_end < Carbon::now()) {
                                    $p->client_agreement->status = 'concluido';
                                    // $p->client_agreement->date_end = Carbon::now();
                                    $p->variant = 'CONCLUIDO';
                                    $p->status = 'CONCLUIDO';
                                    $p->client_agreement->save();
                                    $p->save();
                                }
                                break;
                            } else {
                                $p->status = 'DE BAJA';
                                $p->variant = 'DE BAJA';
                                $p->save();
                                break;
                            }
                        }
                        if ($fecha84->addDays(84) < $hoy) { // 12  semanas
                            $p->client_agreement ?  $p->reduced_price = round($p->normal_price - $p->normal_price * 0.5) : null;
                            $p->variant = 'OPORTUNIDAD';
                            $p->save();
                            break;
                        }
                        // if ($fecha->addDays(70) < $hoy) { // 10 semanas
                        //     $p->reduced_price = round($p->normal_price - $p->normal_price * 0.30); break;
                        // }
                        if ($fecha56->addDays(56) < $hoy) { // 8 semanas
                            if ($p->client_agreement) {
                                $p->reduced_price = round($p->normal_price - $p->normal_price * 0.30);
                            } else {
                                $p->status = 'DE BAJA';
                                $p->variant = 'DE BAJA';
                            }
                            $p->save();
                            break;
                        }
                        if ($fecha42->addDays(42) < $hoy) { // 6 semanas
                            $p->client_agreement ?  $p->reduced_price = round($p->normal_price - $p->normal_price * 0.15) : null;
                            $p->variant = 'OFERTA';
                            $p->save();
                            break;
                        }
                        // if ($fecha->addDays(30) < $hoy) { // 4 semans
                        //     $p->reduced_price = round($p->normal_price - $p->normal_price * 0.15);break;
                        // }
                        // if ($fecha->addDays(14) < $hoy) { //2 semans
                        //     $p->reduced_price = round($p->normal_price - $p->normal_price * 0.1);break;
                        // }
                        break;
                }
            } else {
                if ($p->status == 'VENDIDO') {
                    $dateUpdate = $p->updated_at;
                    if ($dateUpdate->addDays(10) >= $hoy) {
                        $p->variant = 'VENDIDO';
                        $p->save();
                    } else {
                        $p->variant = 'CONCLUIDO'; // TODO: lo guardo concluido despues de que pase 10 dias de su venta, para q no aparesca en la pagina
                        $p->save();
                    }
                }
            }
            return $p;
        });
        return $query;
    }
    public function scopeArea($query, $area)
    {
        if ($area && $area != 'all') {
            return $query->where('category_id', '=', $area);
        }
    }

    public function scopeEstancia($query, $estancia)
    {
        if ($estancia) {
            // obtener las ids de categorias q incluyan este nombre
            $categoriesID = Category::select('id')->where('name', 'iLIKE', '%' . $estancia . '%')->get();
            return $query->whereIn('category_id', $categoriesID);
        }
    }
    public function scopeMaterial($query, $material)
    {
        if ($material) {
            return $query->whereHas('tags', function ($query) use ($material) {
                $query->where('slug', $material);
            });
        }
    }
    public function scopeCiudad($query, $ciudad)
    {
        if ($ciudad) {
            // obtenemos los usuarios q incluyan este nombre de ciudad en su address, localidad, o codigo postal
            $usersID = Profile::select('user_id')->where('provincia', 'iLIKE', "%$ciudad%")
                ->orWhere('localidad', 'iLIKE', "%$ciudad%")->orWhere('address', 'iLIKE', "%$ciudad%")
                ->orWhere('code_postal', 'iLIKE', "%$ciudad%")->get();

            $existe = Company::where('address', 'iLIKE', "%$ciudad%")->orWhere('code_postal', 'iLIKE', "%$ciudad%")->get();
            if ($existe) {
                $contratos =  ClientAgreement::select('id')->where('name', '=', 'premium')->get();
                return $query->whereIn('client_agreement_id', $contratos);
            }
            return $query->whereIn('user_id', $usersID);
        }
    }
}
