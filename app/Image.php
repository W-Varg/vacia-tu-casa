<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    //
    protected $fillable = ['product_id','path', 'visit_id'];

    public function product(){ // a user can have many products
        return $this->belongsTo(Product::class);
    }

    public function visit(){ // a user can have many products
        return $this->belongsTo(Visit::class);
    }
}
