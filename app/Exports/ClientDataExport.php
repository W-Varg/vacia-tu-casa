<?php

namespace App\Exports;
use App\ClientAgreement;
use App\Product;
use App\User;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;

use PhpOffice\PhpSpreadsheet\Shared\Date;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;


use PhpOffice\PhpSpreadsheet\Cell\Cell;
use Maatwebsite\Excel\Concerns\ToModel;
use PhpOffice\PhpSpreadsheet\Cell\DataType;
use Maatwebsite\Excel\Concerns\WithCustomValueBinder;
use PhpOffice\PhpSpreadsheet\Cell\DefaultValueBinder;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithTitle;

class ClientDataExport implements WithHeadings, ShouldAutoSize, WithEvents, WithTitle
{
    private $agreement;
    private $handler;
    private $client;
    use Exportable;

    public function __construct(ClientAgreement $agreement)
    {
        $this->agreement = $agreement;
        $this->client = $agreement->client->user;
        $this->handler = $agreement->handler->user;
    }

    public function title(): string
    {
        return 'DatosContrato';
    }
    
    public function headings(): array // WithHeadings
    {
        return [
            ['N° de Cliente', $this->client->id ],
            ['Nombre', $this->client->name.' '.$this->client->profile->last_name  ],
            ['Telefono Cliente', $this->client->profile->phone_number ],
            ['Contrato N', $this->agreement->id],
            ['Direccion', $this->agreement->visit->address ],
            ['CP', $this->agreement->visit->cp ],
            ['Gestor', $this->handler->name.' '.$this->handler->profile->last_name ],
            ['Telefono Gestor', $this->handler->profile->phone_number ],
         ];
    }
    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function(AfterSheet $event) {
                $cellRange = 'A1:B8'; // All headers
                $sheet = $event->sheet->getDelegate();
                $sheet->getStyle($cellRange)->getFont()->setSize(12);
                $sheet->getStyle($cellRange)->getAlignment()->setWrapText(true)->setHorizontal('right');
                $sheet->getStyle('A1:A8')->getFont()->setSize(12)->setBold(true)->setName('Yu Gothic UI');
                $sheet->getStyle('A1:A8')->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setARGB('00c292');

                $sheet->getStyle('B1:B8')->getAlignment()->setWrapText(true)->setHorizontal('left');
                $sheet->getStyle('B1:B8')->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setARGB('ffff15');
            },
        ];
    }

}
