<?php

namespace App\Exports;
use App\ClientAgreement;
use App\Product;
use App\User;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;

use PhpOffice\PhpSpreadsheet\Shared\Date;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;


use PhpOffice\PhpSpreadsheet\Cell\Cell;
use Maatwebsite\Excel\Concerns\ToModel;
use PhpOffice\PhpSpreadsheet\Cell\DataType;
use Maatwebsite\Excel\Concerns\WithCustomValueBinder;
use PhpOffice\PhpSpreadsheet\Cell\DefaultValueBinder;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithTitle;


class ProductExport implements FromQuery, WithHeadings, ShouldAutoSize, WithColumnFormatting, WithTitle //,WithEvents
{
    private $agreement;
    private $handler;
    private $client;
    use Exportable;

    public function __construct(ClientAgreement $agreement)
    {
        $this->agreement = $agreement;
    }

    public function title(): string
    {
        return 'Articulos';
    }

    public function query()
    {
        return Product::where('client_agreement_id', $this->agreement->id)
            ->select(['id','code','name','normal_price','quantity','description','style','weight', 'long', 'width', 'height', 'status','type']);
    }

    public function headings(): array // WithHeadings
    {
        return [
            ['Identificador','Codigo','Nombre','Precio','stock','Descripcion','Estilos','Peso', 'Largo', 'Ancho', 'Alto', 'Estado','Tipo'], //A13
         ];
    }

    public function columnFormats(): array  //WithColumnFormatting
    {
        return [
            'C' => NumberFormat::FORMAT_CURRENCY_EUR_SIMPLE,
            'D' => NumberFormat::FORMAT_NUMBER,
            'H' => NumberFormat::FORMAT_NUMBER_00,
            'I' => NumberFormat::FORMAT_NUMBER_00,
            'J' => NumberFormat::FORMAT_NUMBER_00,
        ];
    }
    // public function registerEvents(): array
    // {
    //     return [
    //         AfterSheet::class => function(AfterSheet $event) {
    //             $cellRange = 'A1:M1'; // All headers
    //             $sheet = $event->sheet->getDelegate();
    //             $sheet->getStyle($cellRange)->getFont()->setName('Arial')->setSize(11)->setBold(true);
    //             $sheet->getStyle($cellRange)->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setARGB('00c292');
    //         },
    //     ];
    // }
}
