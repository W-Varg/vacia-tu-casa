<?php

namespace App\Exports;

use App\ClientAgreement;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class ConcluidoExcel implements WithMultipleSheets
{
    use Exportable;
    private $agreement;

    public function __construct(ClientAgreement $agreement)
    {
        $this->agreement = $agreement;
    }

    public function sheets(): array
    {
        $sheets = [];
        $sheets[] = new VendidosExport($this->agreement);
        $sheets[] = new NoVendidosExport($this->agreement);
        $sheets[] = new ClientDataExport($this->agreement);
        return $sheets;
    }
}
