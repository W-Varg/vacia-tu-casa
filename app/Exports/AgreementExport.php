<?php

namespace App\Exports;

use App\ClientAgreement;
use App\Product;
use App\User;
use App\Exports\ClientDataExport;
use App\Exports\ProductExport;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;

use PhpOffice\PhpSpreadsheet\Shared\Date;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;


use PhpOffice\PhpSpreadsheet\Cell\Cell;
use Maatwebsite\Excel\Concerns\ToModel;
use PhpOffice\PhpSpreadsheet\Cell\DataType;
use Maatwebsite\Excel\Concerns\WithCustomValueBinder;
use PhpOffice\PhpSpreadsheet\Cell\DefaultValueBinder;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;


class AgreementExport implements WithMultipleSheets
{
    private $agreement;
    use Exportable;

    public function __construct(ClientAgreement $agreement)
    {
        $this->agreement = $agreement;
    }

    public function sheets(): array
    {
        $sheets = [];
        $sheets[] = new ProductExport($this->agreement);
        $sheets[] = new ClientDataExport($this->agreement);
        return $sheets;
    }

    // public function query()
    // {
    //     return Product::where('client_agreement_id', $this->agreement->id)
    //         ->select(['id','code','name','normal_price','quantity','description','style','weight', 'long', 'width', 'height', 'status','type']);
    // }
    // public function headings(): array // WithHeadings
    // {
    //     return [
    //         ['N° de Cliente', $this->agreement->client->user->id ],
    //         ['Nombre', $this->agreement->client->user->name.' '.$this->agreement->client->user->profile->last_name  ],
    //         ['Telefono Cliente', $this->agreement->client->user->profile->phone_number ],
    //         ['Contrato N', $this->agreement->id],
    //         ['Direccion', $this->agreement->visit->address ],
    //         ['CP', $this->agreement->visit->cp ],
    //         ['Gestor', $this->agreement->handler->user->name.' '.$this->agreement->handler->user->profile->last_name ],
    //         ['Telefono Gestor', $this->agreement->handler->user->profile->phone_number ],
    //         [], // A9
    //         ['Nota: Solo debe llenar los datos Nesesarios'],
    //         [], // A11
    //         ['Articulos'], //A12
    //         ['Identificador','Codigo','Nombre','Precio','stock','Descripcion','Estilos','Peso', 'Largo', 'Ancho', 'Alto', 'Estado','Tipo'], //A13
    //      ];
    // }
    // public function columnFormats(): array  //WithColumnFormatting
    // {
    //     return [
    //         'C' => NumberFormat::FORMAT_CURRENCY_EUR_SIMPLE,
    //         'D' => NumberFormat::FORMAT_NUMBER,
    //         'H' => NumberFormat::FORMAT_NUMBER_00,
    //         'I' => NumberFormat::FORMAT_NUMBER_00,
    //         'J' => NumberFormat::FORMAT_NUMBER_00,
    //     ];
    // }

    // public function registerEvents(): array
    // {
    //     // $styleArray = ;

    //     return [
    //         AfterSheet::class => function(AfterSheet $event) {
    //             $cellRange = 'A1:B8'; // All headers
    //             $sheet = $event->sheet->getDelegate();
    //             $sheet->getStyle($cellRange)->getFont()->setSize(12);
    //             $sheet->getStyle($cellRange)->getAlignment()->setWrapText(true)->setHorizontal('right');
    //             $sheet->getStyle('A1:A8')->getFont()->setSize(12)->setBold(true)->setName('Yu Gothic UI');
    //             $sheet->getStyle('A1:A8')->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setARGB('00c292');

    //             $sheet->getStyle('B1:B8')->getAlignment()->setWrapText(true)->setHorizontal('left');
    //             $sheet->getStyle('B1:B8')->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setARGB('ffff15');

    //             $sheet->mergeCells('A10:M10')->getStyle('A10')->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setARGB('faa64b');
    //             // A11
    //             $sheet->getStyle('A12:M12')->getFont()->setName('Arial')->setSize(11)->setBold(true);
    //             $sheet->getStyle('A12:M12')->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setARGB('ffff15');

    //             $sheet->getStyle('A13:M13')->getFont()->setName('Arial')->setSize(11)->setBold(true);
    //             $sheet->getStyle('A13:M13')->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setARGB('00c292');
    //         },
    //     ];
    // }

}
