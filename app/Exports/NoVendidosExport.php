<?php

namespace App\Exports;

use App\Product;
use App\ClientAgreement;
use App\Compra;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\WithMapping;

class NoVendidosExport implements FromQuery, ShouldAutoSize, WithTitle, WithMapping, WithHeadings, WithColumnFormatting //,WithEvents
{
    private $agreement;
    use Exportable;

    public function __construct(ClientAgreement $agreement)
    {
        $this->agreement = $agreement;
    }

    public function title(): string // WithTitle
    {
        return 'No Vendidos';
    }

    public function query() // FromQuery
    {
        return Product::where('client_agreement_id', $this->agreement->id)->where('status', '<>', 'VENDIDO');
    }
    public function map($product): array // WithMapping
    {
        return [
            $product->code,
            $product->name,
            $product->normal_price,
            $product->reduced_price,
            $product->status,
        ];
    }

    public function columnFormats(): array  //WithColumnFormatting
    {
        return [
            'C' => NumberFormat::FORMAT_CURRENCY_EUR_SIMPLE,
            'D' => NumberFormat::FORMAT_CURRENCY_EUR_SIMPLE,
        ];
    }

    public function headings(): array // WithHeadings
    {
        return [
            ['Codigo', 'Nombre', 'Precio Normal', 'Precio Reducido', 'Estado'],
        ];
    }
}
