<?php

namespace App\Exports;

use App\ClientAgreement;
use App\Compra;
use App\Product;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\WithMapping;


class VendidosExport implements FromQuery, ShouldAutoSize, WithTitle, WithMapping, WithHeadings, WithColumnFormatting //,WithEvents
{
    private $agreement;
    use Exportable;

    public function __construct(ClientAgreement $agreement)
    {
        $this->agreement = $agreement;
    }

    public function title(): string // WithTitle
    {
        return 'Articulos Vendidos';
    }

    public function query() // FromQuery
    {
        return  Compra::orderBy('updated_at', 'DESC')->where('status', 'completado')
            ->whereIn('product_id', Product::where('client_agreement_id', $this->agreement->id)->pluck('id'));
    }
    public function map($compra): array // WithMapping
    {
        return [
            $compra->product->code,
            // $compra->status,
            $compra->product->name,
            $compra->product->status,
            $compra->pago,
            date('d-M-Y', strtotime($compra->fecha_venta))
        ];
    }

    public function columnFormats(): array  //WithColumnFormatting
    {
        return [
            'D' => NumberFormat::FORMAT_CURRENCY_EUR_SIMPLE,
            // 'f' => DateFormat,
        ];
    }

    public function headings(): array // WithHeadings
    {
        return [
            ['Codigo', 'Nombre', 'Estado Articulo', 'Precio', 'Fecha de Venta'],
        ];
    }
}
