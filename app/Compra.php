<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Compra extends Model
{
    protected $fillable = [
        'product_id',
        'vendedor',
        'comprador',
        'status',
        'nameClient',
        'email',
        'phone',
        'city',
        'comments',
        'monto_reserva',
        'have_reserva',
        'pago',
        'have_pago',
        'fecha_reserva',
        'fecha_venta',
    ];

    public function getSellerAttribute()
    { // vendedor
        return User::where('id', $this->vendedor)->first();
    }

    public function product()
    { // a user can have many products
        return $this->belongsTo(Product::class);
    }

    public function getBuyerAttribute()
    { // cliente, comprador
        return User::where('id', $this->comprador)->first();
    }
}
