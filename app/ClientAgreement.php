<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClientAgreement extends Model
{
    protected $fillable = [
        'name',
        'status',
        'verified',
        'client_id',
        'agreement_id',
        'handler_id',
        'date_start',
        'date_end',
        'percent_discount_week_2',
        'percent_discount_week_4',
        'percent_discount_week_6',
        'percent_discount_week_8',
        'percent_penalization',
        'percent_negotiation',
        'percent_company',
        'validity',
        'created_at',
        'extended'
    ];

    public function client()
    {
        return $this->belongsTo(Client::class);
    }

    public function agreement()
    {
        return $this->belongsTo(Agreement::class);
    }

    public function handler()
    {
        return $this->belongsTo(Handler::class);
    }

    public function products()
    {
        return $this->hasMany(Product::class)->orderBy('id', 'ASC');
    }

    public function sold()
    {
        $sold = $this->products()->where('status', '=', 'VENDIDO')->get();
        return $sold->count();
    }

    public function published()
    {
        $published = $this->products()->where('status', '=', 'PUBLICADO');
        return $published->count();
    }

    public function visit()
    {
        return $this->hasOne(Visit::class);
    }

    public function verifyAgreement()
    {
        return $this->hasOne(VerifyAgreement::class);
    }
    public function utilidad()
    {
        return $this->hasOne(Utilidad::class);
    }

    public function scopeName($query, $name)
    {
        if ($name) {

            $client_ids =  User::with('profile', 'client')
                ->where('name', 'iLIKE', "%$name%")
                ->orWhereHas('profile', function ($query) use ($name) {
                    $query->where('last_name', 'iLIKE', "%$name%");
                })->get()->pluck('client')->pluck('id');

            return $query->where('id', 'iLIKE', "%$name%")->orWhereIn('client_id', $client_ids);
        }
    }
}
