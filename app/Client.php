<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $fillable = ['user_id'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public function visits()
    {
        return $this->hasMany(Visit::class);
    }

    public function handler()
    {
        return $this->belongsToMany(Handler::class);
    }

    public function client_agreement()
    {
        return $this->hasMany(ClientAgreement::class)->orderBy('created_at', 'DESC');
    }
}
