<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            // 'user_id'       => 'required|integer',
            'category_id'   => 'required|integer',
            // 'code'          => 'required|integer',
            'name'          => 'required',
            // 'slug'          => 'required|unique:products,slug',
            'normal_price'  => 'required|regex:/^\d{1,13}(\,\d{1,2})?$/',
            'weight'  => 'nullable|regex:/^\d{1,13}(\,\d{1,2})?$/',
            'height'  => 'nullable|regex:/^\d{1,13}(\,\d{1,2})?$/',
            'width'  => 'nullable|regex:/^\d{1,13}(\,\d{1,2})?$/',
            'long'  => 'nullable|regex:/^\d{1,13}(\,\d{1,2})?$/',
            'quantity'      => 'required|integer',
            // 'description'   => 'max:2500',
            // 'status'        => 'required|in:PUBLICADO,PENDIENTE,RECHAZADO,VENDIDO,DE BAJA,RESERVADO',
            'type'        => 'required|in:EXCLUSIVE,NORMAL',
            //objetos de relaciones
            'tags'          => 'array',
            'images' => 'required|array',
            'new_article' => 'string'
        ];
        return $rules;
    }
}
