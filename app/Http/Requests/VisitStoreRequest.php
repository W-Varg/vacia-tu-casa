<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class VisitStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'date_visit' => 'required',
            'hour_visit' => 'required',
            'cp'    => 'required|integer',
            'address' => 'required|string',
            'phone' => 'required',
            // 'status' =>'required',
            'images.*' => 'required|image|mimes:jpeg,bmp,png,jpg,svg'
        ];
    }
    public function messages()
    {
        return [
            // 'date_visit.required' => 'required',
            // 'hour_visit.required' => 'required',
            'cp.required'    => 'se requiere un código postal',
            // 'address' => 'required|string',
            'phone.required' => 'Es Obligatorio el Numero de telefono',
            // 'status' =>'required',
            'images[].required' => "Se requiere unas imagenes del Artículo",
            'images[].mimes' => "Los formatos de imagenes deben ser png, jpg, jpeg o bmp",
        ];
    }
}
