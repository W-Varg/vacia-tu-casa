<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ArticuloUpdate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'user_id'       => 'required|integer',
            'category_id'   => 'required|integer',
            // 'code'          => 'required|integer',
            'name'          => 'required',
            // 'slug'          => 'required|unique:products,slug,'. $this->product,
            // 'normal_price'  => 'required',
            // 'quantity'      => 'required|integer',
            'description'   => 'required',
            'status'        => 'required|in:PUBLICADO,PENDIENTE,RECHAZADO,VENDIDO,DE BAJA,RESERVADO',
            'type'        => 'required|in:EXCLUSIVE,NORMAL',
            //objetos de relaciones
            'tags'          => 'array',
         ];
        return $rules;
    }
}
