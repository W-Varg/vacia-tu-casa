<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\User;


class HandlerStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'dni' => 'required|string|max:255',
            'code_postal' => 'required|integer',
            'phone_number' => 'required|integer',
            'email' => 'required|string|email|max:255|unique:users',
            'avatar' => 'sometimes|image|mimes:jpg,jpeg,png,svg',
            'password' => 'required|string|min:8|confirmed',
            // 'condiciones' => 'required'
        ];
    }
}
