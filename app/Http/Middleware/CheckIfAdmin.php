<?php

namespace App\Http\Middleware;
use Illuminate\Contracts\Auth\Guard;
use Closure;

class CheckIfAdmin
{
    private $auth;

    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if ($this->auth->user()->role->id === 1 || $this->auth->user()->role->id === 2) {
            return $next($request);
        }
        return back()->with('error', 'Acceso No Autorizado, No cumple con los permisos necesarios para acceder a la pagina indicada');
        // $this->auth->logout();
        // return redirect()->to('login');
    }
}
