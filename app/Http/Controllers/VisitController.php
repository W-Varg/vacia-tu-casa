<?php

namespace App\Http\Controllers;

use App\Visit, App\Product, App\Category, App\Tag;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Role, App\Handler, App\Agreement, App\ClientAgreement, App\Client, App\Image, App\VerifyAgreement;
use App\Http\Requests\VisitStoreRequest;
use \App\Mail\AgreementRegetedEmail;
use Intervention\Image\Facades\Image as ImageManager;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;
use PhpParser\Node\Expr\FuncCall;

class VisitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (auth()->user()->role->id == \App\Role::ADMIN) {
            $visits_for_assign = Visit::where('handler_id', null)->where('contrato', false)->paginate(20); // las no asigndas(admin),
            $visits = Visit::where('handler_id', '<>', null)->where('contrato', false)->where('status', '<>', 'cerrado')->paginate(20);
            $handlers = Handler::get();
            return view('admin.visits.index', compact('visits_for_assign', 'visits', 'handlers'));
        } else {
            $visits = Visit::where('handler_id', auth()->user()->handler->id)->where('contrato', false)->paginate(20); // visitar por promotor()
            return view('admin.visits.my_visits', compact('visits'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function assign(Request $request)
    {
        $visit = Visit::findOrFail($request->get('id_visit'));
        $visit->handler_id = $request->get('handler_id');
        $visit->save();
        $visit->client_agreement->handler_id = $request->get('handler_id');
        $visit->client_agreement->save();
        $visit->handler->clients()->attach([$visit->client->id]);
        $visit->handler->save();
        return back()->with('info', 'asignacion de visita a ' . $visit->handler->user->name . ' creada con éxito');
    }

    public function reprograming(Request $request)
    {
        $visit = Visit::findOrFail($request->get('id_visit'));
        $visit->fill($request->all());
        $visit->save();
        return back()->with('info', 'Reprogramación éxitosa');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(VisitStoreRequest $request)
    {
        try {
            $request->merge(['client_id' => auth()->user()->client->id]);
            $agreement = Agreement::where('id', $request->client_agreement_id)->get()->first();
            $vencimiento = Carbon::now();
            $client_agreements = ClientAgreement::create([
                'name' => $agreement->name,
                'status' => 'pendiente',
                'client_id' => auth()->user()->client->id,
                'agreement_id' => $agreement->id,
                'date_start' => Carbon::now(),
                'date_end' => $vencimiento->addDays(112),
                'percent_discount_week_2' => $agreement->percent_discount_week_2,
                'percent_discount_week_4' => $agreement->percent_discount_week_4,
                'percent_discount_week_6' => $agreement->percent_discount_week_6,
                'percent_discount_week_8' => $agreement->percent_discount_week_8,
                'percent_penalization' => $agreement->percent_penalization,
                'percent_negotiation' => $agreement->percent_negotiation,
                'percent_company' => $agreement->percent_company,
                'validity' => 1,
                'extended' => 0
            ]);
            $verifyAgree = VerifyAgreement::create(['client_agreement_id' => $client_agreements->id, 'token' => sha1(time())]);
            $request->merge(['client_agreement_id' => $client_agreements->id]);
            $visit = Visit::create($request->input());

            // Images
            if ($request->file('images')) {
                foreach ($request->file('images') as $photo) {
                    $path = Storage::disk('public')->put('img_product', $photo);
                    $filePath = public_path($path);
                    ImageManager::make($filePath)->resize(600, 600, function ($constraint) {
                        $constraint->aspectRatio();
                    })->save($filePath);
                    Image::create(['visit_id' => $visit->id, 'path' => $path]);
                }
            }
            return redirect()->route('client_products')->with('info', 'Visita solicitada con éxito');
        } catch (\Throwable $th) {
            return back()->with('error', 'ocurrio un error: ' . $th);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Visit  $visit
     * @return \Illuminate\Http\Response
     */
    public function show(Visit $visit)
    {
        $handlers = [];
        if (auth()->user()->role->id == \App\Role::ADMIN)
            $handlers = Handler::get();
        return view('admin.visits.show', compact('visit', 'handlers'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Visit  $visit
     * @return \Illuminate\Http\Response
     */
    public function edit(Visit $visit)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Visit  $visit
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Visit $visit)
    {
        //
    }

    public function regected(Visit $visit)
    {
        return view('admin.agreements.rechazar', compact('visit'));
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Visit  $visit
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Visit $visit)
    {
        $contrato = $visit->client_agreement;
        if ($visit->client_agreement->status == 'pendiente') {
            $visit->client_agreement->delete();
        }
        // enviar email
        Mail::to(trim($visit->client->user->email))->send(new AgreementRegetedEmail($visit, $request->razon));
        $visit->status = 'cerrado';
        $visit->save();
        return redirect()->route('visits.index')->with('info', 'Contrato Rechazado');
    }

    public function cotizar(Visit $visit)
    {
        $categories = Category::orderBy('name', 'ASC')->pluck('name', 'id');
        $tags       = Tag::orderBy('name', 'ASC')->get();
        return view('admin.visits.cotizar', compact('visit', 'categories', 'tags'));
    }
    public function generateCode($textAgrement, $number)
    {
        do {
            $codigo = $textAgrement . $number;
            $number++;
        } while (Product::where('code', $codigo)->get()->first());
        return $codigo;
    }
    public function cotizarStore(Request $request, ClientAgreement $agreement)
    { // method for save Ajax

        try {
            $lastYear = date("Y") - 2000;
            if ($agreement->name == 'premium') {
                $request->merge(['type' => 'EXCLUSIVE']);
                $request->merge(['code' => $this->generateCode('P' . $lastYear . '-' . $agreement->id . '/', $request->numero)]); // premiun
            } else {
                $request->merge(['code' => $this->generateCode('E' . $lastYear . '-' . $agreement->id . '/', $request->numero)]); // estandar
            }

            request()->validate([
                'name' => 'required',
                'category_id' => 'required',
                'normal_price' => 'required',
            ]);

            $arr = array('msg' => 'Ocurrio un error al cotizar este producto: ', 'status' => false);
            try {
                $image = Image::findOrFail($request->image_id); // guardar product en imagen $request->image_id
                unset($request['image_id']);
                unset($request['numero']);
                $data = $request->all();
                // \Log::info($data);
                $product = Product::create($data);
                $product->client_agreement_id = $agreement->id;
                $product->img = $image->path;
                $product->save();

                $image->product_id = $product->id;
                $image->save();
                // $product = false;
            } catch (\Throwable $th) {
                return Response()->json($arr);
            }
            // if($product){
            $arr = array('msg' => 'Articulo registrado, Nombre: ' . $request->name . ' Precio: ' . $request->normal_price, 'status' => true);
            // }
            return Response()->json($arr);
        } catch (\Throwable $th) {
            $arr = array('msg' => 'Debe llenar todo los datos', 'status' => false);
            return Response()->json($arr);
        }
    }
    public function addArticle(Request $request)
    {
        $request->validate(['images' => 'required|array|max:4048']);

        if ($request->file('images')) {
            foreach ($request->file('images') as $photo) {
                $path = Storage::disk('public')->put('img_product', $photo);
                $filePath = public_path($path);
                ImageManager::make($filePath)->resize(600, 600, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($filePath);
                Image::create(['visit_id' => $request->visit_id, 'path' => $path]);
            }
        }
        return back()->with('info', 'Articulo añadidos con éxito');
    }
    public function removeArticle(Request $request)
    {
        $image = Image::findOrFail($request->id);
        $image->delete();
        return back()->with('info', 'Articulo Removido con éxito');
    }
    public function removeImage(Image $image)
    {
        if ($image->product->img == $image->path) {
            return back()->with('info', 'Imagen esta de portada no se puede eliminar');
        }
        $image->delete();
        return back()->with('info', 'Imagen Removido con éxito del artículo');
    }

    public function makeVistado(Visit $visit)
    {
        // dd($visit);
        $visit->visitado = true;
        $visit->save();
        return back()->with('info', $visit->address . ' marcado como visitado');
    }
}
