<?php

namespace App\Http\Controllers;

use App\Contacto;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ContactoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $contactos = Contacto::paginate(20);
        return view('admin.contact.index', compact('contactos'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255'],
            'g-recaptcha-response' => 'required|captcha',
            'subject' => ['required', 'string', 'max:255'],
            'phone_number' => ['required'],
            'city' => ['nullable'],
            'description' => ['nullable'],

        ])->validate();
        $contacto = Contacto::create($request->all());
        return back()->with('info', "GRACIAS POR TÚ INTERÉS NOS PONDREMOS EN CONTACTO CONTIGO");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Contacto  $contacto
     * @return \Illuminate\Http\Response
     */
    public function show(Contacto $contacto)
    {
        return view('admin.contact.show', compact('contacto'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Contacto  $contacto
     * @return \Illuminate\Http\Response
     */
    public function destroy(Contacto $contacto)
    {
        if ($contacto) {
            $contacto->delete();
            return back()->with('info', "Contacto Eliminado");
        }
    }
}
