<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\Product;
use App\Category;
use App\Tag;
use App\Visit;
use App\ClientAgreement;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use App\Compra;

use App\Http\Requests\ProductStoreRequest;
use Intervention\Image\Facades\Image as ImageManager;
use App\Http\Requests\ProductUpdateRequest;
use App\Image;
use \App\Mail\SendMail;
use \App\Mail\EmailRegected;
use Illuminate\Support\Facades\Mail;
use Carbon\Carbon;
use Illuminate\Support\Facades\Session;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $name = $request->get('name');
        $published = Product::orderBy('id', 'DESC')->with('category', 'user', 'images')->where('status', 'PUBLICADO')->precio()
            ->name($name)->paginate(20);

        if (auth()->user()->role->id == \App\Role::HANDLER) { // $mis_productos y la de las q soy gestor
            $published = Product::orderBy('created_at', 'DESC')
                ->where('status', 'PUBLICADO')->name($name)->precio()
                ->whereIn('client_agreement_id', ClientAgreement::where('handler_id', auth()->user()->handler->id)->pluck('id'))
                ->orWhere('user_id', auth()->user()->id)->with('category', 'user', 'images')->paginate(20);
        }
        $pagelinks = array();
        if ($name) $pagelinks = array_add($pagelinks, 'name', $name);
        return view('admin.products.index', compact('published', 'name', 'pagelinks'));
    }
    public function outet(Request $request)
    {
        $name = $request->get('name');
        $published = Product::where('code', 'like', 'O%')->orderBy('id', 'DESC')->with('category', 'user', 'images')
            ->where('status', 'PUBLICADO')->name($name)->paginate(20);

        if (auth()->user()->role->id == \App\Role::HANDLER) { // $mis_productos y la de las q soy gestor
            $published = Product::where('code', 'like', 'O%')->orderBy('created_at', 'DESC')
                ->where('status', 'PUBLICADO')->name($name)
                ->orWhere('user_id', auth()->user()->id)->with('category', 'user', 'images')->paginate(20);
        }
        $pagelinks = array();
        if ($name) $pagelinks = array_add($pagelinks, 'name', $name);
        return view('admin.products.index', compact('published', 'name', 'pagelinks'));
    }
    public function pendientes(Request $request)
    {
        $name = $request->get('name');
        $pending = Product::orderBy('id', 'DESC')->with('category', 'user', 'images')->where('status', 'PENDIENTE')
            ->where('client_agreement_id', null)
            ->name($name)->paginate(20);

        $pagelinks = array();
        if ($name) $pagelinks = array_add($pagelinks, 'name', $name);
        return view('admin.products.pendientes', compact('pending', 'name', 'pagelinks'));
    }

    public function debaja(Request $request)
    {
        $name = $request->get('name');
        $low = Product::orderBy('id', 'DESC')->with('category', 'user', 'images')->whereIn('status', ['ELIMINADO', 'RECHAZADO', 'DE BAJA'])
            ->name($name)->paginate(20);

        if (auth()->user()->role->id == \App\Role::HANDLER) { // $mis_productos y la de las q soy gestor
            $low = Product::orderBy('created_at', 'DESC')->name($name)
                ->whereIn('client_agreement_id', ClientAgreement::where('handler_id', auth()->user()->handler->id)->pluck('id'))
                ->whereIn('status', ['ELIMINADO', 'RECHAZADO', 'DE BAJA'])->paginate(20);
        }
        $pagelinks = array();
        if ($name) $pagelinks = array_add($pagelinks, 'name', $name);
        return view('admin.products.debaja', compact('low', 'name', 'pagelinks'));
    }

    public function reservados(Request $request)
    {
        $name = $request->get('name');
        $reserved = Product::orderBy('id', 'DESC')->with('category', 'user', 'images')
            ->where('importance', '>', 0)
            ->where('status', 'RESERVADO')->name($name)->paginate(20);

        if (auth()->user()->role->id == \App\Role::HANDLER) { // $mis_productos y la de las q soy gestor
            $reserved = Product::orderBy('created_at', 'DESC')->name($name)
                ->whereIn('client_agreement_id', ClientAgreement::where('handler_id', auth()->user()->handler->id)->pluck('id'))
                ->where('importance', '>', 0)
                ->where('status', 'RESERVADO')->paginate(20);
        }
        $pagelinks = array();
        if ($name) $pagelinks = array_add($pagelinks, 'name', $name);
        return view('admin.products.reservados', compact('reserved', 'name', 'pagelinks'));
    }

    public function reservadosBasicos(Request $request)
    {
        $name = $request->get('name');
        $reserved = Product::orderBy('id', 'DESC')->with('category', 'user', 'images')
            ->where('status', 'RESERVADO')
            ->where('importance', '=', 0)
            ->name($name)->paginate(20);
        $pagelinks = array();
        if ($name) $pagelinks = array_add($pagelinks, 'name', $name);
        return view('admin.products.reservadosBasico', compact('reserved', 'name', 'pagelinks'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $descuento = true;
        $beliani = false;
        $categories = Category::orderBy('name', 'ASC')->pluck('name', 'id');
        $tags       = Tag::orderBy('name', 'ASC')->get();

        return view('admin.products.create', compact('categories', 'tags', 'descuento', 'beliani'));
    }
    public function createSinDecuento()
    {
        $descuento = false;
        $beliani = false;
        $categories = Category::orderBy('name', 'ASC')->pluck('name', 'id');
        $tags       = Tag::orderBy('name', 'ASC')->get();

        return view('admin.products.create', compact('categories', 'tags', 'descuento', 'beliani'));
    }

    public function createBeliani()
    {
        $descuento = false;
        $beliani = true;
        $categories = Category::orderBy('name', 'ASC')->pluck('name', 'id');
        $tags       = Tag::orderBy('name', 'ASC')->get();

        return view('admin.products.create', compact('categories', 'tags', 'descuento', 'beliani'));
    }

    public function toBeliani(Product $product)
    {
        $this->authorize('pass', $product); // method for protection
        $product->beliani = true;
        $product->reduced_price = 0;
        // dd($product);
        $product->save();
        return redirect()->route('products.show', $product)->with('info', 'Producto convertido a beliani con éxito');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductStoreRequest $request)
    {
        $product = Product::create($request->all());
        //IMAGES
        if ($request->file('images')) {
            foreach ($request->file('images') as $photo) {
                $path = Storage::disk('public')->put('img_product', $photo);
                $filePath = public_path($path);
                ImageManager::make($filePath)->resize(600, 600, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($filePath);
                Image::create([
                    'product_id' => $product->id,
                    'path' => $path
                ]);
                $product->img = $path;
            }
            $product->save();
        }
        //TAGS
        $product->tags()->attach($request->get('tags'));
        if (auth()->user()->role->id == \App\Role::CLIENT) {
            return back()->with('info', 'Producto Registrado correctamente, se le enviara un Email cuando su producto sea Publicado');
        } else {
            return redirect()->route('products.show', $product)->with('info', 'Producto creado con éxito');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product) //$product
    {
        if (auth()->user()->role->id == \App\Role::ADMIN) {
            return view('admin.products.show', compact('product'));
        } else {
            // $this->authorize('pass', $product); // method for protection
            return view('admin.products.show', compact('product'));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        $this->authorize('pass', $product); // method for protection
        $categories = Category::orderBy('name', 'ASC')->pluck('name', 'id');
        $tags       = Tag::orderBy('name', 'ASC')->get();

        return view('admin.products.edit', compact('product', 'categories', 'tags'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(ProductUpdateRequest $request, Product $product)
    {

        $this->authorize('pass', $product); // method for protection
        $user_current = $product->user;
        $product->fill($request->all())->save(); // actualizamos el product

        //IMAGES
        if ($request->file('images')) {
            foreach ($request->file('images') as $photo) {
                $path = Storage::disk('public')->put('img_product', $photo);
                $filePath = public_path($path);
                ImageManager::make($filePath)->resize(600, 600, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($filePath);
                Image::create(['product_id' => $product->id, 'path' => $path]);
            }
        }
        //TAGS
        $product->tags()->sync($request->get('tags')); //update related with tags
        $product->user_id = $user_current->id;
        $product->save();
        return redirect()->route('products.show', $product)->with('info', 'Artículo Actualizado con éxito');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        $this->authorize('pass', $product);
        if ($product->status == 'RESERVADO') {
            return back()->with('error', 'Este Artículo ya se encuentra Reservado con Importe ' . $product->payment . ' Euros');
        }
        if ($product->status == 'VENDIDO' && $product->client_agreement) {
            return back()->with('error', 'Este Artículo ya se encuentra Vendido con Importe ' . $product->payment . ' Euros');
        }
        if ($product->compra) { # si existe compra
            $solicitudes = Compra::where('product_id', $product->id)->where('status', '=', 'solicitado')->get();
            foreach ($solicitudes as $solicitud) {
                $solicitud->delete();
            }
        }
        $product->status = 'DE BAJA';
        $product->variant = 'ELIMINADO';
        $product->save();
        return back()->with('info', 'El Artículo fue dado de baja de la web.');
    }
    public function regected(Product $product)
    {
        $this->authorize('pass', $product); // method for protection
        return view('admin.products.rechazar', compact('product'));
    }

    public function rechazar(Request $request, Product $product)
    {
        $this->authorize('pass', $product);
        Mail::to(trim($product->user->email))->send(new EmailRegected($product, $request->razon));
        // return back()->with('info', 'Se envio un Email al cliente y el Artículo fue Rechazado');
        return redirect()->route('products.pendientes')->with('info', 'Se envio un Email al cliente y el Artículo fue Rechazado');
    }
    public function img_default(Request $request, Product $product)
    {
        try {
            $product->img = $request->img;
            $product->save();
            $arr = array('msg' => 'Imagen seleccionada para Portada', 'status' => true);
        } catch (\Throwable $th) {
            $arr = array('msg' => 'Ocurrio un error', 'status' => false);
        }
        return Response()->json($arr);
    }
    public function assign_portada(Request $request, Product $product)
    {
        try {
            $imageFind = Image::find($request->img);
            $auxPath = $imageFind->path;
            $product->img = $imageFind->path;
            $primero = Image::find($product->images()->orderBy('id')->first()->id);
            $imageFind->path = $primero->path;
            $primero->path = $auxPath;
            $imageFind->save();
            $primero->save();
            $product->save();
            // dd($primero->id, $primero->path);
        } catch (\Throwable $th) {
            return back()->with('error', 'Ocurrio un error');
        }
        return back()->with('info', 'Se Asignó la imagen de portada');
    }

    public function more_images(Request $request, Product $product) // metodo para adicionar mas imagenes
    {
        $request->validate(['images' => 'sometimes|array']);
        $this->authorize('pass', $product);
        try {
            if ($request->file('images')) {
                foreach ($request->file('images') as $photo) {
                    $path = Storage::disk('public')->put('img_product', $photo);
                    $filePath = public_path($path);
                    ImageManager::make($filePath)->resize(600, 600, function ($constraint) {
                        $constraint->aspectRatio();
                    })->save($filePath);
                    Image::create(['product_id' => $product->id, 'path' => $path]);
                }
            }
            return back()->with('info', 'Se Adjunto las Fotografias al articulo');
        } catch (\Throwable $th) {
            return back()->with('error', 'Ocurrio un error al Adjuntar las Fotografias al articulo');
        }
    }

    public function qr_products(Product $product)
    {
        $this->authorize('pass', $product);
        return view('admin.products.qr_product', compact('product'));
    }

    public function publicar(Product $product)
    {
        $this->authorize('pass', $product);
        $product->status = 'PUBLICADO';
        $product->created_at = Carbon::today()->toDateTimeString();
        $product->save();
        Mail::to(trim($product->user->email))->send(new SendMail($product));
        return back()->with('info', 'Artículo Públicado Correctamente');
    }

    public function reservar(Request $request, Product $product)
    {
        // dd($request->get('payment') ? $request->get('payment') : 0, $request);
        if ($product->status == 'RESERVADO') {
            return back()->with('error', 'Este Artículo ya se encuentra Reservado con Importe ' . $product->payment . ' Euros');
        } else {
            Validator::make($request->all(), [
                'have_reserva' => ['required'],
                'monto_reserva' => ['required', 'numeric', 'min:1'],
            ])->validate();
            $precio = $product->reduced_price ? $product->reduced_price : $product->normal_price;
            if ($request->monto_reserva >= $precio) {
                // dd($request->request, $request->monto_reserva >= ($product->reduced_price ? $product->reduced_price : $product->normal_price));
                return back()->with('error', 'La Reserva no puede ser mayor a ' . $precio . ' Euros,  considere hacer una venta directa.');
            }
            // dd($request->request);
            if ($product->compra) { # si existe compra
                $solicitudes = Compra::where('product_id', $product->id)->where('status', '=', 'solicitado')->where('id', '!=', $product->compra->id)->get();
                foreach ($solicitudes as $solicitud) {
                    $solicitud->delete();
                }
                $product->compra->status = 'reservado';
                $product->compra->pago = $request->get('payment') ? $request->get('payment') : 0;
                $product->compra->have_pago = $request->get('have_pago') ? $request->get('have_pago') : 'GESTOR';
                $product->compra->monto_reserva = $request->get('monto_reserva') ? $request->get('monto_reserva') : 0;
                $product->compra->have_reserva = $request->get('have_reserva') ? $request->get('have_reserva') : 'GESTOR';
                $product->compra->fecha_reserva = Carbon::now();
                $product->compra->save();
            } else { # sino existe compra
                $comprador = null;
                if (auth()->user()) {
                    $comprador = auth()->user();
                }
                $vendedor = $product->user;
                if ($product->client_agreement) {
                    if (auth()->user()->role->id == \App\Role::ADMIN) {
                        $vendedor = auth()->user();
                    } else {
                        $vendedor = $product->client_agreement->handler->user;
                    }
                }
                $compra = Compra::create([
                    'product_id' => $product->id,
                    'vendedor' => $vendedor->id,         // vendedor
                    'comprador' => $comprador->id,         // vendedor
                    'nameClient' => $comprador->name, //comprador
                    'email' => $comprador->email,
                    'phone' => $comprador->profile->phone_number,
                    'city' => $comprador->profile->code_postal,
                    'comments' => 'estoy interesado en el articulo',
                    'monto_reserva' => $request->get('monto_reserva') ? $request->get('monto_reserva') : 0,
                    'have_reserva' => $request->get('have_reserva') ? $request->get('have_reserva') : 'GESTOR',
                    'have_pago' => $request->get('have_pago') ? $request->get('have_pago') : 'GESTOR',
                    'pago' => $request->get('monto') ? $request->get('monto') : 0,
                    'fecha_reserva' => Carbon::now(),
                    'status' => 'reservado',
                ]);
            }
            $product->payment = $request->get('monto_reserva') ? $request->get('monto_reserva') : 0;
            $product->status = 'RESERVADO';
            $product->save();
            return back()->with('info', 'Artículo Reservado Correctamente con Importe ' . $request->get('payment') . ' Euros');
        }
    }

    public function detenerReserva(Request $request, Product $product)
    {
        if ($product->status == 'RESERVADO') {
            if ($product->compra) { # si existe compra
                $solicitudes = Compra::where('product_id', $product->id)->where('status', '=', 'solicitado')->get(); # ->where('id','!=',$product->compra->id)->get();
                foreach ($solicitudes as $solicitud) {
                    $solicitud->delete();
                }
                $product->compra->delete();
            }
            $product->payment = 0;
            if ($product->client_agreement && $product->client_agreement->date_end < Carbon::now()) {
                $product->status = 'CONCLUIDO';
                $product->save();
                return back()->with('info', 'El Artículo fue concluido, ya que el contrato  expiró');
            }
            $product->status = 'PUBLICADO';
            $product->save();
            return back()->with('info', 'Artículo Públicado Nuevamente');
        } else {
            return back()->with('info', 'El presente articulo no esta Reservado');
        }
    }
    public function concluirArticulo(Request $request, Product $product)
    {
        if ($product->status == 'PUBLICADO' && $product->client_agreement) {
            $product->status = 'CONCLUIDO';
            $product->save();
            return back()->with('info', 'El Artículo del  contratro fue concluido');
        } else {
            return back()->with('info', 'El Artículo no pertenece a un contrato');
        }
    }
    public function venderConcluido(Request $request, Product $product)
    {
        Session::put('temp_product', $product);
        return redirect()->route('compras.create', $product);
    }
}
