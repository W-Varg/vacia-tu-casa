<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\Client;
use App\ClientAgreement;
use Illuminate\Http\Request;
use App\User, App\Handler;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $name = $request->get('name');
        $title = 'Listado de clientes';
        $pagelinks = array();
        if ($name) $pagelinks = array_add($pagelinks, 'name', $name);
        $add_gestor = false;
        $withAgrement = [];
        $outAgrement = [];
        if (auth()->user()->role->id == \App\Role::ADMIN) {
            $client_ids = ClientAgreement::pluck('client_id');
            // dd($client_ids);
            $withAgrement = User::orderBy('id', 'DESC')->where('deleted', false)
                ->name($name)->where('id', '<>', auth()->user()->id)
                ->whereIn('id', Client::whereIn('id', $client_ids)->pluck('user_id'))->paginate(20);

            $outAgrement = User::orderBy('id', 'DESC')->where('deleted', false)
                ->name($name)->where('id', '<>', auth()->user()->id)
                ->whereIn('id', Client::whereNotIn('id', $client_ids)->pluck('user_id'))->paginate(20);

            return view('admin.users.index', compact('withAgrement', 'outAgrement', 'name', 'add_gestor', 'title', 'pagelinks'));
        } else {
            $clients = auth()->user()->handler->clients;
            $withAgrement = User::where('deleted', false)->whereIn('id', $clients->pluck('user_id'))->where('id', '<>', auth()->user()->id)->paginate(20);
            return view('admin.users.index', compact('withAgrement', 'outAgrement', 'name', 'add_gestor', 'title', 'pagelinks'));
        }
    }

    public function contrato(Request $request)
    {
        $name = $request->get('name');
        $pagelinks = array();
        if ($name) $pagelinks = array_add($pagelinks, 'name', $name);
        $title = 'Lista de clientes con contrato';
        $add_gestor = false;
        $withAgrement = [];
        $outAgrement = [];

        $client_ids = ClientAgreement::pluck('client_id');

        $withAgrement = User::orderBy('id', 'DESC')->where('deleted', false)
            ->name($name)->where('id', '<>', auth()->user()->id)
            ->whereIn('id', Client::whereIn('id', $client_ids)->pluck('user_id'))->paginate(20);

        return view('admin.users.index', compact(
            'withAgrement',
            'outAgrement',
            'add_gestor',
            'title',
            'name',
            'pagelinks'
        ));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function show(Client $client)
    {
        return view('admin.users.profile');
        //
    }
}
