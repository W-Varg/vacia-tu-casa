<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\Category;
use Illuminate\Http\Request;
use App\Http\Requests\CategoryStoreRequest;
use App\Http\Requests\CategoryUpdateRequest;
use Intervention\Image\Facades\Image;
class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::orderBy('id', 'DESC')->withCount('products')->paginate();
        return view('admin.category.index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoryStoreRequest $request)
    {
        if ($request->hasFile('icon')) {
            $icon = $request->file('icon');
            $filename = time() . "." . $icon->getClientOriginalExtension();
            Image::make($icon)->resize(100, 100)->save(public_path('adm/icons/' . $filename));
        }
        $category = Category::create($request->all());
        if ($filename) {
            $category->icon = $filename;
            $category->save();
        }

        return redirect()->route('categories.edit', $category)->with('info', 'Categoria creada con éxito');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        return view('admin.category.show', compact('category'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        return view('admin.category.edit', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $category = Category::find($id);
        $request->validate([
            'name' => 'required|string',
            'icon' => 'sometimes|image|max:2000',
        ]);
        $category->fill($request->all())->save();
        if ($request->hasFile('icon')) {
            $icon = $request->file('icon');
            $filename = time() . "." . $icon->getClientOriginalExtension();
            Image::make($icon)->resize(100, 100)->save(public_path('adm/icons/' . $filename));
            $category->icon = $filename;
            $category->save();
        }
        $categories = Category::orderBy('id', 'DESC')->paginate();
        return redirect()->route('categories.index', compact('categories'))->with('info', 'Categoria Actualizada con éxito');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        $category->delete();
        return back()->with('info', 'Categoria eliminada correctamente');
    }
}
