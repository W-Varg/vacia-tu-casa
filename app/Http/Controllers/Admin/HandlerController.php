<?php

namespace App\Http\Controllers\Admin;

use App\ClientAgreement;
use App\Http\Controllers\Controller;

use App\Handler;
use Illuminate\Http\Request;
use App\User, App\Profile;
use App\Http\Requests\HandlerStoreRequest;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image as ImageManager;

class HandlerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $name = $request->get('name');
        $title = 'Lista de Gestores';
        $add_gestor = false;
        $pagelinks = array();
        if ($name) $pagelinks = array_add($pagelinks, 'name', $name);
        $withAgrement = [];
        $outAgrement = [];

        if (auth()->user()->role->id == \App\Role::ADMIN) {
            $handler_ids = ClientAgreement::pluck('handler_id');

            $users = User::orderBy('id', 'DESC')->where('deleted', false)->whereIn('id', Handler::pluck('user_id'))->paginate();
            $add_gestor = true;
            $withAgrement = User::orderBy('id', 'DESC')->where('deleted', false)->where('id', '<>', auth()->user()->id)
                ->whereIn('id', Handler::whereIn('id', $handler_ids)->pluck('user_id'))->paginate(20);

            $outAgrement = User::orderBy('id', 'DESC')->where('deleted', false)->where('id', '<>', auth()->user()->id)
                ->whereIn('id', Handler::whereNotIn('id', $handler_ids)->pluck('user_id'))->paginate(20);

            return view('admin.users.index', compact('withAgrement', 'outAgrement', 'name', 'add_gestor', 'title', 'pagelinks'));
        } else {
            $withAgrement = User::where('deleted', false)->name($name)->where('id', auth()->user()->id)->paginate(20);
            return view('admin.users.index', compact('users', 'name', 'add_gestor', 'title', 'pagelinks'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(HandlerStoreRequest $request)
    {
        // dd($request);
        try {
            $user = User::create([
                'name' => $request->name,
                'email' => $request->email,
                'password' => Hash::make($request->password),
                'role_id' => \App\Role::HANDLER
            ]);
            $ImagePath = 'adm/images/avatar/user_profile.png';
            if ($request->hasFile('avatar')) {
                $avatar = $request->file('avatar');
                $ImagePath = Storage::disk('public')->put('adm/images/avatar', $avatar);
                $filePath = public_path($ImagePath);
                ImageManager::make($filePath)->resize(300, 300, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($filePath);
            }
            Profile::create([
                'user_id' => $user->id,
                'last_name' => $request->last_name,
                'code_postal' => $request->code_postal,
                'dni' => $request->dni,
                'avatar' => $ImagePath, //
                'phone_number' => $request->countryCode . $request->phone_number,
            ]);
            Handler::create(['user_id' => $user->id]);
            dd($request, $request->countryCode . $request->phone_number);
            return redirect()->route('profile', $user)->with('info', 'Gestor Creado con éxito');
        } catch (\Throwable $th) {
            return back()->with('error', 'Gestor no Creado, ocurrio un error');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Handler  $handler
     * @return \Illuminate\Http\Response
     */
    public function show(Handler $handler)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Handler  $handler
     * @return \Illuminate\Http\Response
     */
    public function edit(Handler $handler)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Handler  $handler
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $request->validate([
            'name' => 'required|string',
            'last_name' => 'required|string',
            'code_postal' => 'required',
            'phone_number' => 'required',
            'avatar' => 'sometimes|image|max:2048',
        ]);

        // user
        $user->name = $request->name;
        $profile = $user->profile;
        $profile->last_name = $request->last_name;
        $profile->code_postal = $request->code_postal;
        $profile->phone_number = $request->countryCode . $request->phone_number;
        $profile->dni = $request->dni;
        // avatar; no funciona aun
        if ($request->hasFile('avatar')) {
            $avatar = $request->file('avatar');
            $ImagePath = Storage::disk('public')->put('adm/images/avatar', $avatar);
            $filePath = public_path($ImagePath);
            ImageManager::make($filePath)->resize(300, 300, function ($constraint) {
                $constraint->aspectRatio();
            })->save($filePath);
            $profile->avatar = $ImagePath; //
        }
        $user->save();
        $profile->save();
        return back()->with('info', 'Usuario Actualizado correctamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Handler  $handler
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        // elimnar compras
        // eliminar comments
        // eliminar->profile
        // // eliminar products
        //eliminar handlres
        // $user->
        $user->delete();
        return back()->with('info', 'Usuario Eliminado correctamente');
    }
}
