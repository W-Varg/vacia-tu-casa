<?php

namespace App\Http\Controllers\Admin;

use App\Client;
use App\ClientAgreement;
use App\Handler;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Role;

use App\User, App\Product;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $name = $request->get('name');
        $title = 'Listado de usuarios';
        $add_gestor = false;
        $pagelinks = array();
        $withAgrement = [];
        $outAgrement = [];

        if ($name) $pagelinks = array_add($pagelinks, 'name', $name);

        if (auth()->user()->role->id == \App\Role::ADMIN) {
            $client_ids = ClientAgreement::pluck('client_id');

            // $users = User::orderBy('id', 'DESC')->where('deleted', false)->name($name)
            //     ->whereIn('id', Handler::pluck('user_id'))->paginate();

            $withAgrement = User::orderBy('id', 'DESC')->where('deleted', false)
                ->name($name)->where('id', '<>', auth()->user()->id)
                ->whereIn('id', Client::whereIn('id', $client_ids)->pluck('user_id'))->paginate(20);

            $outAgrement = User::orderBy('id', 'DESC')->where('deleted', false)
                ->name($name)->where('id', '<>', auth()->user()->id)
                ->whereIn('id', Client::whereNotIn('id', $client_ids)->pluck('user_id'))->paginate(20);

            $add_gestor = true;
            return view('admin.users.index', compact('withAgrement', 'outAgrement', 'name', 'title', 'add_gestor', 'pagelinks'));
        } else {

            $clients = auth()->user()->handler->clients;
            $withAgrement = User::where('deleted', false)->name($name)->whereIn('id', $clients->pluck('user_id'))->where('id', '<>', auth()->user()->id)->paginate(20);

            return view('admin.users.index', compact('withAgrement', 'outAgrement', 'name', 'title', 'add_gestor', 'pagelinks'));
        }
    }

    public function show(User $user) //$user
    {
        //$this->authorize('pass', $user); // method for protection
        $products = Product::orderBy('name', 'ASC')->get();
        return view('admin.users.show', compact('user', 'products'));
    }

    public function edit(User $user)
    {
        return view('admin.users.edit', compact('user'));
    }

    public function update(Request $request, User $user)
    {
        $user->update($request->all()); // actualizamos el user
        //$user->roles->sync($request->roles);
        return redirect()->route('admin.users.edit', $user)->with('info', 'Usuario Actualizado con éxito');
    }

    public function destroy(User $user)
    {
        $user->delete();
        return back()->with('info', 'Usuario eliminado correctamente');
    }
    public function disabled(User $user)
    {
        $user->deleted = true;
        $user->save();
        // dd($user);
        return redirect()->route('clients.index')->with('info', 'Usuario eliminado logicamente');
    }
}
