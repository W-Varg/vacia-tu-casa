<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Caffeinated\Shinobi\Models\Role;
use Caffeinated\Shinobi\Models\Permission;

class RoleController extends Controller
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */

    public function index()
    {
        $roles = Role::paginate();
        return view('admin.roles.index', compact('roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $permisos = Permission::get();
        return view('admin.roles.create', compact('permisos'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */    
    public function store(Request $request)
    {
        // $this->authorize('pass', $role); // method for protection
        $role = Role::create($request->all()); // creamos el rol
        
        $role->permissions()->sync($request->get('permisos')); //update the permission
        return redirect()->route('roles.edit', $role)->with('info', 'Rol Creado con éxito');
    }


    public function show(Role $role) //$role
    {
        $this->authorize('pass', $role); // method for protection
        return view('admin.roles.show', compact('role'));
    }

    public function edit(Role $role)
    {
        $this->authorize('pass', $role); // method for protection
        $permisos = Permission::get();
        return view('admin.roles.edit', compact('role','permisos'));
    }

    public function update(Request $request,Role $role)
    {
        $this->authorize('pass', $role); // method for protection
        $role->update($request->all()); // actualizamos el user
        
        $role->permissions()->sync($request->get('permisos')); //update the permission
        return redirect()->route('roles.edit', $role)->with('info', 'Rol Actualizado con éxito');
    }

    public function destroy(Role $role)
    {
        $this->authorize('pass', $role);
        $role->delete();
        return back()->with('info', 'Rol eliminado correctamente');
    }
}
