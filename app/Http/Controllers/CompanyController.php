<?php

namespace App\Http\Controllers;

use App\Company;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Redirect, Response, File;
class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $company = \App\Company::get()->first();
        return view('admin.company.show', compact('company'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function show(Company $company)
    {
        return view('admin.company.show', compact('company'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function edit(Company $company)
    {
        return view('admin.company.edit', compact('company'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Company $company)
    {
        // Validator::make($request->all(), [
        //     'pdf' => [],
        //     'img' => [],
        // ])->validate();

        $request->validate([
            'company_name' => 'required|string',
            'email' => 'required|string',
            // 'phone_number' => 'required|string',
            'img1' => 'sometimes|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'img2' => 'sometimes|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'img3' => 'sometimes|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'img4' => 'sometimes|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        $destinationPath = public_path('adm/banners/'); // upload path
        if ($file1 = $request->file('img1')) {
            $profileImage = date('YmdHis') . rand(5, 15) . "." . $file1->getClientOriginalExtension();
            $file1->move($destinationPath, $profileImage);
            $request->img1 = "adm/banners/$profileImage";
        }
        if ($file2 = $request->file('img2')) {
            $profileImage = date('YmdHis') . rand(5, 10) . "." . $file2->getClientOriginalExtension();
            $file2->move($destinationPath, $profileImage);
            $request->img2 = "adm/banners/$profileImage";
        }
        if ($file3 = $request->file('img3')) {
            $profileImage = date('YmdHis') . rand(5, 14) . "." . $file3->getClientOriginalExtension();
            $file3->move($destinationPath, $profileImage);
            $request->img3 = "adm/banners/$profileImage";
        }
        if ($file4 = $request->file('img4')) {
            $profileImage = date('YmdHis') . rand(5, 15) . "." . $file4->getClientOriginalExtension();
            $file4->move($destinationPath, $profileImage);
            $request->img4 = "adm/banners/$profileImage";
        }
        $company->company_name = $request->company_name;
        $company->email = $request->email;
        $company->phone_number = $request->phone_number;
        $company->facebook = $request->facebook;
        $company->instagram = $request->instagram;
        $company->code_postal = $request->code_postal;
        $company->twitter = $request->twitter;
        $company->pinterest = $request->pinterest;
        $company->address = $request->address;
        $company->texto1 = $request->texto1;
        $company->texto2 = $request->texto2;
        $company->texto3 = $request->texto3;
        if ($request->img1) {
            $company->img1 = $request->img1;
        }
        if ($request->img2) {
            $company->img2 = $request->img2;
        }
        if ($request->img3) {
            $company->img3 = $request->img3;
        }
        if ($request->img4) {
            $company->img4 = $request->img4;
        }
        $company->save();
        return redirect()->route('company.show', $company)->with('info', 'Informacion actualizada correctamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function destroy(Company $company)
    {
        //
    }
}
