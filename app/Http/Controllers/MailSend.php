<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Mail\RegisterEmail;
use \App\Mail\RegisterEmail2;
use App\ClientAgreement;
use App\Mail\ContratoEndEmail;
use App\User;
use App\Utilidad;
use Illuminate\Support\Facades\Mail;

class MailSend extends Controller
{
    public function mailsend()
    {
        $details = [
            'title' => 'Email de prueba',
            'body' => 'Te agradecemos por confiar en nosotros Vacía Tu Casa'
        ];

        Mail::to('kryshot05@gmail.com')->send(new RegisterEmail($details));
        // Mail::to('kryshot05@gmail.com')->send(new ContratoEndEmail(new User(),new ClientAgreement(),new Utilidad()));
        return view('emails.thanks');
    }

    public function welcome()
    {
        if (auth()->user()) {
            $details = [
                'title' => 'Bienvenido a Vacia tu Casa',
                'body' => 'Te agradecemos por la confianza en nosotros.'
            ];
            try {
                Mail::to(trim(auth()->user()->email))->send(new RegisterEmail($details));
                // Mail::to(auth()->user()->email)->send(new RegisterEmail2($details)); // email for confirmar
            } catch (\Throwable $th) {
                //throw $th;
            }
        }
        return view('emails.thanks');
    }
}
