<?php

namespace App\Http\Controllers;

use App\ClientAgreement;
use App\Product;
use App\Handler;
use App\User, App\Profile;
use App\Utilidad;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ReportController extends Controller
{
    //
    public function vendidos(Request $request)
    {
        $total = 0;
        $start = $request->start ? $request->start : new Carbon('first day of this month');
        $end = $request->end ? new Carbon($request->end) : Carbon::now();

        $products = Product::orderBy('updated_at', 'DESC')->where('status', 'VENDIDO')
            ->whereHas('compra', function ($query) use ($start, $end) {
                return $query->whereBetween('updated_at', [$start, $end->addDays(1)]);
            })->get();

        // if (auth()->user()->role->id == \App\Role::HANDLER) { // $mis_productos y la de las q soy gestor
        //     $products = Product::orderBy('updated_at', 'DESC')
        //         ->where('status', 'VENDIDO')
        //         ->whereHas('compra', function ($query) use ($start, $end) {
        //             return $query->whereBetween('updated_at', [$start, $end->addDays(1)]);
        //         })
        //         ->whereIn('client_agreement_id', ClientAgreement::where('handler_id', auth()->user()->handler->id)->pluck('id'))
        //         ->orWhere('user_id', auth()->user()->id)->with('category', 'user', 'images')->get();
        // }

        foreach ($products as $p) {
            if ($p->compra) {
                $total =  $total + $p->compra->pago;
            }
        }
        $end->subDays(1);
        $basicos  = $products->filter(function ($p) {
            // dd($p);
            if ($p->importance === 0 && $p->beliani == false) {
                return $p;
            }
        });
        $beliani  = $products->filter(function ($p) {
            // dd($p);
            if ($p->importance === 1) {
                return $p;
            }
        });
        $estandar  = $products->filter(function ($p) {
            // dd($p);
            if ($p->importance === 2) {
                return $p;
            }
        });

        $premium  = $products->filter(function ($p) {
            // dd($p);
            if ($p->importance === 3) {
                return $p;
            }
        });
        return view(
            'reportes.report_vendidos',
            compact('start', 'end', 'products', 'estandar', 'premium', 'beliani', 'basicos', 'total')
        );
    }

    public function no_vendidos(Request $request)
    {
        $start = $request->start ? $request->start : new Carbon('first day of this month');
        $end = $request->end ? $request->end : Carbon::now();
        $products = Product::whereIn('status', ['PUBLICADO', 'PENDIENTE', 'RESERVADO'])->whereBetween('updated_at', [$start, $end])->get();
        return view('reportes.no_vendidos', compact('start', 'end', 'products'));
    }
    public function reservados(Request $request)
    {
        $start = $request->start ? $request->start : new Carbon('first day of this month');
        $end = $request->end ? $request->end : Carbon::now();
        $products = Product::where('status', 'RESERVADO')->whereBetween('updated_at', [$start, $end])->get();

        if (auth()->user()->role->id == \App\Role::HANDLER) { // $mis_productos y la de las q soy gestor
            $products = Product::orderBy('created_at', 'DESC')
                ->where('status', 'RESERVADO')->whereBetween('updated_at', [$start, $end])
                ->whereIn('client_agreement_id', ClientAgreement::where('handler_id', auth()->user()->handler->id)->pluck('id'))
                ->orWhere('user_id', auth()->user()->id)->with('category', 'user', 'images')->get();
        }
        return view('reportes.reservados', compact('start', 'end', 'products'));
    }
    public function contratos_active(Request $request)
    {
        $start = $request->start ? $request->start : new Carbon('first day of this month');
        $end = $request->end ? $request->end : Carbon::now();
        if (auth()->user()->role->id == \App\Role::HANDLER) {
            $estandarts = ClientAgreement::whereIn('status', ['enviado', 'aprobado', 'pendiente', 'publicado'])
                ->where('name', '=', 'estandar')->where('handler_id', '=', auth()->user()->handler->id)
                ->whereBetween('created_at', [$start, $end])->get();
            $premiums = ClientAgreement::whereIn('status', ['enviado', 'aprobado', 'pendiente', 'publicado'])
                ->where('name', '=', 'premium')->where('handler_id', '=', auth()->user()->handler->id)
                ->whereBetween('created_at', [$start, $end])->get();
        } else {
            $estandarts = ClientAgreement::whereIn('status', ['enviado', 'aprobado', 'pendiente', 'publicado'])
                ->where('name', '=', 'estandar')->whereBetween('created_at', [$start, $end])->get();
            $premiums = ClientAgreement::whereIn('status', ['enviado', 'aprobado', 'pendiente', 'publicado'])
                ->where('name', '=', 'premium')->whereBetween('created_at', [$start, $end])->get();
        }
        // dd($premiums);
        return view('reportes.contratos_active', compact('start', 'end', 'estandarts', 'premiums'));
    }
    public function contratos_close(Request $request)
    {
        $start = $request->start ? $request->start : new Carbon('first day of this month');
        $end = $request->end ? $request->end : Carbon::now();
        if (auth()->user()->role->id == \App\Role::HANDLER) {
            $estandarts = ClientAgreement::whereIn('status', ['concluido', 'rechazado'])
                ->where('name', '=', 'estandar')->where('handler_id', '=', auth()->user()->handler->id)
                ->whereBetween('date_end', [$start, $end])->get();
            $premiums = ClientAgreement::whereIn('status', ['concluido', 'rechazado'])
                ->where('name', '=', 'premium')->where('handler_id', '=', auth()->user()->handler->id)
                ->whereBetween('date_end', [$start, $end])->get();
        } else {
            $estandarts = ClientAgreement::whereIn('status', ['concluido', 'rechazado'])
                ->where('name', '=', 'estandar')->whereBetween('date_end', [$start, $end])->get();
            $premiums = ClientAgreement::whereIn('status', ['concluido', 'rechazado'])
                ->where('name', '=', 'premium')->whereBetween('date_end', [$start, $end])->get();
        }
        return view('reportes.contratos_close', compact('start', 'end', 'estandarts', 'premiums'));
    }
    public function contrato_gestor(Request $request)
    {
        $gestores = User::orderBy('id', 'DESC')->whereIn('id', Handler::pluck('user_id'))->pluck('name', 'id');
        $start = $request->start ? $request->start : new Carbon('first day of this month');
        $end = $request->end ? $request->end : Carbon::now()->addDays(1);
        $utilidades = Utilidad::whereBetween('updated_at', [$start, $end])->orderBy('updated_at', 'DESC')->get();
        $user = [];
        if ($request->id) {
            $utilidades = Utilidad::whereBetween('updated_at', [$start, $end])->where('user_id', $request->id)->orderBy('updated_at', 'DESC')->get();
            $user = User::find($request->id);
        }
        return view('reportes.contratos_gestor', compact('start', 'end', 'utilidades', 'gestores', 'user'));
    }
}
