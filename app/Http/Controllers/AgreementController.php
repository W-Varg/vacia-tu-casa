<?php

namespace App\Http\Controllers;

use App\Agreement, App\ClientAgreement, App\VerifyAgreement;
use App\Compra;
use Illuminate\Http\Request;
use \App\Mail\ContratoEmail;
use \App\Mail\ContratoPublicadoEmail;
use Illuminate\Support\Facades\Mail;
use App\Exports\AgreementExport;
use App\Imports\AgreementImport;
use App\Product;
use Carbon\Carbon;
use App\Role, App\Handler;
use Maatwebsite\Excel\Facades\Excel;

class AgreementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $name = $request->get('name');
        $pagelinks = array();
        if ($name) $pagelinks = array_add($pagelinks, 'name', $name);

        if (auth()->user()->role->id == \App\Role::HANDLER) {
            $estandarts = ClientAgreement::orderBy('id', 'DESC')->where('name', '=', 'estandar')
                ->name($name)->where('handler_id', '=', auth()->user()->handler->id)->paginate(20);
            $premiums = ClientAgreement::orderBy('id', 'DESC')->where('name', '=', 'premium')
                ->name($name)->where('handler_id', '=', auth()->user()->handler->id)->paginate(20);
        } else {
            $estandarts = ClientAgreement::orderBy('id', 'DESC')->where('name', '=', 'estandar')
                ->name($name)->paginate(20);
            $premiums = ClientAgreement::orderBy('id', 'DESC')->where('name', '=', 'premium')
                ->name($name)->paginate(20);
        }
        return view('admin.agreements.index', compact('estandarts', 'premiums', 'name', 'pagelinks'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ClientAgreement  $agreement
     * @return \Illuminate\Http\Response
     */
    public function show(ClientAgreement $agreement)
    {
        // dd($agreement->visit);
        $total = 0;
        foreach ($agreement->products as $product) {
            $total = $total + $product->normal_price;
        }
        return view('admin.agreements.show', compact('agreement', 'total'));
    }
    public function changeAgreement(ClientAgreement $agreement)
    {

        $products = $agreement->products;
        $new_aggremen = ClientAgreement::create([
            'name' => 'estandar',
            'status' => $agreement->status,
            'verified' => $agreement->verified,
            'client_id' => $agreement->client_id,
            'agreement_id' => 1,
            'handler_id' => $agreement->handler_id,
            'date_start' => $agreement->date_start,
            'date_end' => $agreement->date_end,
            'percent_discount_week_2' => $agreement->percent_discount_week_2,
            'percent_discount_week_4' => $agreement->percent_discount_week_4,
            'percent_discount_week_6' => $agreement->percent_discount_week_6,
            'percent_discount_week_8' => $agreement->percent_discount_week_8,
            'percent_penalization' => $agreement->percent_penalization,
            'percent_negotiation' => $agreement->percent_negotiation,
            'percent_company' => $agreement->percent_company,
            'validity' => $agreement->validity,
            'created_at' => $agreement->created_at,
            'extended' => $agreement->extended
        ]);
        VerifyAgreement::create(['client_agreement_id' => $new_aggremen->id, 'token' => sha1(time())]);
        // codigo
        $lastYear = date("Y") - 2000;
        $numero = 1;
        if ($agreement->name == 'estandar') {
            $code = 'P000' . $new_aggremen->id;
            $new_aggremen->name = 'premium';
            $new_aggremen->agreement_id = 2;
            foreach ($products as $p) {
                if ($p->status == 'PUBLICADO' && !$p->compra) {
                    $p->client_agreement_id = $new_aggremen->id;
                    $p->code = $this->generateCode('P' . $lastYear . '-' . $new_aggremen->id . '/', $numero);
                    $p->importance = 3;
                    $p->save();
                    $numero++;
                }
            }
        } else {
            $code = 'E000' . $new_aggremen->id;
            foreach ($products as $p) {
                if ($p->status == 'PUBLICADO' && !$p->compra) {
                    $p->client_agreement_id = $new_aggremen->id;
                    $p->code = $this->generateCode('E' . $lastYear . '-' . $new_aggremen->id . '/', $numero);
                    $p->importance = 2;
                    $p->save();
                    $numero++;
                }
            }
        }
        $new_aggremen->save();
        $agreement = ClientAgreement::find($agreement->id);
        if ($agreement->products->count() == 0) {
            $agreement->delete();
        }
        return back()->with('info', "El Contrato fue cambiado a " . $new_aggremen->name . " y se generó el codigo " . $code);
    }

    public function generateCode($textAgrement, $number)
    {
        do {
            $codigo = $textAgrement . $number;
            $number++;
        } while (Product::where('code', $codigo)->get()->first());
        return $codigo;
    }


    public function concluirContrato(ClientAgreement $agreement)
    {
        $vendidos =  $agreement->products->filter(function ($p) {
            if ($p->compra && $p->compra->status == 'completado' && $p->status == 'VENDIDO') {
                return $p;
            }
        });
        $no_vendidos =  $agreement->products->filter(function ($p) {
            if ($p->status != 'VENDIDO')
                return $p;
        });
        foreach ($no_vendidos as $p) {
            if ($p->status == 'PUBLICADO') {
                $p->variant = 'CONCLUIDO';
                $p->status = 'CONCLUIDO';
                $p->save();
            }
        }
        $agreement->status = 'concluido';
        $agreement->date_end = Carbon::now();
        $agreement->save();
        return back()->with('info', "Este contrato fue concluido y tambien sus artículos.");
    }
    public function details(ClientAgreement $agreement)
    {
        $total_cliente = 0;
        $total_gestor = 0;
        $total = 0;
        // dd($agreement->products);
        $vendidos =  $agreement->products->filter(function ($p) {
            if ($p->compra && $p->compra->status == 'completado' && $p->status == 'VENDIDO') {
                return $p;
            }
        });
        $no_vendidos =  $agreement->products->filter(function ($p) {
            if ($p->status != 'VENDIDO')
                return $p;
        });
        foreach ($vendidos as $p) {
            // if ($p->compra->monto_reserva) {
            //     if ($p->compra->have_reserva == 'CLIENTE') {
            //         $total_cliente =  $total_cliente + $p->compra->monto_reserva;
            //     } else {
            //         $total_gestor =  $total_gestor + $p->compra->monto_reserva;
            //     }
            // }
            if ($p->compra->pago) {
                if ($p->compra->have_pago == 'CLIENTE') {
                    $total_cliente =  $total_cliente + $p->compra->pago;
                } else {
                    $total_gestor =  $total_gestor + $p->compra->pago;
                }
            }
        }
        $total = $total_cliente + $total_gestor;
        return view('admin.agreements.detail', compact('agreement', 'total', 'vendidos', 'no_vendidos', 'total_cliente', 'total_gestor'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ClientAgreement  $agreement
     * @return \Illuminate\Http\Response
     */
    public function edit(ClientAgreement $agreement)
    {
        $handlers = Handler::get();
        return view('admin.agreements.edit', compact('agreement', 'handlers'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ClientAgreement  $agreement
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ClientAgreement $agreement)
    {

        if (auth()->user()->role->id == \App\Role::ADMIN) {
            $agreement->update($request->all());
            return redirect()->route('agreements.index')->with('info', 'Contrato Actualizado con éxito');
        }
        return back()->with('error', "NO TIENE PERMITIDO MODIFICAR EL CONTRATO");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ClientAgreement  $agreement
     * @return \Illuminate\Http\Response
     */
    public function destroy(ClientAgreement $agreement)
    {
        // 'enviado', 'aprobado', 'rechazado','pendiente'
        if ($agreement->status == 'publicado' || $agreement->status == 'concluido') {
            return back()->with('info', "El contrato se encuentra " . $agreement->status . " no se puede eliminar");
        }
        $agreement->delete();
        return redirect()->route('agreements.index')->with('info', "El Contrato y sus productos fueron eliminados completamente");
    }
    public function verifyAgreement($token)
    {
        $verifyAgreement = VerifyAgreement::where('token', $token)->first();
        if (isset($verifyAgreement)) {
            $agreement = $verifyAgreement->agreement;
            if (!$agreement->verified) {
                $verifyAgreement->agreement->verified = true;
                $verifyAgreement->agreement->status = 'aprobado';
                if ($verifyAgreement->agreement->visit) {
                    $verifyAgreement->agreement->visit->contrato = true;
                    $verifyAgreement->agreement->visit->save();
                }
                $verifyAgreement->agreement->save();
                $status = "Acabas de aprobar el contrato (" . $verifyAgreement->agreement->name . ") muy pronto tus artículos estarán publicados. Haz el seguimiento en tu área de cliente.";
            } else {
                $status = "El contrato ya fue Formalizado, puedes contactarte con el gestor";
            }
        } else {
            return back()->with('info', "Lo sentimos no fue posible Formalizar el Contrato.");
        }
        return redirect()->route('page.index')->with('info', $status);
    }

    public function sendContratoToUser(ClientAgreement $agreement)
    {
        Mail::to(trim($agreement->client->user->email))->send(new ContratoEmail($agreement));
        $agreement->status = 'enviado';
        $agreement->verified = false;
        $agreement->save();
        return back()->with('info', 'Contrato enviado Correctamente');
    }
    public function resendContratoToUser(ClientAgreement $agreement)
    {
        Mail::to(trim($agreement->client->user->email))->send(new ContratoEmail($agreement));
        $agreement->status = 'enviado';
        $agreement->verified = false;
        $agreement->save();
        return back()->with('info', 'Contrato Reenviado Correctamente');
    }
    public function concluirAgreement(ClientAgreement $agreement)
    {
        $agreement->status = 'concluido';
        $agreement->save();
        return back()->with('info', 'Contrato concluido');
    }

    public function export(ClientAgreement $agreement)
    {
        return (new AgreementExport($agreement))->download('Contrato_N000' . $agreement->id . '.xlsx');
    }

    public function importAgre(Request $request, ClientAgreement $agreement)
    {
        try {
            Excel::import(new AgreementImport($agreement), request()->file('excel'));
            return back()->with('info', 'Contrato Cargado Correctamente Con los datos...!!!');
        } catch (\Throwable $th) {
            return back()->with('error', 'Ocurrio un Error al cargar el Contrato');
        }
    }
    public function publicar(ClientAgreement $agreement)
    {
        Mail::to(trim($agreement->handler->user->email))->send(new ContratoPublicadoEmail($agreement));
        Mail::to(trim($agreement->client->user->email))->send(new ContratoPublicadoEmail($agreement));
        return back()->with('info', 'Articulos de contrato Publicados Correctamente');
    }
    public function print_qr(ClientAgreement $agreement)
    {
        $products = Product::where('client_agreement_id', $agreement->id)->get();
        return view('admin.agreements.print_qr', compact('agreement', 'products'));
    }
    public function elminarArticulo(Product $product)
    {
        $this->authorize('pass', $product);
        if ($product->status == 'RESERVADO') {
            return back()->with('error', 'Este Artículo ya se encuentra Reservado con Importe ' . $product->payment . ' Euros');
        }
        if ($product->status == 'VENDIDO' && $product->client_agreement) {
            return back()->with('error', 'Este Artículo ya se encuentra Vendido con Importe ' . $product->payment . ' Euros');
        }
        if ($product->compra) { # si existe compra
            $solicitudes = Compra::where('product_id', $product->id)->where('status', '=', 'solicitado')->get();
            foreach ($solicitudes as $solicitud) {
                $solicitud->delete();
            }
        }
        $product->delete();
        return back()->with('info', 'El Artículo fue eliminado completamente del sistema');
    }
}
