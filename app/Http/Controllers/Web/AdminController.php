<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Client, App\Visit, App\Product, App\User;

class AdminController extends Controller
{
    public function index()
    {
        $clientes = Client::all()->count();
        $porVisitar = Visit::where('handler_id', null)->count();
        $publicados = Product::where('status', 'PUBLICADO')->count();

        if (auth()->user()->role->id == \App\Role::HANDLER) {
            $clientes = Client::all()->count();
            $porVisitar = Visit::where('handler_id', auth()->user()->handler->id)->where('contrato', false)->where('status', '<>', 'cerrado')->count();
            $publicados = Product::where('status', 'PUBLICADO')->count();
        }
        return view('admin', compact('clientes', 'porVisitar', 'publicados'));
    }

    public function profile(User $user)
    {
        $clients = [];
        $contratos = [];

        if ($user->handler) {
            $clients = $user->handler->clients;
            $clients = User::whereIn('id', $clients->pluck('user_id'))->where('id', '<>', auth()->user()->id)->get();
            // dd($user->profile);
            return view('admin.users.profile', compact('user', 'clients', 'contratos'));
        }
        $contratos = $user->client->client_agreement;

        return view('admin.users.profile', compact('user', 'clients', 'contratos'));
    }
}
