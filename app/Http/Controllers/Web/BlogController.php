<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use PDF;
use App\Product, App\Category, App\Tag, App\User, App\Agreement, App\ClientAgreement, App\Compra, App\Image, App\VerifyAgreement;;
use App\Http\Requests\ArticuloUpdate;
use App\Http\Requests\ProductStoreRequest;
use Carbon\Carbon;
use App\Http\Controllers\AgreementController;

class BlogController extends Controller
{
    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return View
     */
    public function index()
    {
        $user = auth()->user();
        return view('client.blog',compact('user'));
    }
}