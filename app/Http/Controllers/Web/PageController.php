<?php

namespace App\Http\Controllers\Web;

use App\Company;
use App\Exports\ConcluidoExcel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use PDF;
use App\Product, App\Category, App\Tag, App\User, App\Agreement, App\ClientAgreement, App\Compra, App\Image, App\VerifyAgreement;

use App\Http\Requests\ArticuloUpdate;
use App\Http\Requests\ProductStoreRequest;
use Carbon\Carbon;
use App\Http\Controllers\AgreementController;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image as ImageManager;

class PageController extends Controller
{
    public function to_buy()
    {
        return view('layouts.comprar');
    }

    public function to_sell()
    {
        return view('layouts.vender');
    }
    public function index()
    {
        $products = Product::orderImportance()->where('status', 'PUBLICADO')
            ->with('category',  'user')->with(['images' => fn ($q) => $q->orderBy('id', 'asc')])
            ->where('client_agreement_id', "<>", null)
            ->orderBy('id', 'DESC')
            ->paginate(20);
        // ->toSql();
        // dd(Product::orderImportance()->orderBy('id', 'DESC')->where('status', 'PUBLICADO')
        // ->with('category',  'user')->with(['images' => fn ($q) => $q->orderBy('id', 'asc')])
        // ->where('client_agreement_id', "<>", null)
        return view('web', compact('products'));
    }

    public function category($slug)
    {
        $category_id = Category::where('slug', $slug)->pluck('id')->first();
        return redirect()->route('page.more_product', ['area' => $category_id]);
    }

    public function tag($slug)
    {
        return redirect()->route('page.more_product', ['material' => $slug]);
    }

    public function more_product(Request $request)
    {
        if ($request->get('close')) {
            session()->put('show-modal', true);
        }

        $name = $request->get('name');
        $estancia = $request->get('estancia');
        $ciudad = $request->get('ciudad');
        $precio = $request->get('precio');
        $descuento = $request->get('descuento');
        $sindescuento = $request->get('sindescuento');
        $beliani = $request->get('beliani');
        $ofertas = $request->get('ofertas');
        $area = $request->get('area');
        $material = $request->get('material');
        $allProducts = Product::where('status', 'PUBLICADO')
            ->with('category')
            ->with(['images' => fn ($q) => $q->orderBy('id', 'desc')])
            ->precio()
            ->variante()
            ->orderImportance()
            ->orderBy('id', 'desc')
            ->paginate(20);


        $products = Product::where('status', 'PUBLICADO')
            ->with('category')
            ->with(['images' => fn ($q) => $q->orderBy('id', 'asc')])
            ->name($name)
            ->estancia($estancia)
            ->material($material)
            ->descuento($descuento, $precio)
            ->beliani($beliani)
            ->sinDescuento($sindescuento)
            ->ofertas($ofertas)
            ->area($area)
            ->ciudad($ciudad)
            ->precio()
            ->variante()
            ->orderImportance()
            ->orderBy('id', 'desc')
            // ->orderByRaw("variant = 'NOVEDAD' DESC")
            // ->orderByRaw("variant = 'OFERTA' DESC")
            // ->orderByRaw("variant = 'OPORTUNIDAD' DESC")
            // ->orderByRaw("variant = 'VENDIDO' DESC")
            ->paginate(20);

        $categories = Category::orderBy('name', 'ASC')->withCount('products')->get();
        $materiales       = Tag::orderBy('name', 'ASC')->withCount('products')->get();

        $message = null;
        $textitle = null;
        $textweb = null;
        // $products = $products;
        if (!$products->count()) {
            $products = $allProducts;
            $message = "La búsqueda no coincide con nuestros artículos.";
        }
        $pagelinks = array();
        if ($name) $pagelinks = array_add($pagelinks, 'name', $name);
        if ($estancia) $pagelinks = array_add($pagelinks, 'estancia', $estancia);
        if ($material) $pagelinks = array_add($pagelinks, 'material', $material);
        if ($beliani) $pagelinks = array_add($pagelinks, 'beliani', $beliani);
        if ($descuento) $pagelinks = array_add($pagelinks, 'descuento', $descuento);
        if ($sindescuento) $pagelinks = array_add($pagelinks, 'sindescuento', $sindescuento);
        if ($precio) $pagelinks = array_add($pagelinks, 'precio', $precio);
        if ($ofertas) $pagelinks = array_add($pagelinks, 'ofertas', $ofertas);
        if ($area) $pagelinks = array_add($pagelinks, 'area', $area);
        if ($ciudad) $pagelinks = array_add($pagelinks, 'ciudad', $ciudad);

        return view('products', compact('products', 'categories', 'materiales', 'message', 'textitle', 'textweb', 'allProducts', 'pagelinks'));
    }

    public function detail_product(Product $product)
    {
        $product = Product::with(['images' => fn ($q) => $q->orderBy('id', 'asc')])->find($product->id);
        $text = 'usuario';
        if ($product->status == 'PUBLICADO' || $product->status == 'RESERVADO' || auth()->user() == $product->user) {
            $user = $product->user;
            if ($product->client_agreement) {
                $user = $product->client_agreement->handler->user;
            }
            $articles = Product::where('user_id', $user->id)->where('id', '!=', $product->id)->where('status', 'PUBLICADO')
                ->with('category')->with(['images' => fn ($q) => $q->orderBy('id', 'asc')])->get();
            if ($product->client_agreement) {
                $text = 'contrato';
                $contratos =  ClientAgreement::select('id')->where('client_id', $product->client_agreement->client_id)->get();
                $articles = Product::whereIn('client_agreement_id', $contratos)->where('id', '!=', $product->id)->where('status', 'PUBLICADO')
                    ->with('category')->with(['images' => fn ($q) => $q->orderBy('id', 'asc')])->get();
            }
            return view('detailproduct', compact('product', 'user', 'articles', 'text'));
        } else {
            return back()->with('error', 'El Articulo Solicitado no se encuentra Públicado en la pagina');
        }
    }

    public function form_product()
    {
        $categories = Category::orderBy('name', 'ASC')->pluck('name', 'id');
        $tags       = Tag::orderBy('name', 'ASC')->get();
        $user = auth()->user();
        return view('form-vender', compact('categories', 'tags', 'user'));
    }

    public function quiero_vender()
    {
        $user = auth()->user();
        if (!$user->profile->dni) {
            return redirect()->route('profile.edit')->with('info', 'Debe Registrar Sus Datos Completos para continuar');
        }
        try {
            $client_agreements = ClientAgreement::where('client_id', auth()->user()->client->id)->get();
            $concluidos = ClientAgreement::where('client_id', auth()->user()->client->id)->where('status', '=', 'concluido')->get();
            return view('quiero-vender', compact('client_agreements', 'concluidos'));
        } catch (\Throwable $th) {
            return back()->with('info', 'Este usuario no puede acceder');
        }
    }

    public function form_visit(Request $request)
    {
        $agreement = Agreement::where('name', $request->plan)->get()->first();
        $profile = auth()->user()->profile;
        return view('form-visit', compact('agreement', 'profile'));
    }

    public function client_products()
    {
        // dd('dd');
        $user = auth()->user();
        $agreements = null;
        $products = Product::orderBy('created_at', 'DESC')->where('user_id', $user->id)->with('category', 'user', 'images', 'client_agreement', 'compra')->get();
        $published = $products->filter(function ($p) {
            if ($p->status == 'PUBLICADO') {
                return $p;
            }
        });
        $basicos = $products->filter(function ($p) {
            if ($p->client_agreement_id == null && $p->importance == 0) {
                if ($p->status == 'PENDIENTE' || $p->status == 'PUBLICADO') {
                    return $p;
                }
            }
        });
        $pending =  $products->filter(function ($product) {
            if ($product->status == 'PENDIENTE') {
                return $product;
            }
        });
        $sold =     $products->filter(function ($product) {
            if ($product->status == 'VENDIDO') {
                return $product;
            }
        });
        $low =      $products->filter(function ($product) {
            if ($product->status == 'DE BAJA') {
                return $product;
            }
        });
        $reservados = $products->filter(function ($product) {
            if ($product->status == 'RESERVADO') {
                return $product;
            }
        });
        $regected = $products->filter(function ($product) {
            if ($product->status == 'RECHAZADO') {
                return $product;
            }
        });
        if ($user->client->client_agreement) {
            $agreements = $user->client->client_agreement;
        }
        // $solicitados = ($user->role->id == \App\Role::CLIENT) ? $products->filter(function($p){if ($p->compra && $p->compra->status == 'solicitado'){return $p;}}) : null;
        $solicitados = ($user->role->id == \App\Role::CLIENT) ? Compra::where('vendedor', $user->id)->where('status', 'solicitado')->with('product')->get() : null;
        return view('client.list', compact('user', 'published', 'basicos', 'pending', 'sold', 'solicitados', 'agreements', 'reservados'));
    }

    public function detail_edit_product(Product $product)
    {
        $this->authorize('pass', $product); // method for protection
        $categories = Category::orderBy('name', 'ASC')->pluck('name', 'id');
        $tags       = Tag::orderBy('name', 'ASC')->get();
        return view('client.edit', compact('product', 'categories', 'tags'));
    }

    public function detail_client_agreement(ClientAgreement $agreement)
    {
        $total = 0;
        foreach ($agreement->products as $product) {
            $total = $total + $product->normal_price;
        }
        return view('products-agreement', compact('agreement', 'total')); // TODO: FALTA PROTEGER POR RUTA
    }


    public function store(ProductStoreRequest $request)
    {
        $rules = [
            "images" => ["required", "array", "min:4", "max:10"], // validate an array contains minimum 2 elements and maximum 4
            "images.*" => ["required", "mimes:jpeg,jpg,png,gif"], // and each element must be a jpeg or jpg or png or gif file
        ];
        $customMessages = [
            'min' => 'Al menos se requiere 4  imagenes como mínimo, (Puedes seleccionar varias imágenes a la vez).',
            'max' => 'Las imagenes no debe tener más de 10 elementos, (Puedes seleccionar varias imágenes a la vez).'
        ];
        $this->validate($request, $rules, $customMessages);
        $product = Product::create($request->all());
        $lastYear = date("Y") - 2000;
        $product->code = 'B' . $lastYear . '-' . $product->id;
        $product->save();
        if ($request->file('images')) {
            foreach ($request->file('images') as $photo) {
                $path = Storage::disk('public')->put('img_product', $photo);
                $filePath = public_path($path);
                ImageManager::make($filePath)->resize(600, 600, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($filePath);
                Image::create(['product_id' => $product->id, 'path' => $path]);
                $product->img = $path;
            }
            $product->save();
        }
        //TAGS
        $product->tags()->attach($request->get('tags'));
        if ($request->new_article === 'true') {
            return redirect()->route('form-vender')->with('info', 'Artículo enviado y pendiente de revisión por control de calidad. Realiza el seguimiento en tu zona de cliente');
        } else {
            return redirect()->route('client_products', $product)->with('info', 'Artículo pendiente de revisión por control de calidad. Realiza el seguimiento en tu zona de cliente');
        }
    }

    public function update(ArticuloUpdate $request, Product $product)
    {
        $this->authorize('pass', $product); // method for protection
        $product->fill($request->all())->save(); // actualizamos el product
        //TAGS
        $product->tags()->sync($request->get('tags')); //update related with tags
        return back()->with('info', 'Articulo Actualizado correctamente');
    }

    public function soldproduct(Request $request, Product $product)
    {
        $this->authorize('pass', $product); // method for protection
        $compra = $product->compra;
        if ($product->client_agreement) {
            return back()->with('error', 'Este Articulo Se encuentra Administrado por un Gestor de Vacía Tu Casa');
        }
        if ($product->status == 'VENDIDO') {
            return back()->with('error', 'Este Articulo ya se encuentra vendido con Importe ' . $product->payment . ' Euros');
        } else {
            if ($compra) {
                $solicitudes = Compra::where('product_id', $product->id)->where('status', '=', 'solicitado')->where('id', '!=', $product->compra->id)->get();
                foreach ($solicitudes as $solicitud) {
                    $solicitud->delete();
                }
                $product->compra->status = 'completado';
                $product->compra->pago = $request->get('pago');
                $product->compra->save();
            }
        }
        $product->payment = $request->get('pago');
        $product->status = 'VENDIDO';
        $product->variant = 'VENDIDO';
        $product->save();
        return back()->with('info', 'El Articulo ' . $product->name . ' fue  vendido con Importe ' . $request->get('pago') . ' Euros');
    }
    public function reservedProduct(Product $product)
    {
        if ($product->status == 'PUBLICADO') {
            $product->status = 'RESERVADO';
            $product->save();
            return back()->with('info', 'El Articulo fue reservado');
        }
    }

    public function PublishedProduct(Product $product)
    {
        if (auth()->user()) {
            # code...
            $product->status = 'PUBLICADO';
            $product->payment = 0;
            $product->save();
            return back()->with('info', 'El Articulo fue quitado de la reserva y Publicado nuevamente');
        }
        return back()->with('error', ' Permisos Insuficientes');
    }

    public function comprar(Product $product)
    {
        if ($product->status == 'PUBLICADO') {
            // dd($product);
            return view('comprar', compact('product'));
        } else {
            return back()->with('error', 'El Articulo Solicitado no se encuentra disponible en la pagina');
        }
    }

    public function comprar_store(Request $request, Product $product)
    {
        Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255'],
            'phone' => ['required', 'min:9'],
            // 'comments' => ['required', 'string'],
            'g-recaptcha-response' => 'required|captcha',
            'city' => ['nullable'],
        ], [
            'phone.min' => 'el numero no contiene codigo de pais'
        ])->validate();

        $comprador = null;
        if (auth()->user()) {
            $comprador = auth()->user()->id;
        }

        if ($product->status == 'PUBLICADO') {
            $vendedor = $product->user;
            if ($product->client_agreement) {
                $vendedor = $product->client_agreement->handler->user;
            }
            $compra = Compra::create([
                'product_id' => $product->id,
                'vendedor' => $vendedor->id,         // vendedor
                'comprador' => $comprador,         // vendedor
                'nameClient' => $request->get('name'), //comprador
                'email' => $request->get('email'),
                'phone' => $request->get('phone'),
                'city' => $request->get('city'),
                'comments' => $request->get('comments'),
                'monto_reserva' =>   0,
                'have_reserva' =>   'GESTOR',
                'have_pago' =>   'GESTOR',
                'pago' =>   0,
                'status' => 'solicitado',
            ]);
            return back()->with('info', 'Compra Solicitada, nos pondremos en contacto contigo');
        } else {
            return back()->with('error', 'No se puede solicitar la Compra de este articulo por que no esta publicado en la web');
        }
    }

    public function pass_agreement($token)
    {
        $verifyAgreement = VerifyAgreement::where('token', $token)->first();

        $user = auth()->user();
        if (!$user->profile->dni) {
            return redirect()->route('profile.edit')->with('info', 'Debe Registrar Sus Datos Completos para continuar');
        }
        if (isset($verifyAgreement)) {
            $agreement = $verifyAgreement->agreement;
            if (!$agreement->verified) {
                $verifyAgreement->agreement->verified = true;
                $verifyAgreement->agreement->status = 'aprobado';
                if ($verifyAgreement->agreement->visit) {
                    $verifyAgreement->agreement->visit->contrato = true;
                    $verifyAgreement->agreement->visit->save();
                }
                $verifyAgreement->agreement->save();
                $status = "Aprobaste el Contrato " . $verifyAgreement->agreement->name . " N° 000" . $verifyAgreement->agreement->id . " , puedes contactarte con el gestor para Completar los datos";
            } else {
                $status = "El contrato ya fue Aprovado, puedes contactarte con el gestor";
            }
        } else {
            return back()->with('info', "Lo sentimos no fue posible aprobar el contrato.");
        }
        if (auth()->user()) {
            return redirect()->route('client_products')->with('info', $status);
        } else {
            return redirect()->route('login')->with('info', $status);
        }
    }

    public function download_pdf(ClientAgreement $agreement)
    {
        // This  $data array will be passed to our PDF blade
        $total = 0;
        foreach ($agreement->products as $product) {
            $total = $total + $product->normal_price;
        }

        $data = [
            'agreement' => $agreement,
            'total' => $total,
        ];
        if ($agreement->name == 'premium') {
            $pdf = PDF::loadView('pdf_viewP', $data);
        } else {
            $pdf = PDF::loadView('pdf_viewS', $data);
        }
        return $pdf->download('Contrato_' . $agreement->name . '_N' . $agreement->id . '.pdf');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        $this->authorize('pass', $product);
        if ($product->status == 'RESERVADO') {
            return back()->with('error', 'Este Articulo ya se encuentra Reservado con Importe ' . $product->payment . ' Euros');
        }
        if ($product->status == 'VENDIDO') {
            return back()->with('error', 'Este Articulo ya se encuentra Vendido con Importe ' . $product->payment . ' Euros');
        }
        if ($product->compra) { # si existe compra
            $solicitudes = Compra::where('product_id', $product->id)->where('status', '=', 'solicitado')->get();
            foreach ($solicitudes as $solicitud) {
                $solicitud->delete();
            }
        }
        $product->status = 'DE BAJA';
        $product->save();
        return back()->with('info', "El Articulo Fue dado de Baja.");
    }
    public function contacto(Product $product)
    {
        $company = Company::first();
        return view('contact', compact('company'));
    }
    public function downloadExcel(ClientAgreement $agreement)
    {
        return (new ConcluidoExcel($agreement))->download('Contrato_N000' . $agreement->id . '.xlsx');
    }
}
