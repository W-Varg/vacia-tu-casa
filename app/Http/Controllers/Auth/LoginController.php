<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Role;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/admin';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    protected function authenticated(Request $request, User $user)
    {
        // if (!$user->deleted) {
        //     // dd(auth());
        //     // return redirect('logout', )->with('error', 'Esta cuenta no puede acceder al sistema');
        //     return redirect()->guest(route('logout'));

        // }

        if (auth()->user()->role_id == Role::CLIENT) {
            return redirect('/');
        }
        if (auth()->user()->role_id == Role::HANDLER) {
            return redirect('/admin');
        }
        if (auth()->user()->role_id == Role::ADMIN) {
            return redirect('/admin');
        }
        return back()->with('error', 'Esta cuenta no puede acceder al sistema');
    }
}
