<?php

namespace App\Http\Controllers;

use App\ClientAgreement;
use App\Mail\ContratoEndEmail;
use App\Utilidad;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;

class UtilidadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, ClientAgreement $agreement)
    {
        if ($agreement->status == 'cerrado') {
            return back()->with('info', "Este Contrato ya se encuentra cerrado, No se puede hacer cambios");
        }
        Validator::make($request->all(), [
            'cobrado_por_todas_partes' => ['required'],
            'entero' => ['required'],
        ])->validate();

        $total_cliente = 0;
        $total_gestor = 0;
        $total = 0;
        $saldo = $request->entero;

        $agreement->status = 'cerrado';
        $agreement->save();
        // =========
        // dd($agreement->products);
        $vendidos =  $agreement->products->filter(function ($p) {
            if ($p->compra && $p->compra->status == 'completado' && $p->status == 'VENDIDO') {
                return $p;
            }
        });
        foreach ($vendidos as $p) {
            if ($p->compra->pago) {
                if ($p->compra->have_pago == 'CLIENTE') {
                    $total_cliente =  $total_cliente + $p->compra->pago;
                } else {
                    $total_gestor =  $total_gestor + $p->compra->pago;
                }
            }
        }
        $total = $total_cliente + $total_gestor;
        // 'total', 'vendidos', 'no_vendidos', 'total_cliente', 'total_gestor'
        $utilidad = Utilidad::create([
            'client_agreement_id' => $agreement->id,
            'type_contrato' => $agreement->name,
            'cantidad_vend' => $vendidos->count(),
            'utilidad' => $total,
            'gestor_name' => $agreement->handler->user->name . ' ' . $agreement->handler->user->profile->last_name,
            'user_id' => $agreement->handler->user->id,
            'gestor_cuota' => $agreement->name == 'estandar' ? ($total * 0.1) : ($total * 0.1),
            'vtc_cuota' => $agreement->name == 'estandar' ? ($total * 0.15) : ($total * 0.3),
            'cliente_cuota' => $agreement->name == 'estandar' ? ($total * 0.75) : ($total * 0.6),
            'status' => 'pendiente',
            // 'fecha_pago' => Carbon::now()
        ]);
        $client =  $agreement->client->user;
        $handler =  $agreement->handler->user;
        Mail::to(trim($handler->email))->send(new ContratoEndEmail($client, $agreement, $utilidad, $saldo, $total_cliente, $total_gestor));
        Mail::to(trim($client->email))->send(new ContratoEndEmail($client, $agreement, $utilidad, $saldo, $total_cliente, $total_gestor));

        $concluidos =  $agreement->products->filter(function ($p) {
            if (!$p->compra && $p->status == 'CONCLUIDO' && $p->client_agreement->name === "premium") {
                return $p;
            }
        });
        foreach ($concluidos as $p) {
            $p->user_id = auth()->user()->id;
            $p->client_agreement_id = null;
            $p->code = str_replace('P', 'O', $p->code);
            // $p->reduced_price = 00.00;
            $p->sale_date = null;
            $p->status = 'PUBLICADO';
            $p->type = 'NORMAL';
            $p->variant = 'OPORTUNIDAD';
            $p->payment = 0;
            $p->importance = 1;
            $p->descuento = false;
            $p->beliani = true;
            $p->save();
        }
        return back()->with('info', "Este Contrato fue cerrado exitosamente, las comisiones se encuentran en Reportes->Contratos y Gestores");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Utilidad  $utilidad
     * @return \Illuminate\Http\Response
     */
    public function show(Utilidad $utilidad)
    {
        $agreement = $utilidad->client_agreement;
        // dd($agreement, $utilidad);
        return view('reportes.detail_utilidad', compact('utilidad', 'agreement'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Utilidad  $utilidad
     * @return \Illuminate\Http\Response
     */
    public function edit(Utilidad $utilidad)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Utilidad  $utilidad
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Utilidad $utilidad)
    {
        $utilidad->status = 'pagado';
        $utilidad->fecha_pago = Carbon::now();
        $utilidad->save();
        return back()->with('info', "La comisión de este contrato fue cancelado al gestor exitosamente");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Utilidad  $utilidad
     * @return \Illuminate\Http\Response
     */
    public function destroy(Utilidad $utilidad)
    {
        //
    }
}
