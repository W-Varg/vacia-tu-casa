<?php

namespace App\Http\Controllers;

use App\Profile;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Intervention\Image\Facades\Image as ImageManager;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function edit()
    {
        $user = auth()->user();
        $profile = $user->profile;
        return view('auth.editProfile', compact('user', 'profile'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $request->validate([
            'name' => 'required|string',
            'last_name' => 'required|string',
            'code_postal' => 'required',
            'phone_number' => 'required',
            'countryCode' => 'required',
            'dni' => 'required',
            'address' => 'required|string',
            'provincia' => 'required|string',
            'localidad' => 'required|string',
            'avatar' => 'sometimes|image|max:2048',
        ]);
        // dd($request, $request->countryCode . $request->phone_number);
        // user

        $user->name = $request->name;
        $profile = $user->profile;
        $profile->last_name = $request->last_name;
        $profile->code_postal = $request->code_postal;
        if (strpos($request->phone_number, $request->countryCode) !== false) {
            $profile->phone_number = $request->phone_number;
        } else {
            $profile->phone_number = $request->countryCode . $request->phone_number;
        }
        $profile->dni = $request->dni;
        $profile->address = $request->address;
        $profile->provincia = $request->provincia;
        $profile->localidad = $request->localidad;
        if ($request->password) {
            $user->password = Hash::make($request->password);
        }

        if ($request->hasFile('avatar')) {
            $avatar = $request->file('avatar');
            $filename = time() . "." . $avatar->getClientOriginalExtension();
            ImageManager::make($avatar)->resize(300, 300)->save(public_path('adm/images/avatar/' . $filename));
            $profile->avatar = 'adm/images/avatar/' . $filename;
        }
        $user->save();
        $profile->save();
        return redirect()->route('client_products')->with('info', 'Datos de Usuario Actualizados Correctamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function destroy(Profile $profile)
    {
        //
    }
}
