<?php

namespace App\Http\Controllers;

use App\ClientAgreement;
use App\Compra, App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Session;

class ComprasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (auth()->user()->role->id == \App\Role::HANDLER) {
            $compras = Compra::orderBy('created_at', 'DESC')->where('status', 'solicitado')
                ->whereIn('product_id', Product::whereIn('client_agreement_id', ClientAgreement::where('handler_id', auth()->user()->handler->id)->pluck('id'))->pluck('id'))
                ->paginate(20);
        } else { //admin
            $compras = Compra::orderBy('created_at', 'DESC')->where('status', 'solicitado')->paginate(20);
        }
        return view('admin.compras.index', compact('compras'));
    }

    public function getArticlesSolds()
    {
        if (auth()->user()->role->id == \App\Role::HANDLER) {
            $compras = Compra::orderBy('updated_at', 'DESC')->where('status', 'completado')
                ->whereIn('product_id', Product::whereIn('client_agreement_id', ClientAgreement::where('handler_id', auth()->user()->handler->id)->pluck('id'))->pluck('id'))
                ->paginate(20);
        } else { //admin
            $compras = Compra::orderBy('updated_at', 'DESC')->where('status', 'completado')->paginate(20);
        }
        return view('admin.compras.articlesSolds', compact('compras'));
    }
    public function getArticlesReserved()
    {
        if (auth()->user()->role->id == \App\Role::HANDLER) {
            $compras = Compra::orderBy('updated_at', 'DESC')->where('status', 'reservado')
                ->whereIn('product_id', Product::whereIn('client_agreement_id', ClientAgreement::where('handler_id', auth()->user()->handler->id)->pluck('id'))->pluck('id'))
                ->paginate(20);
        } else { //admin
            $compras = Compra::orderBy('updated_at', 'DESC')->where('status', 'reservado')->paginate(20);
        }
        return view('admin.compras.articlesReservados', compact('compras'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Product $product)
    {
        $value = Session::get('temp_product');
        if ($value && ($value->id == $product->id)) {
            $product->status = 'PUBLICADO';
        }
        return view('admin.compras.soldcomplete', compact('product'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Product $product)
    {
        $value = Session::get('temp_product');
        if ($value && ($value->id == $product->id)) {
            Session::forget('temp_product');
        }
        $compra = null;
        if ($product->compra) {
            if ($product->status == 'VENDIDO') {
                return back()->with('error', 'Este Articulo ya se encuentra vendido con Importe ' . $product->payment . ' Euros');
            } else {
                $solicitudes = Compra::orderBy('created_at', 'DESC')->where('product_id', $product->id)->where('status', '=', 'solicitado')->where('id', '!=', $product->compra->id)->paginate(20);
                foreach ($solicitudes as $solicitud) {
                    $solicitud->delete();
                }
                $product->compra->status = 'completado';
                $product->compra->fecha_venta = Carbon::now();
                $product->compra->have_pago = $request->get('have_pago') ? $request->get('have_pago') : 'GESTOR';
                $product->compra->pago = $request->get('pago') ? $request->get('pago') : 0;
                $product->compra->save();
            }
            $compra = $product->compra;
        } else {
            $comprador = null;
            if (auth()->user()) {
                $comprador = auth()->user()->id;
            }

            $vendedor = $product->user;
            if ($product->client_agreement) {
                $vendedor = $product->client_agreement->handler->user;
            }
            $compra = Compra::create([
                'product_id' => $product->id,
                'vendedor' => $vendedor->id,         // vendedor
                'comprador' => $comprador,         // vendedor
                'nameClient' => $request->get('name'), //comprador
                'email' => $request->get('email'),
                'phone' => $request->get('phone'),
                'city' => $request->get('city'),
                'comments' => $request->get('comments'),
                'monto_reserva' => $request->get('monto_reserva') ? $request->get('monto_reserva') : 0,
                'have_reserva' => $request->get('have_reserva') ? $request->get('have_reserva') : 'GESTOR',
                'have_pago' => $request->get('have_pago') ? $request->get('have_pago') : 'GESTOR',
                'fecha_venta' => Carbon::now(),
                'pago' => $request->get('pago') ? $request->get('pago') : 0,
                'status' => 'completado',
            ]);
        }
        $product->payment = $request->get('pago') ? $request->get('pago') : 0;
        $product->status = 'VENDIDO';
        $product->variant = 'VENDIDO';
        $product->save();
        return redirect()->route('compras.show', $compra)->with('info', 'El Articulo fue Vendido Correctamente con Importe ' . $request->get('pago') . ' Euros');
        // return redirect()->route('compras.solds')->with('info', 'El Articulo fue Vendido Correctamente con Importe '.$request->get('pago').' Euros');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Compra  $compra
     * @return \Illuminate\Http\Response
     */
    public function show(Compra $compra)
    {
        if ($compra->status == 'solicitado' || $compra->status == 'reservado') {
            $product = $compra->product;
            return view('admin.compras.soldcomplete', compact('product'));
        }
        $product = $compra->product;
        $agreement = null;
        if ($product->agreement) {
            $agreement = $product->agreement;
        }
        return view('admin.compras.show', compact('compra', 'product', 'agreement'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Compra  $compra
     * @return \Illuminate\Http\Response
     */
    public function edit(Compra $compra)
    {
        return view('admin.compras.edit', compact('compra'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Compra  $compra
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Compra $compra)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Compra  $compra
     * @return \Illuminate\Http\Response
     */
    public function destroy(Compra $compra)
    {
        if ($compra->status == 'solicitado') {
            $client = $compra->nameClient;
            $compra->delete();
            return back()->with('info', "La compra solicitada Para el Cliente " . $client . " fue Cancelado.");
        } else {
            return back()->with('info', "La compra no se puede Eliminar.");
        }
    }
}
