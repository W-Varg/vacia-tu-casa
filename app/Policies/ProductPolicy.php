<?php

namespace App\Policies;

use App\User;
use App\Product;
use Illuminate\Auth\Access\HandlesAuthorization;

class ProductPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function pass(User $user, Product $product){
        if($user->role->id == \App\Role::CLIENT){
            return  $user->id == $product->user_id;
        }
        if($user->role->id == \App\Role::ADMIN){
            return true;
        }
        if($user->role->id == \App\Role::HANDLER){
            return true;
        }
    }
}
