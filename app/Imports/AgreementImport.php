<?php

namespace App\Imports;

use App\ClientAgreement;
use App\Product;
use App\Imports\ArticulosImport;
use App\Imports\AgreementData;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class AgreementImport implements WithMultipleSheets 
{
    private $agreement;
    private $error = false;

    public function __construct(ClientAgreement $agreement)
    {
        $this->agreement = $agreement;
    }
    public function sheets(): array
    {
        return [
            //     0 => new ArticulosImport(),
            new ArticulosImport($this->agreement)
            //     1 => new AgreementData(),
        ];
    }
}
