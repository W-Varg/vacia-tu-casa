<?php

namespace App\Imports;
use App\ClientAgreement;
use App\Product;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class ArticulosImport implements ToCollection, WithHeadingRow
{

    private $agreement;
    private $error = false;

    public function __construct(ClientAgreement $agreement)
    {
        $this->agreement = $agreement;
    }
    /**
    * @param Collection $collection
    */

    public function collection(Collection $rows) //ToCollection
    {
        foreach ($rows as $row)
        {
            try {
                $articulo = Product::findOrFail($row['identificador']);

                $articulo->name = $row['nombre'];
                $articulo->normal_price = $row['precio'];
                $articulo->quantity = $row['stock'];
                $articulo->description = $row['descripcion'];
                $articulo->style = $row['estilos'];
                $articulo->weight = $row['peso'];
                $articulo->long = $row['largo'];
                $articulo->width = $row['ancho'];
                $articulo->height = $row['alto'];
                $articulo->save();
            } catch (\Throwable $th) {
                $this->error = true;
                echo 'Ocurrio un Error con el articulo:  ---ERROR-->>   '.$th;
            }
        }
    }

}
