<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Agreement extends Model
{
    // public function visits () {
	// 	return $this->hasMany(Visit::class);
	// }
	public function client_agreement(){
		return $this->hasMany(ClientAgreement::class);
	}
}
