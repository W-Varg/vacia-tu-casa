<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Visit extends Model
{
	protected $fillable = ['id', 'client_id', 'client_agreement_id', 'date_visit', 'hour_visit', 'cp', 'address', 'phone', 'status', 'quantity_product', 'visitado'];

    public function client(){
        return $this->belongsTo(Client::class);
    }

    public function handler(){
        return $this->belongsTo(Handler::class);
    }

    public function client_agreement(){
    	return $this->belongsTo(ClientAgreement::class);
    }

    public function images(){ //many images belong to a product
        return $this->hasMany(Image::class);
    }

}
