<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $fillable = [
        'company_name',
        'email',
        'phone_number',
        'facebook',
        'instagram',
        'twitter',
        'pinterest',
        'address',
        'img1',
        'texto1',
        'img2',
        'texto2',
        'img3',
        'texto3',
        'img4',
        'code_postal',
    ];
    // 'images', // 'slug',

    //
}
