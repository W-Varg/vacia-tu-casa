<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use \App\Visit;

class AgreementRegetedEmail extends Mailable
{
    use Queueable, SerializesModels;
    public $visit;
    public $razon;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Visit $visit, $razon)
    {
        $this->visit = $visit;
        $this->razon = $razon;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $url = request()->getHttpHost();
        $client = $this->visit->client->user->name;
        $razon = $this->razon;
        $tipo = $this->visit->client_agreement->name;
        $code  = $tipo == 'estandar' ?  "E-000" . $this->visit->client_agreement->id : "P-000" . $this->visit->client_agreement->id;
        return $this->subject("Contrato {$tipo} rechazado")->markdown('emails.agreementRegeted', compact('url', 'tipo', 'client', 'code', 'razon'));
    }
}
