<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\ClientAgreement;
use Carbon\Carbon;


class ContratoPublicadoEmail extends Mailable
{
    use Queueable, SerializesModels;
    public $agreement;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(ClientAgreement $agreement)
    {
        $this->agreement = $agreement;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $url = request()->getHttpHost();
        $agreement = $this->agreement;
        $agreement->created_at = Carbon::today()->toDateTimeString();
        $total = 0;
        foreach ($agreement->products as $product) {
            $total = $total + $product->normal_price;
            $product->created_at = $agreement->created_at;
            $product->importance = $agreement->name == 'premium' ? 3 : 2;
            $product->updated_at = $agreement->created_at;
            $product->status = 'PUBLICADO';
            $product->save();
        }
        $agreement->status = 'publicado';
        $vencimiento = Carbon::today();
        $agreement->date_end = $vencimiento->addDays(112);
        $agreement->save();
        return $this->subject('¡Contrato ' . $agreement->name . '  ' . $agreement->id . ' Aprobado!')
            ->markdown('emails.AgreementPublic', compact('agreement', 'url', 'total'));
    }
}
