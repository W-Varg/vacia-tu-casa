<?php

namespace App\Mail;

use App\ClientAgreement;
use App\User;
use App\Utilidad;
use Illuminate\Bus\Queueable;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\ConcluidoExcel;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ContratoEndEmail extends Mailable
{
    use Queueable, SerializesModels;
    public $client;
    public $agreement;
    public $utilidad;
    public $saldo;
    public $cobrado_cliente;
    public $cobrado_gestor;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user, ClientAgreement $contrato = null, Utilidad $util, $saldoCliente, $cobrado_cliente, $cobrado_gestor)
    {
        $this->client = $user;
        $this->agreement = $contrato;
        $this->utilidad = $util;
        $this->saldo = $saldoCliente;
        $this->cobrado_cliente = $cobrado_cliente;
        $this->cobrado_gestor = $cobrado_gestor;
        //
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $url = request()->getHttpHost();
        $cliente = $this->client;
        $contrato = $this->agreement;
        $utilidad = $this->utilidad;
        $saldo = $this->saldo;
        $cobrado_cliente = $this->cobrado_cliente;
        $cobrado_gestor = $this->cobrado_gestor;

        if ($contrato->name == 'premium') {
            return $this->subject('¡Contrato  000' . $contrato->id . ' finalizado!')
                ->markdown(
                    'emails.ContratoFinalizadoP',
                    compact('contrato', 'url', 'cliente', 'utilidad', 'saldo', 'cobrado_cliente', 'cobrado_gestor')
                )->attach(
                    Excel::download(new ConcluidoExcel($contrato), 'ExcelContrato_000' . $contrato->id . '.xlsx')->getFile(),
                    ['as' => 'ExcelContrato_000' . $contrato->id . '.xlsx']
                );
        } else {
            return $this->subject('¡Contrato  000' . $contrato->id . ' finalizado!')
                ->markdown(
                    'emails.ContratoFinalizadoS',
                    compact('contrato', 'url', 'cliente', 'utilidad', 'saldo', 'cobrado_cliente', 'cobrado_gestor')
                )->attach(
                    Excel::download(
                        new ConcluidoExcel($contrato),
                        'ExcelContrato_000' . $contrato->id . '.xlsx'
                    )->getFile(),
                    ['as' => 'ExcelContrato_000' . $contrato->id . '.xlsx']
                );
        }
    }
}
