<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\ClientAgreement;

class ContratoEmail extends Mailable
{
    use Queueable, SerializesModels;
    public $agreement;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(ClientAgreement $agreement = null)
    {
        $this->agreement = $agreement;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $url = request()->getHttpHost();
        $agreement = $this->agreement;
        $gestor = 0000;
        $total = 0;
        foreach ($agreement->products as $product) {
            $total = $total+ $product->normal_price;
        }
        if ($agreement->handler) {
            $gestor = $agreement->handler->user->profile->phone_number;
        }

        if ($agreement->name == 'premium') {
            return $this->subject('Contrato de Servicio')->markdown('emails.ContratoP', compact('agreement', 'url', 'gestor', 'total'));
        }
        else{
            return $this->subject('Contrato de Servicio')->markdown('emails.ContratoS', compact('agreement', 'url', 'gestor', 'total'));
        }
    }
}
