<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Product;
use Carbon\Carbon;

class EmailRegected extends Mailable
{
    use Queueable, SerializesModels;

    public $product;
    public $razon;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Product $product, $razon)
    {

        $this->razon = $razon;
        $this->product = $product;
        $this->product->status = 'RECHAZADO';
        $this->product->created_at = Carbon::today()->toDateTimeString();
        $this->product->save();

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $url = request()->getHttpHost();
        $razon = $this->razon;
        $product = $this->product;
        return $this->subject(config('app.name').', Articulo Rechazado')->markdown('emails.emailRegected', compact('url','product','razon'));
    }
}
