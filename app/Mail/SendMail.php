<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Product;
use Carbon\Carbon;

class SendMail extends Mailable
{
    use Queueable, SerializesModels;

    public $product;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Product $product = null)
    {
        $this->product = $product;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $url = request()->getHttpHost();
        $product = $this->product;
        return $this->subject(env('APP_NAME').' Artículo Publicado')->markdown('emails.ArticuloPublicadoEmail', compact('product', 'url'));
    }
}
