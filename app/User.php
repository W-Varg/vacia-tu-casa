<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Notifications\ResetPassword as ResetPasswordNotification;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'role_id', 'deleted'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    protected $guard_name = 'web'; // or whatever guard you want to use

    public function products()
    { // a user can have many products
        return $this->hasMany(Product::class);
    }

    public function profile()
    {
        return $this->hasOne(Profile::class);
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    public function role()
    {
        return $this->belongsTo(Role::class);
    }

    public function client()
    {
        return $this->hasOne(Client::class);
    }

    public function handler()
    {
        return $this->hasOne(Handler::class);
    }

    public function agrements()
    {
        if ($this->client) {
            $agrements  = $this->client->client_agreement()->get();
            return $agrements->count();
        } else {
            return 0;
        }
    }

    public function compras()
    { // a user can have many products
        return $this->hasMany(Compra::class);
    }

    // public function socialAccount () {
    //     return $this->hasOne(UserSocialAccount::class);
    // }
    public function sendPasswordResetNotification($token)
    {
        // Your your own implementation.
        $this->notify(new ResetPasswordNotification($token));
    }

    public function scopeName($query, $name)
    {
        if ($name)
            return $query->where('name', 'iLIKE', "%$name%")
                ->orWhere('email', 'iLIKE', "%$name%")
                ->with('profile')->orWhereHas('profile', function ($query) use ($name) {
                    $query->where('last_name', 'iLIKE', "%$name%");
                });
    }
}
