<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = ['name','slug','icon'];

    public function products(){ // a user can have many products
        return $this->hasMany(Product::class);
    }
}
