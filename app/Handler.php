<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Handler extends Model
{
	protected $fillable = ['user_id'];

    public function user () {
		return $this->belongsTo(User::class);
	}

	public function visits () {
		return $this->hasMany(Visit::class);
	}

	public function clients(){
		return $this->belongsToMany(Client::class);
	}

    public function num_clientes($id){
        $user = User::findOrFail($id);
        $clients = $user->handler->clients;
        $clients = User::whereIn('id', $clients->pluck('user_id'))->where('id', '<>', $user->id)->get()->count();
		return $clients;
	}

}
