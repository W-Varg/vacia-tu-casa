<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    //
    protected $fillable = [
        'user_id',
        'last_name',
        'code_postal',
        'avatar',
        'phone_number',
        'dni',
        'address',
        'provincia',
        'localidad'
    ];


    public function user()
    {
        return $this->hasOne(User::class);
    }
}
