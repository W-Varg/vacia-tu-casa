<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Utilidad extends Model
{
    //
    protected $fillable = [
        'client_agreement_id',
        'type_contrato',
        'cantidad_vend',
        'utilidad',
        'gestor_name',
        'gestor_cuota',
        'vtc_cuota',
        'cliente_cuota',
        'status',
        'fecha_pago',
        'user_id',
        'created_at',
        'updated_at'
    ];

    public function user()
    { //pertenece a un usuario, (relacion inversa)
        return $this->belongsTo(User::class);
    }
    public function client_agreement()
    {
        return $this->belongsTo(ClientAgreement::class);
    }
}
