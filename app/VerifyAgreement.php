<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\ClientAgreement;
class VerifyAgreement extends Model
{
    protected $guarded = [];
    
    public function agreement()
    {
        return $this->belongsTo(ClientAgreement::class, 'client_agreement_id');
    }
}
