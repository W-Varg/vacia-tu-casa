<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientAgreements extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('client_agreements', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->enum('name', array('basico', 'estandar', 'premium'));
            $table->enum('status', array('enviado', 'aprobado', 'rechazado','pendiente', 'publicado','concluido', 'cerrado'))->nullable();
            $table->boolean('verified')->default(false);
            $table->unsignedBigInteger('client_id')->nullable();
            $table->unsignedBigInteger('agreement_id')->nullable();
            $table->unsignedBigInteger('handler_id')->nullable();

            $table->date('date_start')->nullable();
            $table->date('date_end')->nullable();
            $table->float('percent_discount_week_2', 8, 2)->nullable();
            $table->float('percent_discount_week_4', 8, 2)->nullable();
            $table->float('percent_discount_week_6', 8, 2)->nullable();
            $table->float('percent_discount_week_8', 8, 2)->nullable();
            $table->float('percent_penalization', 8, 2)->nullable();
            $table->float('percent_negotiation', 8, 2)->nullable();
            $table->float('percent_company', 8, 2)->nullable();

            $table->integer('validity'); //vigencia en semanas
            $table->boolean('extended'); //contrato prorrogado a 10 semanas
            $table->timestamps();

            //relations
            $table->foreign('client_id')->references('id')->on('clients')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('agreement_id')->references('id')->on('agreements')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('handler_id')->references('id')->on('handlers')->onDelete('cascade')->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('client_agreements');
    }
    // ALTER TABLE "client_agreements"
    //     DROP CONSTRAINT "client_agreements_status_check",
    //     ADD CONSTRAINT "client_agreements_status_check"
    //         CHECK (status::text = ANY (ARRAY['enviado'::character varying, 'aprobado'::character varying, 'rechazado'::character varying, 'pendiente'::character varying, 'publicado'::character varying, 'concluido'::character varying, 'cerrado'::character varying]::text[]));
}
