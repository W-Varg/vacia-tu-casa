<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('company_name');
            $table->string('email')->unique();
            $table->string('phone_number');
            $table->string('facebook')->nullable()->default('https://www.facebook.com/');
            $table->string('instagram')->nullable()->default('https://www.facebook.com/');
            $table->string('twitter')->nullable()->default('https://twitter.com/');
            $table->string('pinterest')->nullable()->default('https://www.pinterest.es/');
            $table->string('address')->nullable();
            $table->string('img1')->nullable()->default('assets/images/home.jpg');
            $table->string('texto1')->nullable()->default('text1');
            $table->string('img2')->nullable()->default('assets/images/gallery-2.jpg');
            $table->string('texto2')->nullable()->default('text2');
            $table->string('img3')->nullable()->default('assets/images/gallery-3.jpg');
            $table->string('texto3')->nullable()->default('text3');
            $table->string('img4')->nullable()->default('assets/images/gallery-4.jpg');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companies');
    }
}
