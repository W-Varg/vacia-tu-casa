<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('category_id');
            $table->unsignedBigInteger('client_agreement_id')->nullable();

            $table->string('code')->nullable(); //cat-idpro rand()
            $table->string('name',128);
            $table->float('normal_price');
            $table->float('reduced_price')->nullable();
            $table->integer('quantity')->default(1);
            $table->text('description')->nullable()->default('');
            $table->string('style')->nullable();
            $table->string('sale_date')->nullable(); //fecha de rebaja
            $table->string('img')->nullable();
            $table->float('weight')->nullable();
            $table->float('long')->nullable();
            $table->float('width')->nullable();
            $table->float('height')->nullable();
            // $table->enum('status',['PUBLISHED','PENDING','REGECTED','SOLD', 'LOW', 'RESERVED', 'CONCLUIDO'])->default('PENDING');
            $table->enum('status',['PUBLICADO','PENDIENTE','RECHAZADO','VENDIDO', 'DE BAJA', 'RESERVADO', 'CONCLUIDO', 'ELIMINADO', 'CANCELADO'])->default('PENDIENTE');
            $table->boolean('sold_status')->default(0);
            $table->enum('type',['EXCLUSIVE','NORMAL'])->default('NORMAL');
            $table->float('payment')->default(0)->nullable();
            $table->string('variant')->default('NOVEDAD')->nullable();
            $table->softDeletes();
            $table->timestamps();

            // Relations
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('client_agreement_id')->references('id')->on('client_agreements')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('images'); // elimina primero las tabla imagenes que aputan a producto
        Schema::dropIfExists('products');
    }
}
