<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVerifyAgreementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('verify_agreements', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('client_agreement_id');
            $table->string('token');
            $table->timestamps();
            // relations
            $table->foreign('client_agreement_id')->references('id')->on('client_agreements')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('verify_agreements');
    }
}
