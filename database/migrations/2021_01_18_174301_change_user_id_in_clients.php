<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeUserIdInClients extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('clients', function (Blueprint $table) {
            $table->dropForeign('clients_user_id_foreign');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade')->change();
        });
        Schema::table('handlers', function (Blueprint $table) {
            $table->dropForeign('handlers_user_id_foreign');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade')->change();
        });
        Schema::table('visits', function (Blueprint $table) {
            $table->dropForeign('visits_client_id_foreign');
            $table->foreign('client_id')->references('id')->on('clients')->onDelete('cascade')->onUpdate('cascade')->change();

            $table->dropForeign('visits_handler_id_foreign');
            $table->foreign('handler_id')->references('id')->on('handlers')->onDelete('cascade')->onUpdate('cascade')->change();

            $table->dropForeign('visits_client_agreement_id_foreign');
            $table->foreign('client_agreement_id')->references('id')->on('client_agreements')->onDelete('cascade')->onUpdate('cascade')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('clients', function (Blueprint $table) {
            //
        });
    }
}
