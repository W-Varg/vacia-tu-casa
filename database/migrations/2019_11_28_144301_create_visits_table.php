<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVisitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('visits', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('client_id');
            $table->unsignedBigInteger('handler_id')->nullable();
            $table->unsignedBigInteger('client_agreement_id')->nullable();

            $table->integer('quantity_product')->default(1);
            $table->date('date_visit');
            $table->string('hour_visit');
            $table->string('cp');
            $table->string('address')->nullable();
            $table->string('phone');
            $table->string('status')->nullable(); //reprogramado
            $table->boolean('contrato')->default(false); // aprobado, rechazado
            $table->timestamps();

            // //relations
            $table->foreign('client_id')->references('id')->on('clients')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('handler_id')->references('id')->on('handlers')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('client_agreement_id')->references('id')->on('client_agreements')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('visits');
    }
}
