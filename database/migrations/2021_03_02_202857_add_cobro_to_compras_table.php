<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCobroToComprasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('compras', function (Blueprint $table) {
            $table->enum('have_pago',['CLIENTE','GESTOR'])->default('GESTOR');
            $table->float('monto_reserva')->default(0)->nullable();
            $table->enum('have_reserva',['CLIENTE','GESTOR'])->default('GESTOR');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('compras', function (Blueprint $table) {
            $table->dropColumn('have_pago');
            $table->dropColumn('monto_reserva');
            $table->dropColumn('have_reserva');
        });
    }
}
