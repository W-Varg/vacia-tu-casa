<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAgreementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agreements', function (Blueprint $table) {
            $table->bigIncrements('id');
            //$table->string('name')->unique();
            $table->enum('name', array('basico', 'estandar', 'premium'))->unique();
            $table->float('percent_discount_week_2', 8, 2);
            $table->float('percent_discount_week_4', 8, 2);
            $table->float('percent_discount_week_6', 8, 2);
            $table->float('percent_discount_week_8', 8, 2);
            $table->float('percent_penalization', 8, 2);
            $table->float('percent_negotiation', 8, 2);
            $table->float('percent_company', 8, 2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agreements');
    }
}
