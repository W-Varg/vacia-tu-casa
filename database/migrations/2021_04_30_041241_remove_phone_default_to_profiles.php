<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RemovePhoneDefaultToProfiles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('profiles', function (Blueprint $table) {
            $table->string('phone_number')->nullable()->default(null)->change();
        });

        //  $ php artisan migrate --path=database/migrations/2021_04_30_041241_remove_phone_default_to_profiles.php
        // update profiles set phone_number=null WHERE phone_number = '+34';
        // UPDATE profiles set phone_number='+34'||phone_number WHERE phone_number not like '+%';
        // UPDATE "public"."companies" SET "phone_number" = '+34640845424' WHERE "id" = 1;
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('profiles', function (Blueprint $table) {
            //
        });
    }
}
