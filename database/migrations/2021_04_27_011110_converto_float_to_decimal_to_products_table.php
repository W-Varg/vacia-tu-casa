<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ConvertoFloatToDecimalToProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->float('normal_price', 13, 2)->change();
            $table->float('reduced_price', 13, 2)->nullable()->change();

            $table->float('weight', 13, 2)->nullable()->change();
            $table->float('long', 13, 2)->nullable()->change();
            $table->float('width', 13, 2)->nullable()->change();
            $table->float('height', 13, 2)->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {

        });
    }
}
