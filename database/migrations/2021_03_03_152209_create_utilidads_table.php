<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUtilidadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('utilidads', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('client_agreement_id')->nullable();
            $table->string('type_contrato')->nullable();
            $table->integer('cantidad_vend')->nullable()->default(1);
            $table->float('utilidad', 10,2)->nullable();
            $table->string('gestor_name')->nullable();
            $table->unsignedBigInteger('user_id')->nullable();
            $table->float('gestor_cuota', 10,2)->nullable();
            $table->float('cliente_cuota', 10,2)->nullable();
            $table->date('fecha_pago')->date_default_timezone_get()->nullable();            // fecha = models.DateField()
            $table->string('status')->nullable()->default('pendiente');
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->constrained()->onUpdate('cascade');
            $table->foreign('client_agreement_id')->references('id')->on('client_agreements')->constrained()->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('utilidads');
    }
}
