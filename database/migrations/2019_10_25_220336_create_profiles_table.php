<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profiles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');
            $table->string('last_name');
            $table->string('code_postal')->nullable();
            $table->string('phone_number')->nullable()->default(null);
            $table->string('address')->nullable();
            $table->string('avatar')->nullable()->default('assets/images/user-1.png');
            $table->string('dni')->nullable();
            $table->boolean('suscrito')->default(false)->nullable();
            // $table->string('contrato');
            $table->timestamps();

            // relations
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profiles');
    }
}
