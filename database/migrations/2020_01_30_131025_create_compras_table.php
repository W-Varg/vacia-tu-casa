<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateComprasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('compras', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('product_id')->nullable();
            $table->unsignedBigInteger('vendedor')->nullable(); // vendedor
            $table->unsignedBigInteger('comprador')->nullable(); // vendedor
            $table->string('status')->nullable()->default('reservado');
            $table->float('pago')->default(0)->nullable();
            $table->string('nameClient');
            $table->string('email');
            $table->string('phone');
            $table->string('city');
            $table->string('comments')->nullable()->default('');
            $table->timestamps();

            // Relations
            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('vendedor')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('comprador')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('compras');
    }
}
