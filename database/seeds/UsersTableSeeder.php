<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Role::class, 1)->create(['name' => 'admin']);
        factory(\App\Role::class, 1)->create(['name' => 'handler']);
        factory(\App\Role::class, 1)->create(['name' => 'client']);

        App\User::create([  //1
            'name' => 'SuperAdmin',
            'email' => 'admin.h2h@gmail.com',
            'password' => bcrypt('home2home2021'),
            'role_id' => \App\Role::ADMIN
        ]);

        // factory(\App\User::class, 1)->create([ //2
        //     'name' => 'belinda',
        //     'email' => 'belinda@gmail.com',
        //     'password' => bcrypt('vaciatucasa'),
        //     'role_id' => \App\Role::HANDLER
        // ])
        // ->each(function (\App\User $u) {
        //     factory(\App\Handler::class, 1)->create(['user_id' => $u->id]);
        // });

        // factory(\App\User::class, 1)->create([ //3
        //     'name' => 'kike',
        //     'email' => 'kike@gmail.com',
        //     'password' => bcrypt('vaciatucasa'),
        //     'role_id' => \App\Role::HANDLER
        // ])
        // ->each(function (\App\User $u) {
        //     factory(\App\Handler::class, 1)->create(['user_id' => $u->id]);
        // });

        // factory(\App\User::class, 1)->create([ //4
        //     'name' => 'Manolo',
        //     'email' => 'client@gmail.com',
        //     'password' => bcrypt('vaciatucasa'),
        //     'role_id' => \App\Role::CLIENT
        // ])
        // ->each(function (\App\User $u) {
        //     factory(\App\Client::class, 1)->create(['user_id' => $u->id]);
        // });


        //profiles
        App\Profile::create([
            'user_id' => 1,
            'last_name' => 'SuperAdmin',
            // 'code_postal' => 'admin',
            // 'phone_number' => '123548',
            // 'dni' => '1234561'
        ]);
        // App\Profile::create([
        //     'user_id' => 2,
        //     'last_name' => 'Apellido1 Apellido2 ',
        //     'code_postal' => '315',
        //     'phone_number' => '123212548',
        //     'dni' => '1234562'
        // ]);
        // App\Profile::create([
        //     'user_id' => 3,
        //     'last_name' => 'ap1 ap2',
        //     'code_postal' => '13513',
        //     'phone_number' => '12354218',
        //     'dni' => '1234564'
        // ]);
        // App\Profile::create([
        //     'user_id' => 4,
        //     'last_name' => 'Torrez',
        //     'code_postal' => '3155',
        //     'phone_number' => '122548',
        //     'dni' => '1234563'
        // ]);

        // Contratos
        App\Agreement::create([
            'name' => 'estandar',
            'percent_discount_week_2' => 0.1,
            'percent_discount_week_4' => 0.15,
            'percent_discount_week_6' => 0.2,
            'percent_discount_week_8' => 0.3,
            'percent_penalization' => 1.0,
            'percent_negotiation' => 0.15,
            'percent_company' => 1.0
        ]);

        App\Agreement::create([
            'name' => 'premium',
            'percent_discount_week_2' => 0.1,
            'percent_discount_week_4' => 0.15,
            'percent_discount_week_6' => 0.2,
            'percent_discount_week_8' => 0.3,
            'percent_penalization' => 1.0,
            'percent_negotiation' => 0.15,
            'percent_company' => 1.0
        ]);
    }
}

