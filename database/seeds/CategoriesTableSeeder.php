<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //factory(App\Category::class,20)->create();
        App\Category::create([
            'name' => 'Sofa',
            'slug' => str_slug('Sofa', '-'),
            'icon' => 'f-icon f-icon-sofa'
        ]);
        App\Category::create([
            'name' => 'Sillones',
            'slug' => str_slug('Sillones', '-'),
            'icon' => 'f-icon f-icon-armchair'
        ]);
        App\Category::create([
            'name' => 'Sillas',
            'slug' => str_slug('Sillas', '-'),
            'icon' => 'f-icon f-icon-chair'
        ]);
        App\Category::create([
            'name' => 'Mesas',
            'slug' => str_slug('Mesas', '-'),
            'icon' => 'f-icon f-icon-dining-table'
        ]);
        App\Category::create([
            'name' => 'Libreros',
            'slug' => str_slug('Libreros', '-'),
            'icon' => 'f-icon f-icon-bookcase'
        ]);
        App\Category::create([
            'name' => 'Habitación',
            'slug' => str_slug('Habitación', '-'),
            'icon' => 'f-icon f-icon-bedroom'
        ]);
        App\Category::create([
            'name' => 'Cocina',
            'slug' => str_slug('Cocina', '-'),
            'icon' => 'f-icon f-icon-kitchen'
        ]);
        App\Category::create([
            'name' => 'Oficina',
            'slug' => str_slug('Oficina', '-'),
            'icon' => 'f-icon f-icon-office'
        ]);
        App\Category::create([
            'name' => 'Muebles de Bar',
            'slug' => str_slug('Muebles de Bar', '-'),
            'icon' => 'f-icon f-icon-bar-set'
        ]);
        App\Category::create([
            'name' => 'Iluminación',
            'slug' => str_slug('Iluminación', '-'),
            'icon' => 'f-icon f-icon-lightning'
        ]);
        App\Category::create([
            'name' => 'Alfombras',
            'slug' => str_slug('Alfombras', '-'),
            'icon' => 'f-icon f-icon-carpet'
        ]);
        App\Category::create([
            'name' => 'Accesorios',
            'slug' => str_slug('Accesorios', '-'),
            'icon' => 'f-icon f-icon-accessories'
        ]);
    }
}
