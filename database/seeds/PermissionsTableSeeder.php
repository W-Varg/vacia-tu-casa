<?php

use Illuminate\Database\Seeder;

use Caffeinated\Shinobi\Models\Permission;
use Caffeinated\Shinobi\Models\Role;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // permission for user
        Permission::create([
            'name' => 'Usuario Cliente',
            'slug' => 'users.index',
            'description' => 'Lista los usuarios',
        ]);
        Permission::create([
            'name' => 'Usuario Cliente',
            'slug' => 'users.show',
            'description' => 'Visualizar detalle de usuario',
        ]);
        Permission::create([
            'name' => 'Usuario Cliente',
            'slug' => 'users.edit',
            'description' => 'Eitar datos de un usuario',
        ]);
        Permission::create([
            'name' => 'Usuario Cliente',
            'slug' => 'users.destroy',
            'description' => 'Eliminar un usuario',
        ]);

        //Permisions for Roles********************************
        Permission::create([
            'name' => 'Naveaga Roles',
            'slug' => 'roles.index',
            'description' => 'Lista y navega los roles',
        ]);
        Permission::create([
            'name' => 'Ve detalle de Rol',
            'slug' => 'roles.show',
            'description' => 'ver cualquier detalle de rol',
        ]);
        Permission::create([
            'name' => 'Crea un Rol',
            'slug' => 'roles.create',
            'description' => 'Crea un rol para el sistema',
        ]);

        Permission::create([
            'name' => 'Edita un Rol',
            'slug' => 'roles.edit',
            'description' => 'Edita cualquier datos de un rol',
        ]);
        Permission::create([
            'name' => 'Elimina un Rol',
            'slug' => 'roles.destroy',
            'description' => 'Eliminar cualquier rol del sistema',
        ]);

        
        //Permission for Products********************************
        Permission::create([
            'name' => 'Navega Articulos',
            'slug' => 'products.index',
            'description' => 'Lista y navega los Articulos',
        ]);
        Permission::create([
            'name'          => 'Ve detalle de Articulo',
            'slug'          => 'products.show',
            'description'   => 'ver cualquier detalle de Articulo',
        ]);
        Permission::create([
            'name' => 'Crea un Articulo',
            'slug' => 'products.create',
            'description' => 'Crea un Articulo para el sistema',
        ]);

        Permission::create([
            'name' => 'Edita un Articulo',
            'slug' => 'products.edit',
            'description' => 'Edita cualquier datos de un Articulo',
        ]);
        Permission::create([
            'name' => 'Elimina un Articulo',
            'slug' => 'products.destroy',
            'description' => 'Eliminar cualquier Articulo del sistema',
        ]);
        
        /*Seeder para creacion de roles*/
        Role::create([
            'name'      => 'admin',
            'slug'      => 'admin',
            'special'      => 'all-access'
        ]);
    }
}
