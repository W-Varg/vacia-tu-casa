<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Company;
use Faker\Generator as Faker;

$factory->define(Company::class, function (Faker $faker) {
    return [
        'company_name' => 'Vacia tu casa',
        'email' => 'info.vaciatucasa@gmail.com',
        'phone_number' => '123',
       // 'facebook' => 'facebook.com',
       // 'instagram' => 'instagram.com',
       // 'twitter' => 'twittter.com',
        'address' => 'Calle Asturias, 9 Leganés Madrid',
    ];
});
