<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Image;
use Faker\Generator as Faker;

$factory->define(Image::class, function (Faker $faker) {
    return [
        //'path' => $faker->imageUrl($width=640, $height=480),
        'path' => 'https://source.unsplash.com/random/640x480',
        'product_id' => rand(1,20)
    ];
});
