<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Product;
use App\Category;
use Faker\Generator as Faker;

$factory->define(Product::class, function (Faker $faker) {
    return [
        // 'categories_id' => function() { return factory(App\Category::class)->create()->id; },
        'user_id' => \App\Client::all()->random()->id,
        'category_id' => \App\Category::all()->random()->id,
        'code' => $faker->unique()->postcode, // 'Hello wgt',
        'name' => $faker->name,
        'quantity' => $faker->numberBetween($min = 0, $max = 200),
        'description' => $faker->sentence,
        'normal_price' => $faker->randomFloat($nbMaxDecimals = 2, $min = 0, $max = 1260),
        'reduced_price' => $faker->randomFloat($nbMaxDecimals = 2, $min = 0, $max = 1260),
        'weight' => $faker->randomFloat($nbMaxDecimals = 2, $min = 0, $max = 1260),
        'long' => $faker->randomFloat($nbMaxDecimals = 2, $min = 0, $max = 1260),
        'width' => $faker->randomFloat($nbMaxDecimals = 2, $min = 0, $max = 1260),
        'height' => $faker->randomFloat($nbMaxDecimals = 2, $min = 0, $max = 1260),
        'status' => $faker->randomElement(['PUBLICADO','PENDIENTE','RECHAZADO', 'VENDIDO', 'DE BAJA']),
        'type' => $faker->randomElement(['EXCLUSIVE','NORMAL'])
    ];
});
